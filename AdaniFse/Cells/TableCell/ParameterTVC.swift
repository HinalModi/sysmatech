//
//  ParameterTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 24/05/22.
//

import UIKit

class ParameterTVC: UITableViewCell {
    
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnDrop: UIButton!
    @IBOutlet weak var txtSelectedVal: UITextField!
    @IBOutlet weak var viewDropDown: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var viewComment: UIView!
    
    @IBOutlet weak var commentImgStackView: UIStackView!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var lblParamDisp: UILabel!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var addImg: UIImageView!
    @IBOutlet weak var addImgView: UIView!
    @IBOutlet weak var imageCamera: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var lblValues: UILabel!
    var openDropDown : (()->())?
    var openCamera : (()->())?
    var endEditing : (()->())?
    var endCommentEditing : (()->())?
    var addPhoto : (()->())?
    var imageData = UIImage()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        txtSelectedVal.layer.masksToBounds = true
        txtSelectedVal.layer.borderWidth = 1
        txtComment.layer.borderWidth = 1
//        txtComment.setCorner(5)
        lblParamDisp.sizeToFit()
        if #available(iOS 11.0, *) {
            txtSelectedVal.layer.borderColor = Constant.Color.color_grey.cgColor
            txtComment.layer.borderColor = Constant.Color.color_grey.cgColor
//            lblParamDisp.textColor = Constant.Color.color_grey
            btnDrop.tintColor = Constant.Color.color_grey
            btnDrop.titleLabel?.textColor = Constant.Color.color_grey
            imgDown.tintColor = Constant.Color.color_launch
        }else{
            btnDrop.titleLabel?.textColor = .black
        }
        txtSelectedVal.setLeftPaddingPoints(10)
        btnDrop.titleEdgeInsets.right = 15
        txtSelectedVal.setRightPaddingPoints(15)
//        txtSelectedVal.setCorner(5)
        txtSelectedVal.delegate = self
        txtComment.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    @IBAction func btnOpenCamera(_ sender: UIButton) {
        openCamera?()
    }
    @IBAction func btnOpenDropDown(_ sender: UIButton) {
        openDropDown?()
    }
   
    
}
extension ParameterTVC  :UITextFieldDelegate ,UITextViewDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtSelectedVal{
            endEditing?()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtSelectedVal{
            txtSelectedVal.keyboardType = .numberPad
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
            endCommentEditing?()

    }
}

