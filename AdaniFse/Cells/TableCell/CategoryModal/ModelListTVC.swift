//
//  ModelListTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/08/22.
//

import UIKit

class ModelListTVC: UITableViewCell {
    
    
    @IBOutlet weak var imgWorkOrds: UIImageView!
    @IBOutlet weak var imgAssets: UIImageView!
    @IBOutlet weak var workOrdView: UIView!
    @IBOutlet weak var assetView: UIView!
    @IBOutlet weak var lblWorkOrder: UILabel!
    @IBOutlet weak var lblAsset: UILabel!
    @IBOutlet weak var lblModalName: UILabel!
    var showAsset : (()->())?
    var showWorkOrder : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        assetView.setView(5, color: .black, opacity: 0.3, size: CGSize(width: 0, height: 2))
        workOrdView.setView(5, color: .black, opacity: 0.3, size: CGSize(width: 0, height: 2))
        assetView.setCorner(_tp: 5)
        workOrdView.setCorner(_tp: 5)
        if #available(iOS 11.0, *) {
            imgAssets.tintColor = Constant.Color.color_launch
            imgWorkOrds.tintColor = Constant.Color.color_launch
        } else {
            imgAssets.tintColor = .white
            imgWorkOrds.tintColor = .white
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnWorkOrder(_ sender: UIButton) {
        showWorkOrder?()
    }
    
    @IBAction func btnAsset(_ sender: UIButton) {
        showAsset?()
    }
}
