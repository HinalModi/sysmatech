//
//  WorkOrderTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 30/08/22.
//

import UIKit

class WorkOrderTVC: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setCorner(_tp: 8)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
