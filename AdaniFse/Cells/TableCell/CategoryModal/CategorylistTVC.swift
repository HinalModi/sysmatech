//
//  CategorylistTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/08/22.
//

import UIKit

class CategorylistTVC: UITableViewCell {
    
    @IBOutlet weak var imgWorkOrd: UIImageView!
    @IBOutlet weak var imgAsset: UIImageView!
    @IBOutlet weak var imgModel: UIImageView!
    @IBOutlet weak var workOrdView: UIView!
    @IBOutlet weak var assetView: UIView!
    @IBOutlet weak var modelView: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblWorkOrder: UILabel!
    @IBOutlet weak var lblAsset: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    var moveToModel : (()->())?
    var moveToAssets : (()->())?
    var moveToWorkOrder : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        assetView.setView(5, color: .black, opacity: 0.3, size: CGSize(width: 0, height: 2))
        modelView.setView(5, color: .black, opacity: 0.3, size: CGSize(width: 0, height: 2))
        workOrdView.setView(5, color: .black, opacity: 0.3, size: CGSize(width: 0, height: 2))
        assetView.setCorner(_tp: 5)
        modelView.setCorner(_tp: 5)
        workOrdView.setCorner(_tp: 5)
        if #available(iOS 11.0, *) {
            imgModel.tintColor = Constant.Color.color_launch
            imgAsset.tintColor = Constant.Color.color_launch
            imgWorkOrd.tintColor = Constant.Color.color_launch
        } else {
            imgModel.tintColor = .white
            imgAsset.tintColor = .white
            imgWorkOrd.tintColor = .white
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnWorkOrder(_ sender: UIButton) {
        moveToWorkOrder?()
    }
    @IBAction func btnToAssetStatus(_ sender: Any) {
        moveToAssets?()
    }
    
    @IBAction func toModalList(_ sender: UIButton) {
        moveToModel?()
    }
}
