//
//  SelectCompanyTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 25/07/22.
//

import UIKit

class SelectCompanyTVC: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var selectImg: UIImageView!
    @IBOutlet weak var lblComName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            bgView.backgroundColor = Constant.Color.color_launch
            contentView.backgroundColor = Constant.Color.color_launch
        } else {
            bgView.backgroundColor = .clear
            contentView.backgroundColor = .clear
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
    }

}
