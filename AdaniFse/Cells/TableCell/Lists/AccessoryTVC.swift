//
//  AccessoryTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 30/08/22.
//

import UIKit

class AccessoryTVC: UITableViewCell {

    @IBOutlet weak var imgAccessory: UIImageView!
    @IBOutlet weak var lblMinQtyTitle: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblName: UILabel!
    var checkOut : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblName.textColor = Constant.Color.color_launch
            lblCat.textColor = Constant.Color.color_grey
            imgLocation.tintColor = Constant.Color.color_grey
            lblLoc.textColor = Constant.Color.color_grey
            lblMinQtyTitle.textColor = Constant.Color.color_grey
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnCheckOut(_ sender: UIButton) {
        checkOut?()
    }
}
