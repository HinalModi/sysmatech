//
//  PreddefinedKitTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 02/09/22.
//

import UIKit

class PreddefinedKitTVC: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblName: UILabel!
    var checkOut : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnCheckOutAction(_ sender: Any) {
        checkOut?()
    }
}
