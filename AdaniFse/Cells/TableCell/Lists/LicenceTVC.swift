//
//  LicenceTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 01/09/22.
//

import UIKit

class LicenceTVC: UITableViewCell {

    @IBOutlet weak var lblManu: UILabel!
    @IBOutlet weak var lblManuFTitle: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var lblExpDateTitle: UILabel!
    @IBOutlet weak var lbllicName: UILabel!
    @IBOutlet weak var lbllicTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lbllicTitle.textColor = Constant.Color.color_launch
            lbllicName.textColor = Constant.Color.color_launch
            lblExpDateTitle.textColor = Constant.Color.color_grey
            lblExpDate.textColor = Constant.Color.color_grey
            lblManuFTitle.textColor = Constant.Color.color_grey
            lblManu.textColor = Constant.Color.color_grey
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
