//
//  LicSeatsTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 26/09/22.
//

import UIKit

class LicSeatsTVC: UITableViewCell {

    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblTitleUser: UILabel!
    @IBOutlet weak var lblseatName: UILabel!
    var check : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnCheckAction(_ sender: UIButton) {
        check?()
    }
}
