//
//  ComponentTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 01/09/22.
//

import UIKit

class ComponentTVC: UITableViewCell {

    @IBOutlet weak var imgLoc: UIImageView!
    @IBOutlet weak var lblRemTitle: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblcatTitle: UILabel!
    @IBOutlet weak var compName: UILabel!
    @IBOutlet weak var imgComp: UIImageView!
    var checkOut : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            imgLoc.tintColor = Constant.Color.color_grey
            compName.textColor = Constant.Color.color_launch
            lblcatTitle.textColor = Constant.Color.color_grey
            lblLoc.textColor = Constant.Color.color_grey
            lblRemTitle.textColor = Constant.Color.color_grey
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnCheckOut(_ sender: UIButton) {
        checkOut?()
    }
}
