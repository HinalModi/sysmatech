//
//  FileUploadTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 22/09/22.
//

import UIKit

class FileUploadTVC: UITableViewCell {

    @IBOutlet weak var imgSave: UIImageView!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblNoteTitle: UILabel!
    @IBOutlet weak var lblFile: UILabel!
    @IBOutlet weak var lblFileTitle: UILabel!
    @IBOutlet weak var lblLic: UILabel!
    @IBOutlet weak var lblLicTitle: UILabel!
    var share : (()->())?
    var download : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func btnShare(_ sender: UIButton) {
        share?()
    }
    
    @IBAction func btnDownload(_ sender: UIButton) {
        download?()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
