//
//  CheckOutHistoryTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 22/09/22.
//

import UIKit

class CheckOutHistoryTVC: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDateTitle: UILabel!
    @IBOutlet weak var lblTarget: UILabel!
    @IBOutlet weak var lblTargetTitle: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblItemTitle: UILabel!
    @IBOutlet weak var lblAction: UILabel!
    @IBOutlet weak var lblActionTitle: UILabel!
    @IBOutlet weak var lblAdmin: UILabel!
    @IBOutlet weak var lblAdminTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblAdminTitle.textColor = Constant.Color.color_launch
            lblAdmin.textColor = Constant.Color.color_launch
            lblActionTitle.textColor = Constant.Color.color_grey
            lblAction.textColor = Constant.Color.color_grey
            lblItemTitle.textColor = Constant.Color.color_grey
            lblItem.textColor = Constant.Color.color_grey
            lblTargetTitle.textColor = Constant.Color.color_grey
            lblTarget.textColor = Constant.Color.color_grey
            lblDateTitle.textColor = Constant.Color.color_grey
            lblDate.textColor = Constant.Color.color_grey
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
