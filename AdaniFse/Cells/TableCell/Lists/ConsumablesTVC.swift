       //
//  ConsumablesTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 31/08/22.
//

import UIKit

class ConsumablesTVC: UITableViewCell {
   
    @IBOutlet weak var lblMinQtyTitle: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblCatTitle: UILabel!
    @IBOutlet weak var imgConsumable: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var checkout : (()->())?
    var restock : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblName.textColor = Constant.Color.color_launch
            lblCatTitle.textColor = Constant.Color.color_grey
            lblCat.textColor = Constant.Color.color_grey
            imgLocation.tintColor = Constant.Color.color_grey
            lblLoc.textColor = Constant.Color.color_grey
            lblMinQtyTitle.textColor = Constant.Color.color_grey          
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func btnRestockConsume(_ sender: UIButton) {
        restock?()
    }
    @IBAction func btnCheckout(_ sender: UIButton) {
        checkout?()
    }
    
}
