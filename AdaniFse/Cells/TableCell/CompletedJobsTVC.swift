//
//  CompletedJobsTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit

class CompletedJobsTVC: UITableViewCell {

    @IBOutlet weak var imgInspect: UIImageView!
    @IBOutlet weak var lblInspectTitle: UILabel!
    @IBOutlet weak var lblInspectedBy: UILabel!
    @IBOutlet weak var lblAssetTag: UILabel!
    @IBOutlet weak var imgTime: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblDuetitle: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblModalName: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    var setImage : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblProductName.textColor = Constant.Color.color_launch
            lblModalName.textColor = Constant.Color.color_grey
            lblLocation.textColor = Constant.Color.color_grey
            lblDuetitle.textColor = Constant.Color.color_red
            lblDueDate.textColor = Constant.Color.color_grey
            imgTime.tintColor = Constant.Color.color_grey
            imgInspect.tintColor = Constant.Color.color_grey
            lblInspectedBy.textColor = Constant.Color.color_grey
            lblInspectTitle.textColor = Constant.Color.color_grey
            imgLocation.tintColor = Constant.Color.color_grey
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func btnRevealImg(_ sender: UIButton) {
        setImage?()
    }
}
