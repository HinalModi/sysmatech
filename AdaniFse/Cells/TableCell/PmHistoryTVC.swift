//
//  PmHistoryTVCTableViewCell.swift
//  AdaniFse
//
//  Created by Ankit Dave on 15/07/22.
//

import UIKit

class PmHistoryTVC: UITableViewCell {

    @IBOutlet weak var auditImage: UIImageView!
    @IBOutlet weak var imgTime: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblDateTitle: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAuditName: UILabel!
    var setImage : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblAuditName.textColor = Constant.Color.color_launch
            imgTime.tintColor = Constant.Color.color_grey
            lblStatus.textColor = Constant.Color.color_grey
            lblStatusTitle.textColor = Constant.Color.color_red
            imgLocation.tintColor = Constant.Color.color_grey
            lblDateTitle.textColor = Constant.Color.color_red
            lblDate.textColor = Constant.Color.color_grey
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func btnRevealImg(_ sender: UIButton) {
        setImage?()
    }
}
