//
//  SelectLangTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 03/11/22.
//

import UIKit

class SelectLangTVC: UITableViewCell {

    @IBOutlet weak var lblLangName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
        // Configure the view for the selected state
    }

}
