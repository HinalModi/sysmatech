//
//  InspDetailTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 15/06/22.
//

import UIKit

class InspDetailTVC: UITableViewCell {
    
    @IBOutlet weak var comentImgStackView: UIStackView!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var txtAuditParam: UITextField!
    @IBOutlet weak var imgInspection: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblAuditParamName: UILabel!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var viewImg: UIView!
    var setImage : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        txtAuditParam.layer.masksToBounds = true
        txtAuditParam.layer.borderWidth = 1
        txtComment.layer.borderWidth = 1
        lblAuditParamName.sizeToFit()
        txtAuditParam.setRightPaddingPoints(5)
        txtAuditParam.setLeftPaddingPoints(5)
        if #available(iOS 11.0, *) {
            txtAuditParam.layer.borderColor = Constant.Color.color_grey.cgColor
            txtComment.layer.borderColor = Constant.Color.color_grey.cgColor
            lblAuditParamName.textColor = .black
        }
        txtComment.textColor = .gray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func toRevealImage(_ sender: UIButton) {
        setImage?()
    }
}
