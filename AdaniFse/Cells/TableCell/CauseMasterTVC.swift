//
//  CauseMasterTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 01/08/22.
//

import UIKit

class CauseMasterTVC: UITableViewCell {
    @IBOutlet weak var lblQue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            self.backgroundColor = Constant.Color.color_launch
        } else {
            self.backgroundColor = .white
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        accessoryType = selected ? UITableViewCell.AccessoryType.checkmark : UITableViewCell.AccessoryType.none
        
    }
    
}
