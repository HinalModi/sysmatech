//
//  AssetStatusTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 27/07/22.
//

import UIKit

class AssetStatusTVC: UITableViewCell {

    @IBOutlet weak var lblAssetTag: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDueTitle: UILabel!
    @IBOutlet weak var imgClock: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblLocationTitle: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblModalName: UILabel!
    @IBOutlet weak var lblAuditName: UILabel!
    @IBOutlet weak var imgAudit: UIImageView!
    var setImage : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblAuditName.textColor = Constant.Color.color_launch
            lblModalName.textColor = Constant.Color.color_grey
            lblLocation.textColor = Constant.Color.color_grey
            lblDueTitle.textColor = Constant.Color.color_red
            lblDate.textColor = Constant.Color.color_grey
            imgClock.tintColor = Constant.Color.color_grey
            imgLocation.tintColor = Constant.Color.color_grey
            lblAssetTag.textColor = Constant.Color.color_grey

        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func btnRevealImg(_ sender: UIButton) {
        setImage?()
    }
}
