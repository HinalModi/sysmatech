  //
//  AudioListTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 14/10/22.
//

import UIKit

class AudioListTVC: UITableViewCell {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnTick: UIButton!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    var setAudio : (()->())?
    var pause : (()->())?
    var play : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        accessoryType = selected ? .checkmark : .none
    }
    
    @IBAction func btnPlayListItem(_ sender: UIButton) {
        play?()
    }
    
    @IBAction func btnStopListItem(_ sender: UIButton) {
        pause?()
    }
}
