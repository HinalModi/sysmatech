//
//  AccessLogsTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/09/22.
//

import UIKit

class AccessLogsTVC: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblQtyTitle: UILabel!
    @IBOutlet weak var lblStore: UILabel!
    @IBOutlet weak var lblStoreTitle: UILabel!
    var checkIn : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setView(5, color: .black, opacity: 0.2, size: CGSize(width: 0, height: 2))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func btnCheckIn(_ sender: UIButton) {
        checkIn?()
    }
}
