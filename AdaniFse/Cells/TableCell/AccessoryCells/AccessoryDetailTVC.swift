//
//  AccessoryDetailTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/09/22.
//

import UIKit

class AccessoryDetailTVC: UITableViewCell {

    @IBOutlet weak var btnCheckInUser: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblCheck: UILabel!
    @IBOutlet weak var lblCheckTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTypeTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblQtyTitle: UILabel!
    var checkIn :(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setView(5, color: .black, opacity: 0.2, size: CGSize(width: 0, height: 2))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

    @IBAction func btnCheckIn(_ sender: UIButton) {
        checkIn?()
    }
}
