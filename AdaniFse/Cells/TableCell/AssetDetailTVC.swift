//
//  AssetDetailTVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit

class AssetDetailTVC: UITableViewCell {

    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var imgTime: UIImageView!
    @IBOutlet weak var lblDueTitle: UILabel!
    @IBOutlet weak var auditImage: UIImageView!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAssetTagId: UILabel!
    @IBOutlet weak var lblAuditName: UILabel!
    var showImage : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            lblAuditName.textColor = Constant.Color.color_launch
            lblAssetTagId.textColor = Constant.Color.color_grey
            imgTime.tintColor = Constant.Color.color_grey
            lblLocation.textColor = Constant.Color.color_grey
            imgLocation.tintColor = Constant.Color.color_grey
            lblDueTitle.textColor = Constant.Color.color_red
            lblDueDate.textColor = Constant.Color.color_grey
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func btnRevealImage(_ sender: UIButton) {
        showImage?()
    }
}
