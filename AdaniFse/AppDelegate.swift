//
//  AppDelegate.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit
import FirebaseCore
import LGSideMenuController
import IQKeyboardManagerSwift
import FirebaseAuth

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
//    var spinnerView:MMMaterialDesignSpinner?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        userInfo = UserInfoManager.getUserInfoModel()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        VCLangauageManager.sharedInstance.changeAppLanguage(To: VCLangauageManager.sharedInstance.isLanguageArabic() ? .arabic : .english)
        if #available(iOS 13.0, *) {
            IQKeyboardManager.shared.toolbarTintColor = .link
        }
//        signOutOldUser()
        Constant.APPDELEGATE = self
        window?.makeKeyAndVisible()
        setInitialViewController()
        return true
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        // handle the file here

        return true // or false based on whether you were successful or not
    }
    func showSideMenu(viewController : UIViewController){
        let leftView = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier:
            "SideMenuVC") as! SideMenuVC
        let rightView = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier:
            "RightSideMenuVC") as! RightSideMenuVC
        let nav = UINavigationController(rootViewController: viewController)
        nav.navigationBar.isHidden = true
        let lgMenu = LGSideMenuController(rootViewController: nav, leftViewController: leftView, rightViewController: rightView)
        lgMenu.isRightViewSwipeGestureEnabled = false
        lgMenu.isLeftViewSwipeGestureEnabled = false
        lgMenu.leftViewWidth = UIScreen.main.bounds.width / 1.4
        lgMenu.rightViewWidth = UIScreen.main.bounds.width / 1.4
        lgMenu.leftViewPresentationStyle = .slideAbove
        lgMenu.rightViewPresentationStyle = .slideAbove
        if #available(iOS 11.0, *) {
            lgMenu.rootViewCoverColor = UIColor(named: "blackTop")?.withAlphaComponent(0.75)
        }
        Constant.APPDELEGATE.window?.rootViewController = lgMenu
    }
    func setInitialViewController() {
        if Auth.auth().currentUser != nil {
            if UserDefaults.standard.string(forKey: "domain") == nil {
                let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                let nav = UINavigationController(rootViewController: vc)
                showSideMenu(viewController: vc)
                nav.navigationBar.isHidden = true
            }else{

                let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                let nav = UINavigationController(rootViewController: vc)
                showSideMenu(viewController: vc)
                nav.navigationBar.isHidden = true
            }
            
        } else {
            let vc = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav = UINavigationController(rootViewController: vc)
            showSideMenu(viewController: vc)
            nav.navigationBar.isHidden = true
        }
    }
    var orientationLock = UIInterfaceOrientationMask.portrait

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }
   
}

extension AppDelegate{
func signOutOldUser(){
    if let _ = UserDefaults.standard.value(forKey: "isNewuser"){}else{
        do{
            UserDefaults.standard.set(true, forKey: "isNewuser")
            try Auth.auth().signOut()
        }catch{}
    }
}
}
