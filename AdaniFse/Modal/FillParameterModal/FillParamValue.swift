//
//  FillParamValue.swift
//  AdaniFse
//
//  Created by Ankit Dave on 26/05/22.
//

import Foundation
import UIKit
class FillParamValue : Equatable{
    
    static func == (lhs: FillParamValue, rhs: FillParamValue) -> Bool {
        true
    }
    
    var auditName : String!
    var auditparams_id : Int!
    var assetModel : Int!
    var createdDate : String!
    var updateDate : String!
    var deleteAt : String!
    var audit_camera_photos : Int!
    var show_when_photos : Int!
    var show_when_comment : Int!
    var id : Int!
    var status : String!
    var param_disp_name : String!
    var param_type : String!
    var benchmark_range_low : Int!
    var benchmark_range_high : Int!
    var dropdown_values : String!
    var dropdown_reject_value : String!
    var dropdown_accept_value : String!
    var audit_comment : Int!
    var audit_camera_photos_key : String!
    var audit_comment_key : String!
    var dropDownCommentAceept : String!
    var dropDownAcceptphotos : String!
    var dropDownRejectphotos : String!
    var dropDownRejectcomment : String!
    var validation : String!
    var audit_param_audio_path : String!
    var audit_audio_key : String!
    init(auditName: String? = nil, auditparams_id: Int? = nil, assetModel: Int? = nil, createdDate: String? = nil, updateDate: String? = nil, deleteAt: String? = nil, audit_camera_photos: Int? = nil, show_when_photos: Int? = nil, show_when_comment: Int? = nil, id: Int? = nil, status: String? = nil, param_disp_name: String? = nil, param_type: String? = nil, benchmark_range_low: Int? = nil, benchmark_range_high: Int? = nil, dropdown_values: String? = nil, dropdown_reject_value: String? = nil, dropdown_accept_value: String? = nil, audit_comment: Int? = nil, audit_camera_photos_key: String? = nil, audit_comment_key: String? = nil ,validation : String? = nil, dropDownCommentAceept : String? = nil, dropDownAcceptphotos : String? = nil ,dropDownRejectphotos : String? = nil ,dropDownRejectcomment : String? = nil , audit_param_audio_path : String? = nil , audit_audio_key : String? = nil) {
        self.auditName = auditName
        self.auditparams_id = auditparams_id
        self.assetModel = assetModel
        self.createdDate = createdDate
        self.updateDate = updateDate
        self.deleteAt = deleteAt
        self.audit_camera_photos = audit_camera_photos
        self.show_when_photos = show_when_photos
        self.show_when_comment = show_when_comment
        self.id = id
        self.status = status
        self.param_disp_name = param_disp_name
        self.param_type = param_type
        self.benchmark_range_low = benchmark_range_low
        self.benchmark_range_high = benchmark_range_high
        self.dropdown_values = dropdown_values
        self.dropdown_reject_value = dropdown_reject_value
        self.dropdown_accept_value = dropdown_accept_value
        self.audit_comment = audit_comment
        self.audit_camera_photos_key = audit_camera_photos_key
        self.audit_comment_key = audit_comment_key
        self.dropDownCommentAceept = dropDownCommentAceept
        self.dropDownAcceptphotos = dropDownAcceptphotos
        self.dropDownRejectphotos = dropDownRejectphotos
        self.dropDownRejectcomment = dropDownRejectcomment
        self.validation = validation
        self.audit_param_audio_path = audit_param_audio_path
        self.audit_audio_key = audit_audio_key
    }
}
class DropDownValues {
    
//    static func == (lhs: DropDownValues, rhs: DropDownValues) -> Bool {
//        return lhs.validationName == rhs.validationName
//    }
    var dropDownCommentAceept : String!
    var dropDownAcceptphotos : String!
    var dropDownRejectphotos : String!
    var dropDownRejectcomment : String!
    var validationName : String!
    init(dropDownCommentAceept: String? = nil, dropDownAcceptphotos: String? = nil, dropDownRejectphotos: String? = nil, dropDownRejectcomment: String? = nil , validationName : String? = nil) {
        self.dropDownCommentAceept = dropDownCommentAceept
        self.dropDownAcceptphotos = dropDownAcceptphotos
        self.dropDownRejectphotos = dropDownRejectphotos
        self.dropDownRejectcomment = dropDownRejectcomment
        self.validationName = validationName
    }
}

class TextFieldValues: Equatable{
   
    static func == (lhs: TextFieldValues, rhs: TextFieldValues) -> Bool {
        true
    }
    var txtCommentAceept : String!
    var txtAcceptphotos : String!
    var txtRejectphotos : String!
    var txtRejectcomment : String!
    init(txtCommentAceept: String? = nil, txtAcceptphotos: String? = nil, txtRejectphotos: String? = nil, txtRejectcomment: String? = nil) {
        self.txtCommentAceept = txtCommentAceept
        self.txtAcceptphotos = txtAcceptphotos
        self.txtRejectphotos = txtRejectphotos
        self.txtRejectcomment = txtRejectcomment
    }
}
