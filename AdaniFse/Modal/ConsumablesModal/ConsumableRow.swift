//
//	ConsumableRow.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ConsumableRow : NSObject, NSCoding{

	var availableActions : ConsumableAvailableAction!
	var category : AnyObject!
	var company : ConsumableCompany!
	var createdAt : ConsumableCreatedAt!
	var id : Int!
	var image : AnyObject!
	var itemNo : String!
	var location : ConsumableCompany!
	var manufacturer : ConsumableCompany!
	var minAmt : Int!
	var modelNumber : AnyObject!
	var name : String!
	var notes : AnyObject!
	var orderNumber : String!
	var purchaseCost : String!
	var purchaseDate : ConsumablePurchaseDate!
	var qty : Int!
	var remaining : Int!
	var updatedAt : ConsumableCreatedAt!
	var userCanCheckout : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let availableActionsData = dictionary["available_actions"] as? [String:Any]{
			availableActions = ConsumableAvailableAction(fromDictionary: availableActionsData)
		}
		category = dictionary["category"] as? AnyObject
		if let companyData = dictionary["company"] as? [String:Any]{
			company = ConsumableCompany(fromDictionary: companyData)
		}
		if let createdAtData = dictionary["created_at"] as? [String:Any]{
			createdAt = ConsumableCreatedAt(fromDictionary: createdAtData)
		}
		id = dictionary["id"] as? Int
		image = dictionary["image"] as? AnyObject
		itemNo = dictionary["item_no"] as? String
		if let locationData = dictionary["location"] as? [String:Any]{
			location = ConsumableCompany(fromDictionary: locationData)
		}
		if let manufacturerData = dictionary["manufacturer"] as? [String:Any]{
			manufacturer = ConsumableCompany(fromDictionary: manufacturerData)
		}
		minAmt = dictionary["min_amt"] as? Int
		modelNumber = dictionary["model_number"] as? AnyObject
		name = dictionary["name"] as? String
		notes = dictionary["notes"] as? AnyObject
		orderNumber = dictionary["order_number"] as? String
		purchaseCost = dictionary["purchase_cost"] as? String
		if let purchaseDateData = dictionary["purchase_date"] as? [String:Any]{
			purchaseDate = ConsumablePurchaseDate(fromDictionary: purchaseDateData)
		}
		qty = dictionary["qty"] as? Int
		remaining = dictionary["remaining"] as? Int
		if let updatedAtData = dictionary["updated_at"] as? [String:Any]{
			updatedAt = ConsumableCreatedAt(fromDictionary: updatedAtData)
		}
		userCanCheckout = dictionary["user_can_checkout"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if availableActions != nil{
			dictionary["available_actions"] = availableActions.toDictionary()
		}
		if category != nil{
			dictionary["category"] = category
		}
		if company != nil{
			dictionary["company"] = company.toDictionary()
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt.toDictionary()
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if itemNo != nil{
			dictionary["item_no"] = itemNo
		}
		if location != nil{
			dictionary["location"] = location.toDictionary()
		}
		if manufacturer != nil{
			dictionary["manufacturer"] = manufacturer.toDictionary()
		}
		if minAmt != nil{
			dictionary["min_amt"] = minAmt
		}
		if modelNumber != nil{
			dictionary["model_number"] = modelNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if notes != nil{
			dictionary["notes"] = notes
		}
		if orderNumber != nil{
			dictionary["order_number"] = orderNumber
		}
		if purchaseCost != nil{
			dictionary["purchase_cost"] = purchaseCost
		}
		if purchaseDate != nil{
			dictionary["purchase_date"] = purchaseDate.toDictionary()
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if remaining != nil{
			dictionary["remaining"] = remaining
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt.toDictionary()
		}
		if userCanCheckout != nil{
			dictionary["user_can_checkout"] = userCanCheckout
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         availableActions = aDecoder.decodeObject(forKey: "available_actions") as? ConsumableAvailableAction
         category = aDecoder.decodeObject(forKey: "category") as? AnyObject
         company = aDecoder.decodeObject(forKey: "company") as? ConsumableCompany
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? ConsumableCreatedAt
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? AnyObject
         itemNo = aDecoder.decodeObject(forKey: "item_no") as? String
         location = aDecoder.decodeObject(forKey: "location") as? ConsumableCompany
         manufacturer = aDecoder.decodeObject(forKey: "manufacturer") as? ConsumableCompany
         minAmt = aDecoder.decodeObject(forKey: "min_amt") as? Int
         modelNumber = aDecoder.decodeObject(forKey: "model_number") as? AnyObject
         name = aDecoder.decodeObject(forKey: "name") as? String
         notes = aDecoder.decodeObject(forKey: "notes") as? AnyObject
         orderNumber = aDecoder.decodeObject(forKey: "order_number") as? String
         purchaseCost = aDecoder.decodeObject(forKey: "purchase_cost") as? String
         purchaseDate = aDecoder.decodeObject(forKey: "purchase_date") as? ConsumablePurchaseDate
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         remaining = aDecoder.decodeObject(forKey: "remaining") as? Int
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? ConsumableCreatedAt
         userCanCheckout = aDecoder.decodeObject(forKey: "user_can_checkout") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if availableActions != nil{
			aCoder.encode(availableActions, forKey: "available_actions")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if company != nil{
			aCoder.encode(company, forKey: "company")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if itemNo != nil{
			aCoder.encode(itemNo, forKey: "item_no")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if manufacturer != nil{
			aCoder.encode(manufacturer, forKey: "manufacturer")
		}
		if minAmt != nil{
			aCoder.encode(minAmt, forKey: "min_amt")
		}
		if modelNumber != nil{
			aCoder.encode(modelNumber, forKey: "model_number")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if orderNumber != nil{
			aCoder.encode(orderNumber, forKey: "order_number")
		}
		if purchaseCost != nil{
			aCoder.encode(purchaseCost, forKey: "purchase_cost")
		}
		if purchaseDate != nil{
			aCoder.encode(purchaseDate, forKey: "purchase_date")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if remaining != nil{
			aCoder.encode(remaining, forKey: "remaining")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userCanCheckout != nil{
			aCoder.encode(userCanCheckout, forKey: "user_can_checkout")
		}

	}

}