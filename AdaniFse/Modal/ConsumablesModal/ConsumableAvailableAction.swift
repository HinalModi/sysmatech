//
//	ConsumableAvailableAction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ConsumableAvailableAction : NSObject, NSCoding{

	var checkin : Bool!
	var checkout : Bool!
	var delete : Bool!
	var update : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		checkin = dictionary["checkin"] as? Bool
		checkout = dictionary["checkout"] as? Bool
		delete = dictionary["delete"] as? Bool
		update = dictionary["update"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if checkin != nil{
			dictionary["checkin"] = checkin
		}
		if checkout != nil{
			dictionary["checkout"] = checkout
		}
		if delete != nil{
			dictionary["delete"] = delete
		}
		if update != nil{
			dictionary["update"] = update
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         checkin = aDecoder.decodeObject(forKey: "checkin") as? Bool
         checkout = aDecoder.decodeObject(forKey: "checkout") as? Bool
         delete = aDecoder.decodeObject(forKey: "delete") as? Bool
         update = aDecoder.decodeObject(forKey: "update") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if checkin != nil{
			aCoder.encode(checkin, forKey: "checkin")
		}
		if checkout != nil{
			aCoder.encode(checkout, forKey: "checkout")
		}
		if delete != nil{
			aCoder.encode(delete, forKey: "delete")
		}
		if update != nil{
			aCoder.encode(update, forKey: "update")
		}

	}

}