//
//	CategoryModal.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CategoryModal : NSObject, NSCoding{

	var apiStatus : Int!
	var categorylist : [Categorylist]!
	var modelslist : [Modelslist]!
	var statusCode : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apiStatus = dictionary["api_status"] as? Int
		categorylist = [Categorylist]()
		if let categorylistArray = dictionary["categorylist"] as? [[String:Any]]{
			for dic in categorylistArray{
				let value = Categorylist(fromDictionary: dic)
				categorylist.append(value)
			}
		}
		modelslist = [Modelslist]()
		if let modelslistArray = dictionary["modelslist"] as? [[String:Any]]{
			for dic in modelslistArray{
				let value = Modelslist(fromDictionary: dic)
				modelslist.append(value)
			}
		}
		statusCode = dictionary["status_code"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		if categorylist != nil{
			var dictionaryElements = [[String:Any]]()
			for categorylistElement in categorylist {
				dictionaryElements.append(categorylistElement.toDictionary())
			}
			dictionary["categorylist"] = dictionaryElements
		}
		if modelslist != nil{
			var dictionaryElements = [[String:Any]]()
			for modelslistElement in modelslist {
				dictionaryElements.append(modelslistElement.toDictionary())
			}
			dictionary["modelslist"] = dictionaryElements
		}
		if statusCode != nil{
			dictionary["status_code"] = statusCode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int
         categorylist = aDecoder.decodeObject(forKey :"categorylist") as? [Categorylist]
         modelslist = aDecoder.decodeObject(forKey :"modelslist") as? [Modelslist]
         statusCode = aDecoder.decodeObject(forKey: "status_code") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}
		if categorylist != nil{
			aCoder.encode(categorylist, forKey: "categorylist")
		}
		if modelslist != nil{
			aCoder.encode(modelslist, forKey: "modelslist")
		}
		if statusCode != nil{
			aCoder.encode(statusCode, forKey: "status_code")
		}

	}

}