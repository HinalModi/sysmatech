//
//	Modelslist.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Modelslist : NSObject, NSCoding{

	var catId : Int!
	var catName : String!
	var modelId : Int!
	var modelName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		catId = dictionary["cat_id"] as? Int
		catName = dictionary["cat_name"] as? String
		modelId = dictionary["model_id"] as? Int
		modelName = dictionary["model_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if catId != nil{
			dictionary["cat_id"] = catId
		}
		if catName != nil{
			dictionary["cat_name"] = catName
		}
		if modelId != nil{
			dictionary["model_id"] = modelId
		}
		if modelName != nil{
			dictionary["model_name"] = modelName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         catId = aDecoder.decodeObject(forKey: "cat_id") as? Int
         catName = aDecoder.decodeObject(forKey: "cat_name") as? String
         modelId = aDecoder.decodeObject(forKey: "model_id") as? Int
         modelName = aDecoder.decodeObject(forKey: "model_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if catId != nil{
			aCoder.encode(catId, forKey: "cat_id")
		}
		if catName != nil{
			aCoder.encode(catName, forKey: "cat_name")
		}
		if modelId != nil{
			aCoder.encode(modelId, forKey: "model_id")
		}
		if modelName != nil{
			aCoder.encode(modelName, forKey: "model_name")
		}

	}

}