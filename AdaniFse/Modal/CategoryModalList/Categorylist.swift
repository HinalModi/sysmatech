//
//	Categorylist.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Categorylist : NSObject, NSCoding{

	var catId : Int!
	var catName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		catId = dictionary["cat_id"] as? Int
		catName = dictionary["cat_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if catId != nil{
			dictionary["cat_id"] = catId
		}
		if catName != nil{
			dictionary["cat_name"] = catName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         catId = aDecoder.decodeObject(forKey: "cat_id") as? Int
         catName = aDecoder.decodeObject(forKey: "cat_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if catId != nil{
			aCoder.encode(catId, forKey: "cat_id")
		}
		if catName != nil{
			aCoder.encode(catName, forKey: "cat_name")
		}

	}

}