//
//	AccessoryDetailAccessoryCheckoutLog.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AccessoryDetailAccessoryCheckoutLog : NSObject, NSCoding{

	var accessoryId : Int!
	var approvingUser : Int!
	var checkedinTo : String!
	var checkedoutFrom : String!
	var createdAt : String!
	var dateTime : String!
	var deletedAt : String!
	var id : Int!
	var labelType : String!
	var qty : Int!
	var type : String!
	var updatedAt : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		accessoryId = dictionary["accessory_id"] as? Int
		approvingUser = dictionary["approving_user"] as? Int
		checkedinTo = dictionary["checkedin_to"] as? String
		checkedoutFrom = dictionary["checkedout_from"] as? String
		createdAt = dictionary["created_at"] as? String
		dateTime = dictionary["date_time"] as? String
		deletedAt = dictionary["deleted_at"] as? String
		id = dictionary["id"] as? Int
		labelType = dictionary["label_type"] as? String
		qty = dictionary["qty"] as? Int
		type = dictionary["type"] as? String
		updatedAt = dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if accessoryId != nil{
			dictionary["accessory_id"] = accessoryId
		}
		if approvingUser != nil{
			dictionary["approving_user"] = approvingUser
		}
		if checkedinTo != nil{
			dictionary["checkedin_to"] = checkedinTo
		}
		if checkedoutFrom != nil{
			dictionary["checkedout_from"] = checkedoutFrom
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if dateTime != nil{
			dictionary["date_time"] = dateTime
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if labelType != nil{
			dictionary["label_type"] = labelType
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if type != nil{
			dictionary["type"] = type
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accessoryId = aDecoder.decodeObject(forKey: "accessory_id") as? Int
         approvingUser = aDecoder.decodeObject(forKey: "approving_user") as? Int
         checkedinTo = aDecoder.decodeObject(forKey: "checkedin_to") as? String
         checkedoutFrom = aDecoder.decodeObject(forKey: "checkedout_from") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         dateTime = aDecoder.decodeObject(forKey: "date_time") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         labelType = aDecoder.decodeObject(forKey: "label_type") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         type = aDecoder.decodeObject(forKey: "type") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accessoryId != nil{
			aCoder.encode(accessoryId, forKey: "accessory_id")
		}
		if approvingUser != nil{
			aCoder.encode(approvingUser, forKey: "approving_user")
		}
		if checkedinTo != nil{
			aCoder.encode(checkedinTo, forKey: "checkedin_to")
		}
		if checkedoutFrom != nil{
			aCoder.encode(checkedoutFrom, forKey: "checkedout_from")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if dateTime != nil{
			aCoder.encode(dateTime, forKey: "date_time")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if labelType != nil{
			aCoder.encode(labelType, forKey: "label_type")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}