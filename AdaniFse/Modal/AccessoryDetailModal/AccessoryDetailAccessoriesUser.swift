//
//	AccessoryDetailAccessoriesUser.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AccessoryDetailAccessoriesUser : NSObject, NSCoding{

	var accessoryId : Int!
	var assignedAsset : AnyObject!
	var assignedLocation : Int!
	var assignedTo : AnyObject!
	var assignedType : String!
	var createdAt : String!
	var id : Int!
	var note : String!
	var qty : Int!
	var updatedAt : AnyObject!
	var userId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		accessoryId = dictionary["accessory_id"] as? Int
		assignedAsset = dictionary["assigned_asset"] as? AnyObject
		assignedLocation = dictionary["assigned_location"] as? Int
		assignedTo = dictionary["assigned_to"] as? AnyObject
		assignedType = dictionary["assigned_type"] as? String
		createdAt = dictionary["created_at"] as? String
		id = dictionary["id"] as? Int
		note = dictionary["note"] as? String
		qty = dictionary["qty"] as? Int
		updatedAt = dictionary["updated_at"] as? AnyObject
		userId = dictionary["user_id"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if accessoryId != nil{
			dictionary["accessory_id"] = accessoryId
		}
		if assignedAsset != nil{
			dictionary["assigned_asset"] = assignedAsset
		}
		if assignedLocation != nil{
			dictionary["assigned_location"] = assignedLocation
		}
		if assignedTo != nil{
			dictionary["assigned_to"] = assignedTo
		}
		if assignedType != nil{
			dictionary["assigned_type"] = assignedType
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if id != nil{
			dictionary["id"] = id
		}
		if note != nil{
			dictionary["note"] = note
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accessoryId = aDecoder.decodeObject(forKey: "accessory_id") as? Int
         assignedAsset = aDecoder.decodeObject(forKey: "assigned_asset") as? AnyObject
         assignedLocation = aDecoder.decodeObject(forKey: "assigned_location") as? Int
         assignedTo = aDecoder.decodeObject(forKey: "assigned_to") as? AnyObject
         assignedType = aDecoder.decodeObject(forKey: "assigned_type") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         note = aDecoder.decodeObject(forKey: "note") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? AnyObject
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accessoryId != nil{
			aCoder.encode(accessoryId, forKey: "accessory_id")
		}
		if assignedAsset != nil{
			aCoder.encode(assignedAsset, forKey: "assigned_asset")
		}
		if assignedLocation != nil{
			aCoder.encode(assignedLocation, forKey: "assigned_location")
		}
		if assignedTo != nil{
			aCoder.encode(assignedTo, forKey: "assigned_to")
		}
		if assignedType != nil{
			aCoder.encode(assignedType, forKey: "assigned_type")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if note != nil{
			aCoder.encode(note, forKey: "note")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}