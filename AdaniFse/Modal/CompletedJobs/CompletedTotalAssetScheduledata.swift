//
//	CompletedTotalAssetScheduledata.swift
//
//	Create by Ankit Dave on 17/5/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CompletedTotalAssetScheduledata : NSObject, NSCoding{

	var snipeitFan4 : AnyObject!
	var snipeitFeatures6 : AnyObject!
	var snipeitMacAddress1 : String!
	var snipeitNetworkManagement7 : AnyObject!
	var snipeitNetworkPorts9 : AnyObject!
	var snipeitPowerOverEthernetPoe3 : AnyObject!
	var snipeitPowerSupply5 : AnyObject!
	var snipeitUplinkPorts2 : AnyObject!
	var snipeitWarrantyType8 : AnyObject!
	var accepted : AnyObject!
	var archived : Int!
	var assetOwnerId : Int!
	var assetTag : String!
	var assetTagId : Int!
	var assetUserOwnerId : AnyObject!
	var assignedTo : Int!
	var assignedType : String!
	var auditEndDate : String!
	var auditId : Int!
	var auditInspectionDate : String!
	var auditName : String!
	var auditParamsId : Int!
	var auditParamsTransaction : String!
	var auditParamsValues : String!
	var auditResult : String!
	var auditSchduleId : Int!
	var auditStartDate : String!
	var auditStatus : String!
	var categoryName : String!
	var checkinCounter : Int!
	var checkoutCounter : Int!
	var companyId : Int!
	var companyName : String!
	var createdAt : String!
	var deletedAt : AnyObject!
	var depreciate : Int!
	var dyamicModelFieldset : String!
	var expectedCheckin : AnyObject!
	var id : Int!
	var image : String!
	var inspectionDelay : String!
	var institutionId : Int!
	var lastAuditDate : String!
	var lastCheckout : String!
	var location : String!
	var locationId : AnyObject!
	var modelId : Int!
	var modelName : String!
	var name : String!
	var nextAuditDate : AnyObject!
	var notes : AnyObject!
	var orderNumber : AnyObject!
	var paramsMasterId : String!
	var physical : Int!
	var prevMainOwnerType : String!
	var prevMaintenanaceOwnerId : Int!
	var prevMaintenanaceOwnerType : String!
	var purchaseCost : String!
	var purchaseDate : String!
	var requestable : Int!
	var requestsCounter : Int!
	var rtdLocationId : Int!
	var scheduleExpireDate : String!
	var serial : AnyObject!
	var statusId : Int!
	var supplierId : Int!
	var supplierName : String!
	var updatedAt : String!
	var userId : AnyObject!
	var warrantyMonths : Int!
    var inspectionBy : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		snipeitFan4 = dictionary["_snipeit_fan_4"] as? AnyObject
		snipeitFeatures6 = dictionary["_snipeit_features_6"] as? AnyObject
		snipeitMacAddress1 = dictionary["_snipeit_mac_address_1"] as? String
		snipeitNetworkManagement7 = dictionary["_snipeit_network_management_7"] as? AnyObject
		snipeitNetworkPorts9 = dictionary["_snipeit_network_ports_9"] as? AnyObject
		snipeitPowerOverEthernetPoe3 = dictionary["_snipeit_power_over_ethernet_poe_3"] as? AnyObject
		snipeitPowerSupply5 = dictionary["_snipeit_power_supply_5"] as? AnyObject
		snipeitUplinkPorts2 = dictionary["_snipeit_uplink_ports_2"] as? AnyObject
		snipeitWarrantyType8 = dictionary["_snipeit_warranty_type_8"] as? AnyObject
		accepted = dictionary["accepted"] as? AnyObject
		archived = dictionary["archived"] as? Int
		assetOwnerId = dictionary["asset_owner_id"] as? Int
		assetTag = dictionary["asset_tag"] as? String
		assetTagId = dictionary["asset_tag_id"] as? Int
		assetUserOwnerId = dictionary["asset_user_owner_id"] as? AnyObject
		assignedTo = dictionary["assigned_to"] as? Int
		assignedType = dictionary["assigned_type"] as? String
		auditEndDate = dictionary["audit_end_date"] as? String
		auditId = dictionary["audit_id"] as? Int
		auditInspectionDate = dictionary["audit_inspection_date"] as? String
		auditName = dictionary["audit_name"] as? String
		auditParamsId = dictionary["audit_params_id"] as? Int
		auditParamsTransaction = dictionary["audit_params_transaction"] as? String
		auditParamsValues = dictionary["audit_params_values"] as? String
		auditResult = dictionary["audit_result"] as? String
		auditSchduleId = dictionary["audit_schdule_id"] as? Int
		auditStartDate = dictionary["audit_start_date"] as? String
		auditStatus = dictionary["audit_status"] as? String
		categoryName = dictionary["category_name"] as? String
		checkinCounter = dictionary["checkin_counter"] as? Int
		checkoutCounter = dictionary["checkout_counter"] as? Int
		companyId = dictionary["company_id"] as? Int
		companyName = dictionary["company_name"] as? String
		createdAt = dictionary["created_at"] as? String
		deletedAt = dictionary["deleted_at"] as? AnyObject
		depreciate = dictionary["depreciate"] as? Int
		dyamicModelFieldset = dictionary["dyamic_model_fieldset"] as? String
		expectedCheckin = dictionary["expected_checkin"] as? AnyObject
		id = dictionary["id"] as? Int
		image = dictionary["image"] as? String
		inspectionDelay = dictionary["inspection_delay"] as? String
		institutionId = dictionary["institution_id"] as? Int
		lastAuditDate = dictionary["last_audit_date"] as? String
		lastCheckout = dictionary["last_checkout"] as? String
		location = dictionary["location"] as? String
		locationId = dictionary["location_id"] as? AnyObject
		modelId = dictionary["model_id"] as? Int
		modelName = dictionary["model_name"] as? String
		name = dictionary["name"] as? String
		nextAuditDate = dictionary["next_audit_date"] as? AnyObject
		notes = dictionary["notes"] as? AnyObject
		orderNumber = dictionary["order_number"] as? AnyObject
		paramsMasterId = dictionary["params_master_id"] as? String
		physical = dictionary["physical"] as? Int
		prevMainOwnerType = dictionary["prev_main_owner_type"] as? String
		prevMaintenanaceOwnerId = dictionary["prev_maintenanace_owner_id"] as? Int
		prevMaintenanaceOwnerType = dictionary["prev_maintenanace_owner_type"] as? String
		purchaseCost = dictionary["purchase_cost"] as? String
		purchaseDate = dictionary["purchase_date"] as? String
		requestable = dictionary["requestable"] as? Int
		requestsCounter = dictionary["requests_counter"] as? Int
		rtdLocationId = dictionary["rtd_location_id"] as? Int
		scheduleExpireDate = dictionary["schedule_expire_date"] as? String
		serial = dictionary["serial"] as? AnyObject
		statusId = dictionary["status_id"] as? Int
		supplierId = dictionary["supplier_id"] as? Int
		supplierName = dictionary["supplier_name"] as? String
		updatedAt = dictionary["updated_at"] as? String
		userId = dictionary["user_id"] as? AnyObject
		warrantyMonths = dictionary["warranty_months"] as? Int
        inspectionBy = dictionary["inspection_by"] as? String
        
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if snipeitFan4 != nil{
			dictionary["_snipeit_fan_4"] = snipeitFan4
		}
		if snipeitFeatures6 != nil{
			dictionary["_snipeit_features_6"] = snipeitFeatures6
		}
		if snipeitMacAddress1 != nil{
			dictionary["_snipeit_mac_address_1"] = snipeitMacAddress1
		}
		if snipeitNetworkManagement7 != nil{
			dictionary["_snipeit_network_management_7"] = snipeitNetworkManagement7
		}
		if snipeitNetworkPorts9 != nil{
			dictionary["_snipeit_network_ports_9"] = snipeitNetworkPorts9
		}
		if snipeitPowerOverEthernetPoe3 != nil{
			dictionary["_snipeit_power_over_ethernet_poe_3"] = snipeitPowerOverEthernetPoe3
		}
		if snipeitPowerSupply5 != nil{
			dictionary["_snipeit_power_supply_5"] = snipeitPowerSupply5
		}
		if snipeitUplinkPorts2 != nil{
			dictionary["_snipeit_uplink_ports_2"] = snipeitUplinkPorts2
		}
		if snipeitWarrantyType8 != nil{
			dictionary["_snipeit_warranty_type_8"] = snipeitWarrantyType8
		}
		if accepted != nil{
			dictionary["accepted"] = accepted
		}
		if archived != nil{
			dictionary["archived"] = archived
		}
		if assetOwnerId != nil{
			dictionary["asset_owner_id"] = assetOwnerId
		}
		if assetTag != nil{
			dictionary["asset_tag"] = assetTag
		}
		if assetTagId != nil{
			dictionary["asset_tag_id"] = assetTagId
		}
		if assetUserOwnerId != nil{
			dictionary["asset_user_owner_id"] = assetUserOwnerId
		}
		if assignedTo != nil{
			dictionary["assigned_to"] = assignedTo
		}
		if assignedType != nil{
			dictionary["assigned_type"] = assignedType
		}
		if auditEndDate != nil{
			dictionary["audit_end_date"] = auditEndDate
		}
		if auditId != nil{
			dictionary["audit_id"] = auditId
		}
		if auditInspectionDate != nil{
			dictionary["audit_inspection_date"] = auditInspectionDate
		}
		if auditName != nil{
			dictionary["audit_name"] = auditName
		}
		if auditParamsId != nil{
			dictionary["audit_params_id"] = auditParamsId
		}
		if auditParamsTransaction != nil{
			dictionary["audit_params_transaction"] = auditParamsTransaction
		}
		if auditParamsValues != nil{
			dictionary["audit_params_values"] = auditParamsValues
		}
		if auditResult != nil{
			dictionary["audit_result"] = auditResult
		}
		if auditSchduleId != nil{
			dictionary["audit_schdule_id"] = auditSchduleId
		}
		if auditStartDate != nil{
			dictionary["audit_start_date"] = auditStartDate
		}
		if auditStatus != nil{
			dictionary["audit_status"] = auditStatus
		}
		if categoryName != nil{
			dictionary["category_name"] = categoryName
		}
		if checkinCounter != nil{
			dictionary["checkin_counter"] = checkinCounter
		}
		if checkoutCounter != nil{
			dictionary["checkout_counter"] = checkoutCounter
		}
		if companyId != nil{
			dictionary["company_id"] = companyId
		}
		if companyName != nil{
			dictionary["company_name"] = companyName
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if depreciate != nil{
			dictionary["depreciate"] = depreciate
		}
		if dyamicModelFieldset != nil{
			dictionary["dyamic_model_fieldset"] = dyamicModelFieldset
		}
		if expectedCheckin != nil{
			dictionary["expected_checkin"] = expectedCheckin
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if inspectionDelay != nil{
			dictionary["inspection_delay"] = inspectionDelay
		}
		if institutionId != nil{
			dictionary["institution_id"] = institutionId
		}
		if lastAuditDate != nil{
			dictionary["last_audit_date"] = lastAuditDate
		}
		if lastCheckout != nil{
			dictionary["last_checkout"] = lastCheckout
		}
		if location != nil{
			dictionary["location"] = location
		}
		if locationId != nil{
			dictionary["location_id"] = locationId
		}
		if modelId != nil{
			dictionary["model_id"] = modelId
		}
		if modelName != nil{
			dictionary["model_name"] = modelName
		}
		if name != nil{
			dictionary["name"] = name
		}
		if nextAuditDate != nil{
			dictionary["next_audit_date"] = nextAuditDate
		}
		if notes != nil{
			dictionary["notes"] = notes
		}
		if orderNumber != nil{
			dictionary["order_number"] = orderNumber
		}
		if paramsMasterId != nil{
			dictionary["params_master_id"] = paramsMasterId
		}
		if physical != nil{
			dictionary["physical"] = physical
		}
		if prevMainOwnerType != nil{
			dictionary["prev_main_owner_type"] = prevMainOwnerType
		}
		if prevMaintenanaceOwnerId != nil{
			dictionary["prev_maintenanace_owner_id"] = prevMaintenanaceOwnerId
		}
		if prevMaintenanaceOwnerType != nil{
			dictionary["prev_maintenanace_owner_type"] = prevMaintenanaceOwnerType
		}
		if purchaseCost != nil{
			dictionary["purchase_cost"] = purchaseCost
		}
		if purchaseDate != nil{
			dictionary["purchase_date"] = purchaseDate
		}
		if requestable != nil{
			dictionary["requestable"] = requestable
		}
		if requestsCounter != nil{
			dictionary["requests_counter"] = requestsCounter
		}
		if rtdLocationId != nil{
			dictionary["rtd_location_id"] = rtdLocationId
		}
		if scheduleExpireDate != nil{
			dictionary["schedule_expire_date"] = scheduleExpireDate
		}
		if serial != nil{
			dictionary["serial"] = serial
		}
		if statusId != nil{
			dictionary["status_id"] = statusId
		}
		if supplierId != nil{
			dictionary["supplier_id"] = supplierId
		}
		if supplierName != nil{
			dictionary["supplier_name"] = supplierName
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if warrantyMonths != nil{
			dictionary["warranty_months"] = warrantyMonths
		}
        if inspectionBy != nil{
            dictionary["inspection_by"] = inspectionBy
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         snipeitFan4 = aDecoder.decodeObject(forKey: "_snipeit_fan_4") as? AnyObject
         snipeitFeatures6 = aDecoder.decodeObject(forKey: "_snipeit_features_6") as? AnyObject
         snipeitMacAddress1 = aDecoder.decodeObject(forKey: "_snipeit_mac_address_1") as? String
         snipeitNetworkManagement7 = aDecoder.decodeObject(forKey: "_snipeit_network_management_7") as? AnyObject
         snipeitNetworkPorts9 = aDecoder.decodeObject(forKey: "_snipeit_network_ports_9") as? AnyObject
         snipeitPowerOverEthernetPoe3 = aDecoder.decodeObject(forKey: "_snipeit_power_over_ethernet_poe_3") as? AnyObject
         snipeitPowerSupply5 = aDecoder.decodeObject(forKey: "_snipeit_power_supply_5") as? AnyObject
         snipeitUplinkPorts2 = aDecoder.decodeObject(forKey: "_snipeit_uplink_ports_2") as? AnyObject
         snipeitWarrantyType8 = aDecoder.decodeObject(forKey: "_snipeit_warranty_type_8") as? AnyObject
         accepted = aDecoder.decodeObject(forKey: "accepted") as? AnyObject
         archived = aDecoder.decodeObject(forKey: "archived") as? Int
         assetOwnerId = aDecoder.decodeObject(forKey: "asset_owner_id") as? Int
         assetTag = aDecoder.decodeObject(forKey: "asset_tag") as? String
         assetTagId = aDecoder.decodeObject(forKey: "asset_tag_id") as? Int
         assetUserOwnerId = aDecoder.decodeObject(forKey: "asset_user_owner_id") as? AnyObject
         assignedTo = aDecoder.decodeObject(forKey: "assigned_to") as? Int
         assignedType = aDecoder.decodeObject(forKey: "assigned_type") as? String
         auditEndDate = aDecoder.decodeObject(forKey: "audit_end_date") as? String
         auditId = aDecoder.decodeObject(forKey: "audit_id") as? Int
         auditInspectionDate = aDecoder.decodeObject(forKey: "audit_inspection_date") as? String
         auditName = aDecoder.decodeObject(forKey: "audit_name") as? String
         auditParamsId = aDecoder.decodeObject(forKey: "audit_params_id") as? Int
         auditParamsTransaction = aDecoder.decodeObject(forKey: "audit_params_transaction") as? String
         auditParamsValues = aDecoder.decodeObject(forKey: "audit_params_values") as? String
         auditResult = aDecoder.decodeObject(forKey: "audit_result") as? String
         auditSchduleId = aDecoder.decodeObject(forKey: "audit_schdule_id") as? Int
         auditStartDate = aDecoder.decodeObject(forKey: "audit_start_date") as? String
         auditStatus = aDecoder.decodeObject(forKey: "audit_status") as? String
         categoryName = aDecoder.decodeObject(forKey: "category_name") as? String
         checkinCounter = aDecoder.decodeObject(forKey: "checkin_counter") as? Int
         checkoutCounter = aDecoder.decodeObject(forKey: "checkout_counter") as? Int
         companyId = aDecoder.decodeObject(forKey: "company_id") as? Int
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? AnyObject
         depreciate = aDecoder.decodeObject(forKey: "depreciate") as? Int
         dyamicModelFieldset = aDecoder.decodeObject(forKey: "dyamic_model_fieldset") as? String
         expectedCheckin = aDecoder.decodeObject(forKey: "expected_checkin") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         inspectionDelay = aDecoder.decodeObject(forKey: "inspection_delay") as? String
         institutionId = aDecoder.decodeObject(forKey: "institution_id") as? Int
         lastAuditDate = aDecoder.decodeObject(forKey: "last_audit_date") as? String
         lastCheckout = aDecoder.decodeObject(forKey: "last_checkout") as? String
         location = aDecoder.decodeObject(forKey: "location") as? String
         locationId = aDecoder.decodeObject(forKey: "location_id") as? AnyObject
         modelId = aDecoder.decodeObject(forKey: "model_id") as? Int
         modelName = aDecoder.decodeObject(forKey: "model_name") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         nextAuditDate = aDecoder.decodeObject(forKey: "next_audit_date") as? AnyObject
         notes = aDecoder.decodeObject(forKey: "notes") as? AnyObject
         orderNumber = aDecoder.decodeObject(forKey: "order_number") as? AnyObject
         paramsMasterId = aDecoder.decodeObject(forKey: "params_master_id") as? String
         physical = aDecoder.decodeObject(forKey: "physical") as? Int
         prevMainOwnerType = aDecoder.decodeObject(forKey: "prev_main_owner_type") as? String
         prevMaintenanaceOwnerId = aDecoder.decodeObject(forKey: "prev_maintenanace_owner_id") as? Int
         prevMaintenanaceOwnerType = aDecoder.decodeObject(forKey: "prev_maintenanace_owner_type") as? String
         purchaseCost = aDecoder.decodeObject(forKey: "purchase_cost") as? String
         purchaseDate = aDecoder.decodeObject(forKey: "purchase_date") as? String
         requestable = aDecoder.decodeObject(forKey: "requestable") as? Int
         requestsCounter = aDecoder.decodeObject(forKey: "requests_counter") as? Int
         rtdLocationId = aDecoder.decodeObject(forKey: "rtd_location_id") as? Int
        scheduleExpireDate = aDecoder.decodeObject(forKey: "schedule_expire_date") as? String
        serial = aDecoder.decodeObject(forKey: "serial") as? AnyObject
        statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
        supplierId = aDecoder.decodeObject(forKey: "supplier_id") as? Int
        supplierName = aDecoder.decodeObject(forKey: "supplier_name") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? AnyObject
        warrantyMonths = aDecoder.decodeObject(forKey: "warranty_months") as? Int
        inspectionBy = aDecoder.decodeObject(forKey: "inspection_by") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if snipeitFan4 != nil{
			aCoder.encode(snipeitFan4, forKey: "_snipeit_fan_4")
		}
		if snipeitFeatures6 != nil{
			aCoder.encode(snipeitFeatures6, forKey: "_snipeit_features_6")
		}
		if snipeitMacAddress1 != nil{
			aCoder.encode(snipeitMacAddress1, forKey: "_snipeit_mac_address_1")
		}
		if snipeitNetworkManagement7 != nil{
			aCoder.encode(snipeitNetworkManagement7, forKey: "_snipeit_network_management_7")
		}
		if snipeitNetworkPorts9 != nil{
			aCoder.encode(snipeitNetworkPorts9, forKey: "_snipeit_network_ports_9")
		}
		if snipeitPowerOverEthernetPoe3 != nil{
			aCoder.encode(snipeitPowerOverEthernetPoe3, forKey: "_snipeit_power_over_ethernet_poe_3")
		}
		if snipeitPowerSupply5 != nil{
			aCoder.encode(snipeitPowerSupply5, forKey: "_snipeit_power_supply_5")
		}
		if snipeitUplinkPorts2 != nil{
			aCoder.encode(snipeitUplinkPorts2, forKey: "_snipeit_uplink_ports_2")
		}
		if snipeitWarrantyType8 != nil{
			aCoder.encode(snipeitWarrantyType8, forKey: "_snipeit_warranty_type_8")
		}
		if accepted != nil{
			aCoder.encode(accepted, forKey: "accepted")
		}
		if archived != nil{
			aCoder.encode(archived, forKey: "archived")
		}
		if assetOwnerId != nil{
			aCoder.encode(assetOwnerId, forKey: "asset_owner_id")
		}
		if assetTag != nil{
			aCoder.encode(assetTag, forKey: "asset_tag")
		}
		if assetTagId != nil{
			aCoder.encode(assetTagId, forKey: "asset_tag_id")
		}
		if assetUserOwnerId != nil{
			aCoder.encode(assetUserOwnerId, forKey: "asset_user_owner_id")
		}
		if assignedTo != nil{
			aCoder.encode(assignedTo, forKey: "assigned_to")
		}
		if assignedType != nil{
			aCoder.encode(assignedType, forKey: "assigned_type")
		}
		if auditEndDate != nil{
			aCoder.encode(auditEndDate, forKey: "audit_end_date")
		}
		if auditId != nil{
			aCoder.encode(auditId, forKey: "audit_id")
		}
		if auditInspectionDate != nil{
			aCoder.encode(auditInspectionDate, forKey: "audit_inspection_date")
		}
		if auditName != nil{
			aCoder.encode(auditName, forKey: "audit_name")
		}
		if auditParamsId != nil{
			aCoder.encode(auditParamsId, forKey: "audit_params_id")
		}
		if auditParamsTransaction != nil{
			aCoder.encode(auditParamsTransaction, forKey: "audit_params_transaction")
		}
		if auditParamsValues != nil{
			aCoder.encode(auditParamsValues, forKey: "audit_params_values")
		}
		if auditResult != nil{
			aCoder.encode(auditResult, forKey: "audit_result")
		}
		if auditSchduleId != nil{
			aCoder.encode(auditSchduleId, forKey: "audit_schdule_id")
		}
		if auditStartDate != nil{
			aCoder.encode(auditStartDate, forKey: "audit_start_date")
		}
		if auditStatus != nil{
			aCoder.encode(auditStatus, forKey: "audit_status")
		}
		if categoryName != nil{
			aCoder.encode(categoryName, forKey: "category_name")
		}
		if checkinCounter != nil{
			aCoder.encode(checkinCounter, forKey: "checkin_counter")
		}
		if checkoutCounter != nil{
			aCoder.encode(checkoutCounter, forKey: "checkout_counter")
		}
		if companyId != nil{
			aCoder.encode(companyId, forKey: "company_id")
		}
		if companyName != nil{
			aCoder.encode(companyName, forKey: "company_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if depreciate != nil{
			aCoder.encode(depreciate, forKey: "depreciate")
		}
		if dyamicModelFieldset != nil{
			aCoder.encode(dyamicModelFieldset, forKey: "dyamic_model_fieldset")
		}
		if expectedCheckin != nil{
			aCoder.encode(expectedCheckin, forKey: "expected_checkin")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if inspectionDelay != nil{
			aCoder.encode(inspectionDelay, forKey: "inspection_delay")
		}
		if institutionId != nil{
			aCoder.encode(institutionId, forKey: "institution_id")
		}
		if lastAuditDate != nil{
			aCoder.encode(lastAuditDate, forKey: "last_audit_date")
		}
		if lastCheckout != nil{
			aCoder.encode(lastCheckout, forKey: "last_checkout")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if locationId != nil{
			aCoder.encode(locationId, forKey: "location_id")
		}
		if modelId != nil{
			aCoder.encode(modelId, forKey: "model_id")
		}
		if modelName != nil{
			aCoder.encode(modelName, forKey: "model_name")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if nextAuditDate != nil{
			aCoder.encode(nextAuditDate, forKey: "next_audit_date")
		}
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if orderNumber != nil{
			aCoder.encode(orderNumber, forKey: "order_number")
		}
		if paramsMasterId != nil{
			aCoder.encode(paramsMasterId, forKey: "params_master_id")
		}
		if physical != nil{
			aCoder.encode(physical, forKey: "physical")
		}
		if prevMainOwnerType != nil{
			aCoder.encode(prevMainOwnerType, forKey: "prev_main_owner_type")
		}
		if prevMaintenanaceOwnerId != nil{
			aCoder.encode(prevMaintenanaceOwnerId, forKey: "prev_maintenanace_owner_id")
		}
		if prevMaintenanaceOwnerType != nil{
			aCoder.encode(prevMaintenanaceOwnerType, forKey: "prev_maintenanace_owner_type")
		}
		if purchaseCost != nil{
			aCoder.encode(purchaseCost, forKey: "purchase_cost")
		}
		if purchaseDate != nil{
			aCoder.encode(purchaseDate, forKey: "purchase_date")
		}
		if requestable != nil{
			aCoder.encode(requestable, forKey: "requestable")
		}
		if requestsCounter != nil{
			aCoder.encode(requestsCounter, forKey: "requests_counter")
		}
		if rtdLocationId != nil{
			aCoder.encode(rtdLocationId, forKey: "rtd_location_id")
		}
		if scheduleExpireDate != nil{
			aCoder.encode(scheduleExpireDate, forKey: "schedule_expire_date")
		}
		if serial != nil{
			aCoder.encode(serial, forKey: "serial")
		}
		if statusId != nil{
			aCoder.encode(statusId, forKey: "status_id")
		}
		if supplierId != nil{
			aCoder.encode(supplierId, forKey: "supplier_id")
		}
		if supplierName != nil{
			aCoder.encode(supplierName, forKey: "supplier_name")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if warrantyMonths != nil{
			aCoder.encode(warrantyMonths, forKey: "warranty_months")
		}
        if inspectionBy != nil{
            aCoder.encode(inspectionBy, forKey: "inspection_by")
        }
	}

}
