//
//	CheckCompData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CheckCompData : NSObject, NSCoding{

	var bearerToken : String!
	var assetBreakdwonInsepctionImgPath : String!
	var assetImgPath : String!
	var assetInsepctionImgPath : String!
	var companyId : String!
	var companyName : String!
	var domainName : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		bearerToken = dictionary["BearerToken"] as? String
		assetBreakdwonInsepctionImgPath = dictionary["asset_breakdwon_insepction_img_path"] as? String
		assetImgPath = dictionary["asset_img_path"] as? String
		assetInsepctionImgPath = dictionary["asset_insepction_img_path"] as? String
		companyId = dictionary["company_id"] as? String
		companyName = dictionary["company_name"] as? String
		domainName = dictionary["domain_name"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if bearerToken != nil{
			dictionary["BearerToken"] = bearerToken
		}
		if assetBreakdwonInsepctionImgPath != nil{
			dictionary["asset_breakdwon_insepction_img_path"] = assetBreakdwonInsepctionImgPath
		}
		if assetImgPath != nil{
			dictionary["asset_img_path"] = assetImgPath
		}
		if assetInsepctionImgPath != nil{
			dictionary["asset_insepction_img_path"] = assetInsepctionImgPath
		}
		if companyId != nil{
			dictionary["company_id"] = companyId
		}
		if companyName != nil{
			dictionary["company_name"] = companyName
		}
		if domainName != nil{
			dictionary["domain_name"] = domainName
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bearerToken = aDecoder.decodeObject(forKey: "BearerToken") as? String
         assetBreakdwonInsepctionImgPath = aDecoder.decodeObject(forKey: "asset_breakdwon_insepction_img_path") as? String
         assetImgPath = aDecoder.decodeObject(forKey: "asset_img_path") as? String
         assetInsepctionImgPath = aDecoder.decodeObject(forKey: "asset_insepction_img_path") as? String
         companyId = aDecoder.decodeObject(forKey: "company_id") as? String
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         domainName = aDecoder.decodeObject(forKey: "domain_name") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if bearerToken != nil{
			aCoder.encode(bearerToken, forKey: "BearerToken")
		}
		if assetBreakdwonInsepctionImgPath != nil{
			aCoder.encode(assetBreakdwonInsepctionImgPath, forKey: "asset_breakdwon_insepction_img_path")
		}
		if assetImgPath != nil{
			aCoder.encode(assetImgPath, forKey: "asset_img_path")
		}
		if assetInsepctionImgPath != nil{
			aCoder.encode(assetInsepctionImgPath, forKey: "asset_insepction_img_path")
		}
		if companyId != nil{
			aCoder.encode(companyId, forKey: "company_id")
		}
		if companyName != nil{
			aCoder.encode(companyName, forKey: "company_name")
		}
		if domainName != nil{
			aCoder.encode(domainName, forKey: "domain_name")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}