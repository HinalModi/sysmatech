//
//	pending .swift
//
//	Create by Ankit Dave on 17/5/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Pending : NSObject, NSCoding{

	var apiMessage : String!
	var apiStatus : Int!
	var totalAssetScheduledata : [PendingTotalAssetScheduledata]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apiMessage = dictionary["api_message"] as? String
		apiStatus = dictionary["api_status"] as? Int
		totalAssetScheduledata = [PendingTotalAssetScheduledata]()
		if let totalAssetScheduledataArray = dictionary["totalAssetScheduledata"] as? [[String:Any]]{
			for dic in totalAssetScheduledataArray{
				let value = PendingTotalAssetScheduledata(fromDictionary: dic)
				totalAssetScheduledata.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apiMessage != nil{
			dictionary["api_message"] = apiMessage
		}
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		if totalAssetScheduledata != nil{
			var dictionaryElements = [[String:Any]]()
			for totalAssetScheduledataElement in totalAssetScheduledata {
				dictionaryElements.append(totalAssetScheduledataElement.toDictionary())
			}
			dictionary["totalAssetScheduledata"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiMessage = aDecoder.decodeObject(forKey: "api_message") as? String
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int
         totalAssetScheduledata = aDecoder.decodeObject(forKey :"totalAssetScheduledata") as? [PendingTotalAssetScheduledata]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if apiMessage != nil{
			aCoder.encode(apiMessage, forKey: "api_message")
		}
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}
		if totalAssetScheduledata != nil{
			aCoder.encode(totalAssetScheduledata, forKey: "totalAssetScheduledata")
		}

	}

}
