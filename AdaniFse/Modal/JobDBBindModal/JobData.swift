//
//  PendingJobData.swift
//  AdaniFse
//
//  Created by Ankit Dave on 20/05/22.
//

import Foundation
class PendingJobData : Equatable{
    static func == (lhs: PendingJobData, rhs: PendingJobData) -> Bool {
        true
    }
    
    var audit_schduleId: Int64
    var id : Int64
    var name: String
    var assetTag: String
    var location : String
    var warrentyTime : Int64
    var supplier : String
    var purchaseDate : String
    var modalName : String
    var category : String
    var company : String
    var auditValue : String
    var dueDate : String
    var image : String
    var auditName : String
    var assetTagId : Int64
    var escalatedAuditLevels : Int64
    var maintainanceType : String
    var can_checkin : Int64
    var can_checkout : Int64
    var asset_image : String
    
    init(audit_schduleId : Int64?, id : Int64?, name: String?, assetTag: String? , location: String? , warrentyTime: Int64? ,supplier : String? ,purchaseDate : String?,modalName : String? , category : String? , company : String? , auditValue : String? , dueDate : String? ,image : String? ,auditName : String? ,assetTagId : Int64? , escalatedAuditLevels : Int64? , maintainanceType : String? , can_checkin : Int64? ,can_checkout : Int64? ,asset_image : String){
        self.audit_schduleId = audit_schduleId ?? 0
        self.id = id ?? 0
        self.name =  name ?? ""
        self.assetTag = assetTag ?? ""
        self.location = location ?? ""
        self.warrentyTime = warrentyTime ?? 0
        self.supplier = supplier ?? ""
        self.purchaseDate = purchaseDate ?? ""
        self.modalName = modalName ?? ""
        self.category = category ?? ""
        self.company = company ?? ""
        self.auditValue = auditValue ?? ""
        self.dueDate = dueDate ?? ""
        self.image = image ?? ""
        self.auditName = auditName ?? ""
        self.assetTagId = assetTagId ?? 0
        self.escalatedAuditLevels = escalatedAuditLevels ?? 0
        self.maintainanceType = maintainanceType ?? ""
        self.can_checkout = can_checkout ?? 0
        self.can_checkin = can_checkin ?? 0
        self.asset_image = asset_image
    }
}
class CompletedJobs : Equatable{
    static func == (lhs: CompletedJobs, rhs: CompletedJobs) -> Bool {
        true
    }
    
    var audit_schduleId: Int64
    var id : Int64
    var name: String
    var assetTag: String
    var location : String
    var warrentyTime : Int64
    var supplier : String
    var purchaseDate : String
    var modalName : String
    var category : String
    var company : String
    var auditValue : String
    var dueDate : String
    var image : String
    var auditName : String
    var lastAuditDate : String
    var auditParamsTransaction : String
    var assetTagId : Int64
    var inspectedBy : String
    init(audit_schduleId : Int64, id : Int64, name: String, assetTag: String , location: String , warrentyTime: Int64 ,supplier : String ,purchaseDate : String ,modalName : String , category : String , company : String , auditValue : String , dueDate : String ,image : String ,auditName : String , lastAuditDate : String , auditParamsTransaction : String , assetTagId : Int64, inspectedBy : String){
        self.audit_schduleId = audit_schduleId
        self.id = id
        self.name = name
        self.assetTag = assetTag
        self.location = location
        self.warrentyTime = warrentyTime
        self.supplier = supplier
        self.purchaseDate = purchaseDate
        self.modalName = modalName
        self.category = category
        self.company = company
        self.auditValue = auditValue
        self.dueDate = dueDate
        self.image = image
        self.auditName = auditName
        self.lastAuditDate = lastAuditDate
        self.auditParamsTransaction = auditParamsTransaction
        self.assetTagId = assetTagId
        self.inspectedBy = inspectedBy
    }
}

class CompletedJobDetail{
    
    var id : Int64
    var audit_params_transaction_id : Int64
    var audit_update_date : String
    var audit_asset_image : String
    var audit_gps_location : String
    var audit_param_name : String
    var inspection_img : String
    var inspection_comment : String
    var audit_param_value : String
    var audit_schedule_id : Int64
    var audit_param_result : String
    var audit_param_name_val_id : Int64
    var audit_param_audio : String
    init(id: Int64, audit_params_transaction_id: Int64, audit_update_date: String, audit_asset_image: String, audit_gps_location: String, audit_param_name: String, inspection_img: String, inspection_comment: String, audit_param_value: String, audit_schedule_id: Int64, audit_param_result: String , audit_param_name_val_id : Int64 , audit_param_audio : String) {
        self.id = id
        self.audit_params_transaction_id = audit_params_transaction_id
        self.audit_update_date = audit_update_date
        self.audit_asset_image = audit_asset_image
        self.audit_gps_location = audit_gps_location
        self.audit_param_name = audit_param_name
        self.inspection_img = inspection_img
        self.inspection_comment = inspection_comment
        self.audit_param_value = audit_param_value
        self.audit_schedule_id = audit_schedule_id
        self.audit_param_result = audit_param_result
        self.audit_param_name_val_id = audit_param_name_val_id
        self.audit_param_audio = audit_param_audio
    }
}
class PageValue : Equatable{
    
    static func == (lhs: PageValue, rhs: PageValue) -> Bool {
        true
    }
    var indexId : Int!
    var paramValue : String
    var validation : String!
    init(indexId: Int? = nil, paramValue: String , validation : String? = nil) {
        self.indexId = indexId
        self.paramValue = paramValue
        self.validation = validation
    }
}
class pmHistoryData {
     
    var audit_SchduleId  : Int64
    var assetownerId : Int64
    var assettagId : Int64
    var assetuserOwnerId : Int64
    var auditendDate : String!
    var auditid : Int64
    var auditinspectionDate : String!
    var audit_Name : String!
    var audit_ParamsId : Int64
    var audit_Result : String!
    var audit_StartDate : String!
    var audit_Status : String!
    var audited_Params : String!
    init(audit_SchduleId: Int64, assetownerId: Int64, assettagId: Int64, assetuserOwnerId: Int64, auditendDate: String? = nil, auditid: Int64, auditinspectionDate: String? = nil, audit_Name: String? = nil, audit_ParamsId: Int64, audit_Result: String? = nil, audit_StartDate: String? = nil, audit_Status: String? = nil, audited_Params: String? = nil) {
       self.audit_SchduleId = audit_SchduleId
       self.assetownerId = assetownerId
       self.assettagId = assettagId
       self.assetuserOwnerId = assetuserOwnerId
       self.auditendDate = auditendDate
       self.auditid = auditid
       self.auditinspectionDate = auditinspectionDate
       self.audit_Name = audit_Name
       self.audit_ParamsId = audit_ParamsId
       self.audit_Result = audit_Result
       self.audit_StartDate = audit_StartDate
       self.audit_Status = audit_Status
       self.audited_Params = audited_Params
   }
}
class assetListData : Equatable{
    static func == (lhs: assetListData, rhs: assetListData) -> Bool {
        true
    }
    
    var id : Int64
    var name: String
    var assetTag: String
    var location : String
    var warrentyTime : Int64
    var supplier : String
    var purchaseDate : String
    var modalName : String
    var category : String
    var company : String
    var auditValue : String
    var assetStatus : Int64
    var image : String
    var modalId : Int64
    var lastAuditDate : String
    var categoryId : Int64
    init(id : Int64?, name: String?, assetTag: String? , location: String? , warrentyTime: Int64? ,supplier : String? ,purchaseDate : String?,modalName : String? , category : String? , company : String? , auditValue : String? , assetStatus : Int64? ,image : String? , modalId : Int64? , lastAuditDate : String ,categoryId : Int64){
        self.id = id ?? 0
        self.name =  name ?? ""
        self.assetTag = assetTag ?? ""
        self.location = location ?? ""
        self.warrentyTime = warrentyTime ?? 0
        self.supplier = supplier ?? ""
        self.purchaseDate = purchaseDate ?? ""
        self.modalName = modalName ?? ""
        self.category = category ?? ""
        self.company = company ?? ""
        self.auditValue = auditValue ?? ""
        self.assetStatus = assetStatus ?? 0
        self.image = image ?? ""
        self.modalId = modalId ?? 0
        self.lastAuditDate = lastAuditDate
        self.categoryId = categoryId ?? 0
    }
}
