//
//	Causes.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Causes : NSObject, NSCoding{

	var messges : String!
	var apiStatus : Int!
	var assetsData : [CausesAssetsData]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		messges = dictionary["Messges"] as? String
		apiStatus = dictionary["api_status"] as? Int
		assetsData = [CausesAssetsData]()
		if let assetsDataArray = dictionary["assetsData"] as? [[String:Any]]{
			for dic in assetsDataArray{
				let value = CausesAssetsData(fromDictionary: dic)
				assetsData.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if messges != nil{
			dictionary["Messges"] = messges
		}
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		if assetsData != nil{
			var dictionaryElements = [[String:Any]]()
			for assetsDataElement in assetsData {
				dictionaryElements.append(assetsDataElement.toDictionary())
			}
			dictionary["assetsData"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         messges = aDecoder.decodeObject(forKey: "Messges") as? String
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int
         assetsData = aDecoder.decodeObject(forKey :"assetsData") as? [CausesAssetsData]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if messges != nil{
			aCoder.encode(messges, forKey: "Messges")
		}
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}
		if assetsData != nil{
			aCoder.encode(assetsData, forKey: "assetsData")
		}

	}

}