//
//	CausesAssetsData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CausesAssetsData : NSObject, NSCoding{

	var causeName : String!
	var causeQuestion : String!
	var causeType : String!
	var id : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		causeName = dictionary["cause_name"] as? String
		causeQuestion = dictionary["cause_question"] as? String
		causeType = dictionary["cause_type"] as? String
		id = dictionary["id"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if causeName != nil{
			dictionary["cause_name"] = causeName
		}
		if causeQuestion != nil{
			dictionary["cause_question"] = causeQuestion
		}
		if causeType != nil{
			dictionary["cause_type"] = causeType
		}
		if id != nil{
			dictionary["id"] = id
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         causeName = aDecoder.decodeObject(forKey: "cause_name") as? String
         causeQuestion = aDecoder.decodeObject(forKey: "cause_question") as? String
         causeType = aDecoder.decodeObject(forKey: "cause_type") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if causeName != nil{
			aCoder.encode(causeName, forKey: "cause_name")
		}
		if causeQuestion != nil{
			aCoder.encode(causeQuestion, forKey: "cause_question")
		}
		if causeType != nil{
			aCoder.encode(causeType, forKey: "cause_type")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}

	}

}