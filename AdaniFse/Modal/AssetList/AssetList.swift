//
//	AssetList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AssetList : NSObject, NSCoding{

	var allUserAssetAssgainData : [AllUserAssetAssgainData]!
	var apiMessage : String!
	var apiStatus : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		allUserAssetAssgainData = [AllUserAssetAssgainData]()
		if let allUserAssetAssgainDataArray = dictionary["AllUserAssetAssgainData"] as? [[String:Any]]{
			for dic in allUserAssetAssgainDataArray{
				let value = AllUserAssetAssgainData(fromDictionary: dic)
				allUserAssetAssgainData.append(value)
			}
		}
		apiMessage = dictionary["api_message"] as? String
		apiStatus = dictionary["api_status"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if allUserAssetAssgainData != nil{
			var dictionaryElements = [[String:Any]]()
			for allUserAssetAssgainDataElement in allUserAssetAssgainData {
				dictionaryElements.append(allUserAssetAssgainDataElement.toDictionary())
			}
			dictionary["AllUserAssetAssgainData"] = dictionaryElements
		}
		if apiMessage != nil{
			dictionary["api_message"] = apiMessage
		}
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         allUserAssetAssgainData = aDecoder.decodeObject(forKey :"AllUserAssetAssgainData") as? [AllUserAssetAssgainData]
         apiMessage = aDecoder.decodeObject(forKey: "api_message") as? String
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if allUserAssetAssgainData != nil{
			aCoder.encode(allUserAssetAssgainData, forKey: "AllUserAssetAssgainData")
		}
		if apiMessage != nil{
			aCoder.encode(apiMessage, forKey: "api_message")
		}
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}

	}

}