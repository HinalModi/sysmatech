//
//	AllUserAssetAssgainData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AllUserAssetAssgainData : NSObject, NSCoding{

	var snipeitFan4 : AnyObject!
	var snipeitFeatures6 : AnyObject!
	var snipeitIpAddress11 : AnyObject!
	var snipeitMacAddress1 : AnyObject!
	var snipeitMacId10 : AnyObject!
	var snipeitNetworkManagement7 : AnyObject!
	var snipeitNetworkPorts9 : AnyObject!
	var snipeitPassword13 : AnyObject!
	var snipeitPortNumber14 : AnyObject!
	var snipeitPowerOverEthernetPoe3 : AnyObject!
	var snipeitPowerSupply5 : AnyObject!
	var snipeitUplinkPorts2 : AnyObject!
	var snipeitUsername12 : AnyObject!
	var snipeitWarrantyType8 : AnyObject!
	var accepted : AnyObject!
	var archived : Int!
	var assetTag : String!
	var assignedTo : AnyObject!
	var assignedType : AnyObject!
	var auditParamsTransaction : AnyObject!
	var auditParamsValues : String!
	var categoryName : String!
	var checkinCounter : Int!
	var checkoutCounter : Int!
	var companyId : Int!
	var companyName : String!
	var createdAt : String!
	var deletedAt : AnyObject!
	var depreciate : Int!
	var dyamicModelFieldset : AnyObject!
	var expectedCheckin : AnyObject!
	var id : Int!
	var image : String!
	var lastAuditDate : String!
	var lastCheckout : AnyObject!
	var location : String!
	var locationId : Int!
	var modelId : Int!
	var modelName : String!
	var name : String!
	var nextAuditDate : AnyObject!
	var notes : AnyObject!
	var orderNumber : String!
	var physical : Int!
	var pmHistory : [AnyObject]!
	var prevMainOwnerType : String!
	var purchaseCost : AnyObject!
	var purchaseDate : String!
	var requestable : Int!
	var requestsCounter : Int!
	var rtdLocationId : Int!
	var serial : AnyObject!
	var status : String!
	var statusId : Int!
	var supplierId : Int!
	var supplierName : String!
	var updatedAt : String!
	var userId : Int!
	var warrantyMonths : Int!
    var assetStatus : Int!
    var categoryId : Int!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		snipeitFan4 = dictionary["_snipeit_fan_4"] as? AnyObject
		snipeitFeatures6 = dictionary["_snipeit_features_6"] as? AnyObject
		snipeitIpAddress11 = dictionary["_snipeit_ip_address_11"] as? AnyObject
		snipeitMacAddress1 = dictionary["_snipeit_mac_address_1"] as? AnyObject
		snipeitMacId10 = dictionary["_snipeit_mac_id_10"] as? AnyObject
		snipeitNetworkManagement7 = dictionary["_snipeit_network_management_7"] as? AnyObject
		snipeitNetworkPorts9 = dictionary["_snipeit_network_ports_9"] as? AnyObject
		snipeitPassword13 = dictionary["_snipeit_password_13"] as? AnyObject
		snipeitPortNumber14 = dictionary["_snipeit_port_number_14"] as? AnyObject
		snipeitPowerOverEthernetPoe3 = dictionary["_snipeit_power_over_ethernet_poe_3"] as? AnyObject
		snipeitPowerSupply5 = dictionary["_snipeit_power_supply_5"] as? AnyObject
		snipeitUplinkPorts2 = dictionary["_snipeit_uplink_ports_2"] as? AnyObject
		snipeitUsername12 = dictionary["_snipeit_username_12"] as? AnyObject
		snipeitWarrantyType8 = dictionary["_snipeit_warranty_type_8"] as? AnyObject
		accepted = dictionary["accepted"] as? AnyObject
		archived = dictionary["archived"] as? Int
		assetTag = dictionary["asset_tag"] as? String
		assignedTo = dictionary["assigned_to"] as? AnyObject
		assignedType = dictionary["assigned_type"] as? AnyObject
		auditParamsTransaction = dictionary["audit_params_transaction"] as? AnyObject
		auditParamsValues = dictionary["audit_params_values"] as? String
		categoryName = dictionary["category_name"] as? String
		checkinCounter = dictionary["checkin_counter"] as? Int
		checkoutCounter = dictionary["checkout_counter"] as? Int
		companyId = dictionary["company_id"] as? Int
		companyName = dictionary["company_name"] as? String
		createdAt = dictionary["created_at"] as? String
		deletedAt = dictionary["deleted_at"] as? AnyObject
		depreciate = dictionary["depreciate"] as? Int
		dyamicModelFieldset = dictionary["dyamic_model_fieldset"] as? AnyObject
		expectedCheckin = dictionary["expected_checkin"] as? AnyObject
		id = dictionary["id"] as? Int
		image = dictionary["image"] as? String
		lastAuditDate = dictionary["last_audit_date"] as? String
		lastCheckout = dictionary["last_checkout"] as? AnyObject
		location = dictionary["location"] as? String
		locationId = dictionary["location_id"] as? Int
		modelId = dictionary["model_id"] as? Int
		modelName = dictionary["model_name"] as? String
		name = dictionary["name"] as? String
		nextAuditDate = dictionary["next_audit_date"] as? AnyObject
		notes = dictionary["notes"] as? AnyObject
		orderNumber = dictionary["order_number"] as? String
		physical = dictionary["physical"] as? Int
		pmHistory = dictionary["pm_history"] as? [AnyObject]
		prevMainOwnerType = dictionary["prev_main_owner_type"] as? String
		purchaseCost = dictionary["purchase_cost"] as? AnyObject
		purchaseDate = dictionary["purchase_date"] as? String
		requestable = dictionary["requestable"] as? Int
		requestsCounter = dictionary["requests_counter"] as? Int
		rtdLocationId = dictionary["rtd_location_id"] as? Int
		serial = dictionary["serial"] as? AnyObject
		status = dictionary["status"] as? String
		statusId = dictionary["status_id"] as? Int
		supplierId = dictionary["supplier_id"] as? Int
		supplierName = dictionary["supplier_name"] as? String
		updatedAt = dictionary["updated_at"] as? String
		userId = dictionary["user_id"] as? Int
		warrantyMonths = dictionary["warranty_months"] as? Int
        assetStatus = dictionary["asset_status"] as? Int
        categoryId = dictionary["category_id"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if snipeitFan4 != nil{
			dictionary["_snipeit_fan_4"] = snipeitFan4
		}
		if snipeitFeatures6 != nil{
			dictionary["_snipeit_features_6"] = snipeitFeatures6
		}
		if snipeitIpAddress11 != nil{
			dictionary["_snipeit_ip_address_11"] = snipeitIpAddress11
		}
		if snipeitMacAddress1 != nil{
			dictionary["_snipeit_mac_address_1"] = snipeitMacAddress1
		}
		if snipeitMacId10 != nil{
			dictionary["_snipeit_mac_id_10"] = snipeitMacId10
		}
		if snipeitNetworkManagement7 != nil{
			dictionary["_snipeit_network_management_7"] = snipeitNetworkManagement7
		}
		if snipeitNetworkPorts9 != nil{
			dictionary["_snipeit_network_ports_9"] = snipeitNetworkPorts9
		}
		if snipeitPassword13 != nil{
			dictionary["_snipeit_password_13"] = snipeitPassword13
		}
		if snipeitPortNumber14 != nil{
			dictionary["_snipeit_port_number_14"] = snipeitPortNumber14
		}
		if snipeitPowerOverEthernetPoe3 != nil{
			dictionary["_snipeit_power_over_ethernet_poe_3"] = snipeitPowerOverEthernetPoe3
		}
		if snipeitPowerSupply5 != nil{
			dictionary["_snipeit_power_supply_5"] = snipeitPowerSupply5
		}
		if snipeitUplinkPorts2 != nil{
			dictionary["_snipeit_uplink_ports_2"] = snipeitUplinkPorts2
		}
		if snipeitUsername12 != nil{
			dictionary["_snipeit_username_12"] = snipeitUsername12
		}
		if snipeitWarrantyType8 != nil{
			dictionary["_snipeit_warranty_type_8"] = snipeitWarrantyType8
		}
		if accepted != nil{
			dictionary["accepted"] = accepted
		}
		if archived != nil{
			dictionary["archived"] = archived
		}
		if assetTag != nil{
			dictionary["asset_tag"] = assetTag
		}
		if assignedTo != nil{
			dictionary["assigned_to"] = assignedTo
		}
		if assignedType != nil{
			dictionary["assigned_type"] = assignedType
		}
		if auditParamsTransaction != nil{
			dictionary["audit_params_transaction"] = auditParamsTransaction
		}
		if auditParamsValues != nil{
			dictionary["audit_params_values"] = auditParamsValues
		}
		if categoryName != nil{
			dictionary["category_name"] = categoryName
		}
		if checkinCounter != nil{
			dictionary["checkin_counter"] = checkinCounter
		}
		if checkoutCounter != nil{
			dictionary["checkout_counter"] = checkoutCounter
		}
		if companyId != nil{
			dictionary["company_id"] = companyId
		}
		if companyName != nil{
			dictionary["company_name"] = companyName
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if depreciate != nil{
			dictionary["depreciate"] = depreciate
		}
		if dyamicModelFieldset != nil{
			dictionary["dyamic_model_fieldset"] = dyamicModelFieldset
		}
		if expectedCheckin != nil{
			dictionary["expected_checkin"] = expectedCheckin
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if lastAuditDate != nil{
			dictionary["last_audit_date"] = lastAuditDate
		}
		if lastCheckout != nil{
			dictionary["last_checkout"] = lastCheckout
		}
		if location != nil{
			dictionary["location"] = location
		}
		if locationId != nil{
			dictionary["location_id"] = locationId
		}
		if modelId != nil{
			dictionary["model_id"] = modelId
		}
		if modelName != nil{
			dictionary["model_name"] = modelName
		}
		if name != nil{
			dictionary["name"] = name
		}
		if nextAuditDate != nil{
			dictionary["next_audit_date"] = nextAuditDate
		}
		if notes != nil{
			dictionary["notes"] = notes
		}
		if orderNumber != nil{
			dictionary["order_number"] = orderNumber
		}
		if physical != nil{
			dictionary["physical"] = physical
		}
		if pmHistory != nil{
			dictionary["pm_history"] = pmHistory
		}
		if prevMainOwnerType != nil{
			dictionary["prev_main_owner_type"] = prevMainOwnerType
		}
		if purchaseCost != nil{
			dictionary["purchase_cost"] = purchaseCost
		}
		if purchaseDate != nil{
			dictionary["purchase_date"] = purchaseDate
		}
		if requestable != nil{
			dictionary["requestable"] = requestable
		}
		if requestsCounter != nil{
			dictionary["requests_counter"] = requestsCounter
		}
		if rtdLocationId != nil{
			dictionary["rtd_location_id"] = rtdLocationId
		}
		if serial != nil{
			dictionary["serial"] = serial
		}
		if status != nil{
			dictionary["status"] = status
		}
		if statusId != nil{
			dictionary["status_id"] = statusId
		}
		if supplierId != nil{
			dictionary["supplier_id"] = supplierId
		}
		if supplierName != nil{
			dictionary["supplier_name"] = supplierName
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if warrantyMonths != nil{
			dictionary["warranty_months"] = warrantyMonths
		}
        if assetStatus != nil{
            dictionary["asset_status"] = assetStatus
        }
        if categoryId != nil{
            dictionary["category_id"] = categoryId
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         snipeitFan4 = aDecoder.decodeObject(forKey: "_snipeit_fan_4") as? AnyObject
         snipeitFeatures6 = aDecoder.decodeObject(forKey: "_snipeit_features_6") as? AnyObject
         snipeitIpAddress11 = aDecoder.decodeObject(forKey: "_snipeit_ip_address_11") as? AnyObject
         snipeitMacAddress1 = aDecoder.decodeObject(forKey: "_snipeit_mac_address_1") as? AnyObject
         snipeitMacId10 = aDecoder.decodeObject(forKey: "_snipeit_mac_id_10") as? AnyObject
         snipeitNetworkManagement7 = aDecoder.decodeObject(forKey: "_snipeit_network_management_7") as? AnyObject
         snipeitNetworkPorts9 = aDecoder.decodeObject(forKey: "_snipeit_network_ports_9") as? AnyObject
         snipeitPassword13 = aDecoder.decodeObject(forKey: "_snipeit_password_13") as? AnyObject
         snipeitPortNumber14 = aDecoder.decodeObject(forKey: "_snipeit_port_number_14") as? AnyObject
         snipeitPowerOverEthernetPoe3 = aDecoder.decodeObject(forKey: "_snipeit_power_over_ethernet_poe_3") as? AnyObject
         snipeitPowerSupply5 = aDecoder.decodeObject(forKey: "_snipeit_power_supply_5") as? AnyObject
         snipeitUplinkPorts2 = aDecoder.decodeObject(forKey: "_snipeit_uplink_ports_2") as? AnyObject
         snipeitUsername12 = aDecoder.decodeObject(forKey: "_snipeit_username_12") as? AnyObject
         snipeitWarrantyType8 = aDecoder.decodeObject(forKey: "_snipeit_warranty_type_8") as? AnyObject
         accepted = aDecoder.decodeObject(forKey: "accepted") as? AnyObject
         archived = aDecoder.decodeObject(forKey: "archived") as? Int
         assetTag = aDecoder.decodeObject(forKey: "asset_tag") as? String
         assignedTo = aDecoder.decodeObject(forKey: "assigned_to") as? AnyObject
         assignedType = aDecoder.decodeObject(forKey: "assigned_type") as? AnyObject
         auditParamsTransaction = aDecoder.decodeObject(forKey: "audit_params_transaction") as? AnyObject
         auditParamsValues = aDecoder.decodeObject(forKey: "audit_params_values") as? String
         categoryName = aDecoder.decodeObject(forKey: "category_name") as? String
         checkinCounter = aDecoder.decodeObject(forKey: "checkin_counter") as? Int
         checkoutCounter = aDecoder.decodeObject(forKey: "checkout_counter") as? Int
         companyId = aDecoder.decodeObject(forKey: "company_id") as? Int
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? AnyObject
         depreciate = aDecoder.decodeObject(forKey: "depreciate") as? Int
         dyamicModelFieldset = aDecoder.decodeObject(forKey: "dyamic_model_fieldset") as? AnyObject
         expectedCheckin = aDecoder.decodeObject(forKey: "expected_checkin") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         lastAuditDate = aDecoder.decodeObject(forKey: "last_audit_date") as? String
         lastCheckout = aDecoder.decodeObject(forKey: "last_checkout") as? AnyObject
         location = aDecoder.decodeObject(forKey: "location") as? String
         locationId = aDecoder.decodeObject(forKey: "location_id") as? Int
         modelId = aDecoder.decodeObject(forKey: "model_id") as? Int
         modelName = aDecoder.decodeObject(forKey: "model_name") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         nextAuditDate = aDecoder.decodeObject(forKey: "next_audit_date") as? AnyObject
         notes = aDecoder.decodeObject(forKey: "notes") as? AnyObject
         orderNumber = aDecoder.decodeObject(forKey: "order_number") as? String
         physical = aDecoder.decodeObject(forKey: "physical") as? Int
         pmHistory = aDecoder.decodeObject(forKey: "pm_history") as? [AnyObject]
         prevMainOwnerType = aDecoder.decodeObject(forKey: "prev_main_owner_type") as? String
         purchaseCost = aDecoder.decodeObject(forKey: "purchase_cost") as? AnyObject
         purchaseDate = aDecoder.decodeObject(forKey: "purchase_date") as? String
         requestable = aDecoder.decodeObject(forKey: "requestable") as? Int
         requestsCounter = aDecoder.decodeObject(forKey: "requests_counter") as? Int
         rtdLocationId = aDecoder.decodeObject(forKey: "rtd_location_id") as? Int
         serial = aDecoder.decodeObject(forKey: "serial") as? AnyObject
         status = aDecoder.decodeObject(forKey: "status") as? String
         statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
         supplierId = aDecoder.decodeObject(forKey: "supplier_id") as? Int
         supplierName = aDecoder.decodeObject(forKey: "supplier_name") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int
         warrantyMonths = aDecoder.decodeObject(forKey: "warranty_months") as? Int
        assetStatus = aDecoder.decodeObject(forKey: "asset_status") as? Int
        categoryId = aDecoder.decodeObject(forKey: "category_id") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if snipeitFan4 != nil{
			aCoder.encode(snipeitFan4, forKey: "_snipeit_fan_4")
		}
		if snipeitFeatures6 != nil{
			aCoder.encode(snipeitFeatures6, forKey: "_snipeit_features_6")
		}
		if snipeitIpAddress11 != nil{
			aCoder.encode(snipeitIpAddress11, forKey: "_snipeit_ip_address_11")
		}
		if snipeitMacAddress1 != nil{
			aCoder.encode(snipeitMacAddress1, forKey: "_snipeit_mac_address_1")
		}
		if snipeitMacId10 != nil{
			aCoder.encode(snipeitMacId10, forKey: "_snipeit_mac_id_10")
		}
		if snipeitNetworkManagement7 != nil{
			aCoder.encode(snipeitNetworkManagement7, forKey: "_snipeit_network_management_7")
		}
		if snipeitNetworkPorts9 != nil{
			aCoder.encode(snipeitNetworkPorts9, forKey: "_snipeit_network_ports_9")
		}
		if snipeitPassword13 != nil{
			aCoder.encode(snipeitPassword13, forKey: "_snipeit_password_13")
		}
		if snipeitPortNumber14 != nil{
			aCoder.encode(snipeitPortNumber14, forKey: "_snipeit_port_number_14")
		}
		if snipeitPowerOverEthernetPoe3 != nil{
			aCoder.encode(snipeitPowerOverEthernetPoe3, forKey: "_snipeit_power_over_ethernet_poe_3")
		}
		if snipeitPowerSupply5 != nil{
			aCoder.encode(snipeitPowerSupply5, forKey: "_snipeit_power_supply_5")
		}
		if snipeitUplinkPorts2 != nil{
			aCoder.encode(snipeitUplinkPorts2, forKey: "_snipeit_uplink_ports_2")
		}
		if snipeitUsername12 != nil{
			aCoder.encode(snipeitUsername12, forKey: "_snipeit_username_12")
		}
		if snipeitWarrantyType8 != nil{
			aCoder.encode(snipeitWarrantyType8, forKey: "_snipeit_warranty_type_8")
		}
		if accepted != nil{
			aCoder.encode(accepted, forKey: "accepted")
		}
		if archived != nil{
			aCoder.encode(archived, forKey: "archived")
		}
		if assetTag != nil{
			aCoder.encode(assetTag, forKey: "asset_tag")
		}
		if assignedTo != nil{
			aCoder.encode(assignedTo, forKey: "assigned_to")
		}
		if assignedType != nil{
			aCoder.encode(assignedType, forKey: "assigned_type")
		}
		if auditParamsTransaction != nil{
			aCoder.encode(auditParamsTransaction, forKey: "audit_params_transaction")
		}
		if auditParamsValues != nil{
			aCoder.encode(auditParamsValues, forKey: "audit_params_values")
		}
		if categoryName != nil{
			aCoder.encode(categoryName, forKey: "category_name")
		}
		if checkinCounter != nil{
			aCoder.encode(checkinCounter, forKey: "checkin_counter")
		}
		if checkoutCounter != nil{
			aCoder.encode(checkoutCounter, forKey: "checkout_counter")
		}
		if companyId != nil{
			aCoder.encode(companyId, forKey: "company_id")
		}
		if companyName != nil{
			aCoder.encode(companyName, forKey: "company_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if depreciate != nil{
			aCoder.encode(depreciate, forKey: "depreciate")
		}
		if dyamicModelFieldset != nil{
			aCoder.encode(dyamicModelFieldset, forKey: "dyamic_model_fieldset")
		}
		if expectedCheckin != nil{
			aCoder.encode(expectedCheckin, forKey: "expected_checkin")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if lastAuditDate != nil{
			aCoder.encode(lastAuditDate, forKey: "last_audit_date")
		}
		if lastCheckout != nil{
			aCoder.encode(lastCheckout, forKey: "last_checkout")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if locationId != nil{
			aCoder.encode(locationId, forKey: "location_id")
		}
		if modelId != nil{
			aCoder.encode(modelId, forKey: "model_id")
		}
		if modelName != nil{
			aCoder.encode(modelName, forKey: "model_name")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if nextAuditDate != nil{
			aCoder.encode(nextAuditDate, forKey: "next_audit_date")
		}
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if orderNumber != nil{
			aCoder.encode(orderNumber, forKey: "order_number")
		}
		if physical != nil{
			aCoder.encode(physical, forKey: "physical")
		}
		if pmHistory != nil{
			aCoder.encode(pmHistory, forKey: "pm_history")
		}
		if prevMainOwnerType != nil{
			aCoder.encode(prevMainOwnerType, forKey: "prev_main_owner_type")
		}
		if purchaseCost != nil{
			aCoder.encode(purchaseCost, forKey: "purchase_cost")
		}
		if purchaseDate != nil{
			aCoder.encode(purchaseDate, forKey: "purchase_date")
		}
		if requestable != nil{
			aCoder.encode(requestable, forKey: "requestable")
		}
		if requestsCounter != nil{
			aCoder.encode(requestsCounter, forKey: "requests_counter")
		}
		if rtdLocationId != nil{
			aCoder.encode(rtdLocationId, forKey: "rtd_location_id")
		}
		if serial != nil{
			aCoder.encode(serial, forKey: "serial")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if statusId != nil{
			aCoder.encode(statusId, forKey: "status_id")
		}
		if supplierId != nil{
			aCoder.encode(supplierId, forKey: "supplier_id")
		}
		if supplierName != nil{
			aCoder.encode(supplierName, forKey: "supplier_name")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if warrantyMonths != nil{
			aCoder.encode(warrantyMonths, forKey: "warranty_months")
		}
        if assetStatus != nil{
            aCoder.encode(assetStatus, forKey: "asset_status")
        }
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "category_id")
        }
        
    }
    
}
