//
//	AccessoryRow.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class AccessoryRow : NSObject, NSCoding{

	var availableActions : AccessoryAvailableAction!
	var category : AnyObject!
	var company : AccessoryCompany!
	var createdAt : AccessoryCreatedAt!
	var id : Int!
	var image : AnyObject!
	var location : AccessoryCompany!
	var manufacturer : AccessoryCompany!
	var minQty : Int!
	var modelNumber : String!
	var name : String!
	var notes : AnyObject!
	var orderNumber : String!
	var purchaseCost : String!
	var purchaseDate : AccessoryPurchaseDate!
	var qty : Int!
	var remainingQty : Int!
	var supplier : AccessoryCompany!
	var updatedAt : AccessoryCreatedAt!
	var userCanCheckout : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let availableActionsData = dictionary["available_actions"] as? [String:Any]{
			availableActions = AccessoryAvailableAction(fromDictionary: availableActionsData)
		}
		category = dictionary["category"] as? AnyObject
		if let companyData = dictionary["company"] as? [String:Any]{
			company = AccessoryCompany(fromDictionary: companyData)
		}
		if let createdAtData = dictionary["created_at"] as? [String:Any]{
			createdAt = AccessoryCreatedAt(fromDictionary: createdAtData)
		}
		id = dictionary["id"] as? Int
		image = dictionary["image"] as? AnyObject
		if let locationData = dictionary["location"] as? [String:Any]{
			location = AccessoryCompany(fromDictionary: locationData)
		}
		if let manufacturerData = dictionary["manufacturer"] as? [String:Any]{
			manufacturer = AccessoryCompany(fromDictionary: manufacturerData)
		}
		minQty = dictionary["min_qty"] as? Int
		modelNumber = dictionary["model_number"] as? String
		name = dictionary["name"] as? String
		notes = dictionary["notes"] as? AnyObject
		orderNumber = dictionary["order_number"] as? String
		purchaseCost = dictionary["purchase_cost"] as? String
		if let purchaseDateData = dictionary["purchase_date"] as? [String:Any]{
			purchaseDate = AccessoryPurchaseDate(fromDictionary: purchaseDateData)
		}
		qty = dictionary["qty"] as? Int
		remainingQty = dictionary["remaining_qty"] as? Int
		if let supplierData = dictionary["supplier"] as? [String:Any]{
			supplier = AccessoryCompany(fromDictionary: supplierData)
		}
		if let updatedAtData = dictionary["updated_at"] as? [String:Any]{
			updatedAt = AccessoryCreatedAt(fromDictionary: updatedAtData)
		}
		userCanCheckout = dictionary["user_can_checkout"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if availableActions != nil{
			dictionary["available_actions"] = availableActions.toDictionary()
		}
		if category != nil{
			dictionary["category"] = category
		}
		if company != nil{
			dictionary["company"] = company.toDictionary()
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt.toDictionary()
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if location != nil{
			dictionary["location"] = location.toDictionary()
		}
		if manufacturer != nil{
			dictionary["manufacturer"] = manufacturer.toDictionary()
		}
		if minQty != nil{
			dictionary["min_qty"] = minQty
		}
		if modelNumber != nil{
			dictionary["model_number"] = modelNumber
		}
		if name != nil{
			dictionary["name"] = name
		}
		if notes != nil{
			dictionary["notes"] = notes
		}
		if orderNumber != nil{
			dictionary["order_number"] = orderNumber
		}
		if purchaseCost != nil{
			dictionary["purchase_cost"] = purchaseCost
		}
		if purchaseDate != nil{
			dictionary["purchase_date"] = purchaseDate.toDictionary()
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if remainingQty != nil{
			dictionary["remaining_qty"] = remainingQty
		}
		if supplier != nil{
			dictionary["supplier"] = supplier.toDictionary()
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt.toDictionary()
		}
		if userCanCheckout != nil{
			dictionary["user_can_checkout"] = userCanCheckout
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         availableActions = aDecoder.decodeObject(forKey: "available_actions") as? AccessoryAvailableAction
         category = aDecoder.decodeObject(forKey: "category") as? AnyObject
         company = aDecoder.decodeObject(forKey: "company") as? AccessoryCompany
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? AccessoryCreatedAt
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? AnyObject
         location = aDecoder.decodeObject(forKey: "location") as? AccessoryCompany
         manufacturer = aDecoder.decodeObject(forKey: "manufacturer") as? AccessoryCompany
         minQty = aDecoder.decodeObject(forKey: "min_qty") as? Int
         modelNumber = aDecoder.decodeObject(forKey: "model_number") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         notes = aDecoder.decodeObject(forKey: "notes") as? AnyObject
         orderNumber = aDecoder.decodeObject(forKey: "order_number") as? String
         purchaseCost = aDecoder.decodeObject(forKey: "purchase_cost") as? String
         purchaseDate = aDecoder.decodeObject(forKey: "purchase_date") as? AccessoryPurchaseDate
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         remainingQty = aDecoder.decodeObject(forKey: "remaining_qty") as? Int
         supplier = aDecoder.decodeObject(forKey: "supplier") as? AccessoryCompany
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? AccessoryCreatedAt
         userCanCheckout = aDecoder.decodeObject(forKey: "user_can_checkout") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if availableActions != nil{
			aCoder.encode(availableActions, forKey: "available_actions")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if company != nil{
			aCoder.encode(company, forKey: "company")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if manufacturer != nil{
			aCoder.encode(manufacturer, forKey: "manufacturer")
		}
		if minQty != nil{
			aCoder.encode(minQty, forKey: "min_qty")
		}
		if modelNumber != nil{
			aCoder.encode(modelNumber, forKey: "model_number")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if orderNumber != nil{
			aCoder.encode(orderNumber, forKey: "order_number")
		}
		if purchaseCost != nil{
			aCoder.encode(purchaseCost, forKey: "purchase_cost")
		}
		if purchaseDate != nil{
			aCoder.encode(purchaseDate, forKey: "purchase_date")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if remainingQty != nil{
			aCoder.encode(remainingQty, forKey: "remaining_qty")
		}
		if supplier != nil{
			aCoder.encode(supplier, forKey: "supplier")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userCanCheckout != nil{
			aCoder.encode(userCanCheckout, forKey: "user_can_checkout")
		}

	}

}