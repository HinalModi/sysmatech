//
//	InspDetail .swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class InspDetail : NSObject, NSCoding{

	var apiMessage : String!
	var apiStatus : Int!
	var auditScheduleId : [InspDetailAuditScheduleId]!
	var statsCode : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apiMessage = dictionary["api_message"] as? String
		apiStatus = dictionary["api_status"] as? Int
		auditScheduleId = [InspDetailAuditScheduleId]()
		if let auditScheduleIdArray = dictionary["audit_schedule_id"] as? [[String:Any]]{
			for dic in auditScheduleIdArray{
				let value = InspDetailAuditScheduleId(fromDictionary: dic)
				auditScheduleId.append(value)
			}
		}
		statsCode = dictionary["stats_code"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apiMessage != nil{
			dictionary["api_message"] = apiMessage
		}
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		if auditScheduleId != nil{
			var dictionaryElements = [[String:Any]]()
			for auditScheduleIdElement in auditScheduleId {
				dictionaryElements.append(auditScheduleIdElement.toDictionary())
			}
			dictionary["audit_schedule_id"] = dictionaryElements
		}
		if statsCode != nil{
			dictionary["stats_code"] = statsCode
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiMessage = aDecoder.decodeObject(forKey: "api_message") as? String
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int
         auditScheduleId = aDecoder.decodeObject(forKey :"audit_schedule_id") as? [InspDetailAuditScheduleId]
         statsCode = aDecoder.decodeObject(forKey: "stats_code") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if apiMessage != nil{
			aCoder.encode(apiMessage, forKey: "api_message")
		}
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}
		if auditScheduleId != nil{
			aCoder.encode(auditScheduleId, forKey: "audit_schedule_id")
		}
		if statsCode != nil{
			aCoder.encode(statsCode, forKey: "stats_code")
		}

	}

}
