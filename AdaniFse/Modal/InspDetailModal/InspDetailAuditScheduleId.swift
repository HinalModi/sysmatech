//
//	InspDetailAuditScheduleId.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class InspDetailAuditScheduleId : NSObject, NSCoding{

	var auditAssetImage : String!
	var auditGpsLocation : String!
	var auditParamName : String!
	var auditParamNameValId : Int!
	var auditParamResult : String!
	var auditParamValue : String!
	var auditParamsTransactionId : Int!
	var auditScheduleId : Int!
	var auditUpdateDate : String!
	var id : Int!
	var inspectionComment : AnyObject!
	var inspectionImg : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		auditAssetImage = dictionary["audit_asset_image"] as? String
		auditGpsLocation = dictionary["audit_gps_location"] as? String
		auditParamName = dictionary["audit_param_name"] as? String
		auditParamNameValId = dictionary["audit_param_name_val_id"] as? Int
		auditParamResult = dictionary["audit_param_result"] as? String
		auditParamValue = dictionary["audit_param_value"] as? String
		auditParamsTransactionId = dictionary["audit_params_transaction_id"] as? Int
		auditScheduleId = dictionary["audit_schedule_id"] as? Int
		auditUpdateDate = dictionary["audit_update_date"] as? String
		id = dictionary["id"] as? Int
		inspectionComment = dictionary["inspection_comment"] as? AnyObject
		inspectionImg = dictionary["inspection_img"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if auditAssetImage != nil{
			dictionary["audit_asset_image"] = auditAssetImage
		}
		if auditGpsLocation != nil{
			dictionary["audit_gps_location"] = auditGpsLocation
		}
		if auditParamName != nil{
			dictionary["audit_param_name"] = auditParamName
		}
		if auditParamNameValId != nil{
			dictionary["audit_param_name_val_id"] = auditParamNameValId
		}
		if auditParamResult != nil{
			dictionary["audit_param_result"] = auditParamResult
		}
		if auditParamValue != nil{
			dictionary["audit_param_value"] = auditParamValue
		}
		if auditParamsTransactionId != nil{
			dictionary["audit_params_transaction_id"] = auditParamsTransactionId
		}
		if auditScheduleId != nil{
			dictionary["audit_schedule_id"] = auditScheduleId
		}
		if auditUpdateDate != nil{
			dictionary["audit_update_date"] = auditUpdateDate
		}
		if id != nil{
			dictionary["id"] = id
		}
		if inspectionComment != nil{
			dictionary["inspection_comment"] = inspectionComment
		}
		if inspectionImg != nil{
			dictionary["inspection_img"] = inspectionImg
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         auditAssetImage = aDecoder.decodeObject(forKey: "audit_asset_image") as? String
         auditGpsLocation = aDecoder.decodeObject(forKey: "audit_gps_location") as? String
         auditParamName = aDecoder.decodeObject(forKey: "audit_param_name") as? String
         auditParamNameValId = aDecoder.decodeObject(forKey: "audit_param_name_val_id") as? Int
         auditParamResult = aDecoder.decodeObject(forKey: "audit_param_result") as? String
         auditParamValue = aDecoder.decodeObject(forKey: "audit_param_value") as? String
         auditParamsTransactionId = aDecoder.decodeObject(forKey: "audit_params_transaction_id") as? Int
         auditScheduleId = aDecoder.decodeObject(forKey: "audit_schedule_id") as? Int
         auditUpdateDate = aDecoder.decodeObject(forKey: "audit_update_date") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         inspectionComment = aDecoder.decodeObject(forKey: "inspection_comment") as? AnyObject
         inspectionImg = aDecoder.decodeObject(forKey: "inspection_img") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if auditAssetImage != nil{
			aCoder.encode(auditAssetImage, forKey: "audit_asset_image")
		}
		if auditGpsLocation != nil{
			aCoder.encode(auditGpsLocation, forKey: "audit_gps_location")
		}
		if auditParamName != nil{
			aCoder.encode(auditParamName, forKey: "audit_param_name")
		}
		if auditParamNameValId != nil{
			aCoder.encode(auditParamNameValId, forKey: "audit_param_name_val_id")
		}
		if auditParamResult != nil{
			aCoder.encode(auditParamResult, forKey: "audit_param_result")
		}
		if auditParamValue != nil{
			aCoder.encode(auditParamValue, forKey: "audit_param_value")
		}
		if auditParamsTransactionId != nil{
			aCoder.encode(auditParamsTransactionId, forKey: "audit_params_transaction_id")
		}
		if auditScheduleId != nil{
			aCoder.encode(auditScheduleId, forKey: "audit_schedule_id")
		}
		if auditUpdateDate != nil{
			aCoder.encode(auditUpdateDate, forKey: "audit_update_date")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if inspectionComment != nil{
			aCoder.encode(inspectionComment, forKey: "inspection_comment")
		}
		if inspectionImg != nil{
			aCoder.encode(inspectionImg, forKey: "inspection_img")
		}

	}

}