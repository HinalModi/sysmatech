//
//    UserData.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserData : NSObject, NSCoding{

    var activated : Bool!
    var activatedAt : AnyObject!
    var activationCode : AnyObject!
    var address : AnyObject!
    var availableActions : UserAvailableAction!
    var avatar : AnyObject!
    var city : AnyObject!
    var companyId : Int!
    var country : AnyObject!
    var createdAt : String!
    var deletedAt : AnyObject!
    var departmentId : AnyObject!
    var email : String!
    var employeeNum : AnyObject!
    var firstName : String!
    var gravatar : AnyObject!
    var groups : [UserGroup]!
    var id : Int!
    var jobtitle : AnyObject!
    var lastLogin : String!
    var lastName : AnyObject!
    var ldapImport : Int!
    var locale : AnyObject!
    var locationId : AnyObject!
    var managerId : AnyObject!
    var notes : AnyObject!
    var phone : String!
    var showInList : Int!
    var skin : AnyObject!
    var state : AnyObject!
    var token : AnyObject!
    var twoFactorEnrolled : Int!
    var twoFactorOptin : Int!
    var twoFactorSecret : AnyObject!
    var updatedAt : String!
    var username : String!
    var website : AnyObject!
    var zip : AnyObject!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        activated = dictionary["activated"] as? Bool
        activatedAt = dictionary["activated_at"] as? AnyObject
        activationCode = dictionary["activation_code"] as? AnyObject
        address = dictionary["address"] as? AnyObject
        if let availableActionsData = dictionary["available_actions"] as? [String:Any]{
            availableActions = UserAvailableAction(fromDictionary: availableActionsData)
        }
        avatar = dictionary["avatar"] as? AnyObject
        city = dictionary["city"] as? AnyObject
        companyId = dictionary["company_id"] as? Int
        country = dictionary["country"] as? AnyObject
        createdAt = dictionary["created_at"] as? String
        deletedAt = dictionary["deleted_at"] as? AnyObject
        departmentId = dictionary["department_id"] as? AnyObject
        email = dictionary["email"] as? String
        employeeNum = dictionary["employee_num"] as? AnyObject
        firstName = dictionary["first_name"] as? String
        gravatar = dictionary["gravatar"] as? AnyObject
        groups = [UserGroup]()
        if let groupsArray = dictionary["groups"] as? [[String:Any]]{
            for dic in groupsArray{
                let value = UserGroup(fromDictionary: dic)
                groups.append(value)
            }
        }
        id = dictionary["id"] as? Int
        jobtitle = dictionary["jobtitle"] as? AnyObject
        lastLogin = dictionary["last_login"] as? String
        lastName = dictionary["last_name"] as? AnyObject
        ldapImport = dictionary["ldap_import"] as? Int
        locale = dictionary["locale"] as? AnyObject
        locationId = dictionary["location_id"] as? AnyObject
        managerId = dictionary["manager_id"] as? AnyObject
        notes = dictionary["notes"] as? AnyObject
        phone = dictionary["phone"] as? String
        showInList = dictionary["show_in_list"] as? Int
        skin = dictionary["skin"] as? AnyObject
        state = dictionary["state"] as? AnyObject
        token = dictionary["token"] as? AnyObject
        twoFactorEnrolled = dictionary["two_factor_enrolled"] as? Int
        twoFactorOptin = dictionary["two_factor_optin"] as? Int
        twoFactorSecret = dictionary["two_factor_secret"] as? AnyObject
        updatedAt = dictionary["updated_at"] as? String
        username = dictionary["username"] as? String
        website = dictionary["website"] as? AnyObject
        zip = dictionary["zip"] as? AnyObject
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if activated != nil{
            dictionary["activated"] = activated
        }
        if activatedAt != nil{
            dictionary["activated_at"] = activatedAt
        }
        if activationCode != nil{
            dictionary["activation_code"] = activationCode
        }
        if address != nil{
            dictionary["address"] = address
        }
        if availableActions != nil{
            dictionary["available_actions"] = availableActions.toDictionary()
        }
        if avatar != nil{
            dictionary["avatar"] = avatar
        }
        if city != nil{
            dictionary["city"] = city
        }
        if companyId != nil{
            dictionary["company_id"] = companyId
        }
        if country != nil{
            dictionary["country"] = country
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if deletedAt != nil{
            dictionary["deleted_at"] = deletedAt
        }
        if departmentId != nil{
            dictionary["department_id"] = departmentId
        }
        if email != nil{
            dictionary["email"] = email
        }
        if employeeNum != nil{
            dictionary["employee_num"] = employeeNum
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if gravatar != nil{
            dictionary["gravatar"] = gravatar
        }
        if groups != nil{
            var dictionaryElements = [[String:Any]]()
            for groupsElement in groups {
                dictionaryElements.append(groupsElement.toDictionary())
            }
            dictionary["groups"] = dictionaryElements
        }
        if id != nil{
            dictionary["id"] = id
        }
        if jobtitle != nil{
            dictionary["jobtitle"] = jobtitle
        }
        if lastLogin != nil{
            dictionary["last_login"] = lastLogin
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if ldapImport != nil{
            dictionary["ldap_import"] = ldapImport
        }
        if locale != nil{
            dictionary["locale"] = locale
        }
        if locationId != nil{
            dictionary["location_id"] = locationId
        }
        if managerId != nil{
            dictionary["manager_id"] = managerId
        }
        if notes != nil{
            dictionary["notes"] = notes
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if showInList != nil{
            dictionary["show_in_list"] = showInList
        }
        if skin != nil{
            dictionary["skin"] = skin
        }
        if state != nil{
            dictionary["state"] = state
        }
        if token != nil{
            dictionary["token"] = token
        }
        if twoFactorEnrolled != nil{
            dictionary["two_factor_enrolled"] = twoFactorEnrolled
        }
        if twoFactorOptin != nil{
            dictionary["two_factor_optin"] = twoFactorOptin
        }
        if twoFactorSecret != nil{
            dictionary["two_factor_secret"] = twoFactorSecret
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if username != nil{
            dictionary["username"] = username
        }
        if website != nil{
            dictionary["website"] = website
        }
        if zip != nil{
            dictionary["zip"] = zip
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         activated = aDecoder.decodeObject(forKey: "activated") as? Bool
         activatedAt = aDecoder.decodeObject(forKey: "activated_at") as? AnyObject
         activationCode = aDecoder.decodeObject(forKey: "activation_code") as? AnyObject
         address = aDecoder.decodeObject(forKey: "address") as? AnyObject
         availableActions = aDecoder.decodeObject(forKey: "available_actions") as? UserAvailableAction
         avatar = aDecoder.decodeObject(forKey: "avatar") as? AnyObject
         city = aDecoder.decodeObject(forKey: "city") as? AnyObject
         companyId = aDecoder.decodeObject(forKey: "company_id") as? Int
         country = aDecoder.decodeObject(forKey: "country") as? AnyObject
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? AnyObject
         departmentId = aDecoder.decodeObject(forKey: "department_id") as? AnyObject
         email = aDecoder.decodeObject(forKey: "email") as? String
         employeeNum = aDecoder.decodeObject(forKey: "employee_num") as? AnyObject
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         gravatar = aDecoder.decodeObject(forKey: "gravatar") as? AnyObject
         groups = aDecoder.decodeObject(forKey :"groups") as? [UserGroup]
         id = aDecoder.decodeObject(forKey: "id") as? Int
         jobtitle = aDecoder.decodeObject(forKey: "jobtitle") as? AnyObject
         lastLogin = aDecoder.decodeObject(forKey: "last_login") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? AnyObject
         ldapImport = aDecoder.decodeObject(forKey: "ldap_import") as? Int
         locale = aDecoder.decodeObject(forKey: "locale") as? AnyObject
         locationId = aDecoder.decodeObject(forKey: "location_id") as? AnyObject
         managerId = aDecoder.decodeObject(forKey: "manager_id") as? AnyObject
         notes = aDecoder.decodeObject(forKey: "notes") as? AnyObject
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         showInList = aDecoder.decodeObject(forKey: "show_in_list") as? Int
         skin = aDecoder.decodeObject(forKey: "skin") as? AnyObject
         state = aDecoder.decodeObject(forKey: "state") as? AnyObject
         token = aDecoder.decodeObject(forKey: "token") as? AnyObject
         twoFactorEnrolled = aDecoder.decodeObject(forKey: "two_factor_enrolled") as? Int
         twoFactorOptin = aDecoder.decodeObject(forKey: "two_factor_optin") as? Int
         twoFactorSecret = aDecoder.decodeObject(forKey: "two_factor_secret") as? AnyObject
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String
         website = aDecoder.decodeObject(forKey: "website") as? AnyObject
         zip = aDecoder.decodeObject(forKey: "zip") as? AnyObject

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if activated != nil{
            aCoder.encode(activated, forKey: "activated")
        }
        if activatedAt != nil{
            aCoder.encode(activatedAt, forKey: "activated_at")
        }
        if activationCode != nil{
            aCoder.encode(activationCode, forKey: "activation_code")
        }
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if availableActions != nil{
            aCoder.encode(availableActions, forKey: "available_actions")
        }
        if avatar != nil{
            aCoder.encode(avatar, forKey: "avatar")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if companyId != nil{
            aCoder.encode(companyId, forKey: "company_id")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if deletedAt != nil{
            aCoder.encode(deletedAt, forKey: "deleted_at")
        }
        if departmentId != nil{
            aCoder.encode(departmentId, forKey: "department_id")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if employeeNum != nil{
            aCoder.encode(employeeNum, forKey: "employee_num")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if gravatar != nil{
            aCoder.encode(gravatar, forKey: "gravatar")
        }
        if groups != nil{
            aCoder.encode(groups, forKey: "groups")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if jobtitle != nil{
            aCoder.encode(jobtitle, forKey: "jobtitle")
        }
        if lastLogin != nil{
            aCoder.encode(lastLogin, forKey: "last_login")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if ldapImport != nil{
            aCoder.encode(ldapImport, forKey: "ldap_import")
        }
        if locale != nil{
            aCoder.encode(locale, forKey: "locale")
        }
        if locationId != nil{
            aCoder.encode(locationId, forKey: "location_id")
        }
        if managerId != nil{
            aCoder.encode(managerId, forKey: "manager_id")
        }
        if notes != nil{
            aCoder.encode(notes, forKey: "notes")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if showInList != nil{
            aCoder.encode(showInList, forKey: "show_in_list")
        }
        if skin != nil{
            aCoder.encode(skin, forKey: "skin")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if twoFactorEnrolled != nil{
            aCoder.encode(twoFactorEnrolled, forKey: "two_factor_enrolled")
        }
        if twoFactorOptin != nil{
            aCoder.encode(twoFactorOptin, forKey: "two_factor_optin")
        }
        if twoFactorSecret != nil{
            aCoder.encode(twoFactorSecret, forKey: "two_factor_secret")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
        if website != nil{
            aCoder.encode(website, forKey: "website")
        }
        if zip != nil{
            aCoder.encode(zip, forKey: "zip")
        }

    }

}
