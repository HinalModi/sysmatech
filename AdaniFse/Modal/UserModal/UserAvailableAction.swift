//
//	UserAvailableAction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserAvailableAction : NSObject, NSCoding{

	var changeParameterAudio : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		changeParameterAudio = dictionary["change_parameter_audio"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if changeParameterAudio != nil{
			dictionary["change_parameter_audio"] = changeParameterAudio
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         changeParameterAudio = aDecoder.decodeObject(forKey: "change_parameter_audio") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if changeParameterAudio != nil{
			aCoder.encode(changeParameterAudio, forKey: "change_parameter_audio")
		}

	}

}