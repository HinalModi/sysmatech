//
//	SelectCompany.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SelectCompany : NSObject, NSCoding{

	var apiMessage : String!
	var apiStatus : Int!
	var data : [SelectCompanyData]!
	var error : [AnyObject]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apiMessage = dictionary["api_message"] as? String
		apiStatus = dictionary["api_status"] as? Int
		data = [SelectCompanyData]()
		if let dataArray = dictionary["data"] as? [[String:Any]]{
			for dic in dataArray{
				let value = SelectCompanyData(fromDictionary: dic)
				data.append(value)
			}
		}
		error = dictionary["error"] as? [AnyObject]
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apiMessage != nil{
			dictionary["api_message"] = apiMessage
		}
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		if data != nil{
			var dictionaryElements = [[String:Any]]()
			for dataElement in data {
				dictionaryElements.append(dataElement.toDictionary())
			}
			dictionary["data"] = dictionaryElements
		}
		if error != nil{
			dictionary["error"] = error
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiMessage = aDecoder.decodeObject(forKey: "api_message") as? String
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int
         data = aDecoder.decodeObject(forKey :"data") as? [SelectCompanyData]
         error = aDecoder.decodeObject(forKey: "error") as? [AnyObject]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if apiMessage != nil{
			aCoder.encode(apiMessage, forKey: "api_message")
		}
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if error != nil{
			aCoder.encode(error, forKey: "error")
		}

	}

}