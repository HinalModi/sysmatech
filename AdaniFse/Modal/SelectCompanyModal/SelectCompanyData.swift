//
//	SelectCompanyData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class SelectCompanyData : NSObject, NSCoding{

	var companyId : String!
	var companyName : String!
	var domainName : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		companyId = dictionary["company_id"] as? String
		companyName = dictionary["company_name"] as? String
		domainName = dictionary["domain_name"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if companyId != nil{
			dictionary["company_id"] = companyId
		}
		if companyName != nil{
			dictionary["company_name"] = companyName
		}
		if domainName != nil{
			dictionary["domain_name"] = domainName
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         companyId = aDecoder.decodeObject(forKey: "company_id") as? String
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         domainName = aDecoder.decodeObject(forKey: "domain_name") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if companyId != nil{
			aCoder.encode(companyId, forKey: "company_id")
		}
		if companyName != nil{
			aCoder.encode(companyName, forKey: "company_name")
		}
		if domainName != nil{
			aCoder.encode(domainName, forKey: "domain_name")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}