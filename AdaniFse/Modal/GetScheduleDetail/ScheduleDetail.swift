//
//	ScheduleDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ScheduleDetail : NSObject, NSCoding{

	var apiMessage : String!
	var apiStatus : Int!
	var statsCode : Int!
	var totalAssetScheduledata : TotalAssetScheduledata!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apiMessage = dictionary["api_message"] as? String
		apiStatus = dictionary["api_status"] as? Int
		statsCode = dictionary["stats_code"] as? Int
		if let totalAssetScheduledataData = dictionary["totalAssetScheduledata"] as? [String:Any]{
			totalAssetScheduledata = TotalAssetScheduledata(fromDictionary: totalAssetScheduledataData)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apiMessage != nil{
			dictionary["api_message"] = apiMessage
		}
		if apiStatus != nil{
			dictionary["api_status"] = apiStatus
		}
		if statsCode != nil{
			dictionary["stats_code"] = statsCode
		}
		if totalAssetScheduledata != nil{
			dictionary["totalAssetScheduledata"] = totalAssetScheduledata.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiMessage = aDecoder.decodeObject(forKey: "api_message") as? String
         apiStatus = aDecoder.decodeObject(forKey: "api_status") as? Int
         statsCode = aDecoder.decodeObject(forKey: "stats_code") as? Int
         totalAssetScheduledata = aDecoder.decodeObject(forKey: "totalAssetScheduledata") as? TotalAssetScheduledata

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if apiMessage != nil{
			aCoder.encode(apiMessage, forKey: "api_message")
		}
		if apiStatus != nil{
			aCoder.encode(apiStatus, forKey: "api_status")
		}
		if statsCode != nil{
			aCoder.encode(statsCode, forKey: "stats_code")
		}
		if totalAssetScheduledata != nil{
			aCoder.encode(totalAssetScheduledata, forKey: "totalAssetScheduledata")
		}

	}

}
