//
//	TotalAssetScheduledata.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class TotalAssetScheduledata : NSObject, NSCoding{

	var assetOwnerId : Int!
	var assetTagId : Int!
	var assetUserOwnerId : AnyObject!
	var auditEndDate : String!
	var auditId : Int!
	var auditInspectionDate : String!
	var auditName : AnyObject!
	var auditParamsId : Int!
	var auditParamsTransaction : String!
	var auditParamsValues : String!
	var auditResult : String!
	var auditSchduleId : Int!
	var auditStartDate : String!
	var auditStatus : String!
	var createdAt : String!
	var deletedAt : AnyObject!
	var escalatedAuditLevels : AnyObject!
	var inspectionDelay : String!
	var institutionId : Int!
	var isProcess : Int!
	var paramsMasterId : String!
	var prevMaintenanaceOwnerId : Int!
	var prevMaintenanaceOwnerType : String!
	var pushSendNotification : Int!
	var scheduleExpireDate : String!
	var updatedAt : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		assetOwnerId = dictionary["asset_owner_id"] as? Int
		assetTagId = dictionary["asset_tag_id"] as? Int
		assetUserOwnerId = dictionary["asset_user_owner_id"] as? AnyObject
		auditEndDate = dictionary["audit_end_date"] as? String
		auditId = dictionary["audit_id"] as? Int
		auditInspectionDate = dictionary["audit_inspection_date"] as? String
		auditName = dictionary["audit_name"] as? AnyObject
		auditParamsId = dictionary["audit_params_id"] as? Int
		auditParamsTransaction = dictionary["audit_params_transaction"] as? String
		auditParamsValues = dictionary["audit_params_values"] as? String
		auditResult = dictionary["audit_result"] as? String
		auditSchduleId = dictionary["audit_schdule_id"] as? Int
		auditStartDate = dictionary["audit_start_date"] as? String
		auditStatus = dictionary["audit_status"] as? String
		createdAt = dictionary["created_at"] as? String
		deletedAt = dictionary["deleted_at"] as? AnyObject
		escalatedAuditLevels = dictionary["escalated_audit_levels"] as? AnyObject
		inspectionDelay = dictionary["inspection_delay"] as? String
		institutionId = dictionary["institution_id"] as? Int
		isProcess = dictionary["is_process"] as? Int
		paramsMasterId = dictionary["params_master_id"] as? String
		prevMaintenanaceOwnerId = dictionary["prev_maintenanace_owner_id"] as? Int
		prevMaintenanaceOwnerType = dictionary["prev_maintenanace_owner_type"] as? String
		pushSendNotification = dictionary["push_send_notification"] as? Int
		scheduleExpireDate = dictionary["schedule_expire_date"] as? String
		updatedAt = dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if assetOwnerId != nil{
			dictionary["asset_owner_id"] = assetOwnerId
		}
		if assetTagId != nil{
			dictionary["asset_tag_id"] = assetTagId
		}
		if assetUserOwnerId != nil{
			dictionary["asset_user_owner_id"] = assetUserOwnerId
		}
		if auditEndDate != nil{
			dictionary["audit_end_date"] = auditEndDate
		}
		if auditId != nil{
			dictionary["audit_id"] = auditId
		}
		if auditInspectionDate != nil{
			dictionary["audit_inspection_date"] = auditInspectionDate
		}
		if auditName != nil{
			dictionary["audit_name"] = auditName
		}
		if auditParamsId != nil{
			dictionary["audit_params_id"] = auditParamsId
		}
		if auditParamsTransaction != nil{
			dictionary["audit_params_transaction"] = auditParamsTransaction
		}
		if auditParamsValues != nil{
			dictionary["audit_params_values"] = auditParamsValues
		}
		if auditResult != nil{
			dictionary["audit_result"] = auditResult
		}
		if auditSchduleId != nil{
			dictionary["audit_schdule_id"] = auditSchduleId
		}
		if auditStartDate != nil{
			dictionary["audit_start_date"] = auditStartDate
		}
		if auditStatus != nil{
			dictionary["audit_status"] = auditStatus
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if escalatedAuditLevels != nil{
			dictionary["escalated_audit_levels"] = escalatedAuditLevels
		}
		if inspectionDelay != nil{
			dictionary["inspection_delay"] = inspectionDelay
		}
		if institutionId != nil{
			dictionary["institution_id"] = institutionId
		}
		if isProcess != nil{
			dictionary["is_process"] = isProcess
		}
		if paramsMasterId != nil{
			dictionary["params_master_id"] = paramsMasterId
		}
		if prevMaintenanaceOwnerId != nil{
			dictionary["prev_maintenanace_owner_id"] = prevMaintenanaceOwnerId
		}
		if prevMaintenanaceOwnerType != nil{
			dictionary["prev_maintenanace_owner_type"] = prevMaintenanaceOwnerType
		}
		if pushSendNotification != nil{
			dictionary["push_send_notification"] = pushSendNotification
		}
		if scheduleExpireDate != nil{
			dictionary["schedule_expire_date"] = scheduleExpireDate
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         assetOwnerId = aDecoder.decodeObject(forKey: "asset_owner_id") as? Int
         assetTagId = aDecoder.decodeObject(forKey: "asset_tag_id") as? Int
         assetUserOwnerId = aDecoder.decodeObject(forKey: "asset_user_owner_id") as? AnyObject
         auditEndDate = aDecoder.decodeObject(forKey: "audit_end_date") as? String
         auditId = aDecoder.decodeObject(forKey: "audit_id") as? Int
         auditInspectionDate = aDecoder.decodeObject(forKey: "audit_inspection_date") as? String
         auditName = aDecoder.decodeObject(forKey: "audit_name") as? AnyObject
         auditParamsId = aDecoder.decodeObject(forKey: "audit_params_id") as? Int
         auditParamsTransaction = aDecoder.decodeObject(forKey: "audit_params_transaction") as? String
         auditParamsValues = aDecoder.decodeObject(forKey: "audit_params_values") as? String
         auditResult = aDecoder.decodeObject(forKey: "audit_result") as? String
         auditSchduleId = aDecoder.decodeObject(forKey: "audit_schdule_id") as? Int
         auditStartDate = aDecoder.decodeObject(forKey: "audit_start_date") as? String
         auditStatus = aDecoder.decodeObject(forKey: "audit_status") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? AnyObject
         escalatedAuditLevels = aDecoder.decodeObject(forKey: "escalated_audit_levels") as? AnyObject
         inspectionDelay = aDecoder.decodeObject(forKey: "inspection_delay") as? String
         institutionId = aDecoder.decodeObject(forKey: "institution_id") as? Int
         isProcess = aDecoder.decodeObject(forKey: "is_process") as? Int
         paramsMasterId = aDecoder.decodeObject(forKey: "params_master_id") as? String
         prevMaintenanaceOwnerId = aDecoder.decodeObject(forKey: "prev_maintenanace_owner_id") as? Int
         prevMaintenanaceOwnerType = aDecoder.decodeObject(forKey: "prev_maintenanace_owner_type") as? String
         pushSendNotification = aDecoder.decodeObject(forKey: "push_send_notification") as? Int
         scheduleExpireDate = aDecoder.decodeObject(forKey: "schedule_expire_date") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if assetOwnerId != nil{
			aCoder.encode(assetOwnerId, forKey: "asset_owner_id")
		}
		if assetTagId != nil{
			aCoder.encode(assetTagId, forKey: "asset_tag_id")
		}
		if assetUserOwnerId != nil{
			aCoder.encode(assetUserOwnerId, forKey: "asset_user_owner_id")
		}
		if auditEndDate != nil{
			aCoder.encode(auditEndDate, forKey: "audit_end_date")
		}
		if auditId != nil{
			aCoder.encode(auditId, forKey: "audit_id")
		}
		if auditInspectionDate != nil{
			aCoder.encode(auditInspectionDate, forKey: "audit_inspection_date")
		}
		if auditName != nil{
			aCoder.encode(auditName, forKey: "audit_name")
		}
		if auditParamsId != nil{
			aCoder.encode(auditParamsId, forKey: "audit_params_id")
		}
		if auditParamsTransaction != nil{
			aCoder.encode(auditParamsTransaction, forKey: "audit_params_transaction")
		}
		if auditParamsValues != nil{
			aCoder.encode(auditParamsValues, forKey: "audit_params_values")
		}
		if auditResult != nil{
			aCoder.encode(auditResult, forKey: "audit_result")
		}
		if auditSchduleId != nil{
			aCoder.encode(auditSchduleId, forKey: "audit_schdule_id")
		}
		if auditStartDate != nil{
			aCoder.encode(auditStartDate, forKey: "audit_start_date")
		}
		if auditStatus != nil{
			aCoder.encode(auditStatus, forKey: "audit_status")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if escalatedAuditLevels != nil{
			aCoder.encode(escalatedAuditLevels, forKey: "escalated_audit_levels")
		}
		if inspectionDelay != nil{
			aCoder.encode(inspectionDelay, forKey: "inspection_delay")
		}
		if institutionId != nil{
			aCoder.encode(institutionId, forKey: "institution_id")
		}
		if isProcess != nil{
			aCoder.encode(isProcess, forKey: "is_process")
		}
		if paramsMasterId != nil{
			aCoder.encode(paramsMasterId, forKey: "params_master_id")
		}
		if prevMaintenanaceOwnerId != nil{
			aCoder.encode(prevMaintenanaceOwnerId, forKey: "prev_maintenanace_owner_id")
		}
		if prevMaintenanaceOwnerType != nil{
			aCoder.encode(prevMaintenanaceOwnerType, forKey: "prev_maintenanace_owner_type")
		}
		if pushSendNotification != nil{
			aCoder.encode(pushSendNotification, forKey: "push_send_notification")
		}
		if scheduleExpireDate != nil{
			aCoder.encode(scheduleExpireDate, forKey: "schedule_expire_date")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}