//
//	Licence .swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Licence  : NSObject, NSCoding{

	var rows : [LicenceRow]!
	var total : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		rows = [LicenceRow]()
		if let rowsArray = dictionary["rows"] as? [[String:Any]]{
			for dic in rowsArray{
				let value = LicenceRow(fromDictionary: dic)
				rows.append(value)
			}
		}
		total = dictionary["total"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if rows != nil{
			var dictionaryElements = [[String:Any]]()
			for rowsElement in rows {
				dictionaryElements.append(rowsElement.toDictionary())
			}
			dictionary["rows"] = dictionaryElements
		}
		if total != nil{
			dictionary["total"] = total
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         rows = aDecoder.decodeObject(forKey :"rows") as? [LicenceRow]
         total = aDecoder.decodeObject(forKey: "total") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if rows != nil{
			aCoder.encode(rows, forKey: "rows")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}

	}

}