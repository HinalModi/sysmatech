//
//	LicenceCreatedAt.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LicenceCreatedAt : NSObject, NSCoding{

	var datetime : String!
	var formatted : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		datetime = dictionary["datetime"] as? String
		formatted = dictionary["formatted"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if datetime != nil{
			dictionary["datetime"] = datetime
		}
		if formatted != nil{
			dictionary["formatted"] = formatted
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         datetime = aDecoder.decodeObject(forKey: "datetime") as? String
         formatted = aDecoder.decodeObject(forKey: "formatted") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if datetime != nil{
			aCoder.encode(datetime, forKey: "datetime")
		}
		if formatted != nil{
			aCoder.encode(formatted, forKey: "formatted")
		}

	}

}