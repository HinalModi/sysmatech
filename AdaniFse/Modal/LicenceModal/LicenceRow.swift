//
//	LicenceRow.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LicenceRow : NSObject, NSCoding{

	var availableActions : LicenceAvailableAction!
	var category : AnyObject!
	var company : LicenceCompany!
	var createdAt : LicenceCreatedAt!
	var deletedAt : AnyObject!
	var depreciation : AnyObject!
	var expirationDate : LicenceExpirationDate!
	var freeSeatsCount : Int!
	var id : Int!
	var licenseEmail : String!
	var licenseName : String!
	var maintained : Bool!
	var manufacturer : LicenceCompany!
	var name : String!
	var notes : String!
	var orderNumber : String!
	var productKey : String!
	var purchaseCost : String!
	var purchaseCostNumeric : String!
	var purchaseDate : LicenceExpirationDate!
	var purchaseOrder : String!
	var reassignable : Bool!
	var seats : Int!
	var supplier : LicenceCompany!
	var terminationDate : AnyObject!
	var updatedAt : LicenceCreatedAt!
	var userCanCheckout : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let availableActionsData = dictionary["available_actions"] as? [String:Any]{
			availableActions = LicenceAvailableAction(fromDictionary: availableActionsData)
		}
		category = dictionary["category"] as? AnyObject
		if let companyData = dictionary["company"] as? [String:Any]{
			company = LicenceCompany(fromDictionary: companyData)
		}
		if let createdAtData = dictionary["created_at"] as? [String:Any]{
			createdAt = LicenceCreatedAt(fromDictionary: createdAtData)
		}
		deletedAt = dictionary["deleted_at"] as? AnyObject
		depreciation = dictionary["depreciation"] as? AnyObject
		if let expirationDateData = dictionary["expiration_date"] as? [String:Any]{
			expirationDate = LicenceExpirationDate(fromDictionary: expirationDateData)
		}
		freeSeatsCount = dictionary["free_seats_count"] as? Int
		id = dictionary["id"] as? Int
		licenseEmail = dictionary["license_email"] as? String
		licenseName = dictionary["license_name"] as? String
		maintained = dictionary["maintained"] as? Bool
		if let manufacturerData = dictionary["manufacturer"] as? [String:Any]{
			manufacturer = LicenceCompany(fromDictionary: manufacturerData)
		}
		name = dictionary["name"] as? String
		notes = dictionary["notes"] as? String
		orderNumber = dictionary["order_number"] as? String
		productKey = dictionary["product_key"] as? String
		purchaseCost = dictionary["purchase_cost"] as? String
		purchaseCostNumeric = dictionary["purchase_cost_numeric"] as? String
		if let purchaseDateData = dictionary["purchase_date"] as? [String:Any]{
			purchaseDate = LicenceExpirationDate(fromDictionary: purchaseDateData)
		}
		purchaseOrder = dictionary["purchase_order"] as? String
		reassignable = dictionary["reassignable"] as? Bool
		seats = dictionary["seats"] as? Int
		if let supplierData = dictionary["supplier"] as? [String:Any]{
			supplier = LicenceCompany(fromDictionary: supplierData)
		}
		terminationDate = dictionary["termination_date"] as? AnyObject
		if let updatedAtData = dictionary["updated_at"] as? [String:Any]{
			updatedAt = LicenceCreatedAt(fromDictionary: updatedAtData)
		}
		userCanCheckout = dictionary["user_can_checkout"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if availableActions != nil{
			dictionary["available_actions"] = availableActions.toDictionary()
		}
		if category != nil{
			dictionary["category"] = category
		}
		if company != nil{
			dictionary["company"] = company.toDictionary()
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt.toDictionary()
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if depreciation != nil{
			dictionary["depreciation"] = depreciation
		}
		if expirationDate != nil{
			dictionary["expiration_date"] = expirationDate.toDictionary()
		}
		if freeSeatsCount != nil{
			dictionary["free_seats_count"] = freeSeatsCount
		}
		if id != nil{
			dictionary["id"] = id
		}
		if licenseEmail != nil{
			dictionary["license_email"] = licenseEmail
		}
		if licenseName != nil{
			dictionary["license_name"] = licenseName
		}
		if maintained != nil{
			dictionary["maintained"] = maintained
		}
		if manufacturer != nil{
			dictionary["manufacturer"] = manufacturer.toDictionary()
		}
		if name != nil{
			dictionary["name"] = name
		}
		if notes != nil{
			dictionary["notes"] = notes
		}
		if orderNumber != nil{
			dictionary["order_number"] = orderNumber
		}
		if productKey != nil{
			dictionary["product_key"] = productKey
		}
		if purchaseCost != nil{
			dictionary["purchase_cost"] = purchaseCost
		}
		if purchaseCostNumeric != nil{
			dictionary["purchase_cost_numeric"] = purchaseCostNumeric
		}
		if purchaseDate != nil{
			dictionary["purchase_date"] = purchaseDate.toDictionary()
		}
		if purchaseOrder != nil{
			dictionary["purchase_order"] = purchaseOrder
		}
		if reassignable != nil{
			dictionary["reassignable"] = reassignable
		}
		if seats != nil{
			dictionary["seats"] = seats
		}
		if supplier != nil{
			dictionary["supplier"] = supplier.toDictionary()
		}
		if terminationDate != nil{
			dictionary["termination_date"] = terminationDate
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt.toDictionary()
		}
		if userCanCheckout != nil{
			dictionary["user_can_checkout"] = userCanCheckout
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         availableActions = aDecoder.decodeObject(forKey: "available_actions") as? LicenceAvailableAction
         category = aDecoder.decodeObject(forKey: "category") as? AnyObject
         company = aDecoder.decodeObject(forKey: "company") as? LicenceCompany
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? LicenceCreatedAt
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? AnyObject
         depreciation = aDecoder.decodeObject(forKey: "depreciation") as? AnyObject
         expirationDate = aDecoder.decodeObject(forKey: "expiration_date") as? LicenceExpirationDate
         freeSeatsCount = aDecoder.decodeObject(forKey: "free_seats_count") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         licenseEmail = aDecoder.decodeObject(forKey: "license_email") as? String
         licenseName = aDecoder.decodeObject(forKey: "license_name") as? String
         maintained = aDecoder.decodeObject(forKey: "maintained") as? Bool
         manufacturer = aDecoder.decodeObject(forKey: "manufacturer") as? LicenceCompany
         name = aDecoder.decodeObject(forKey: "name") as? String
         notes = aDecoder.decodeObject(forKey: "notes") as? String
         orderNumber = aDecoder.decodeObject(forKey: "order_number") as? String
         productKey = aDecoder.decodeObject(forKey: "product_key") as? String
         purchaseCost = aDecoder.decodeObject(forKey: "purchase_cost") as? String
         purchaseCostNumeric = aDecoder.decodeObject(forKey: "purchase_cost_numeric") as? String
         purchaseDate = aDecoder.decodeObject(forKey: "purchase_date") as? LicenceExpirationDate
         purchaseOrder = aDecoder.decodeObject(forKey: "purchase_order") as? String
         reassignable = aDecoder.decodeObject(forKey: "reassignable") as? Bool
         seats = aDecoder.decodeObject(forKey: "seats") as? Int
         supplier = aDecoder.decodeObject(forKey: "supplier") as? LicenceCompany
         terminationDate = aDecoder.decodeObject(forKey: "termination_date") as? AnyObject
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? LicenceCreatedAt
         userCanCheckout = aDecoder.decodeObject(forKey: "user_can_checkout") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if availableActions != nil{
			aCoder.encode(availableActions, forKey: "available_actions")
		}
		if category != nil{
			aCoder.encode(category, forKey: "category")
		}
		if company != nil{
			aCoder.encode(company, forKey: "company")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if depreciation != nil{
			aCoder.encode(depreciation, forKey: "depreciation")
		}
		if expirationDate != nil{
			aCoder.encode(expirationDate, forKey: "expiration_date")
		}
		if freeSeatsCount != nil{
			aCoder.encode(freeSeatsCount, forKey: "free_seats_count")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if licenseEmail != nil{
			aCoder.encode(licenseEmail, forKey: "license_email")
		}
		if licenseName != nil{
			aCoder.encode(licenseName, forKey: "license_name")
		}
		if maintained != nil{
			aCoder.encode(maintained, forKey: "maintained")
		}
		if manufacturer != nil{
			aCoder.encode(manufacturer, forKey: "manufacturer")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if orderNumber != nil{
			aCoder.encode(orderNumber, forKey: "order_number")
		}
		if productKey != nil{
			aCoder.encode(productKey, forKey: "product_key")
		}
		if purchaseCost != nil{
			aCoder.encode(purchaseCost, forKey: "purchase_cost")
		}
		if purchaseCostNumeric != nil{
			aCoder.encode(purchaseCostNumeric, forKey: "purchase_cost_numeric")
		}
		if purchaseDate != nil{
			aCoder.encode(purchaseDate, forKey: "purchase_date")
		}
		if purchaseOrder != nil{
			aCoder.encode(purchaseOrder, forKey: "purchase_order")
		}
		if reassignable != nil{
			aCoder.encode(reassignable, forKey: "reassignable")
		}
		if seats != nil{
			aCoder.encode(seats, forKey: "seats")
		}
		if supplier != nil{
			aCoder.encode(supplier, forKey: "supplier")
		}
		if terminationDate != nil{
			aCoder.encode(terminationDate, forKey: "termination_date")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userCanCheckout != nil{
			aCoder.encode(userCanCheckout, forKey: "user_can_checkout")
		}

	}

}