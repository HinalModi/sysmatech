//
//  AssetStatusVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 27/07/22.
//

import UIKit
import LGSideMenuController
import SQLite

class AssetStatusVC: UIViewController {

    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var assetListTVC: UITableView!
    @IBOutlet weak var btnOpr: UIButton!
    @IBOutlet weak var btnBreakDwn: UIButton!
    var getAssetlist : [AllUserAssetAssgainData] = []
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    var assetList : [assetListData] = []
    var refreshControl: UIRefreshControl!
    var isOpr = true
    var break_prevent = "preventive"
    var categoryId = Int64()
    var isFromCategory = false
    var maintainStatus = 1
    var isfromModel = false
    var modelName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getAssetListApi()
        createDB()
        
        if #available(iOS 11.0, *) {
            btnOpr.backgroundColor = Constant.Color.color_launch
        }else{
            btnOpr.backgroundColor = .blue
        }
        btnOpr.tintColor = .white
        if #available(iOS 11.0, *) {
            btnBreakDwn.backgroundColor = Constant.Color.color_bg
            btnBreakDwn.tintColor = Constant.Color.color_grey
        }else{
            btnBreakDwn.backgroundColor = .white
            btnBreakDwn.tintColor = .gray
        }
        if isFromCategory{
            readValuesForCategories()
        }
        if isfromModel{
            readValuesModel()
        }

    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            
            getAssetListApi()
            createDB()
        }
        if isFromCategory{
            readValuesForCategories()
        }else if isfromModel{
            readValuesModel()
        }else{
            if isOpr{
                readValuesOperational()
            }else{
                readValuesBreakDown()
            }
        }
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            assetListTVC.refreshControl = refreshControl
        } else {
            assetListTVC.addSubview(refreshControl)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAssetListApi()
        createDB()
        addPullRequest()
        assetListTVC.tableFooterView = UIView()
        
    }
    //MARK: - Action
   
    @IBAction func operational(_ sender: UIButton) {
       
            break_prevent = "preventive"
            isOpr = true
            maintainStatus = 1
            if #available(iOS 11.0, *) {
                btnOpr.backgroundColor = Constant.Color.color_launch
                btnBreakDwn.backgroundColor = Constant.Color.color_bg
                btnBreakDwn.tintColor = Constant.Color.color_grey
            }else{
                btnOpr.backgroundColor = .blue
            }
            btnOpr.tintColor = .white
            activityIndicator.startAnimating()
            if isFromCategory{
                readValuesForCategories()
            }else if isfromModel{
                readValuesModel()
            }else{
                readValuesOperational()
            }
            assetListTVC.reloadData()
            activityIndicator.stopAnimating()
            if assetList.count == 0{
                lblNoData.isHidden = false
            }else{
                lblNoData.isHidden = true
        }
    }
    @IBAction func breakDown(_ sender: UIButton) {
       
            isOpr = false
            break_prevent = "breakdown"
            maintainStatus = 0
            if #available(iOS 11.0, *) {
                btnBreakDwn.backgroundColor = Constant.Color.color_launch
                btnOpr.backgroundColor = Constant.Color.color_bg
                btnOpr.tintColor = Constant.Color.color_grey
            }else{
                btnBreakDwn.backgroundColor = .blue
            }
            btnBreakDwn.tintColor = .white
            activityIndicator.startAnimating()
            if isFromCategory{
                readValuesForCategories()
            }else if isfromModel{
                readValuesModel()
            }else{
                readValuesBreakDown()
            }
            assetListTVC.reloadData()
            activityIndicator.stopAnimating()
            if assetList.count == 0{
                lblNoData.isHidden = false
            }else{
                lblNoData.isHidden = true
            }
    }
    
    @IBAction func btnShowSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    //MARK: - Api call
    
    
    
    func getAssetListApi(){
        activityIndicator.startAnimating()
        let param : Parameters = [
            .user_id : user_Id ,
        ]
        print(param)
      
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .assetList, baseURL: selectedComp ?? "", setToken: glbtoken)
        URLSession.shared.getAssetList(with: request) { isSucess, assetList , message, status  in
            DispatchQueue.main.async { [self] in
                if isSucess{
                    print(message)
                    activityIndicator.startAnimating()
                    getAssetlist = assetList ?? []
                    insertData()
                    if isFromCategory{
                        readValuesForCategories()
                    }else if isfromModel{
                        readValuesModel()
                    }else{
                        if isOpr {
                            readValuesOperational()
                        }else{
                            readValuesBreakDown()
                        }
                    }
                    if getAssetlist.count == 0{
                        lblNoData.isHidden = false
                    }else{
                        lblNoData.isHidden = true
                    }
                    assetListTVC.reloadData()
                    activityIndicator.stopAnimating()
                }else{
                    print(message)
                    activityIndicator.stopAnimating()
                }
            }
        }
    }
    //MARK: -StoreDataInSqlite
    
    let algorithms = Table("AssetList")
    var auditInspectionDate = Expression<String>("audit_inspection_date")
    var auditResult = Expression<String>("audit_result")
    var auditParamsId = Expression<Int>("audit_params_id")
    var prevMaintenanaceOwnerId = Expression<Int>("prev_maintenanace_owner_id")
    var prevMaintenanaceOwnerType = Expression<String>("prev_maintenanace_owner_type")
    var paramsMasterId = Expression<String>("params_master_id")
    var id = Expression<Int>("id")
    let name = Expression<String>("name")
    let asset_tag = Expression<String>("asset_tag")
    var modelId = Expression<Int>("model_id")
    let model_name = Expression<String>("model_name")
    let image = Expression<String>("image")
    var userId = Expression<Int>("user_id")
    let warrantyMonths = Expression<Int>("warranty_months")
    var location = Expression<String>("location")
    var auditParamsValues = Expression<String>("audit_params_values")
    var auditParamsTransaction = Expression<String>("audit_params_transaction")
    var categoryName = Expression<String>("category_name")
    var supplierName = Expression<String>("supplier_name")
    var companyName = Expression<String>("company_name")
    var purchaseDate = Expression<String>("purchase_date")
    var assetStatus = Expression<Int>("asset_status")
    let lastAuditDate = Expression<String>("last_audit_date")
    var category_id = Expression<Int>("category_id")
    
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                if Reachability.isConnectedToNetwork(){
                    let drop = algorithms.drop(ifExists: true)
                    try db?.run(drop)
                }
                db = try Connection("\(path)/assetList.sqlite3")
                // Initialize tables
                try db?.run(algorithms.create { table in
                    table.column(id, primaryKey: true)
                    table.column(name)
                    table.column(asset_tag)
                    table.column(modelId)
                    table.column(model_name)
                    table.column(purchaseDate)
                    table.column(image)
                    table.column(warrantyMonths)
                    table.column(location)
                    table.column(auditParamsValues)
                    table.column(categoryName)
                    table.column(supplierName)
                    table.column(companyName)
                    table.column(assetStatus)
                    table.column(lastAuditDate)
                    table.column(category_id)
                })
                
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    func insertData(){
        for dataset in getAssetlist{
            do{
                print(dataset.categoryId)
                try db?.run(algorithms.insert(id <- dataset.id ?? 0 ,name <- dataset.name  ?? "",asset_tag <- dataset.assetTag  ?? "",modelId <- dataset.modelId ?? 0 ,image <- dataset.image  ?? "",warrantyMonths <- dataset.warrantyMonths  ?? 0,location <- dataset.location ?? "",supplierName <- dataset.supplierName ?? "",model_name <- dataset.modelName  ?? "",categoryName <- dataset.categoryName  ?? "" ,companyName <- dataset.companyName  ?? "",purchaseDate <- dataset.purchaseDate  ?? "" ,auditParamsValues <- dataset.auditParamsValues ?? "" ,assetStatus <- dataset.assetStatus  ?? 0 , lastAuditDate <- dataset.lastAuditDate ?? "" , category_id <- dataset.categoryId ?? 0))
            }
            catch{
                print("Error in inserting data",error.localizedDescription)
                print(String(describing: error))
                }
        }
    }
    
    func readValuesOperational(){
        assetList.removeAll()
       let status = 1
        do{
          // WHERE schedule_expire_date > '\(today)' AND
            for row in try db!.prepare("SELECT * FROM AssetList WHERE asset_status = \(status)") {
                    print("row277",row)
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[2]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])")
                assetList.append(assetListData(id: (row[0]) as? Int64 ?? 0, name: (row[1]) as? String ?? "", assetTag: (row[2]) as? String ?? "", location: (row[8]) as? String ?? "", warrentyTime: (row[7]) as? Int64 ?? 0, supplier: (row[11]) as? String ?? "", purchaseDate: (row[5]) as? String ?? "", modalName: (row[4]) as? String ?? "", category: (row[10]) as? String ?? "", company: (row[12]) as? String ?? "", auditValue: (row[9]) as? String ?? "", assetStatus: (row[13]) as? Int64 ?? 0, image: (row[6]) as? String ?? "" , modalId: (row[3]) as? Int64 ?? 0, lastAuditDate: (row[14]) as? String ?? "", categoryId: (row[15]) as? Int64 ?? 0))
                assetListTVC.reloadData()
                
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
       
    }
    func readValuesBreakDown(){
        assetList.removeAll()
       let status = 0
        do{
          // WHERE schedule_expire_date > '\(today)' AND
            for row in try db!.prepare("SELECT * FROM AssetList WHERE asset_status = \(status)") {
               
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[2]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])")
                assetList.append(assetListData(id: (row[0]) as? Int64 ?? 0, name: (row[1]) as? String ?? "", assetTag: (row[2]) as? String ?? "", location: (row[8]) as? String ?? "", warrentyTime: (row[7]) as? Int64 ?? 0, supplier: (row[11]) as? String ?? "", purchaseDate: (row[5]) as? String ?? "", modalName: (row[4]) as? String ?? "", category: (row[10]) as? String ?? "", company: (row[12]) as? String ?? "", auditValue: (row[9]) as? String ?? "", assetStatus: (row[13]) as? Int64 ?? 0, image: (row[6]) as? String ?? "" , modalId: (row[3]) as? Int64 ?? 0, lastAuditDate: (row[14]) as? String ?? "", categoryId: (row[15]) as? Int64 ?? 0))
                assetListTVC.reloadData()
                
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
        if assetList.count == 0{
            lblNoData.isHidden = false
        }else{
            lblNoData.isHidden = true
        }
    }
    func readValuesForCategories(){
        assetList.removeAll()
        do{
          // WHERE schedule_expire_date > '\(today)' AND
            for row in try db!.prepare("SELECT * FROM AssetList WHERE asset_status = \(maintainStatus) AND category_id = \(categoryId)") {
               
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[2]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])")
                assetList.append(assetListData(id: (row[0]) as? Int64 ?? 0, name: (row[1]) as? String ?? "", assetTag: (row[2]) as? String ?? "", location: (row[8]) as? String ?? "", warrentyTime: (row[7]) as? Int64 ?? 0, supplier: (row[11]) as? String ?? "", purchaseDate: (row[5]) as? String ?? "", modalName: (row[4]) as? String ?? "", category: (row[10]) as? String ?? "", company: (row[12]) as? String ?? "", auditValue: (row[9]) as? String ?? "", assetStatus: (row[13]) as? Int64 ?? 0, image: (row[6]) as? String ?? "" , modalId: (row[3]) as? Int64 ?? 0, lastAuditDate: (row[14]) as? String ?? "", categoryId: (row[15]) as? Int64 ?? 0))
                    assetListTVC.reloadData()
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
        
    }
    
    func readValuesModel(){
        assetList.removeAll()
        do{
          // WHERE schedule_expire_date > '\(today)' AND
            for row in try db!.prepare("SELECT * FROM AssetList WHERE asset_status = \(maintainStatus) AND category_id = \(categoryId) AND model_name = '\(modelName)'") {
               
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[2]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])")
                assetList.append(assetListData(id: (row[0]) as? Int64 ?? 0, name: (row[1]) as? String ?? "", assetTag: (row[2]) as? String ?? "", location: (row[8]) as? String ?? "", warrentyTime: (row[7]) as? Int64 ?? 0, supplier: (row[11]) as? String ?? "", purchaseDate: (row[5]) as? String ?? "", modalName: (row[4]) as? String ?? "", category: (row[10]) as? String ?? "", company: (row[12]) as? String ?? "", auditValue: (row[9]) as? String ?? "", assetStatus: (row[13]) as? Int64 ?? 0, image: (row[6]) as? String ?? "" , modalId: (row[3]) as? Int64 ?? 0, lastAuditDate: (row[14]) as? String ?? "", categoryId: (row[15]) as? Int64 ?? 0))
                    assetListTVC.reloadData()
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
    }
}
extension  AssetStatusVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("assetList",getAssetlist.count)
        return assetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssetStatusTVC") as! AssetStatusTVC
        if assetList.count > 0{            
            let list = assetList[indexPath.row]
            cell.lblAuditName.text = list.name
            cell.lblModalName.text = list.modalName
            cell.lblLocation.text = list.location
            if list.lastAuditDate != "" {
                let newdate = GlobalFunction.convertDateString(dateString: list.lastAuditDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
            }
            cell.lblAssetTag.text = "-\(list.assetTag)"
            
            let urlImg = URL(string: baseUrlImage + list.image)
            cell.imgAudit.sd_setImage(with: urlImg, placeholderImage: UIImage(named: "setting2"))
            cell.setImage = {
                if list.image != ""{
                    let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                    vc.imgUrl = urlImg
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
        if assetList.count > 0{
            let list = assetList[indexPath.row]
            vc.productName = list.name
            vc.assetStatus = list.assetStatus
            vc.companyName = list.company
            print(list.auditValue)
            vc.category = list.category
            vc.modalnNo = list.modalName
            vc.assetTag = list.assetTag
            vc.purchaseDate = list.purchaseDate
            vc.supplier = list.supplier
            vc.assetTagiD = list.id
            vc.warranty = list.warrentyTime
            vc.location = list.location
            let urlImg = URL(string: baseUrlImage + list.image)
            vc.auditImageUrl = urlImg
            vc.isFromAssetStatus = true
            vc.break_prevent = break_prevent
        }
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
