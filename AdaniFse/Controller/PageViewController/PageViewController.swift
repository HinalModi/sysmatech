//
//  PageViewController.swift
//  AdaniFse
//
//  Created by Ankit Dave on 28/06/22.
//

import UIKit
import DropDown
import AVKit
import AVFoundation
import Combine
import Speech
import MLKit

@available(iOS 13.0, *)
class PageViewController: UIViewController , UITextFieldDelegate , UITextViewDelegate , AVAudioPlayerDelegate ,AVAudioRecorderDelegate{
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnDeleteRec: UIButton!
    @IBOutlet weak var viewAudiorec: UIView!
    @IBOutlet weak var horizontalConstaint: NSLayoutConstraint!
    @IBOutlet weak var recordedSlider: UISlider!
    @IBOutlet weak var btnStopRecorded: UIButton!
    @IBOutlet weak var btnPlayrecorded: UIButton!
    @IBOutlet weak var btnRecStop: UIButton!
    @IBOutlet weak var btnRec: UIButton!
    @IBOutlet weak var playRecordView: UIView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var lblRecTime: UILabel!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var btnPause: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var audioSlide: UISlider!
    @IBOutlet weak var imgDrop: UIImageView!
    @IBOutlet weak var imgeSelected: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrev: UIButton!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var lblParams: UILabel!
    @IBOutlet weak var lblPageNo: UILabel!
    @IBOutlet weak var txtParamValues: UITextField!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var btnDropDown: UIButton!
    var pageIndex: Int = 0
    var setLbldata = String()
    var setAudioUrl = String()
    var setParam = String()
    var strTitle: String!
    let dropDown = DropDown()
    var openDropDown : (()->())?
    var toNext : (()->())?
    var toPrev : (()->())?
    var hideComment = String()
    var hidePhoto = String()
    var endEditing : (()->())?
    var setPlaceholder = String()
    var setBtnHide = Bool()
    var setSelectedImage = UIImage()
    var commentEndEdit :(()->())?
    var setComment = String()
    var setImgdrop = Bool()
    var val = 0
    var audio = String()
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    var viewOpened = 0
    var paramId = Int()
    let audioEngine = AVAudioEngine()
//    var speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask:SFSpeechRecognitionTask?
    var speechRecognizerHindi = SFSpeechRecognizer(locale: Locale.init(identifier: "hi-IN"))
    var speechRecognizerSpan = SFSpeechRecognizer(locale: Locale.init(identifier: "es-ES"))
    var speechRecognizerGerman = SFSpeechRecognizer(locale: Locale.init(identifier: "de-DE"))
    var speechRecognizerFrench = SFSpeechRecognizer(locale: Locale.init(identifier: "fr-CH"))
    var speechRecognizerEng = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    var audioVal =  UserDefaults.standard.integer(forKey: "change_parameter_audio")
    var mp3FileData = Data()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPageNo.text = strTitle
        txtParamValues.text = setParam
        print(SFSpeechRecognizer.supportedLocales())
        if #available(iOS 11.0, *) {
            btnPrev.backgroundColor = Constant.Color.color_bg
            btnNext.backgroundColor = Constant.Color.color_launch
            btnNext.setTitleColor(.white, for: .normal)
            btnPrev.setTitleColor(Constant.Color.color_launch, for: .normal)
            lblParams.textColor = Constant.Color.color_grey
            lblPageNo.textColor = Constant.Color.color_launch
            btnNext.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 3))
            btnPrev.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 3))
            imgDrop.tintColor = Constant.Color.color_launch
        }else{
            btnPrev.backgroundColor = .white
            btnNext.backgroundColor = .blue
            btnNext.setTitleColor(.white, for: .normal)
            btnPrev.setTitleColor(.blue, for: .normal)
            lblParams.textColor = .darkGray
            lblPageNo.textColor = .blue
            btnNext.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 3))
            btnPrev.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 3))
        }
        imgDrop.isHidden = setImgdrop
        btnPrev.setCorner(_tp: 5)
        btnNext.setCorner(_tp: 5)
        txtParamValues.setBorder(.darkGray, width: 1)
        txtComment.setBorder(.darkGray, width: 1)
        lblParams.text = setLbldata
        txtComment.text = setComment
        txtComment.setCorner(_tp: 10)
        txtParamValues.setCorner(_tp: 10)
        txtParamValues.setLeftPaddingPoints(20)
        txtParamValues.setRightPaddingPoints(20)
        txtParamValues.placeholder = setPlaceholder
        btnDropDown.isHidden = setBtnHide
        txtParamValues.delegate = self
        if hidePhoto == "Hide"{
            viewImage.isHidden = true
        }else{
            viewImage.isHidden = false
            imgeSelected.image = setSelectedImage
        }
        if hideComment == "Hide"{
            viewComment.isHidden = true
            btnRec.isHidden = true
        }else{
            viewComment.isHidden = false
            btnRec.isHidden = false
        }
        if Reachability.isConnectedToNetwork(){
            btnPlayPause.isHidden = false
            btnRec.isHidden = false
        }else{
            btnPlayPause.isHidden = true
            btnRec.isHidden = true
        }
       setAudioName()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if audio == ""{
            audioView.isHidden = true
        }else{
            audioView.isHidden = false
        }
        if audioVal == 1{
            btnAdd.isHidden = false
        }else{
            btnAdd.isHidden = true
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if viewOpened == 2{
            horizontalConstaint.constant = 0
            btnDeleteRec.isHidden = true
            if Reachability.isConnectedToNetwork(){
                btnRec.isHidden = false
            }
            viewOpened = 0
        }
    }
    @IBAction func btnNextPage(_ sender: UIButton) {
        
        toNext?()
    }
    @IBAction func btnPrevious(_ sender: UIButton) {

        toPrev?()
    }
    
    @IBAction func toPreviousVC(_ sender: UIButton) {
        player = nil
        timer?.invalidate()
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func openDropDown(_ sender: UIButton) {
        openDropDown?()
    }
    @IBAction func openCamera(_ sender: UIButton) {
        presentCamera()
    }
    
    var player: AVAudioPlayer!
    var timer: Timer?
    
    
    @IBAction func btnPlayAudio(_ sender: UIButton) {
        val += 1
        viewOpened += 1
        let url : URL!
        if Reachability.isConnectedToNetwork(){
            if audio == ""{
                self.view.makeToast("No audio found")
                return
            }
            url = URL(string: setAudioUrl)
        }else{
            if audio == ""{
                self.view.makeToast("No audio found")
                return
            }
            url = URL(fileURLWithPath: setAudioUrl)
            print(url)
            
        }
        do {
            try url.download(to: .documentDirectory) { url, error in
                do {
                    let data = try Data(contentsOf: url!)
                    try AVAudioSession.sharedInstance().setCategory(.playback ,mode: .default, options: [])
                    try AVAudioSession.sharedInstance().setActive(true)
                    self.player = try AVAudioPlayer(data: data)
                    DispatchQueue.main.async {
                        self.player.play()
                        self.btnPlayPause.isHidden =  true
                        self.btnPause.isHidden = false
                        self.player.delegate = self
                        self.audioSlide.maximumValue = Float((self.player?.duration ?? 0))
                        self.timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
                    }
                }catch{
                    self.view.makeToast("Something went wrong , Please try again later!")
                    print(error)
                }
            }
        }catch{
            print(error)
        }
    }
    func sendAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func recordAndRecognizeSpeechEnglish() {
        let node = audioEngine.inputNode
//        let recordingFormat = node.outputFormat(forBus: 0)
        let recordingFormat = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)

        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
       
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
           
            self.sendAlert(title: "Speech Recognizer Error", message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not currently available. Check back at a later time.")
            // Recognizer is not available right now
            return
        }
        
        recognitionTask = speechRecognizerEng?.recognitionTask(with: request, resultHandler: { result, error in
            
            if let result = result {
                print("result",result)
                let bestString = result.bestTranscription.formattedString
                print("bestString",bestString)
                self.txtComment.text = bestString

            } else if let error = error {
                self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                print(error)
            }
        })
    }
    func cancelRecording() {
        recognitionTask?.finish()
        recognitionTask = nil
        // stop audio
        request.endAudio()
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    var translator: Translator!
    let locale = Locale.current
    var allLanguages = TranslateLanguage.allLanguages()
    func translate() {
      let translatorForDownloading = self.translator!

      translatorForDownloading.downloadModelIfNeeded { error in
        guard error == nil else {
          self.txtComment.text = "Failed to ensure model downloaded with error \(error!)"
          return
        }
        if translatorForDownloading == self.translator {
          translatorForDownloading.translate(self.txtComment.text ?? "") { result, error in
            guard error == nil else {
              self.txtComment.text = "Failed with error \(error!)"
              return
            }
            if translatorForDownloading == self.translator {
              self.txtComment.text = result
                print(result)
                
            }
          }
        }
      }
    }
    
    func model(forLanguage: TranslateLanguage) -> TranslateRemoteModel {
      return TranslateRemoteModel.translateRemoteModel(language: forLanguage)
    }
    func isLanguageDownloaded(_ language: TranslateLanguage) -> Bool {
      let model = self.model(forLanguage: language)
      let modelManager = ModelManager.modelManager()
      return modelManager.isModelDownloaded(model)
    }
    func recordAndRecognizeSpeechFrench() {
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            self.sendAlert(title: "Speech Recognizer Error", message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not currently available. Check back at a later time.")
            return
        }
        recognitionTask = speechRecognizerFrench?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {

                print("result",result)
                let bestString = result.bestTranscription.formattedString
                let inputLanguage = TranslateLanguage.french
                let outputLanguage = TranslateLanguage.english
                print("bestString",bestString)
                self.txtComment.text = bestString
                let options = TranslatorOptions(sourceLanguage: inputLanguage, targetLanguage: outputLanguage)
                self.translator = Translator.translator(options: options)
                self.translate()
            } else if let error = error {
                self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                print(error)
            }
        })
    }
    func recordAndRecognizeSpeechSpanish() {
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            self.sendAlert(title: "Speech Recognizer Error", message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not currently available. Check back at a later time.")
            return
        }
        recognitionTask = speechRecognizerSpan?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {

                print("result",result)
                let bestString = result.bestTranscription.formattedString
                let inputLanguage = TranslateLanguage.spanish
                let outputLanguage = TranslateLanguage.english
                print("bestString",bestString)
                self.txtComment.text = bestString
                let options = TranslatorOptions(sourceLanguage: inputLanguage, targetLanguage: outputLanguage)
                self.translator = Translator.translator(options: options)
                self.translate()
            } else if let error = error {
                self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                print(error)
            }
        })
    }
    func recordAndRecognizeSpeechGerman() {
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            self.sendAlert(title: "Speech Recognizer Error", message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not currently available. Check back at a later time.")
            return
        }
        recognitionTask = speechRecognizerGerman?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {

                print("result",result)
                let bestString = result.bestTranscription.formattedString
                let inputLanguage = TranslateLanguage.german
                let outputLanguage = TranslateLanguage.english
                print("bestString",bestString)
                self.txtComment.text = bestString
                let options = TranslatorOptions(sourceLanguage: inputLanguage, targetLanguage: outputLanguage)
                self.translator = Translator.translator(options: options)
                self.translate()
            } else if let error = error {
                self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                print(error)
            }
        })
    }
    func configureAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch { }
    }
    func recordAndRecognizeSpeechHindi() {
        let node = audioEngine.inputNode
//        let recordingFormat = node.outputFormat(forBus: 0)
        let recordingFormat = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)
        configureAudioSession()
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            self.sendAlert(title: "Speech Recognizer Error", message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(title: "Speech Recognizer Error", message: "Speech recognition is not currently available. Check back at a later time.")
            return
        }
        recognitionTask = speechRecognizerHindi?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {

                print("result",result)
                let bestString = result.bestTranscription.formattedString
                let inputLanguage = TranslateLanguage.hindi
                let outputLanguage = TranslateLanguage.english
                print("bestString",bestString)
                self.txtComment.text = bestString
                let options = TranslatorOptions(sourceLanguage: inputLanguage, targetLanguage: outputLanguage)
                self.translator = Translator.translator(options: options)
                self.translate()
            } else if let error = error {
                self.sendAlert(title: "Speech Recognizer Error", message: "There has been a speech recognition error.")
                print(error)
            }
        })
    }
    @objc func updateSlider(){
        audioSlide.value = Float(player?.currentTime ?? 0)
//        print("Changing works",player?.currentTime)
        
    }
    @IBAction func btnPauseAudio(_ sender: UIButton) {
        player?.pause()
        timer?.invalidate()
        btnPlayPause.isHidden =  false
        btnPause.isHidden = true
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        btnPause.isHidden = true
        btnPlayPause.isHidden = false
        timeTimer?.invalidate()
        if player.isPlaying{
            viewOpened += 1
        }
        if ((audioPlayer?.isPlaying) != nil){
            btnRec.isHidden = false
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtParamValues{
            endEditing?()
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        recordView.isHidden = true
        playRecordView.isHidden = true
        btnRecStop.isHidden = true
        if Reachability.isConnectedToNetwork(){
            btnRec.isHidden = false
        }
        btnDeleteRec.isHidden = true
        horizontalConstaint.constant = 0
        if textView == txtComment{
            commentEndEdit?()
        }
    }
    //MARK: -Check Camera and Settings
    func checkCameraAccess(isAllowed: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for:.video) {
        case .denied:
            isAllowed(false)
        case .restricted:
            isAllowed(false)
        case .authorized:
            isAllowed(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { isAllowed($0) }
        default:
            print("There is no authorization")
        }
    }
    func presentCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }else{
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
  
    func presentCameraSettings() {
        let alert = UIAlertController.init(title: "Allow the Camera", message: "Move To the setting", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: { (_) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true)
    }
    //MARK: - Audio Record
    var disposable = Set<AnyCancellable>()
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    func convertAudio(){
        let input = getDocumentsDirectory().appendingPathComponent("sound.caf")
        let output = getDocumentsDirectory().appendingPathComponent("converted.mp3")
        print("Outputpath",output)
        let converter = MP3Converter()
        /// Run it in a different queue and update the UI when it's done executing, otherwise your UI will freeze.
        DispatchQueue.global(qos: .userInteractive).async {
                converter.convert(input: input, output: output)
                    .receive(on: DispatchQueue.global())
                    .sink(receiveCompletion: { result in
                        DispatchQueue.main.async {
                            if case .failure(let error) = result {
                                print("Conversion failed: \n\(error.localizedDescription)")
                                
                            }
                        }
                    }, receiveValue: { result in
                        DispatchQueue.main.async {
                            print(result)
                            print("File saved as converted.mp3 in \nthe documents directory.")
                            if let imageData = try? Data(contentsOf: result) {
                                self.mp3FileData = imageData // HERE IS YOUR IMAGE! Do what you want with it!
                                print(self.mp3FileData)
                            }
                        }
                    }).store(in: &self.disposable)
           
        }
    }
    
    var timeTimer: Timer?
    var milliseconds: Int = 0
    
    func setAudioName(){
        let fileMgr = FileManager.default
        
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.caf")
        
        let recordSettings =
        [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
              AVEncoderBitRateKey: 16,
            AVNumberOfChannelsKey: 2,
                  AVSampleRateKey: 44100.0,
                   ] as [String : Any]

        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(
                AVAudioSession.Category.playAndRecord)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        do {
            try audioRecorder = AVAudioRecorder(url: soundFileURL,
                                                settings: recordSettings as [String : AnyObject])
            audioRecorder?.prepareToRecord()
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
    }
    @objc func updateTimeLabel(timer: Timer) {
        milliseconds+=1
        let milli = (milliseconds % 60) + 39
        let sec = (milliseconds / 60) % 60
        let min = milliseconds / 3600
        lblRecTime.text = NSString(format: "%02d:%02d.%02d", min, sec, milli) as String
        
    }
    @IBAction func btnPlayRecording(_ sender: UIButton) {
        if audioRecorder?.isRecording == false {
            
            do {
                try audioPlayer = AVAudioPlayer(contentsOf:
                                                    (audioRecorder?.url)!)
            
                milliseconds = 0
                audioPlayer!.prepareToPlay()
                audioPlayer!.delegate = self
                audioPlayer!.play()
                recordedSlider.maximumValue = Float((self.audioPlayer?.duration ?? 0))
                timeTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(playRecSlider), userInfo: nil, repeats: true)
                
            } catch let error as NSError {
                print(String(describing: error))
                print("audioPlayer error: \(error.localizedDescription)")
            }
        }
    }
    @objc func playRecSlider(){
        recordedSlider.value = Float(audioPlayer?.currentTime ?? 0)
        print(Float(audioPlayer?.currentTime ?? 0))
    }
    
    
    @IBAction func btnDelRecording(_ sender: UIButton) {
        mp3FileData = Data()
//        btnRec.isHidden = false
        horizontalConstaint.constant = 0
//        btnDeleteRec.isHidden = true
        
        self.recordView.isHidden = true
        self.btnRecStop.isHidden = true
        self.playRecordView.isHidden = true
        self.btnRec.isHidden = false
        self.btnDeleteRec.isHidden = true
        
    }
    @IBAction func btnStopRecording(_ sender: UIButton) {
        recordView.isHidden = true
        playRecordView.isHidden = false
        btnRec.isHidden = false
        btnRecStop.isHidden = true
        btnDeleteRec.isHidden = false
        timeTimer?.invalidate()
        convertAudio()
        cancelRecording()
        if audioRecorder?.isRecording == true {
            audioRecorder?.stop()
        } else {
            audioPlayer?.stop()
        }
    }
    
    @IBAction func btnOpenRecView(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Select Language", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: UserDefaults.standard.string(forKey: "Lang") ?? "", style: .default){ [self] _ in
            if UserDefaults.standard.string(forKey: "Lang") == "Hindi"{
                recordAndRecognizeSpeechHindi()
            }else if UserDefaults.standard.string(forKey: "Lang") == "French"{
                recordAndRecognizeSpeechFrench()
            }else if UserDefaults.standard.string(forKey: "Lang") == "German"{
                recordAndRecognizeSpeechGerman()
            }else if UserDefaults.standard.string(forKey: "Lang") == "Spanish"{
                recordAndRecognizeSpeechSpanish()
            }
            UIView.animate(withDuration: 0.1) {
                self.txtComment.resignFirstResponder()
                self.recordView.isHidden = false
                self.btnRecStop.isHidden = false
                self.playRecordView.isHidden = true
                self.btnRec.isHidden = true
                self.btnDeleteRec.isHidden = true
                self.horizontalConstaint.constant = 100
                
            }
            timeTimer?.invalidate()
            milliseconds = 0
            lblRecTime.text = "00:00:00"
            timeTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTimeLabel), userInfo: nil, repeats: true)
            
            if audioRecorder?.isRecording == false {
                audioRecorder?.record()
            }
        })
        alert.addAction(UIAlertAction(title: "English", style: .default){ [self] _ in
            recordAndRecognizeSpeechEnglish()
            UIView.animate(withDuration: 0.1) {
                self.txtComment.resignFirstResponder()
                self.recordView.isHidden = false
                self.btnRecStop.isHidden = false
                self.playRecordView.isHidden = true
                self.btnRec.isHidden = true
                self.btnDeleteRec.isHidden = true
                self.horizontalConstaint.constant = 100
                
            }
            timeTimer?.invalidate()
            milliseconds = 0
            lblRecTime.text = "00:00:00"
            timeTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTimeLabel), userInfo: nil, repeats: true)
            
            if audioRecorder?.isRecording == false {
                audioRecorder?.record()
            }
        })
        self.present(alert, animated: true)
        
    }
    
    @IBAction func btnToChangeAudio(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AudioListVC") as! AudioListVC
        vc.audioId = paramId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error")
    }

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Audio Record Encode Error")
    }
}
extension PageViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imgeSelected.image = image
            setSelectedImage = image
            imgeSelected.image = setSelectedImage
        }
    }
}

