//
//  FillParamPageVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 28/06/22.
//

import UIKit
import CoreMedia

class FillParamPageVC: UIViewController ,UIPageViewControllerDataSource , UIPageViewControllerDelegate{
    
    
    var arrPageTitle = ["1", "2", "3"]
    var auditValues = String()
    var getallValues : [FillParamValue] = []
    //jsondata
    var auditName = String()
    var auditparams_id = Int()
    var assetModel = Int()
    var createdDate = String()
    var updateDate = String()
    var deleteAt = String()
    var audit_camera_photos = Int()
    var show_when_photos = Int()
    var show_when_comment = Int()
    var id = Int()
    var status = String()
    var param_disp_name = String()
    var param_type = String()
    var benchmark_range_low = Int()
    var benchmark_range_high = Int()
    var dropdown_values = String()
    var dropdown_reject_value = String()
    var dropdown_accept_value = String()
    var audit_comment = Int()
    var audit_camera_photos_key = String()
    var audit_comment_key = String()
    var validation = String()
    var totlatCount = String()
    var pageIndex = 0
    var pageViewController:UIPageViewController!
    var imageSelected = UIImage()
    var txtCommentAceept = String()
    var txtRejectPhoto = String()
    var txtCommentReject = String()
    var txtAcceptPhoto = String()
    var getTextFieldValues : [TextFieldValues] = []
    var auditCommentValue = [String]()
    var auditScheduleId = Int64()
    var productImage : URL!
    var mainTainType = String()
    var audit_param_audio_path = String()
    let audioPath = UserDefaults.standard.string(forKey: "audit_param_audio_path")
    var audit_audio_key = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        readJson()
        totlatCount = "/\(getallValues.count)"
        self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyPageViewController") as? UIPageViewController
       
        
        self.pageViewController.dataSource = self
        
        let initialContenViewController = self.pageTutorialAtIndex(0) as PageViewController
        
        self.pageViewController.setViewControllers([initialContenViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)

        self.addChild(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.view.frame = self.view.frame
        self.pageViewController.didMove(toParent: self)
        removeSwipeGesture()
    }
   
    func removeSwipeGesture(){
        for view in self.pageViewController!.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
        
    func readJson(){
        let newData = auditValues.replacingOccurrences(of: "\n", with: "\\n")
        if let data = newData.data(using: String.Encoding.utf8) {
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: Any]]
                print(response)
                response.compactMap { data in
                    auditName = data["audit_params_name"] as? String ?? ""
                    auditparams_id = data["audit_params_id"] as? Int ?? 0
                    assetModel = (data["asset_model"] as! NSString).integerValue
                    createdDate = data["created_date"] as? String ?? ""
                    updateDate = data["updated_date"] as? String ?? ""
                    deleteAt = data["deleted_at"] as? String ?? ""
                    audit_camera_photos = (data["audit_camera_photos"] as? NSString)?.integerValue ?? 0
                    show_when_photos = data["show_when_photos"] as? Int ?? 0
                    show_when_comment = data["show_when_comment"] as? Int ?? 0
                    id = data["id"] as? Int ?? 0
                    status = data["status"] as? String ?? ""
                    param_disp_name = data["param_disp_name"] as? String ?? ""
                    param_type = data["param_type"] as? String ?? ""
                    benchmark_range_low = data["benchmark_range_low"] as? Int ?? 0
                    benchmark_range_high = data["benchmark_range_high"] as? Int ?? 0
                    dropdown_values = data["dropdown_values"] as? String ?? ""
                    dropdown_reject_value = data["dropdown_reject_value"] as? String ?? ""
                    dropdown_accept_value = data["dropdown_accept_value"] as? String ?? ""
                    audit_comment = data["audit_comment"] as? Int ?? 0
                    audit_camera_photos_key = data["audit_camera_photos_key"] as? String ?? ""
                    audit_comment_key = data["audit_comment_key"] as? String ?? ""
                    validation = data["validation"] as? String ?? ""
                    audit_param_audio_path = data["audit_param_audio_path"] as? String ?? ""
                    audit_audio_key = data["audit_audio_key"] as? String ?? ""
                    if param_type == "Drop-down"{
                        if let validationData = validation.data(using: String.Encoding.utf8) {
                            do {
                                let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [[String: Any]]
                                print(response)
                               
                            }catch{
                                print(error.localizedDescription)
                            }
                        }
                    }else if param_type == "Number"{
                        if let validationData = validation.data(using: String.Encoding.utf8) {
                            do {
                                let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [String: Any]
                                txtCommentAceept = response?["textfield_accept_comment_validation"] as? String ?? ""
                                txtRejectPhoto = response?["textfield_reject_photos_validation"] as? String ?? ""
                                txtCommentReject = response?["textfield_reject_comment_validation"] as? String ?? ""
                                txtAcceptPhoto = response?["textfield_aacept_photos_validation"] as? String ?? ""
                                getTextFieldValues.append(TextFieldValues(txtCommentAceept: txtCommentAceept, txtAcceptphotos: txtAcceptPhoto, txtRejectphotos: txtRejectPhoto, txtRejectcomment: txtCommentReject))
                            }catch{
                                print(error.localizedDescription)
                            }
                        }
                    }

                    getallValues.append((FillParamValue(auditName: auditName, auditparams_id: auditparams_id, assetModel: assetModel, createdDate: createdDate, updateDate: updateDate, deleteAt: deleteAt, audit_camera_photos: audit_camera_photos, show_when_photos: show_when_photos, show_when_comment: show_when_comment, id: id, status: status, param_disp_name: param_disp_name, param_type: param_type, benchmark_range_low: benchmark_range_low, benchmark_range_high: benchmark_range_high, dropdown_values: dropdown_values, dropdown_reject_value: dropdown_reject_value, dropdown_accept_value: dropdown_accept_value, audit_comment: audit_comment, audit_camera_photos_key: audit_camera_photos_key, audit_comment_key: audit_comment_key , validation: validation ,audit_param_audio_path: audit_param_audio_path , audit_audio_key: audit_audio_key)))
                    print(getallValues.count)
                    
                }
                
            } catch {
                print("ERROR \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: - PageVC Delegate

    let defaults = UserDefaults.standard
    var arrayVAl = [PageValue]()
    var dropDownAcceptComment = String()
    var dropDownAcceptPhoto = String()
    var dropDownRejectComment = String()
    var dropDownRejectPhoto = String()
    var getDroppDownValues : [DropDownValues] = []
    var auditPhotoValue = [UIImage]()
    var auditCommentKey = [String]()
    var auditPhotoKey = [String]()
    var auditAudioKey = [String]()
    var auditAudioValue = [Data]()
    func pageTutorialAtIndex(_ indexVal: Int) ->PageViewController
    {
        let pageVC = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! PageViewController
       
        let arryInd = "\(indexVal + 1)" + totlatCount
        pageVC.strTitle = arryInd
        let listedVal = getallValues[indexVal]
        pageVC.setLbldata = listedVal.param_disp_name.components(separatedBy: "%").joined(separator: " ").firstCapitalized
        pageVC.paramId = listedVal.id
        pageVC.audio = listedVal.audit_param_audio_path
        let path = (audioPath ?? "") + listedVal.audit_param_audio_path
        pageVC.setAudioUrl = path
        
        
        if listedVal.param_type == "Drop-down"{
            pageVC.setBtnHide = false
            pageVC.setPlaceholder = "Select"
            pageVC.setImgdrop = false
        }else{
            pageVC.dropDown.hide()
            pageVC.setBtnHide = true
            pageVC.setPlaceholder = "Enter"
            pageVC.setImgdrop = true
        }
        if arrayVAl.indices.contains(indexVal) {
            pageVC.setParam = arrayVAl[indexVal].paramValue
       
            if listedVal.param_type == "Drop-down"{
                getDroppDownValues.removeAll()
                if let validationData = arrayVAl[indexVal].validation.data(using: String.Encoding.utf8) {
                    do {
                        let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [[String: Any]]
                        response?.map { data in
                            let dropDownvalidation = data["validation"] as? [String : Any]
                            dropDownvalidation.map { valid in
                                dropDownAcceptComment = valid["dropdown_values_accept_comment_validation"] as? String ?? ""
                                dropDownAcceptPhoto = valid["dropdown_values_aacept_photos_validation"] as? String ?? ""
                                dropDownRejectComment = valid["dropdown_values_reject_comment_validation"] as? String ?? ""
                                dropDownRejectPhoto = valid["dropdown_values_reject_photos_validation"] as? String ?? ""
                                
                                getDroppDownValues.append(DropDownValues(dropDownCommentAceept: dropDownAcceptComment, dropDownAcceptphotos: dropDownAcceptPhoto, dropDownRejectphotos: dropDownRejectPhoto, dropDownRejectcomment: dropDownRejectComment))
                            }
                        }
                    }catch{
                        print(error.localizedDescription)
                    }
                }
                let dropdownList = getDroppDownValues[arrayVAl[indexVal].indexId]
                print("dropdownList",dropdownList)
                if(dropdownList.dropDownRejectcomment == "Required"){
                    pageVC.hideComment = ""
                }
                if(dropdownList.dropDownRejectphotos == "Required"){
                    pageVC.hidePhoto = ""
                }
                if(dropdownList.dropDownAcceptphotos == "Required"){
                    pageVC.hidePhoto = ""
                }
                if(dropdownList.dropDownCommentAceept == "Required"){
                    pageVC.hideComment = ""
                }
                if(dropdownList.dropDownRejectcomment == "NotRequired" || dropdownList.dropDownRejectcomment == "Not Required"){
                    pageVC.hideComment = "Hide"
                }
                if(dropdownList.dropDownRejectphotos == "NotRequired" || dropdownList.dropDownRejectphotos == "Not Required"){
                    pageVC.hidePhoto = "Hide"
                }
                if(dropdownList.dropDownAcceptphotos == "NotRequired" || dropdownList.dropDownAcceptphotos == "Not Required"){
                    pageVC.hidePhoto = "Hide"
                }
                if(dropdownList.dropDownCommentAceept == "NotRequired" || dropdownList.dropDownCommentAceept == "Not Required"){
                    pageVC.hideComment = "Hide"
                }
                if pageVC.hideComment == ""{
                    pageVC.setComment = auditCommentValue[indexVal]
                }
                if pageVC.hidePhoto == ""{
                    pageVC.setSelectedImage = auditPhotoValue[indexVal]
                }
            }else{
//                pageVC.setParam = arrayVAl[indexVal].paramValue
                let textfieldInt: Int? = Int(pageVC.setParam)
               if (textfieldInt ?? 0 == 0) || (pageVC.setParam == ""){
                    pageVC.hideComment = "Hide"
                    pageVC.hidePhoto = "Hide"
                    pageVC.setParam = ""
                    pageVC.imgeSelected.image = UIImage(named: "default")

                }
                if (textfieldInt ?? 0 < listedVal.benchmark_range_low) || (textfieldInt ?? 0 > listedVal.benchmark_range_high){
                    self.getTextFieldValues.map { data in
                        if data.txtCommentAceept == "Required"{
                            pageVC.hideComment = ""
                        }else{
                            pageVC.hideComment = "Hide"
                        }
                        if data.txtAcceptphotos == "Required"{
                            pageVC.hidePhoto = ""
                        }else{
                            pageVC.hidePhoto = "Hide"
                        }
                        if data.txtRejectphotos == "Required"{
                            pageVC.hidePhoto = ""
                        }else{
                            pageVC.hidePhoto = "Hide"
                        }
                        if data.txtRejectcomment == "Required"{
                            pageVC.hideComment = ""
                        }else{
                            pageVC.hideComment = "Hide"
                        }
                     
                        if pageVC.hideComment == ""{
                            pageVC.setComment = auditCommentValue[indexVal]
                        }
                        if pageVC.hidePhoto == ""{
                            pageVC.setSelectedImage = auditPhotoValue[indexVal]
                        }
                    }
                }
                else{
                    pageVC.hideComment = "Hide"
                    pageVC.hidePhoto = "Hide"
                }

            }
        }else {
           print("None")
            pageVC.hideComment = "Hide"
            pageVC.hidePhoto = "Hide"
        }
        //Textfield
        pageVC.endEditing = { [self] in
            
            pageVC.dropDown.hide()
            
            if listedVal.param_type == "Number"{
                
                let textfieldInt: Int? = Int(pageVC.txtParamValues.text!)
                if arrayVAl.indices.contains(indexVal) {
                    arrayVAl.remove(at: indexVal)
                }
                arrayVAl.insert(PageValue(paramValue: pageVC.txtParamValues.text ?? ""), at: indexVal)

                if (textfieldInt ?? 0 == 0) || (pageVC.txtParamValues.text == ""){
                    pageVC.viewComment.isHidden = true
                    pageVC.viewImage.isHidden = true
                    pageVC.txtParamValues.text = ""
                    pageVC.imgeSelected.image = UIImage(named: "default")
                    return
                }
                if (textfieldInt ?? 0 < listedVal.benchmark_range_low) || (textfieldInt ?? 0 > listedVal.benchmark_range_high){
                    self.getTextFieldValues.map { data in
                        if data.txtCommentAceept == "Required" || data.txtCommentAceept == "Optional"{
                            pageVC.viewComment.isHidden = false
                            pageVC.viewOpened += 2
                        }
                        if data.txtAcceptphotos == "Required"  || data.txtAcceptphotos == "Optional"{
                            pageVC.viewImage.isHidden = false
                        }
                        if data.txtRejectphotos == "Required" || data.txtRejectphotos == "Optional"{
                            pageVC.viewImage.isHidden = false
                        }
                        if data.txtRejectcomment == "Required" || data.txtRejectcomment == "Optional"{
                            pageVC.viewComment.isHidden = false
                            pageVC.viewOpened += 2
                        }
                    }
                }
                else{
                    pageVC.viewComment.isHidden = true
                    pageVC.viewImage.isHidden = true
                }
            }
        }
        pageVC.pageIndex = indexVal
        
        //MARK: -ToNextPage
        pageVC.toNext = { [self] in
            if pageVC.txtParamValues.text == ""{
                GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
            }else if pageVC.viewComment.isHidden == false{
                if pageVC.txtComment.text == "" {
                    GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
                }else{
                    pageVC.player = nil
                    pageVC.timer?.invalidate()
                    print(pageVC.mp3FileData)
                    if auditCommentValue.indices.contains(indexVal) {
                        auditCommentValue.remove(at: indexVal)
                    }
                    if auditCommentKey.indices.contains(indexVal) {
                        auditCommentKey.remove(at: indexVal)
                    }
                    auditCommentValue.insert(pageVC.txtComment.text, at: indexVal)
                    auditCommentKey.insert(listedVal.audit_comment_key, at: indexVal)
                    if auditPhotoValue.indices.contains(indexVal) {
                        auditPhotoValue.remove(at: indexVal)
                    }
                    if auditPhotoKey.indices.contains(indexVal) {
                        auditPhotoKey.remove(at: indexVal)
                    }
                    auditAudioKey.insert(listedVal.audit_audio_key, at: indexVal)
                    auditPhotoKey.insert(listedVal.audit_camera_photos_key, at: indexVal)
                    if pageVC.viewImage.isHidden == true{
                        auditPhotoValue.insert(UIImage(), at: indexVal)
                    }else{
                        auditPhotoValue.insert(pageVC.imgeSelected.image ?? UIImage(), at: indexVal)
                    }
                    print("mp3FileData",pageVC.mp3FileData)
//                    if auditAudioValue.indices.contains(indexVal) {
//                        auditAudioValue.remove(at: indexVal)
//                    }
                    auditAudioValue.insert(pageVC.mp3FileData, at: indexVal)
                    
                    let cur = pageViewController.viewControllers![0] as! PageViewController
                    let p = pageViewController(pageViewController, viewControllerAfter: cur)
                    if p != nil{
                        pageViewController.setViewControllers([p!], direction: .forward, animated: false, completion: nil)
                    }else{
                        let vc = storyboard?.instantiateViewController(withIdentifier: "TakeAssetPhotoVC") as! TakeAssetPhotoVC
                        pageVC.player = nil
                        pageVC.timer?.invalidate()
                        vc.arrayVAl = arrayVAl
                        vc.auditPhotoKey = auditPhotoKey
                        vc.auditPhotoValue = auditPhotoValue
                        vc.auditCommentValue = auditCommentValue
                        vc.auditCommentKey = auditCommentKey
                        vc.auditAudioKey = auditAudioKey
                        vc.auditAudioValue = auditAudioValue
                        vc.auditValues = auditValues
                        vc.auditScheduleId = auditScheduleId
                        vc.productImage = productImage
                        vc.maintainType = mainTainType
                        navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else if pageVC.viewImage.isHidden == false{
                if pageVC.imgeSelected.image == UIImage(named: "default"){
                    GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
                }else{
                    pageVC.player = nil
                    pageVC.timer?.invalidate()
                    if auditCommentValue.indices.contains(indexVal) {
                        auditCommentValue.remove(at: indexVal)
                    }
                    if auditCommentKey.indices.contains(indexVal) {
                        auditCommentKey.remove(at: indexVal)
                    }
                    auditCommentValue.insert(pageVC.txtComment.text, at: indexVal)
                    auditCommentKey.insert(listedVal.audit_comment_key, at: indexVal)
                    if auditPhotoValue.indices.contains(indexVal) {
                        auditPhotoValue.remove(at: indexVal)
                    }
                    if auditPhotoKey.indices.contains(indexVal) {
                        auditPhotoKey.remove(at: indexVal)
                    }
//                    if auditAudioKey.indices.contains(indexVal) {
//                        auditAudioKey.remove(at: indexVal)
//                    }
                    auditAudioKey.insert(listedVal.audit_audio_key, at: indexVal)
                    auditPhotoKey.insert(listedVal.audit_camera_photos_key, at: indexVal)
                    if pageVC.viewImage.isHidden == true{
                        auditPhotoValue.insert(UIImage(), at: indexVal)
                    }else{
                        auditPhotoValue.insert(pageVC.imgeSelected.image ?? UIImage(), at: indexVal)
                    }
                    print("mp3FileData",pageVC.mp3FileData)
//                    if auditAudioValue.indices.contains(indexVal) {
//                        auditAudioValue.remove(at: indexVal)
//                    }
                    auditAudioValue.insert(pageVC.mp3FileData, at: indexVal)
                    let cur = pageViewController.viewControllers![0] as! PageViewController
                    let p = pageViewController(pageViewController, viewControllerAfter: cur)
                    if p != nil{
                        pageViewController.setViewControllers([p!], direction: .forward, animated: false, completion: nil)
                    }else{
                        let vc = storyboard?.instantiateViewController(withIdentifier: "TakeAssetPhotoVC") as! TakeAssetPhotoVC
                        pageVC.player = nil
                        pageVC.timer?.invalidate()
                        vc.arrayVAl = arrayVAl
                        vc.auditPhotoKey = auditPhotoKey
                        vc.auditPhotoValue = auditPhotoValue
                        vc.auditCommentValue = auditCommentValue
                        vc.auditCommentKey = auditCommentKey
                        vc.auditAudioKey = auditAudioKey
                        vc.auditAudioValue = auditAudioValue
                        vc.auditValues = auditValues
                        vc.auditScheduleId = auditScheduleId
                        vc.productImage = productImage
                        vc.maintainType = mainTainType
                        navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } else{
                pageVC.player = nil
                pageVC.timer?.invalidate()
                if auditCommentValue.indices.contains(indexVal) {
                    auditCommentValue.remove(at: indexVal)
                }
                if auditCommentKey.indices.contains(indexVal) {
                    auditCommentKey.remove(at: indexVal)
                }
                auditCommentValue.insert(pageVC.txtComment.text, at: indexVal)
                auditCommentKey.insert(listedVal.audit_comment_key, at: indexVal)
                if auditPhotoValue.indices.contains(indexVal) {
                    auditPhotoValue.remove(at: indexVal)
                }
//                if auditAudioValue.indices.contains(indexVal) {
//                    auditAudioValue.remove(at: indexVal)
//                }
                if auditPhotoKey.indices.contains(indexVal) {
                    auditPhotoKey.remove(at: indexVal)
                }
                auditPhotoKey.insert(listedVal.audit_camera_photos_key, at: indexVal)
//                if auditAudioKey.indices.contains(indexVal) {
//                    auditAudioKey.remove(at: indexVal)
//                }
                auditAudioKey.insert(listedVal.audit_audio_key, at: indexVal)
                if pageVC.viewImage.isHidden == true{
                    auditPhotoValue.insert(UIImage(), at: indexVal)
                }else{
                    auditPhotoValue.insert(pageVC.imgeSelected.image ?? UIImage(), at: indexVal)
                }
                print("mp3FileData",pageVC.mp3FileData)
                auditAudioValue.insert(pageVC.mp3FileData, at: indexVal)
                let cur = pageViewController.viewControllers![0] as! PageViewController
                let p = pageViewController(pageViewController, viewControllerAfter: cur)
                if p != nil{
                    pageViewController.setViewControllers([p!], direction: .forward, animated: false, completion: nil)
                }else{
                    let vc = storyboard?.instantiateViewController(withIdentifier: "TakeAssetPhotoVC") as! TakeAssetPhotoVC
                    pageVC.player = nil
                    pageVC.timer?.invalidate()
                    vc.arrayVAl = arrayVAl
                    vc.auditPhotoKey = auditPhotoKey
                    vc.auditPhotoValue = auditPhotoValue
                    vc.auditCommentValue = auditCommentValue
                    vc.auditCommentKey = auditCommentKey
                    vc.auditAudioKey = auditAudioKey
                    vc.auditAudioValue = auditAudioValue
                    vc.auditValues = auditValues
                    vc.auditScheduleId = auditScheduleId
                    vc.productImage = productImage
                    vc.maintainType = mainTainType
                    navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        //MARK: -ToPrevPage
        pageVC.toPrev = { [self] in
            if auditCommentValue.indices.contains(indexVal) {
                auditCommentValue.remove(at: indexVal)
            }
            if auditCommentKey.indices.contains(indexVal) {
                auditCommentKey.remove(at: indexVal)
            }
            auditCommentValue.insert(pageVC.txtComment.text, at: indexVal)
            auditCommentKey.insert(listedVal.audit_comment_key, at: indexVal)
            if auditPhotoValue.indices.contains(indexVal) {
                auditPhotoValue.remove(at: indexVal)
            }
            if auditPhotoKey.indices.contains(indexVal) {
                auditPhotoKey.remove(at: indexVal)
            }
            auditPhotoKey.insert(listedVal.audit_camera_photos_key, at: indexVal)
//            if auditAudioKey.indices.contains(indexVal) {
//                auditAudioKey.remove(at: indexVal)
//            }
            auditAudioKey.insert(listedVal.audit_audio_key, at: indexVal)
            if pageVC.viewImage.isHidden == true{
                auditPhotoValue.insert(UIImage(), at: indexVal)
            }else{
                auditPhotoValue.insert(pageVC.imgeSelected.image ?? UIImage(), at: indexVal)
            }
            print("mp3FileData",pageVC.mp3FileData)
//            if auditAudioValue.indices.contains(indexVal) {
//                auditAudioValue.remove(at: indexVal)
//            }
            auditAudioValue.insert(pageVC.mp3FileData, at: indexVal)
            if pageVC.viewComment.isHidden == false  {
                if pageVC.txtComment.text == "" {
                    GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
                }else{
                    if pageVC.viewImage.isHidden == false{
                        if pageVC.imgeSelected.image == UIImage(named: "default") {
                            GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
                        }else{
                            
                            let cur = self.pageViewController.viewControllers![0]
                            let p = self.pageViewController(self.pageViewController, viewControllerBefore: cur)
                            if p != nil{
                                pageVC.player = nil
                                pageVC.timer?.invalidate()
                                self.pageViewController.setViewControllers([p!], direction: .reverse, animated: false, completion: nil)
                            }
                        }
                    }else{
                       
                        let cur = self.pageViewController.viewControllers![0]
                        let p = self.pageViewController(self.pageViewController, viewControllerBefore: cur)
                        if p != nil{
                            pageVC.player = nil
                            pageVC.timer?.invalidate()
                            self.pageViewController.setViewControllers([p!], direction: .reverse, animated: false, completion: nil)
                        }
                    }
                }
            }else if pageVC.viewImage.isHidden == false{
                if pageVC.imgeSelected.image == UIImage(named: "default") {
                    GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
                }else{
                    if pageVC.viewComment.isHidden == false{
                        if pageVC.txtComment.text == "" {
                            GlobalFunction.showAlert(message: "Please fill all the data", actionTitle: "Ok", viewcontroller: self)
                        }else{
                            
                            let cur = self.pageViewController.viewControllers![0]
                            let p = self.pageViewController(self.pageViewController, viewControllerBefore: cur)
                            if p != nil{
                                pageVC.player = nil
                                pageVC.timer?.invalidate()
                                self.pageViewController.setViewControllers([p!], direction: .reverse, animated: false, completion: nil)
                            }
                        }
                    }else{
                        
                        let cur = self.pageViewController.viewControllers![0]
                        let p = self.pageViewController(self.pageViewController, viewControllerBefore: cur)
                        if p != nil{
                            pageVC.player = nil
                            pageVC.timer?.invalidate()
                            self.pageViewController.setViewControllers([p!], direction: .reverse, animated: false, completion: nil)
                        }
                    }
                }
                
            }else{
               
                let cur = self.pageViewController.viewControllers![0]
                let p = self.pageViewController(self.pageViewController, viewControllerBefore: cur)
                if p != nil{
                    pageVC.player = nil
                    pageVC.timer?.invalidate()
                    self.pageViewController.setViewControllers([p!], direction: .reverse, animated: false, completion: nil)
                }
            }
            
        }
        
        //OpenDropDown
        pageVC.openDropDown = {
            let dropdownValue = listedVal.dropdown_values.components(separatedBy: ",")
            pageVC.dropDown.dataSource = dropdownValue
            self.getDroppDownValues.removeAll()
            pageVC.dropDown.show()
            pageVC.dropDown.anchorView = pageVC.dropDownView
            pageVC.dropDown.selectionAction = { [self] (index: Int, item: String) in
                pageVC.txtParamValues.text = item
                let validationData = listedVal.validation
               
                if arrayVAl.indices.contains(indexVal) {
                    arrayVAl.remove(at: indexVal)
                }
                arrayVAl.insert(PageValue(indexId: index, paramValue: pageVC.txtParamValues.text ?? "", validation: validationData ?? ""), at: indexVal)
                if let validationData = validationData?.data(using: String.Encoding.utf8) {
                    do {
                        let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [[String: Any]]
                        print("responseDropdown",response)
                        response?.map { data in
                            let dropDownvalidation = data["validation"] as? [String : Any]
                            dropDownvalidation.map { valid in
                                dropDownAcceptComment = valid["dropdown_values_accept_comment_validation"] as? String ?? ""
                                dropDownAcceptPhoto = valid["dropdown_values_aacept_photos_validation"] as? String ?? ""
                                dropDownRejectComment = valid["dropdown_values_reject_comment_validation"] as? String ?? ""
                                dropDownRejectPhoto = valid["dropdown_values_reject_photos_validation"] as? String ?? ""
                                
                                getDroppDownValues.append(DropDownValues(dropDownCommentAceept: dropDownAcceptComment, dropDownAcceptphotos: dropDownAcceptPhoto, dropDownRejectphotos: dropDownRejectPhoto, dropDownRejectcomment: dropDownRejectComment))
                            }
                        }
                    }catch{
                        print(error.localizedDescription)
                    }
                }
                let dropdownList = getDroppDownValues[index]
                if(dropdownList.dropDownRejectcomment == "Required" || dropdownList.dropDownRejectcomment == "Optional"){
                    pageVC.viewComment.isHidden = false
                    pageVC.viewOpened += 1
                    pageVC.txtComment.becomeFirstResponder()
                }
                if(dropdownList.dropDownRejectphotos == "Required" || dropdownList.dropDownRejectphotos == "Optional"){
                    pageVC.viewImage.isHidden = false
                }
                if(dropdownList.dropDownAcceptphotos == "Required" || dropdownList.dropDownAcceptphotos == "Optional"){
                    pageVC.viewImage.isHidden = false
                }
                if(dropdownList.dropDownCommentAceept == "Required" || dropdownList.dropDownCommentAceept == "Optional"){
                    pageVC.viewComment.isHidden = false
                    pageVC.viewOpened += 1
                    pageVC.txtComment.becomeFirstResponder()
                }
                if(dropdownList.dropDownRejectcomment == "NotRequired" || dropdownList.dropDownRejectcomment == "Not Required"){
                    pageVC.viewComment.isHidden = true
                }
                if(dropdownList.dropDownRejectphotos == "NotRequired" || dropdownList.dropDownRejectphotos == "Not Required"){
                    pageVC.viewImage.isHidden = true

                }
                if(dropdownList.dropDownAcceptphotos == "NotRequired" || dropdownList.dropDownAcceptphotos == "Not Required"){
                    pageVC.viewImage.isHidden = true
                }
                if(dropdownList.dropDownCommentAceept == "NotRequired" || dropdownList.dropDownCommentAceept == "Not Required"){
                    pageVC.viewComment.isHidden = true
                    
                }
            }
        }
        
        return pageVC
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let viewController = viewController as! PageViewController
        var index = viewController.pageIndex as Int
        
        if(index == 0 || index == NSNotFound)
        {
            return nil
        }
        
        index -= 1
        
        return self.pageTutorialAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let viewController = viewController as! PageViewController
        var index = viewController.pageIndex as Int
        if((index == NSNotFound))
        {
            return nil
        }
        index += 1
        
        if(index == getallValues.count)
        {
            return nil
        }
        return self.pageTutorialAtIndex(index)
    }
}

