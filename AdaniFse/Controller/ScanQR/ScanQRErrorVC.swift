//
//  ScanQRErrorVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 27/05/22.
//

import UIKit

class ScanQRErrorVC: UIViewController {

    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var btnError: UIButton!
    @IBOutlet weak var errorView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnError.setCorner(_tp: 10)
        errorView.setCorner(_tp: 20)
        errorView.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 4, shadowOffset: CGSize.zero)
        if #available(iOS 11.0, *) {
            btnError.backgroundColor = Constant.Color.color_launch
            lblErrorMsg.textColor = Constant.Color.color_grey
            lblError.textColor = Constant.Color.color_red
            errorView.backgroundColor = Constant.Color.color_bg
            superView.backgroundColor = Constant.Color.color_bg
            topView.backgroundColor = Constant.Color.color_bg
        }
    }

    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popToViewController(ofClass: DashBoardVC.self)
    }
    
    @IBAction func btnToScanQR(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRVC
        navigationController?.pushViewController(vc, animated: true)
    }
}
