//
//  ScanQRVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 13/05/22.
//

import UIKit
import AVKit
import SQLite
class ScanQRVC: UIViewController ,AVCaptureMetadataOutputObjectsDelegate{
    
    @IBOutlet weak var txtAssetTagId: UITextField!
    @IBOutlet weak var scanImg: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnTorch: UIButton!
    @IBOutlet weak var scanImgIcon: UIImageView!
    var productImage : URL!
    var assetTagId = Int64()
    var assettag = String()
    var isFromAssetDetail = false
    var captureSession : AVCaptureSession!
    var previewLayer : AVCaptureVideoPreviewLayer!
    var val = 0
    var auditValues = String()
    var auditScheduleId = Int64()
    var auditVal = String()
    var auditTransVal = String()
    var urlImage = String()
    var productName = String()
    var location = String()
    var warranty = Int64()
    var supplier = String()
    var purchaseDate = String()
    var modalNo = String()
    var category = String()
    var companyName = String()
    var auditImageUrl = String()
    var maintainanceType = String()
    var completedJobs = CompletedJobsVC()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    override func viewDidLoad() {
        super.viewDidLoad()
        btnClose.isUserInteractionEnabled = true
        txtAssetTagId.autocorrectionType = .no
        captureSession = AVCaptureSession()
        txtAssetTagId.setBorder(.white, width: 1)
        txtAssetTagId.setCorner(_tp: 10)
        txtAssetTagId.textColor = .white
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            txtAssetTagId.setLeftPaddingPoints(10)
        }else{
            txtAssetTagId.setRightPaddingPoints(10)
        }
        txtAssetTagId.attributedPlaceholder = NSAttributedString(
            string: "Tag ID",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
            
        )
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let videoCapture = AVCaptureDevice.default(for: .video) else {
                self.failed()
                print("Your device is not applicable for video capture")
                return
            }
            
            let avCaptureInput : AVCaptureInput
            do {
                avCaptureInput = try AVCaptureDeviceInput(device: videoCapture)
            }catch{
                self.failed()
                print("Your device cannot give video input")
                return
            }
            if self.captureSession.canAddInput(avCaptureInput) {
                self.captureSession.addInput(avCaptureInput)
            }else{
                self.failed()
                print("Your device cannot add video input")
                return
            }
            
            let metaDataOutput = AVCaptureMetadataOutput()
            if (self.captureSession.canAddOutput(metaDataOutput)) {
                self.captureSession.addOutput(metaDataOutput)
                metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                let barCodeTypes = [AVMetadataObject.ObjectType.upce,
                                    AVMetadataObject.ObjectType.code39,
                                    AVMetadataObject.ObjectType.code39Mod43,
                                    AVMetadataObject.ObjectType.ean13,
                                    AVMetadataObject.ObjectType.ean8,
                                    AVMetadataObject.ObjectType.code93,
                                    AVMetadataObject.ObjectType.code128,
                                    AVMetadataObject.ObjectType.pdf417,
                                    AVMetadataObject.ObjectType.qr,
                                    AVMetadataObject.ObjectType.aztec
                ]
                metaDataOutput.metadataObjectTypes = barCodeTypes
                
            }else{
                return
            }
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            self.previewLayer.frame = self.scanImg.frame
            self.previewLayer.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(self.previewLayer)
            self.view.bringSubviewToFront(self.topView)
            self.view.bringSubviewToFront(self.scanImg)
            self.view.bringSubviewToFront(self.scanImgIcon)
            self.view.bringSubviewToFront(self.btnTorch)
            self.view.bringSubviewToFront(self.lblTitle)
            self.view.bringSubviewToFront(self.btnClose)
            self.view.bringSubviewToFront(self.txtAssetTagId)
            self.view.bringSubviewToFront(self.btnNext)
            DispatchQueue.global(qos:.background).async {
                self.captureSession.startRunning()
            }
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        completedJobs.createDB()
//        completedJobs.insertCompleted()
//        completedJobs.readValuesCompleted()
        if captureSession.isRunning == false{
            DispatchQueue.global(qos:.background).async {
                self.captureSession.startRunning()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if captureSession.isRunning == true{
            captureSession.stopRunning()
        }
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanner not supported", message: "Please use a device with a camera. Because this device does not support scanning a code", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        let current = dateFormatter.string(from: Date())
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            let url = NSURL(string: stringValue)
            let lastPath = url?.lastPathComponent
            print(url)
            if isFromAssetDetail == true{
                if assetTagId != Int64(lastPath ?? "") {
                    let vc2 = storyboard?.instantiateViewController(withIdentifier: "ScanQRErrorVC") as! ScanQRErrorVC
                    navigationController?.pushViewController(vc2, animated: true)
                }else{
                    let storyBoard = UIStoryboard(name: "Detail", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "FillParamPageVC") as! FillParamPageVC
                    vc.auditValues = auditValues
                    vc.auditScheduleId = auditScheduleId
                    vc.productImage = productImage
                    vc.mainTainType = maintainanceType
                    navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                let assetTagID = Int64(lastPath ?? "") ?? 0
                assetTagId = assetTagID
                var isfromAsset = Bool()
                do{
//                    if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first{
//                        db = try Connection("\(path)/PreventiveJobs.sqlite3")
                        for row in try db!.prepare("SELECT * FROM PreventiveJobs"){
                            print(row)
                        }
                        for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE asset_tag_id = '\(assetTagId)'") {
                            assettag = row[3] as! String
                            productName = row[2] as! String
                            location = row[9] as! String
                            warranty = row[8] as! Int64
                            supplier = row[13] as! String
                            assetTagId = row[17] as! Int64
                            purchaseDate = row[6] as! String
                            modalNo = row[5] as! String
                            category = row[12] as! String
                            companyName = row[13] as! String
                            // print(row)
                            val = 1
                        }
                        let dateFormatter: DateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        
                        if val == 1{
                            let vc = storyboard?.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
                            vc.assetTag = assettag
                            vc.productName = productName
                            vc.location = location
                            vc.warranty = warranty
                            vc.supplier = supplier
                            vc.purchaseDate = purchaseDate
                            vc.modalnNo = modalNo
                            vc.category = category
                            vc.companyName = companyName
                            vc.assetTagiD = assetTagId
                            let urlImg = URL(string: baseUrlImage + urlImage)
                            vc.auditImageUrl = urlImg
                            vc.isFromQrScanned = true
                            navigationController?.pushViewController(vc, animated: true)
                            val = 0
                            isfromAsset = true
                        }else{
                            let vc2 = storyboard?.instantiateViewController(withIdentifier: "ScanQRErrorVC") as! ScanQRErrorVC
                            navigationController?.pushViewController(vc2, animated: true)
                        }
//                    }
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }

                do{
                    if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first{
                        db = try Connection("\(path)/Completedjobs.sqlite3")
                        
                        for _ in try db!.prepare("SELECT * FROM Completedjobs"){
//                            print(row)
                        }
                        for row in try db!.prepare("SELECT * FROM Completedjobs WHERE asset_tag_id = '\(assetTagId)' AND last_audit_date LIKE '%\(current)%'") {
                              print(row)
                              print("auditValue",row[11] as! String)
                              auditVal = row[11] as! String
                              auditTransVal = row[17] as! String
                              auditScheduleId = row[0] as! Int64
                              urlImage = row[7] as! String
                              print("auditTransValue",row[17] as! String)
                              val = 3
                          }
                          for row in try db!.prepare("SELECT * FROM Completedjobs WHERE last_audit_date < '\(current)' AND asset_tag_id = '\(assetTagId)'") {
                              productName = row[15] as! String
                              location = row[9] as! String
                              warranty = row[8] as! Int64
                              supplier = row[13] as! String
                              urlImage = row[7] as! String
                              purchaseDate = row[6] as! String
                              modalNo = row[5] as! String
                              category = row[12] as! String
                              companyName = row[13] as! String
                              assettag = row[3] as! String
                              val = 2
                          }
                        if val == 2{
                            let vc = storyboard?.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
                            vc.assetTag = assettag
                            vc.productName = productName
                            vc.location = location
                            vc.warranty = warranty
                            vc.supplier = supplier
                            vc.purchaseDate = purchaseDate
                            vc.modalnNo = modalNo
                            vc.category = category
                            vc.companyName = companyName
                            let urlImg = URL(string: baseUrlImage + urlImage)
                            vc.auditImageUrl = urlImg
                            vc.isFromQrScanned = true
                            if isfromAsset == true{
                                return
                            }else{
                                navigationController?.pushViewController(vc, animated: true)
                                val = 0
                            }
                        }else if val == 3{
                            let stroyBoard = UIStoryboard(name: "Detail", bundle: nil)
                            let vc = stroyBoard.instantiateViewController(withIdentifier: "InspectionDetailVC") as! InspectionDetailVC
                            vc.auditTransactionVal = auditTransVal
                            vc.auditValue = auditVal
                            let urlImg = URL(string: baseUrlImage + urlImage)
                            vc.urlImg = urlImg
                            vc.audit_ScheduleId = auditScheduleId
                            navigationController?.pushViewController(vc, animated: true)
                            val = 0
                        }
                    }
                }catch{
                    print(String(describing: error))
                }
                
            }
        }else{
            print("Not able to read the code! Please try again or keep your device on bar code ")
        }
    }
    
    func found(code: String) {
        let ac = UIAlertController(title: "", message: "Your code is :- \(code)" , preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { alert in
            self.captureSession.startRunning()
        }))
        present(ac, animated: true)
        print(code)
    }

    //MARK: -Action
    @IBAction func btnConfirmAssetTag(_ sender: UIButton) {
        let current = dateFormatter.string(from: Date())
        let prevent = "preventive"
        let breakDown = "breakdown"
        DispatchQueue.main.async { [self] in
            if isFromAssetDetail == true{
                if assettag != txtAssetTagId.text?.uppercased() {
                    let vc2 = storyboard?.instantiateViewController(withIdentifier: "ScanQRErrorVC") as! ScanQRErrorVC
                    navigationController?.pushViewController(vc2, animated: true)
                }else{
                    let storyBoard = UIStoryboard(name: "Detail", bundle: nil)

                    let vc = storyBoard.instantiateViewController(withIdentifier: "FillParamPageVC") as! FillParamPageVC
                    vc.auditValues = auditValues
                    vc.auditScheduleId = auditScheduleId
                    vc.productImage = productImage
                    vc.mainTainType = maintainanceType
                    navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                do{

                    if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first{
                        db = try Connection("\(path)/Completedjobs.sqlite3")
                        for _ in try db!.prepare("SELECT * FROM Completedjobs"){
//                            print(row)
                        }
                        for row in try db!.prepare("SELECT * FROM Completedjobs WHERE asset_tag = '\(txtAssetTagId.text?.uppercased() ?? "")' AND last_audit_date LIKE '%\(current)%'") {
                            print(row)
                            print("auditValue",row[11] as! String)
                            auditVal = row[11] as! String
                            auditTransVal = row[17] as! String
                            auditScheduleId = row[0] as! Int64
                            urlImage = row[7] as! String
                            print("auditRow",row)
                            val = 2
                        }
                        for row in try db!.prepare("SELECT * FROM Completedjobs WHERE last_audit_date < '\(current)' AND asset_tag = '\(txtAssetTagId.text?.uppercased() ?? "")'") {
                            productName = row[15] as! String
                            location = row[9] as! String
                            warranty = row[8] as! Int64
                            supplier = row[13] as! String
                            urlImage = row[7] as! String
                            purchaseDate = row[6] as! String
                            modalNo = row[5] as! String
                            category = row[12] as! String
                            companyName = row[13] as! String
                            val = 1
                        }
                        if val == 2{
                            let stroyBoard = UIStoryboard(name: "Detail", bundle: nil)
                            let vc = stroyBoard.instantiateViewController(withIdentifier: "InspectionDetailVC") as! InspectionDetailVC
                            vc.auditTransactionVal = auditTransVal
                            vc.auditValue = auditVal
                            let urlImg = URL(string: baseUrlImage + urlImage)
                            vc.urlImg = urlImg
                            vc.audit_ScheduleId = auditScheduleId
                            navigationController?.pushViewController(vc, animated: true)
                            val = 0
                        }
                        if val == 1{
                            let vc = storyboard?.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
                            vc.assetTag = txtAssetTagId.text?.uppercased() ?? ""
                            vc.productName = productName
                            vc.location = location
                            vc.warranty = warranty
                            vc.supplier = supplier
                            vc.purchaseDate = purchaseDate
                            vc.modalnNo = modalNo
                            vc.category = category
                            vc.companyName = companyName
                            vc.assetTagiD = assetTagId
                            let urlImg = URL(string: baseUrlImage + urlImage)
                            vc.auditImageUrl = urlImg
                            vc.isFromQrScanned = false
                            navigationController?.pushViewController(vc, animated: true)
                            val = 0
                        }
                    }
                }catch{
                 print(String(describing: error))
                }
                do {
                    let dateFormatter: DateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    
//                    print("apiSelectDate",current)
                    for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE asset_tag = '\(txtAssetTagId.text?.uppercased() ?? "")' AND maintenance_type LIKE '%\(prevent)%' OR maintenance_type LIKE '%\(breakDown)%'") {
//                        assetTag = row[0] as! String
                        productName = row[2] as! String
                        location = row[9] as! String
                        warranty = row[8] as! Int64
                        supplier = row[13] as! String
                        assetTagId = row[17] as! Int64
                        purchaseDate = row[6] as! String
                        modalNo = row[5] as! String
                        category = row[12] as! String
                        companyName = row[13] as! String
                        maintainanceType = row[19] as! String
                        print(row)
                        val = 1
                    }
                   
                    if val == 1{
                        let vc = storyboard?.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
                        vc.assetTag = txtAssetTagId.text?.uppercased() ?? ""
                        vc.productName = productName
                        vc.location = location
                        vc.warranty = warranty
                        vc.supplier = supplier
                        vc.purchaseDate = purchaseDate
                        vc.modalnNo = modalNo
                        vc.category = category
                        vc.companyName = companyName
                        vc.assetTagiD = assetTagId
                        let urlImg = URL(string: baseUrlImage + urlImage)
                        vc.auditImageUrl = urlImg
                        vc.isFromQrScanned = false
                        navigationController?.pushViewController(vc, animated: true)
                        val = 0
                    }else{
                        let vc2 = storyboard?.instantiateViewController(withIdentifier: "ScanQRErrorVC") as! ScanQRErrorVC
                        navigationController?.pushViewController(vc2, animated: true)
                    }
                    
                }catch{
                    print(error.localizedDescription)
                    print(String(describing: error))
                }
            }
        }
        
    }
    
    @IBAction func btnCloseView(_ sender: UIButton) {
        navigationController?.popToViewController(ofClass: DashBoardVC.self)        
    }
    @IBAction func didTouchFlashButton(sender: AnyObject) {
        if let avDevice = AVCaptureDevice.default(for: AVMediaType.video) {
                if (avDevice.hasTorch) {
                    do {
                        try avDevice.lockForConfiguration()
                    } catch {
                        print("aaaa")
                    }

                    if avDevice.isTorchActive {
                        avDevice.torchMode = AVCaptureDevice.TorchMode.off
                        btnTorch.setBackgroundImage(UIImage(named: "fleshicon2"), for: .normal)
                    } else {
                        avDevice.torchMode = AVCaptureDevice.TorchMode.on
                        btnTorch.setBackgroundImage(UIImage(named: "fleshicon1"), for: .normal)
                    }
                }
                avDevice.unlockForConfiguration()
            }
    }
}
extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
