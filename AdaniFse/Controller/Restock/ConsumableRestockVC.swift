//
//  ConsumableRestockVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 16/09/22.
//

import UIKit
import DropDown
import Alamofire
import AVKit

class ConsumableRestockVC: UIViewController {

    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var btnUpDoc: UIButton!
    @IBOutlet weak var docImg: UIImageView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var btnReStck: UIButton!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var txtPurchaseCost: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtPurchaseOrd: UITextField!
    @IBOutlet weak var txtSupplier: UITextField!
    var selectList : [UserList] = []
    var resultList = [[String : Any]]()
    var dropDownSupplier = DropDown()
    var nameList = [String]()
    var idList = [Int]()
    var datePicker: UIDatePicker!
    var iselectedDate:String = ""
    var selectedDate: String = String()
    var supplierid = Int()
    var consumableId = Int()
    var filterNameList = [String]()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAcc = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()

        btnUpDoc.setCorner(_tp: 5)
        btnReStck.setCorner(_tp: 5)
        getSupplierList()
        if (self.iselectedDate != ""){
            txtDate.text = iselectedDate
        }else{
            self.datePicker = UIDatePicker(frame: CGRect(x:0,y: 0,width: self.view.frame.size.width,height: (self.view.frame.size.height / 2)))
            self.datePicker.datePickerMode = .date
            self.datePicker.setDate(Date(), animated: true)
            self.datePicker.maximumDate = Date()
            self.dateSelected()
        }
        txtSupplier.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
    }
  
    @IBAction func btnDropDown(_ sender: UIButton) {
        self.dropDownSupplier.dataSource = nameList
        self.dropDownSupplier.show()
        self.dropDownSupplier.anchorView = dropView
        self.dropDownSupplier.selectionAction = { [self] (index: Int, item: String) in
            txtSupplier.text = item
            supplierid = idList[index]
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnUploadDoc(_ sender: UIButton) {
        presentCamera()
    }
    
    @IBAction func btnCalender(_ sender: UIButton) {
        if #available(iOS 14.0, *) {
            datePicker.locale = .current
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.preferredDatePickerStyle = .inline
            datePicker.center = self.view.center
            datePicker.backgroundColor = UIColor.white
            datePicker.layer.shadowColor = UIColor.gray.cgColor
            datePicker.layer.shadowRadius = 5
            datePicker.layer.shadowOpacity = 0.3
            datePicker.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            self.datePicker.addTarget(self, action:#selector(self.dateChanged), for: .valueChanged)
            self.view.addSubview(self.datePicker)
        }
    }
    
    @IBAction func btnRestock(_ sender: UIButton) {
        restock()
    }
    
    @objc func dateSelected()
    {
        selectedDate =  dateformatterDateTime(date: datePicker.date)
        print(selectedDate)
        datePicker.setDate(datePicker.date, animated: true)
        datePicker.removeFromSuperview()
        txtDate.text = selectedDate
    }
    func dateformatterDateTime(date: Date) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"//"dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    @objc private func dateChanged() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        dateSelected()
    }
    //MARK: -Check Camera and Settings
    func checkCameraAccess(isAllowed: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for:.video) {
        case .denied:
            isAllowed(false)
        case .restricted:
            isAllowed(false)
        case .authorized:
            isAllowed(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { isAllowed($0) }
        default:
            print("There is no authorization")
        }
    }
    func presentCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }else{
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    func presentCameraSettings() {
        let alert = UIAlertController.init(title: "Allow the Camera", message: "Move To the setting", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: { (_) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true)
    }
    //MARK: - APi call
    func getSupplierList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")suppliers/selectlist")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAcc)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        self.resultList = (dic["results"] as? [[String : Any]])!
                        for data in self.resultList{
                            let id = data["id"]
                            let name = data["text"]
                            let image = data["image"]
                            self.nameList.append(name as? String ?? "")
                            self.idList.append(id as? Int ?? 0)
                            self.selectList.append(UserList(id: id as? Int ?? 0,name: name as? String ?? "",image: image as? String ?? ""))
                        }
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    

    func restock(){
        var params : [String:Any] = [:]
        params["qty_procured"]  = txtQty.text ?? ""
        params["supplier_id"]  = "\(supplierid)"
        params["date_procured"] = txtDate.text ?? ""
        params["cost_procured"] = txtPurchaseCost.text ?? ""
        
        print(params)
        let url = URL(string: "\(selectedComp ?? "")consumables/\(consumableId)/restock")!
        let headers : HTTPHeaders = [
            "Content-Type": "multipart/form-data",
            "Authorization":"\(tokenAcc)",
            "Accept" : "application/json"
        ]
        AF.upload(multipartFormData: { multipartFormData in
            let image = self.docImg.image?.jpegData(compressionQuality: 0.5)
                multipartFormData.append(image ?? Data(), withName: "images" , fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: ".jpeg")

            for (key, value) in params {
                print(key,":",value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue) ?? Data(), withName: key)
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response { (result) in
            if result.error != nil{
                print(result.error)
            }else{
                if let jsonData = result.data
                {
                    do{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        print(parsedData)
                        let message = parsedData["messages"] as? String ?? ""
                        let success = parsedData["status"] as? String ?? ""
                        if success == "success"{
                            self.view.makeToast(message)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }else{
                            self.view.makeToast(message)
                        }
                    }catch{
                        self.view.makeToast("Something went wrong please try again later!")
                        print(String(describing: error))
                    }
                }
            }
        }
    }
    
}
extension ConsumableRestockVC : UITextFieldDelegate {
    @objc func editingChanged() {
        if txtSupplier.text != "" {
            filterNameList = nameList.filter { $0.localizedCaseInsensitiveContains(txtSupplier.text ?? "")}
            self.dropDownSupplier.dataSource = filterNameList
            self.dropDownSupplier.anchorView = dropView
            self.dropDownSupplier.show()
            self.dropDownSupplier.selectionAction = { [self] (index: Int, item: String) in
                txtSupplier.text = item
            }
        }else{
            filterNameList = []
        }
    }
}
extension ConsumableRestockVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            docImg.image = image
            imgView.isHidden = false
        }
    }
}
