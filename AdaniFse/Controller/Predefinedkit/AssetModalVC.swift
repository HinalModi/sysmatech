//
//  AssetModalVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 27/09/22.
//

import UIKit

class AssetModalVC: UIViewController {

    @IBOutlet weak var assetModalTV: UITableView!
    var kitId = Int()
    var assetModalDetail : [AssetModalDetail] = []
    var isFromSearch = Bool()
    var searchable = [String : Any]()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckOut = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if isFromSearch{
            kitId = searchable["id"] as? Int ?? 0
            getdetail()
        }else{
            getdetail()
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func getdetail(){
//        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")kits/\(kitId)/models")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        for data in dic["rows"] as! [[String : Any]]{
                            var id = data["id"] as? Int
                            var name = data["name"] as? String
                            var quantity = data["quantity"] as? Int
                            self.assetModalDetail.append(AssetModalDetail(id: id ?? 0, name: name ?? "", quantity: quantity ?? 0))
                        }
                        self.assetModalTV.reloadData()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
}
extension AssetModalVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assetModalDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssetModalTVC") as! AssetModalTVC
        cell.lblName.text = assetModalDetail[indexPath.row].name
        cell.lblQty.text = "\(assetModalDetail[indexPath.row].quantity)"
        return cell
    }
    
}
struct AssetModalDetail{
    init(id: Int, name: String, quantity: Int) {
        self.id = id
        self.name = name
        self.quantity = quantity
    }
    var id : Int
    var name : String
    var quantity : Int
}
