//
//  CategoryListVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/08/22.
//

import UIKit
import SQLite
import LGSideMenuController

class CategoryListVC: UIViewController {

   
    @IBOutlet weak var lblNoDataMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var categoryListTVC: UITableView!
    var getCategory : [Categorylist] = []
    var getModal : [Modelslist] = []
    var categoryList : [GetCategory] = []
    var modalList : [GetModelsList] = []
    var modelArryCount = [String]()
    var assetArryCount = [String]()
    var workOrderCount = [String]()
    var assetStatus = AssetStatusVC()
    var preventiveTable = AssetSchTypeVC()
    var refreshControl: UIRefreshControl!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    override func viewDidLoad() {
        super.viewDidLoad()
        addPullRequest()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        categoryListTVC.tableFooterView = UIView()
        getCategoryList()
        createCategoryDB()
        
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                let drop2 = modalTable.drop(ifExists: true)
                try db?.run(drop)
                try db?.run(drop2)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            
            getCategoryList()
        }
        readCategoryValues()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            categoryListTVC.refreshControl = refreshControl
        } else {
            categoryListTVC.addSubview(refreshControl)
        }
    }
    //MARK: - APi call
    func getCategoryList(){
        activityIndicator.startAnimating()
        let param : Parameters = [:]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .category_model_list, baseURL: selectedComp ?? "", setToken: glbtoken)
        URLSession.shared.getCategoryModal(with: request) { isSuccess, categoryList, modelList , status  in
            DispatchQueue.main.async {
                if isSuccess{
                    self.activityIndicator.startAnimating()
                    self.getCategory = categoryList ?? []
                    self.getModal = modelList ?? []
                    self.createCategoryDB()
                    self.insertCategoryData()
                    self.readCategoryValues()
                    self.insertModelData()
                    self.readModelValues()
                    if self.categoryList.count == 0{
                        self.lblNoDataMsg.isHidden = false
                    }else{
                        self.lblNoDataMsg.isHidden = true
                    }
                    self.activityIndicator.stopAnimating()
                    self.categoryListTVC.reloadData()
                }else{
                    if self.categoryList.count == 0{
                        self.lblNoDataMsg.isHidden = false
                    }else{
                        self.lblNoDataMsg.isHidden = true
                    }
                    self.activityIndicator.stopAnimating()
                    self.categoryListTVC.reloadData()
                }
            }
        }
    }
    //MARK: - Sqlite DB for Offline
    let algorithms = Table("CategoryList")
    var catId = Expression<Int>("cat_id")
    var catName = Expression<String>("cat_name")
    var modelId = Expression<Int>("model_id")
    var modelName = Expression<String>("model_name")
    let modalTable = Table("ModelList")
    func createCategoryDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/categoryList.sqlite3")

                // Initialize tables
                try db?.run(algorithms.create { table in
                    table.column(catId, primaryKey: true)
                    table.column(catName)
                })
                
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    func createModelDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/modelList.sqlite3")

                if Reachability.isConnectedToNetwork(){
                    let drop = modalTable.drop(ifExists: true)
                    try db?.run(drop)
                }
                // Initialize tables
                try db?.run(modalTable.create { table in
                    
                    table.column(modelId, primaryKey: true)
                    table.column(catId)
                    table.column(catName)
                    table.column(modelName)
                })
                
            }
        } catch {
            print(String(describing: error))
            
        }
    }
    func insertCategoryData(){
        for dataset in getCategory{
            do{
                try db?.run(algorithms.insert(catId <- dataset.catId , catName <- dataset.catName))
            }
            catch{
                print("Error in inserting data",String(describing: error))
            }
        }
    }
    func insertModelData(){
        for dataset in getModal{
            do{
                try db?.run(modalTable.insert(catId <- dataset.catId , catName <- dataset.catName , modelId <- dataset.modelId , modelName <- dataset.modelName))
            }
            catch{
                print("Error in inserting data",String(describing: error))
            }
        }
    }
    func readCategoryValues(){
        categoryList.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM CategoryList") {
//                print(row)
                categoryList.append(GetCategory(cat_id: row[0] as? Int64 ?? 0, cat_name: row[1] as? String ?? ""))
                categoryListTVC.reloadData()
            }
        }        catch{
            print("Error in inserting data",String(describing: error))
        }

    }
    
    func readModelValues(){
        modalList.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ModelList") {
//                print(row)
                modalList.append(GetModelsList(cat_id: row[1] as? Int64 ?? 0, cat_name: row[2] as? String ?? "", model_Id: row[1] as? Int64 ?? 0, model_Name: row[3] as? String ?? ""))
                categoryListTVC.reloadData()
            }
        }        catch{
            print("Error in inserting data",String(describing: error))
        }
        
    }
    
    //MARK: - Action
    
    @IBAction func btnOpenMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
}
extension CategoryListVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategorylistTVC") as! CategorylistTVC
        if categoryList.count > 0{
            cell.lblCategoryName.text = categoryList[indexPath.row].cat_name
            createModelDB()
            insertModelData()
            do {
                modelArryCount.removeAll()
                cell.lblCategory.text = "Models" + "(" + "0" + ")"
                for row in try db!.prepare("SELECT * FROM ModelList WHERE cat_name = '\(cell.lblCategoryName.text ?? "")'") {
                    modelArryCount.append(row[2] as! String)
                    cell.lblCategory.text = "Models" + "(" + "\(modelArryCount.count)" + ")"
                }
                
            }catch{
                print(String(describing: error))
            }
            assetStatus.createDB()
//            assetStatus.insertData()
            do{
                assetArryCount.removeAll()
                cell.lblAsset.text = "Assets" + "(" + "0" + ")"
                for row in try db!.prepare("SELECT * FROM AssetList WHERE category_id = \(categoryList[indexPath.row].cat_id)") {
                    assetArryCount.append(row[1] as! String)
                    cell.lblAsset.text = "Assets" + "(" + "\(assetArryCount.count)" + ")"
                }
            }catch{
                print(String(describing: error))
            }
            preventiveTable.createDB()
            preventiveTable.insertData()
            do{
                workOrderCount.removeAll()
                cell.lblWorkOrder.text = "Work Order" + "(" + "0" + ")"
                for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE schedule_expire_date >= '\(today)' AND category_id = \(categoryList[indexPath.row].cat_id)"){
                    
                    workOrderCount.append(row[2] as! String)
                    cell.lblWorkOrder.text = "Work Order" + "(" + "\(workOrderCount.count)" + ")"
                }
            }catch{
                print(String(describing: error))
            }
        }
        
        cell.moveToModel = {
            self.getCategoryList()
            self.createModelDB()
            self.insertModelData()
            do{
                self.modalList.removeAll()
                for row in try db!.prepare("SELECT * FROM ModelList WHERE cat_name = '\(cell.lblCategoryName.text ?? "")'") {
                    self.modalList.append(GetModelsList(cat_id: row[1] as? Int64 ?? 0, cat_name: row[2] as? String ?? "", model_Id: row[1] as? Int64 ?? 0, model_Name: row[3] as? String ?? ""))
                }
            }catch{
                print(String(describing: error))
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ModalListVC") as! ModalListVC
            vc.modalList = self.modalList
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.moveToAssets = {
            let storyBoard = UIStoryboard(name: "AssetStatus", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetStatusVC") as! AssetStatusVC
            vc.categoryId = self.categoryList[indexPath.row].cat_id
            vc.isFromCategory = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.moveToWorkOrder = {
            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            vc.isfromCatgeory = true
            vc.categoryId = self.categoryList[indexPath.row].cat_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    
    
}
struct GetCategory {
     init(cat_id: Int64, cat_name: String) {
        self.cat_id = cat_id
        self.cat_name = cat_name
    }
    
    var cat_id : Int64
    var cat_name : String
    
}
struct GetModelsList {
    init(cat_id: Int64, cat_name: String, model_Id: Int64, model_Name: String) {
        self.cat_id = cat_id
        self.cat_name = cat_name
        self.model_Id = model_Id
        self.model_Name = model_Name
    }
    
    var cat_id : Int64
    var cat_name : String
    var model_Id : Int64
    var model_Name : String
}
