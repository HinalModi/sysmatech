//
//  ModalListVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/08/22.
//

import UIKit

class ModalListVC: UIViewController {
    
   
    @IBOutlet weak var lblNoDataMsg: UILabel!
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    @IBOutlet weak var modelTVC: UITableView!
    var modalList : [GetModelsList] = []
    var assetArryCount = [String]()
    var workOrderCount = [String]()
    var assetStatus = AssetStatusVC()
    var preventiveTable = AssetSchTypeVC()
    override func viewDidLoad() {
        super.viewDidLoad()

        print(modalList.count)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        modelTVC.tableFooterView = UIView()
        if modalList.count == 0{
            activityInd.stopAnimating()
            lblNoDataMsg.isHidden = false
        }else{
            activityInd.stopAnimating()
            lblNoDataMsg.isHidden = true
        }
    }
//MARK: - Actions
    @IBAction func btnPrevious(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension ModalListVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        modalList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ModelListTVC") as! ModelListTVC
        activityInd.startAnimating()
        cell.lblModalName.text = modalList[indexPath.row].model_Name
        if modalList.count > 0{
            assetStatus.createDB()
            assetStatus.insertData()
            do{
                assetArryCount.removeAll()
                cell.lblAsset.text = "Assets" + "(" + "0" + ")"
                for row in try db!.prepare("SELECT * FROM AssetList WHERE model_name = '\(modalList[indexPath.row].model_Name)'") {
                    print(row)
                    assetArryCount.append(row[1] as! String)
                    cell.lblAsset.text = "Assets" + "(" + "\(assetArryCount.count)" + ")"
                }
            }catch{
                print(String(describing: error))
            }
            preventiveTable.createDB()
            preventiveTable.insertData()
            do{
                workOrderCount.removeAll()
                cell.lblWorkOrder.text = "Work Order" + "(" + "0" + ")"
                for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE model_name = '\(modalList[indexPath.row].model_Name)' AND schedule_expire_date >= '\(today)'"){
                    
                    workOrderCount.append(row[2] as! String)
                    cell.lblWorkOrder.text = "Work Order" + "(" + "\(workOrderCount.count)" + ")"
                }
            }catch{
                print(String(describing: error))
            }
        }
        activityInd.stopAnimating()
        cell.showAsset = {
            let storyBoard = UIStoryboard(name: "AssetStatus", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetStatusVC") as! AssetStatusVC
            vc.categoryId = self.modalList[indexPath.row].cat_id
            vc.isfromModel = true
            vc.modelName = self.modalList[indexPath.row].model_Name
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.showWorkOrder = {
            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            vc.isFromModel = true
            vc.modelName = self.modalList[indexPath.row].model_Name
            vc.categoryId = self.modalList[indexPath.row].cat_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    
    
}
