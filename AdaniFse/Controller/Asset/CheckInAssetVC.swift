//
//  CheckInAssetVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 09/12/22.
//

import UIKit
import DropDown
class CheckInAssetVC: UIViewController {
    
    
    @IBOutlet weak var locationDropView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusDropView: UIView!
    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtLoc: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var txtCheckDate: UITextField!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckIn = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    var datePicker: UIDatePicker!
    var iselectedDate:String = ""
    var selectedDate: String = String()
    let dropDown = DropDown()
    var locationDrop = DropDown()
    var status_id = Int()
    var locationId = Int()
    var assetId = Int()
    var statusList : [[String : Any]] = []
    var nameList = [String]()
    var idList = [Int]()
    var locationList : [[String : Any]] = []
    var locationArray = [String]()
    var locationListArry = [LocationList]()
    var assetTagid = Int64()
    override func viewDidLoad() {
        super.viewDidLoad()
        locationDrop = DropDown(frame: CGRect(x: 0, y: 0, width: locationDropView.frame.width, height: 200))
        txtCheckDate.setRightPaddingPoints(50)
        btnCheckIn.setCorner(_tp: 5)
        txtNote.setBorder(borderColor: .lightGray, borderWidth: 0.5)
        txtNote.setCorner(_tp: 5)
        if (self.iselectedDate != ""){
            txtCheckDate.text = iselectedDate
        }else{
            self.datePicker = UIDatePicker(frame: CGRect(x:0,y: 0,width: self.view.frame.size.width,height: (self.view.frame.size.height / 2)))
            self.datePicker.datePickerMode = .date
            self.datePicker.setDate(Date(), animated: true)
            self.datePicker.maximumDate = Date()
            self.dateSelected()
        }
        getStatus()
        getLocationList()
    }
    
    @objc func dateSelected()
    {
        selectedDate =  dateformatterDateTime(date: datePicker.date)
        print(selectedDate)
        datePicker.setDate(datePicker.date, animated: true)
        datePicker.removeFromSuperview()
        txtCheckDate.text = selectedDate
    }
    func dateformatterDateTime(date: Date) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    @objc private func dateChanged() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        dateSelected()
       
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCal(_ sender: UIButton) {
        if #available(iOS 14.0, *) {
            datePicker.locale = .current
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.preferredDatePickerStyle = .inline
            datePicker.center = self.view.center
            datePicker.backgroundColor = UIColor.white
            datePicker.layer.shadowColor = UIColor.gray.cgColor
            datePicker.layer.shadowRadius = 5
            datePicker.layer.shadowOpacity = 0.3
            datePicker.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            self.datePicker.addTarget(self, action:#selector(self.dateChanged), for: .valueChanged)
            self.view.addSubview(self.datePicker)
        }
    }
    
    @IBAction func btnSelectStatus(_ sender: UIButton) {
        self.dropDown.dataSource = nameList
        self.dropDown.show()
        self.dropDown.anchorView = statusDropView
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            txtStatus.text = item
            status_id = idList[index]
        }
    }
    
    @IBAction func btnSelectLoc(_ sender: UIButton) {
        self.locationDrop.dataSource = locationArray
        self.locationDrop.show()
        self.locationDrop.anchorView = locationDropView
        self.locationDrop.selectionAction = { [self] (index: Int, item: String) in
            txtLoc.text = item
            let location = locationListArry[index]
            locationId = location.id
        }
    }
    
    @IBAction func btnCheckInAsset(_ sender: UIButton) {
        checkInAssets()
    }
    //MARK: - Api call
    func checkInAssets(){
        let parameters : [String: String] = [
            "status_id": "\(status_id)",
            "location_id": "\(locationId)",
            "checkin_at": txtCheckDate.text ?? "",
            "note": txtNote.text ?? ""
        ]
        
        let url = URL(string: "\(selectedComp ?? "")check-in-out/asset/\(assetTagid)/checkin")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
                
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String
                    let status = dic["success"] as? String
                    print(status)
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked in successfully")
                        }
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong , Please try again later!")
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    
    func getLocationList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")locations/selectlist")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    self.locationList = dic["results"]  as! [[String : Any]]
                    print(self.locationList)
                    for name in self.locationList{
                        let nameData = name["text"] as? String ?? ""
                        let id = name["id"] as? Int ?? 0
                        let img = name["image"] as? String ?? ""
                        self.locationArray.append(nameData)
                        self.locationListArry.append(LocationList(id: id, name: nameData, image: img))
                    }
                    print(self.locationArray)
                    
                    
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func getStatus(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")statuslabels")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "Authorization")
//        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
                
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        self.statusList = dic["rows"]  as! [[String : Any]]
                        for name in self.statusList{
                            let nameData = name["name"] as? String ?? ""
                            let id = name["id"] as? Int ?? 0
                            self.nameList.append(nameData)
                            self.idList.append(id)
                        }
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
}
