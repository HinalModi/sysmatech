//
//  CheckOutAssetVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 09/12/22.
//

import UIKit
import DropDown

class CheckOutAssetVC: UIViewController {

    @IBOutlet weak var txtExpDate: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txttitleCheckTo: UILabel!
    @IBOutlet weak var dropUserTypeView: UIView!
    @IBOutlet weak var dropCheckView: UIView!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtAssign: UITextField!
    @IBOutlet weak var txtCheckType: UITextField!
    @IBOutlet weak var txtNotes: UITextView!
    @IBOutlet weak var statusDropView: UIView!
    
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckOut = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? ""))
    var checkoutToList = ["User","Asset","Location"]
    let dropDown = DropDown()
    var nameList = [String]()
    var nameUserList : [[String : Any]] = []
    var nameArray = [String]()
    var userList = [UserList]()
    var assetList : [[String : Any]] = []
    var assetArray = [String]()
    var locationList : [[String : Any]] = []
    var locationArray = [String]()
    var locationListArry = [LocationList]()
    var assetId = Int()
    var status_id = Int()
    var checkOutTo = String()
    var locationId = Int()
    var userData = String()
    var usedID = Int()
    var filterNameArray = [String]()
    var datePicker: UIDatePicker!
    var iselectedDate:String = ""
    var selectedDate: String = String()
    var statusList : [[String : Any]] = []
    var idList = [Int]()
    var assetIdList = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtCheckType.setRightPaddingPoints(50)
        txtAssign.setRightPaddingPoints(50)
        txtNotes.setBorder(borderColor: .lightGray, borderWidth: 0.5)
        txtNotes.setCorner(_tp: 5)
        txtAssign.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        if (self.iselectedDate != ""){
            txtDate.text = iselectedDate
        }else{
            self.datePicker = UIDatePicker(frame: CGRect(x:0,y: 0,width: self.view.frame.size.width,height: (self.view.frame.size.height / 2)))
            self.datePicker.datePickerMode = .date
            self.datePicker.setDate(Date(), animated: true)
            self.datePicker.maximumDate = Date()
            self.dateSelected()
        }
        getUserList()
        getAssetList()
        getLocationList()
        getStatus()
    }
    //MARK: - Api Call
    func getUserList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")users/selectlist")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.nameUserList = dic["results"]  as! [[String : Any]]
                    for name in self.nameUserList{
                        let nameData = name["text"] as? String ?? ""
                        let id = name["id"] as? Int ?? 0
                        let img = name["image"] as? String ?? ""
                        self.nameArray.append(nameData)
                        self.userList.append(UserList(id: id, name: nameData, image: img))
                    }
                 
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func getAssetList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")hardware")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    self.assetList = dic["rows"]  as! [[String : Any]]
                    print(self.assetList)
                    for name in self.assetList{
                        let id = name["id"] as? Int ?? 0
                        let nameData = name["name"] as? String ?? ""
                        self.assetArray.append(nameData)
                        self.assetIdList.append(id)
                    }
                    print(self.assetArray)
                    
                    
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    
    func getLocationList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")locations/selectlist")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    self.locationList = dic["results"]  as! [[String : Any]]
                    print(self.locationList)
                    for name in self.locationList{
                        let nameData = name["text"] as? String ?? ""
                        let id = name["id"] as? Int ?? 0
                        let img = name["image"] as? String ?? ""
                        self.locationArray.append(nameData)
                        self.locationListArry.append(LocationList(id: id, name: nameData, image: img))
                    }
                    print(self.locationArray)
                    
                    
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func getPostString(params: [String: Any]) -> String {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    func checkOutAsset(){
        let parameters : [String: Any] = [
            "status_id" : status_id,
            "checkout_to_type" : checkOutTo,
            "assigned_user" :  checkOutTo == "user" ? usedID : "" ,
            "assigned_asset" : checkOutTo == "asset" ? assetId : "",
            "assigned_location" : checkOutTo == "location" ? locationId : "" ,
            "checkout_at": txtDate.text ?? "",
            "expected_checkin": txtExpDate.text ?? "",
            "note" : txtNotes.text ?? "",
        ]
        print(parameters)
        
        let url = URL(string: "\(selectedComp ?? "")check-in-out/asset/\(assetId)/checkout?user_id=\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    
                    let message = dic["messages"] as? String
                    let status = dic["status"] as? String
                    let payLoad = dic["payload"] as? [String : Any] ?? [String : Any]()
                    let error = payLoad["errors"] as? [String]

//                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked out successfully")
                        }

                    }else{
                        if message != ""{
                            if error != nil {
                                self.view.makeToast((error?[0] ?? ""))
                            }else{
                                self.view.makeToast(message)
                            }
                        }else{
                            self.view.makeToast("Something went wrong!")
                        }

                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func getStatus(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")statuslabels")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
//        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
//                    self.activityIndicator.stopAnimating()
                }
                
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        self.statusList = dic["rows"]  as! [[String : Any]]
                        for name in self.statusList{
                            let nameData = name["name"] as? String ?? ""
                            let id = name["id"] as? Int ?? 0
                            self.nameList.append(nameData)
                            self.idList.append(id)
                        }
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    //MARK: - Action
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSelectUser(_ sender: UIButton) {
        
        if txtCheckType.text == "Location"{
            checkOutTo = "location"
            self.dropDown.dataSource = locationArray
            self.dropDown.show()
            self.dropDown.anchorView = dropUserTypeView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                txtAssign.text = item
                let location = locationListArry[index]
                locationId = location.id
            }
        }else if txtCheckType.text == "Asset"{
            checkOutTo = "asset"
            self.dropDown.dataSource = assetArray
            self.dropDown.show()
            self.dropDown.anchorView = dropUserTypeView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                txtAssign.text = item
                assetId = assetIdList[index]
            }
        }else{
            checkOutTo = "user"
            self.dropDown.dataSource = nameArray
            self.dropDown.show()
            self.dropDown.anchorView = dropUserTypeView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                txtAssign.text = item
                usedID = userList[index].id
                print(usedID)
                
            }
        }
    }
    func selectDate(){
        if #available(iOS 14.0, *) {
            datePicker.locale = .current
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.preferredDatePickerStyle = .inline
            datePicker.center = self.view.center
            datePicker.backgroundColor = UIColor.white
            datePicker.layer.shadowColor = UIColor.gray.cgColor
            datePicker.layer.shadowRadius = 5
            datePicker.layer.shadowOpacity = 0.3
            datePicker.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            self.datePicker.addTarget(self, action:#selector(self.dateChanged), for: .valueChanged)
            self.view.addSubview(self.datePicker)
        }
    }
    @IBAction func btnOpenDate(_ sender: UIButton) {
        selectDate()
    }
    
    @IBAction func btnExpectDate(_ sender: UIButton) {
        selectDate()
    }
    @IBAction func btnCheckOutTo(_ sender: UIButton) {
        userData = txtAssign.text ?? ""
        dropDown.dataSource = checkoutToList
        dropDown.show()
        dropDown.anchorView = dropCheckView
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            txtCheckType.text = item
            
            if txtCheckType.text == "Location"{
                txttitleCheckTo.text = "Select Location"
            }else if txtCheckType.text == "Asset"{
                txttitleCheckTo.text = "Select Asset"
            }else if txtCheckType.text == "User"{
                txttitleCheckTo.text = "Select User"
            }
        }
    }
    
    @IBAction func btnSelectStatus(_ sender: UIButton) {
        self.dropDown.dataSource = nameList
        self.dropDown.show()
        self.dropDown.anchorView = statusDropView
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            txtStatus.text = item
            status_id = idList[index]
        }
    }
    @IBAction func btnCheckOut(_ sender: UIButton) {
        checkOutAsset()
    }
    @objc func dateSelected()
    {
        selectedDate =  dateformatterDateTime(date: datePicker.date)
        print(selectedDate)
        datePicker.setDate(datePicker.date, animated: true)
        datePicker.removeFromSuperview()
        txtDate.text = selectedDate
        txtExpDate.text = selectedDate
    }
    func dateformatterDateTime(date: Date) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    @objc private func dateChanged() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        dateSelected()
       
    }
}
extension CheckOutAssetVC : UITextFieldDelegate {
    @objc func editingChanged() {
        if txtAssign.text != "" {
            if txtCheckType.text == "Location"{
                filterNameArray = locationArray.filter { $0.localizedCaseInsensitiveContains(txtAssign.text ?? "")}
                dropDown.anchorView = dropUserTypeView
                dropDown.dataSource = filterNameArray
                dropDown.show()
            }else if txtCheckType.text == "Asset"{
                filterNameArray = assetArray.filter { $0.localizedCaseInsensitiveContains(txtAssign.text ?? "")}
                dropDown.anchorView = dropUserTypeView
                dropDown.dataSource = filterNameArray
                dropDown.show()
            }else{
                filterNameArray = nameArray.filter { $0.localizedCaseInsensitiveContains(txtAssign.text ?? "")}
                dropDown.anchorView = dropUserTypeView
                dropDown.dataSource = filterNameArray
                dropDown.show()
            }
        }else{
            filterNameArray = []
        }
    }
}

