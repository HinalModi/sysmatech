//
//  TaskDashboardVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 30/08/22.
//

import UIKit
import LGSideMenuController

class TaskDashboardVC: UIViewController {

    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var notifiView: UIView!
    @IBOutlet weak var processView: UIView!
    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var addBGview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        scanView.setRoundCorner()
        processView.setRoundCorner()
        notifiView.setRoundCorner()
        addView.setRoundCorner()
        addBGview.setCornerRadius(with: [.topLeft , .bottomLeft],radius: 8)
    }
  
    @IBAction func showSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
    @IBAction func btnEdit(_ sender: UIButton) {
        print("Click")
    }
    
}


extension TaskDashboardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkOrderTVC") as! WorkOrderTVC
        return cell
    }
    
    
}
