//
//  DashBoardVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit
import LGSideMenuController
import DropDown

class DashBoardVC: UIViewController ,UISearchBarDelegate {
     
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSearchItem: UIButton!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var btnBreakDown: UIButton!
    @IBOutlet weak var btnPreventJob: UIButton!
    @IBOutlet weak var btnCompJob: UIButton!
    @IBOutlet weak var bgView: UIView!
    var setCompanyBase = String()
    var isfromCatgeory = false
    var categoryId = Int64()
    var isFromModel = false
    var modelName = String()
    var searchModal : [[String : Any]] = []
    var searchList : [SearchListData] = []
    var dataName: [String] = []
    var dataFiltered: [String] = []
    var dropButton = DropDown()
    var searchableData = [String : Any]()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenDash = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitview()
        searchBar.autocapitalizationType = .none
        dataFiltered = dataName
        dropButton.anchorView = searchBar
        dropButton.bottomOffset = CGPoint(x: 0, y:(dropButton.anchorView?.plainView.bounds.height)!)
        dropButton.backgroundColor = .white
        dropButton.direction = .bottom
        
    }
    override func viewWillAppear(_ animated: Bool) {
        dropButton.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)") //Selected item: code at index: 0
            searchBar.text = item
            
            let list = searchList[index]
            print(list.type)
            
            if list.type == "Asset" {
                let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
                vc.isfromSearch = true
                vc.searchable = list.searchable
                navigationController?.pushViewController(vc, animated: true)
            }else if list.type == "Accessory" {
                let storyBoard = UIStoryboard(name: "List", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "AccessoryDetailVC") as! AccessoryDetailVC
                vc.isFromSearch = true
                vc.isFromAccessory = true
                vc.searchable = list.searchable
                navigationController?.pushViewController(vc, animated: true)
            }else if list.type == "Consumable" {
                let storyBoard = UIStoryboard(name: "List", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "AccessoryDetailVC") as! AccessoryDetailVC
                vc.isFromSearch = true
                vc.isFromConsumable = true
                vc.searchable = list.searchable
                navigationController?.pushViewController(vc, animated: true)
            }else if list.type == "License" {
                let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
                vc.isfromSearch = true
                vc.isfromLicence = true
                vc.searchable = list.searchable
                navigationController?.pushViewController(vc, animated: true)
            }else if list.type == "Component" {
                let storyBoard = UIStoryboard(name: "List", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "AccessoryDetailVC") as! AccessoryDetailVC
                vc.isFromSearch = true
                vc.isFromComponent = true
                vc.searchable = list.searchable
                navigationController?.pushViewController(vc, animated: true)
            }else if list.type == "Predefined Kit" {
                let storyBoard = UIStoryboard(name: "CategoryModal", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "AssetModalVC") as! AssetModalVC
                vc.isFromSearch = true
                vc.searchable = list.searchable
                navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
   //MARK: - SetUi
    func setInitview(){
        
        remove(asChildViewController: completedJobsVC)
        remove(asChildViewController: preventiveJobsVC)
        add(asChildViewController: breakdownJobsVC)
        if #available(iOS 11.0, *) {
            btnBreakDown.backgroundColor = Constant.Color.color_launch
        }else{
            btnBreakDown.backgroundColor = .blue
        }
        btnBreakDown.tintColor = .white
        if #available(iOS 11.0, *) {
            btnCompJob.backgroundColor = Constant.Color.color_bg
            btnCompJob.tintColor = Constant.Color.color_grey
        }else{
            btnCompJob.backgroundColor = .white
            btnCompJob.tintColor = .gray
        }       
        if #available(iOS 11.0, *) {
            btnPreventJob.backgroundColor = Constant.Color.color_bg
            btnPreventJob .tintColor = Constant.Color.color_grey
        }else{
            btnPreventJob.backgroundColor = .white
            btnPreventJob.tintColor = .gray
        }
        
    }
   
    private lazy var breakdownJobsVC: AssetSchTypeVC = {
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "AssetSchTypeVC") as! AssetSchTypeVC
        viewController.isBreakDown = true
        viewController.isDashBoard = true
        viewController.isfromCategory = isfromCatgeory
        viewController.isFromModel = isFromModel
        viewController.categoryID = categoryId
        viewController.modelName = modelName
        self.add(asChildViewController: viewController)
        return viewController
    }()
    private lazy var completedJobsVC: CompletedJobsVC = {
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "CompletedJobsVC") as! CompletedJobsVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    private lazy var preventiveJobsVC: AssetSchTypeVC = {
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "AssetSchTypeVC") as! AssetSchTypeVC
        viewController.isBreakDown = false
        viewController.isDashBoard = true
        viewController.isfromCategory = isfromCatgeory
        viewController.categoryID = categoryId
        viewController.isFromModel = isFromModel
        viewController.modelName = modelName
        self.add(asChildViewController: viewController)
        return viewController
    }()
   
    @IBAction func btnSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }        
    }
    
    //MARK: - SearchBar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchData(text: searchText)
        print(dataName)
        dataFiltered = dataName.filter { $0.localizedCaseInsensitiveContains((searchBar.text!))}
        print("dataName",dataFiltered)
        dropButton.dataSource = dataFiltered
        dropButton.show()
        
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = dataName
        searchBar.isHidden = true
        dropButton.hide()
    }
    //MARK: - Api Call
    func searchData(text : String){
        
        let parameters : [String: String] = [:]
        guard let url = URL(string: "\(selectedComp ?? "")search?term=\(text)") else{ return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("\(tokenDash)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.searchModal.removeAll()
                    self.dataName.removeAll()
                    self.searchList.removeAll()
                    self.searchModal = dic["rows"] as? [[String : Any]] ?? [[String : Any]]()
                    print("searchModal",self.searchModal)
                    for data in self.searchModal{
                        let searchable = data["searchable"] as! [String : Any]
                        let title = data["title"] as? String
                        let url = data["url"] as? String
                        let type = data["type"] as? String
                        self.dataName.append(title ?? "")
                        print(searchable)
                        self.searchList.append(SearchListData(searchable: searchable, title: title ?? "", url: url ?? "", type: type ?? ""))
 
                    }
                    
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
    //MARK: - Action
    @IBAction func showSearchBarAction(_ sender: Any) {
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func ScanQR(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Jobs", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRVC
        navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func openPendingJob(_ sender: UIButton) {
        remove(asChildViewController: completedJobsVC)
        remove(asChildViewController: preventiveJobsVC)
        add(asChildViewController: breakdownJobsVC)
        btnBreakDown.tintColor = .white
        if #available(iOS 11.0, *) {
            btnBreakDown.backgroundColor = Constant.Color.color_launch
            btnCompJob.backgroundColor = Constant.Color.color_bg
            btnCompJob.tintColor = Constant.Color.color_grey
            btnPreventJob.backgroundColor = Constant.Color.color_bg
            btnPreventJob .tintColor = Constant.Color.color_grey
        } else {
            btnBreakDown.backgroundColor = .blue
            btnCompJob.backgroundColor = .white
            btnCompJob.tintColor = .gray
            btnPreventJob.backgroundColor = .white
            btnPreventJob .tintColor = .gray
        }
    }
    
    
    @IBAction func btnOpenCompletedJob(_ sender: UIButton) {
        remove(asChildViewController: breakdownJobsVC)
        remove(asChildViewController: preventiveJobsVC)
        add(asChildViewController: completedJobsVC)
        btnCompJob.tintColor = .white
        if #available(iOS 11.0, *) {
            btnCompJob.backgroundColor = Constant.Color.color_launch
            btnBreakDown.backgroundColor = Constant.Color.color_bg
            btnBreakDown.tintColor = Constant.Color.color_grey
            btnPreventJob.backgroundColor = Constant.Color.color_bg
            btnPreventJob .tintColor = Constant.Color.color_grey
        } else {
            btnCompJob.backgroundColor = .blue
            btnBreakDown.backgroundColor = .white
            btnBreakDown.tintColor = .gray
            btnPreventJob.backgroundColor = .white
            btnPreventJob .tintColor = .gray
        }
    }
   
    @IBAction func btnPreventive(_ sender: UIButton) {
        remove(asChildViewController: completedJobsVC)
        remove(asChildViewController: breakdownJobsVC)
        add(asChildViewController: preventiveJobsVC)
        btnPreventJob.tintColor = .white
        if #available(iOS 11.0, *) {
            btnPreventJob.backgroundColor = Constant.Color.color_launch
            btnBreakDown.backgroundColor = Constant.Color.color_bg
            btnBreakDown.tintColor = Constant.Color.color_grey
            btnCompJob.backgroundColor = Constant.Color.color_bg
            btnCompJob.tintColor = Constant.Color.color_grey
        } else {
            
    }
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        bgView.addSubview(viewController.view)
        viewController.view.frame = bgView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}
struct SearchListData {
     init(searchable: [String : Any], title: String, url: String, type: String) {
        self.searchable = searchable
        self.title = title
        self.url = url
        self.type = type
    }
    
    var searchable : [String : Any]
    var title : String
    var url : String
    var type : String
}
struct SearchedDataList{
     init(id: Int, name: String, asset_tag: String, model_id: Int, model_name: String, serial: String, purchase_date: String, purchase_cost: String, order_number: String, assigned_to: String, notes: String, image: String, user_id: Int, created_at: String, updated_at: String, physical: Int, deleted_at: String, status_id: Int, archived: Int, warranty_months: Int, depreciate: Int, supplier_id: Int, requestable: Int, rtd_location_id: Int, location: String, _snipeit_mac_address_1: String, accepted: String, last_checkout: String, expected_checkin: String, company_id: Int, assigned_type: String, last_audit_date: String, next_audit_date: String, location_id: Int, checkin_counter: Int, checkout_counter: Int, requests_counter: Int, audit_params_values: String, audit_params_transaction: String, dyamic_model_fieldset: String, category_name: String, supplier_name: String, company_name: String, prev_main_owner_type: String, mail_status: Int) {
         
        self.id = id
        self.name = name
        self.asset_tag = asset_tag
        self.model_id = model_id
        self.model_name = model_name
        self.serial = serial
        self.purchase_date = purchase_date
        self.purchase_cost = purchase_cost
        self.order_number = order_number
        self.assigned_to = assigned_to
        self.notes = notes
        self.image = image
        self.user_id = user_id
        self.created_at = created_at
        self.updated_at = updated_at
        self.physical = physical
        self.deleted_at = deleted_at
        self.status_id = status_id
        self.archived = archived
        self.warranty_months = warranty_months
        self.depreciate = depreciate
        self.supplier_id = supplier_id
        self.requestable = requestable
        self.rtd_location_id = rtd_location_id
        self.location = location
        self._snipeit_mac_address_1 = _snipeit_mac_address_1
        self.accepted = accepted
        self.last_checkout = last_checkout
        self.expected_checkin = expected_checkin
        self.company_id = company_id
        self.assigned_type = assigned_type
        self.last_audit_date = last_audit_date
        self.next_audit_date = next_audit_date
        self.location_id = location_id
        self.checkin_counter = checkin_counter
        self.checkout_counter = checkout_counter
        self.requests_counter = requests_counter
        self.audit_params_values = audit_params_values
        self.audit_params_transaction = audit_params_transaction
        self.dyamic_model_fieldset = dyamic_model_fieldset
        self.category_name = category_name
        self.supplier_name = supplier_name
        self.company_name = company_name
        self.prev_main_owner_type = prev_main_owner_type
        self.mail_status = mail_status
    }
    
    var id : Int
    var name : String
    var asset_tag : String
    var model_id : Int
    var model_name : String
    var serial : String
    var purchase_date : String
    var purchase_cost : String
    var order_number : String
    var assigned_to : String
    var notes : String
    var image : String
    var user_id : Int
    var created_at : String
    var updated_at : String
    var physical : Int
    var deleted_at : String
    var status_id : Int
    var archived : Int
    var warranty_months : Int
    var depreciate : Int
    var supplier_id : Int
    var requestable : Int
    var rtd_location_id : Int
    var location : String
    var _snipeit_mac_address_1 : String
    var accepted : String
    var last_checkout : String
    var expected_checkin : String
    var company_id : Int
    var assigned_type : String
    var last_audit_date : String
    var next_audit_date : String
    var location_id : Int
    var checkin_counter : Int
    var checkout_counter : Int
    var requests_counter : Int
    var audit_params_values : String
    var audit_params_transaction : String
    var dyamic_model_fieldset : String
    var category_name : String
    var supplier_name : String
    var company_name : String
    var prev_main_owner_type : String
    var mail_status : Int
}
