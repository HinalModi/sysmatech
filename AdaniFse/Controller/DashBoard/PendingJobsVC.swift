//
//  PendingJobsVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit
import SDWebImage
import CoreMedia
import SQLite

class PendingJobsVC: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNodataMsg: UILabel!
    @IBOutlet weak var jobsTableView: UITableView!
    var getAllAssetlist : [PendingTotalAssetScheduledata] = []
    var pendingJobsData : [PendingJobData] = []
    var refreshControl: UIRefreshControl!
    var pmHistory : [[String : Any]] = []
    var pmHistoryList : [pmHistoryData] = []
    var setCompanyBase = String()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            activityIndicator.color = .black
        } else {
            activityIndicator.color = .blue
        }
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                let drop2 = pmHistoryTable.drop(ifExists: true)
                try db?.run(drop)
                try db?.run(drop2)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            getPendingJobsApi()
            createDB()
        }
        readValues()
        insertPmHistory()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            jobsTableView.refreshControl = refreshControl
        } else {
            jobsTableView.addSubview(refreshControl)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        jobsTableView.tableFooterView = UIView()
        jobsTableView.delegate = self
        jobsTableView.dataSource = self
        
        getPendingJobsApi()
        createDB()
        insertPmHistory()
        readValues()
        addPullRequest()
    }
    
    //MARK: -StoreDataInSqlite
    
    let algorithms = Table("Pendingjobs")
    let audit_schdule_id = Expression<Int>("audit_schdule_id")
    var auditId = Expression<Int>("audit_id")
    var auditName = Expression<String>("audit_name")
    var assetTagId = Expression<Int>("asset_tag_id")
    var auditStartDate = Expression<String>("audit_start_date")
    var auditEndDate = Expression<String>("audit_end_date")
    var scheduleExpireDate = Expression<String>("schedule_expire_date")
    var auditInspectionDate = Expression<String>("audit_inspection_date")
    var auditStatus = Expression<String>("audit_status")
    var auditResult = Expression<String>("audit_result")
    var inspectionDelay = Expression<String>("inspection_delay")
    var auditParamsId = Expression<Int>("audit_params_id")
    var assetOwnerId = Expression<Int>("asset_owner_id")
    var assetUserOwnerId = Expression<String>("asset_user_owner_id")
    var deletedAt = Expression<String>("deleted_at")
    var institutionId = Expression<Int>("institution_id")
    var prevMaintenanaceOwnerId = Expression<Int>("prev_maintenanace_owner_id")
    var prevMaintenanaceOwnerType = Expression<String>("prev_maintenanace_owner_type")
    var paramsMasterId = Expression<String>("params_master_id")
    var createdAt = Expression<String>("created_at")
    var updatedAt = Expression<String>("updated_at")
    var id = Expression<Int>("id")
    let name = Expression<String>("name")
    let asset_tag = Expression<String>("asset_tag")
    var modelId = Expression<Int>("model_id")
    let model_name = Expression<String>("model_name")
    var serial = Expression<String>("serial")
    var purchaseDate = Expression<String>("purchase_date")
    var purchaseCost = Expression<String>("purchase_cost")
    var orderNumber = Expression<String>("order_number")
    var assignedTo = Expression<String>("assigned_to")
    var notes = Expression<String>("notes")
    let image = Expression<String>("image")
    var userId = Expression<Int>("user_id")
    var physical = Expression<Int>("physical")
    var statusId = Expression<Int>("status_id")
    var archived = Expression<Int>("archived")
    let warrantyMonths = Expression<Int>("warranty_months")
    var depreciate = Expression<Int>("depreciate")
    var supplierId = Expression<Int>("supplier_id")
    var requestable = Expression<Int>("requestable")
    var rtdLocationId = Expression<Int>("rtd_location_id")
    var location = Expression<String>("location")
    var snipeitMacAddress1 = Expression<String>("_snipeit_mac_address_1")
    var accepted = Expression<String>("accepted")
    var lastCheckout = Expression<String>("last_checkout")
    var expectedCheckin = Expression<String>("expected_checkin")
    var companyId = Expression<Int>("company_id")
    var assignedType = Expression<String>("assigned_type")
    let lastAuditDate = Expression<String>("last_audit_date")
    var nextAuditDate = Expression<String>("next_audit_date")
    var locationId = Expression<Int>("location_id")
    var checkinCounter = Expression<Int>("checkin_counter")
    var checkoutCounter  = Expression<Int>("checkout_counter")
    var requestsCounter  = Expression<Int>("requests_counter")
    var auditParamsValues = Expression<String>("audit_params_values")
    var auditParamsTransaction = Expression<String>("audit_params_transaction")
    var dyamicModelFieldset = Expression<String>("dyamic_model_fieldset")
    var categoryName = Expression<String>("category_name")
    var supplierName = Expression<String>("supplier_name")
    var companyName = Expression<String>("company_name")
    var snipeitUplinkPorts2 = Expression<String>("_snipeit_uplink_ports_2")
    var snipeitPowerOverEthernetPoe3 = Expression<String>("_snipeit_power_over_ethernet_poe_3")
    var snipeitFan4 = Expression<String>("_snipeit_fan_4")
    var snipeitPowerSupply5 = Expression<String>("_snipeit_power_supply_5")
    var snipeitFeatures6 = Expression<String>("_snipeit_features_6")
    var snipeitNetworkManagement7 = Expression<String>("_snipeit_network_management_7")
    var snipeitWarrantyType8 = Expression<String>("_snipeit_warranty_type_8")
    var snipeitNetworkPorts9 = Expression<String>("_snipeit_network_ports_9")
    var prevMainOwnerType = Expression<String>("prev_main_owner_type")
    var escalatedAuditLevels = Expression<Int>("escalated_audit_levels")
    
    
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/jobs.sqlite3")

                // Initialize tables
                try db?.run(algorithms.create { table in
                    table.column(audit_schdule_id, primaryKey: true)
                    table.column(id)
                    table.column(name)
                    table.column(asset_tag)
                    table.column(modelId)
                    table.column(model_name)
                    table.column(purchaseDate)
                    table.column(image)
                    table.column(warrantyMonths)
                    table.column(location)
                    table.column(auditEndDate)
                    table.column(auditParamsValues)
                    table.column(categoryName)
                    table.column(supplierName)
                    table.column(companyName)
                    table.column(auditName)
                    table.column(scheduleExpireDate)
                    table.column(assetTagId)
                    table.column(escalatedAuditLevels)
                })
                try db?.run(pmHistoryTable.create { table in
                    table.column(audit_SchduleId, primaryKey: true)
                    table.column(assetownerId)
                    table.column(assettagId)
                    table.column(assetuserOwnerId)
                    table.column(auditendDate)
                    table.column(auditid)
                    table.column(auditinspectionDate)
                    table.column(audit_Name)
                    table.column(audit_ParamsId)
                    table.column(audit_Result)
                    table.column(audit_StartDate)
                    table.column(audit_Status)
                    table.column(audited_Params)
                })

            }
        } catch {
            print(error.localizedDescription)
        }
    }
   
    func insertData(){
        
            for dataset in getAllAssetlist{
               
                do{
                    try db?.run(algorithms.insert(audit_schdule_id <- dataset.auditSchduleId ?? 0, id <- dataset.id ?? 0,name <- dataset.name  ?? "",asset_tag <- dataset.assetTag  ?? "",modelId <- dataset.modelId ?? 0 ,image <- dataset.image  ?? "",warrantyMonths <- dataset.warrantyMonths  ?? 0,location <- dataset.location ?? "",auditEndDate <- dataset.auditEndDate  ?? "",supplierName <- dataset.supplierName ?? "",model_name <- dataset.modelName  ?? "",categoryName <- dataset.categoryName  ?? "" ,companyName <- dataset.companyName  ?? "",purchaseDate <- dataset.purchaseDate  ?? "" ,auditParamsValues <- dataset.auditParamsValues  ?? "" ,auditName <- dataset.auditName  ?? "", scheduleExpireDate <- dataset.scheduleExpireDate ?? "", assetTagId <- dataset.assetTagId ?? 0,escalatedAuditLevels <- dataset.escalatedAuditLevels ?? 0 ))
                }
                
                catch{
                  
                    print("Error in inserting data",error.localizedDescription)
                }
                
        }
    }
    
    func readValues(){
        pendingJobsData.removeAll()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let currenDate = dateFormatter.string(from: today)
        print("today",currenDate)
        do{
           
            for row in try db!.prepare("SELECT * FROM Pendingjobs WHERE schedule_expire_date > '\(today)'") {
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[15]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])\n,supplierName:\(row[13]) , companyName:\(row[14]), scheduleEnddate :\(row[16]) , assetTagId : \(row[17])")
//                let nextDate = row[10] as? String .sorted().first!
                
//                pendingJobsData.append(PendingJobData(audit_schduleId: row[0] as? Int64, id: row[1] as? Int64, name: row[2] as? String, assetTag: row[3] as? String, location: row[9] as? String , warrentyTime: row[8] as? Int64, supplier: row[13]  as? String, purchaseDate: row[6] as? String , modalName: row[5] as? String, category: row[12] as? String, company: row[13] as? String, auditValue: row[11] as? String , dueDate: row[10] as? String  , image: row[7] as? String, auditName: row[15] as? String , assetTagId: row[17] as? Int64 , escalatedAuditLevels: row[18] as? Int64, maintainanceType: ""))
                jobsTableView.reloadData()
            }
        }        catch{
            print("Error in reading data")
        }
    }
    //MARK: - PMHistory
    var audit_SchduleId = Expression<Int>("audit_schdule_id")
    var assetownerId  = Expression<Int>("asset_owner_id")
    var assettagId  = Expression<Int>("asset_tag_id")
    var assetuserOwnerId = Expression<String>("asset_user_owner_id")
    var auditendDate = Expression<String>("audit_end_date")
    var auditid = Expression<Int>("audit_id")
    var auditinspectionDate = Expression<String>("audit_inspection_date")
    var audit_Name = Expression<String>("audit_name")
    var audit_ParamsId = Expression<Int>("audit_params_id")
    var audit_Result = Expression<String>("audit_result")
    var audit_StartDate = Expression<String>("audit_start_date")
    var audit_Status = Expression<String>("audit_status")
    var audited_Params = Expression<String>("audited_params")
    let pmHistoryTable = Table("PMHistory")

    func insertPmHistory(){
        for data in getAllAssetlist{
            pmHistory = data.pmHistory
            pmHistory.map { data in
                do{
                    try db?.run(pmHistoryTable.insert(audit_SchduleId <- data["audit_schdule_id"] as? Int ?? 0, assetownerId <- data["asset_owner_id"] as? Int ?? 0 ,assettagId <- data["asset_tag_id"] as? Int ?? 0 , assetuserOwnerId <- data["asset_user_owner_id"] as? String ?? "" ,auditendDate  <- data["asset_user_owner_id"] as? String ?? "" , auditid <- data["audit_id"] as? Int ?? 0 ,auditinspectionDate <- data["audit_inspection_date"] as? String ?? "" , audit_Name <- data["audit_name"] as? String ?? "" , audit_ParamsId <- data["audit_params_id"] as? Int ?? 0 , audit_Result <- data["audit_result"] as? String ?? "" , audit_StartDate <-  data["audit_start_date"] as? String ?? "", audit_Status <- data["audit_status"] as? String ?? "" , audited_Params <- data["audited_params"] as? String ?? ""))
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
    }
    func readPmHistory(assetidtag : Int64){
        pmHistoryList.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM PMHistory WHERE asset_tag_id = \(assetidtag)") {
//                print("row283",row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12])
                pmHistoryList.append(pmHistoryData(audit_SchduleId: row[0] as? Int64 ?? 0,  assetownerId:row[1] as? Int64 ?? 0, assettagId: row[2] as? Int64 ?? 0, assetuserOwnerId:row[3] as? Int64 ?? 0, auditendDate: row[4] as? String, auditid: row[5] as? Int64 ?? 0 , auditinspectionDate: row[6] as? String , audit_Name: row[7] as? String, audit_ParamsId: row[8] as? Int64 ?? 0, audit_Result: row[9] as? String, audit_Status: row[11] as? String, audited_Params: row[12] as? String))
            }
        }catch{
            print(error.localizedDescription)
            print(String(describing: error))
        }
        
    }
    //MARK: - ApiCall
    func getPendingJobsApi(){
        let param : Parameters = [
            .user_id : user_Id ?? "" ,
        ]
       
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .pendingJob, baseURL: selectedComp ?? "", setToken: glbtoken )
        URLSession.shared.getPendingJobs(with: request) { sucess, totalAllAsset , message in
            DispatchQueue.main.async {
                if sucess{
                    self.jobsTableView.isHidden = false
                    print("success")
                    self.getAllAssetlist = totalAllAsset ?? []
                    self.activityIndicator.startAnimating()
                    self.insertData()
                    self.readValues()
                    self.jobsTableView.reloadData()
                    self.activityIndicator.stopAnimating()
                }else{
                    if !Reachability.isConnectedToNetwork(){
                        self.activityIndicator.stopAnimating()
                        self.readValues()
                    }
                    if message != ""{
                        self.activityIndicator.stopAnimating()
                        self.view.makeToast(message)
                    }else{
                        self.activityIndicator.stopAnimating()
                        self.view.makeToast("Something went wrong! Please try again later")
                    }
                        
                    self.jobsTableView.reloadData()
                    print("Fail")
                }
                
            }
        }
    }
}
extension PendingJobsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("PendingjobCount",pendingJobsData.count)
        return pendingJobsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingJobsTVC", for: indexPath) as! PendingJobsTVC
        if pendingJobsData.count > 0{
            
            let list = pendingJobsData[indexPath.row]
            cell.lblproductName.text = list.auditName
            cell.lblModalName.text = list.modalName
            cell.lblLocation.text = list.location
            if list.dueDate != "" {
                let newdate = GlobalFunction.convertDateString(dateString: list.dueDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                
                let dateformatter = DateFormatter()
                dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let currentTime = dateformatter.string(from: Date())
                if list.dueDate > currentTime {
                    cell.lblDuetitle.text = "Due:"
                }else{
                    cell.lblDuetitle.text = "Overdue:"
                }
                cell.lblDueDate.text = newdate
            }
            cell.lblAssetTag.text = "-\(list.assetTag)"
            if list.escalatedAuditLevels > 0 {
                cell.stackEscView.isHidden = false
                cell.lblEscValue.text = "L\(list.escalatedAuditLevels)"
            }else{
                cell.stackEscView.isHidden = true
            }
            let urlImg = URL(string: baseUrlImage + list.image)
            cell.imgProduct.sd_setImage(with: urlImg, placeholderImage: UIImage(named: "default"))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
        
        if pendingJobsData.count > 0{
            let list = pendingJobsData[indexPath.row]
         
            vc.productName = list.name
            vc.assetTag = list.assetTag
            vc.location = list.location
            vc.dueDate = list.dueDate
            vc.warranty = list.warrentyTime
            vc.supplier = list.supplier
            vc.purchaseDate = list.purchaseDate
            vc.modalnNo = list.modalName
            vc.category = list.category
            vc.companyName = list.company
            vc.assetTagiD = list.assetTagId
           
            vc.auditImageUrl = URL(string: baseUrlImage + list.image)
            vc.auditValues = list.auditValue
            vc.auditScheduleID = list.audit_schduleId
            vc.pmHistoryList = pmHistoryList
            vc.isPendingJob = true
            readPmHistory(assetidtag: list.assetTagId)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
   
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
