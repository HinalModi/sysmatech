//
//  CompletedJobsVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit
import SQLite

class CompletedJobsVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var lblNodataMsg: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var jobsTableView: UITableView!
    var datePicker: UIDatePicker!
    var iselectedDate:String = ""
    var getScheduleAssetlist : [CompletedTotalAssetScheduledata] = []
    var completedJobsData : [CompletedJobs] = []
    var selectedDate: String = String()
    var apiSelectDate = String()
    var dataSetDate = String()
    var refreshControl: UIRefreshControl!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        incrval = 0
        if #available(iOS 11.0, *) {
            activityIndicator.color = .black
        } else {
            activityIndicator.color = .blue
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        jobsTableView.tableFooterView = UIView()
        
        if (self.iselectedDate != ""){
            lblDate.text = iselectedDate
        }else{
           
            self.datePicker = UIDatePicker(frame: CGRect(x:0,y: 0,width: self.view.frame.size.width,height: (self.view.frame.size.height / 2)))
            self.datePicker.datePickerMode = .date
           
            self.datePicker.setDate(Date(), animated: true)
            self.datePicker.maximumDate = Date()
            self.dateSelected()
        }
        dateView.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 0))
        if #available(iOS 11.0, *) {
            imgDown.tintColor = Constant.Color.color_launch
        } else {
            imgDown.tintColor = .black
        }
       addPullRequest()
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                incrval = 0
            }catch{
                print(error.localizedDescription)
            }
            createDB()
        }
        getCompletedJobsApi()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            jobsTableView.refreshControl = refreshControl
        } else {
            jobsTableView.addSubview(refreshControl)
        }
        
    }
    
    //MARK: -StoreDataInSqlite
    private var db: Connection?
    let algorithms = Table("Completedjobs")
    let audit_schdule_id = Expression<Int>("audit_schdule_id")
    var auditId = Expression<Int>("audit_id")
    var auditName = Expression<String>("audit_name")
    var assetTagId = Expression<Int>("asset_tag_id")
    var auditStartDate = Expression<String>("audit_start_date")
    var auditEndDate = Expression<String>("audit_end_date")
    var scheduleExpireDate = Expression<String>("schedule_expire_date")
    var auditInspectionDate = Expression<String>("audit_inspection_date")
    var auditStatus = Expression<String>("audit_status")
    var auditResult = Expression<String>("audit_result")
    var inspectionDelay = Expression<String>("inspection_delay")
    var auditParamsId = Expression<Int>("audit_params_id")
    var assetOwnerId = Expression<Int>("asset_owner_id")
    var assetUserOwnerId = Expression<String>("asset_user_owner_id")
    var deletedAt = Expression<String>("deleted_at")
    var institutionId = Expression<Int>("institution_id")
    var prevMaintenanaceOwnerId = Expression<Int>("prev_maintenanace_owner_id")
    var prevMaintenanaceOwnerType = Expression<String>("prev_maintenanace_owner_type")
    var paramsMasterId = Expression<String>("params_master_id")
    var createdAt = Expression<String>("created_at")
    var updatedAt = Expression<String>("updated_at")
    var id = Expression<Int>("id")
    let name = Expression<String>("name")
    let asset_tag = Expression<String>("asset_tag")
    var modelId = Expression<Int>("model_id")
    let model_name = Expression<String>("model_name")
    var serial = Expression<String>("serial")
    var purchaseDate = Expression<String>("purchase_date")
    var purchaseCost = Expression<String>("purchase_cost")
    var orderNumber = Expression<String>("order_number")
    var assignedTo = Expression<String>("assigned_to")
    var notes = Expression<String>("notes")
    let image = Expression<String>("image")
    var userId = Expression<Int>("user_id")
    var physical = Expression<Int>("physical")
    var statusId = Expression<Int>("status_id")
    var archived = Expression<Int>("archived")
    let warrantyMonths = Expression<Int>("warranty_months")
    var depreciate = Expression<Int>("depreciate")
    var supplierId = Expression<Int>("supplier_id")
    var requestable = Expression<Int>("requestable")
    var rtdLocationId = Expression<Int>("rtd_location_id")
    let location = Expression<String>("location")
    var snipeitMacAddress1 = Expression<String>("_snipeit_mac_address_1")
    var accepted = Expression<String>("accepted")
    var lastCheckout = Expression<String>("last_checkout")
    var expectedCheckin = Expression<String>("expected_checkin")
    var companyId = Expression<Int>("company_id")
    var assignedType = Expression<String>("assigned_type")
    let lastAuditDate = Expression<String>("last_audit_date")
    var nextAuditDate = Expression<String>("next_audit_date")
    var locationId = Expression<Int>("location_id")
    var checkinCounter = Expression<Int>("checkin_counter")
    var checkoutCounter  = Expression<Int>("checkout_counter")
    var requestsCounter  = Expression<Int>("requests_counter")
    var auditParamsValues = Expression<String>("audit_params_values")
    var auditParamsTransaction = Expression<String>("audit_params_transaction")
    var dyamicModelFieldset = Expression<String>("dyamic_model_fieldset")
    var categoryName = Expression<String>("category_name")
    var supplierName = Expression<String>("supplier_name")
    var companyName = Expression<String>("company_name")
    var snipeitUplinkPorts2 = Expression<String>("_snipeit_uplink_ports_2")
    var snipeitPowerOverEthernetPoe3 = Expression<String>("_snipeit_power_over_ethernet_poe_3")
    var snipeitFan4 = Expression<String>("_snipeit_fan_4")
    var snipeitPowerSupply5 = Expression<String>("_snipeit_power_supply_5")
    var snipeitFeatures6 = Expression<String>("_snipeit_features_6")
    var snipeitNetworkManagement7 = Expression<String>("_snipeit_network_management_7")
    var snipeitWarrantyType8 = Expression<String>("_snipeit_warranty_type_8")
    var snipeitNetworkPorts9 = Expression<String>("_snipeit_network_ports_9")
    var prevMainOwnerType = Expression<String>("prev_main_owner_type")
    var inspectedBy = Expression<String>("prev_main_owner_type")
    
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                // If delta-math-helper.sqlite3 exists, rename it
                print(path)
               
                // Connect to database
                db = try Connection("\(path)/Completedjobs.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    let drop = algorithms.drop(ifExists: true)
                    try db?.run(drop)
                }
                // Initialize tables
                try db?.run(algorithms.create(ifNotExists: true) { table in
                    table.column(audit_schdule_id, primaryKey: true)
                    table.column(id)
                    table.column(name)
                    table.column(asset_tag)
                    table.column(modelId)
                    table.column(model_name)
                    table.column(purchaseDate)
                    table.column(image)
                    table.column(warrantyMonths)
                    table.column(location)
                    table.column(auditEndDate)
                    table.column(auditParamsValues)
                    table.column(categoryName)
                    table.column(supplierName)
                    table.column(companyName)
                    table.column(auditName)
                    table.column(lastAuditDate)
                    table.column(auditParamsTransaction)
                    table.column(assetTagId)
                    table.column(inspectedBy)
                })
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
   
    func insertData(){
        
        DispatchQueue.main.async { [self] in
            
            for dataset in getScheduleAssetlist{
                do{
                    completedJobsData.removeAll()
                 
                    dataSetDate = dataset.lastAuditDate ?? ""
                    try db?.run(algorithms.insert(audit_schdule_id <- dataset.auditSchduleId ?? 0, id <- dataset.id ?? 0,name <- dataset.name ?? "",asset_tag <- dataset.assetTag ?? "",modelId <- dataset.modelId ?? 0 ,image <- dataset.image ?? "",warrantyMonths <- dataset.warrantyMonths ?? 0,location <- dataset.location ?? "",auditEndDate <- dataset.auditEndDate ?? "",supplierName <- dataset.supplierName ?? "",model_name <- dataset.modelName ?? "",categoryName <- dataset.categoryName ?? "",companyName <- dataset.companyName ?? "",purchaseDate <- dataset.purchaseDate ?? "",auditParamsValues <- dataset.auditParamsValues ?? "",auditName <- dataset.auditName ?? "", lastAuditDate <- dataset.lastAuditDate ?? "", auditParamsTransaction <- dataset.auditParamsTransaction ?? "" , assetTagId <- dataset.assetTagId ?? 0 , inspectedBy <- dataset.inspectionBy ?? ""))
                    print(dataset.lastAuditDate)
                    readValues()
                }
                catch{
                    if completedJobsData.count == 0{
                        self.lblNodataMsg.isHidden = false
                    }
                    print("Error in inserting data",error.localizedDescription)
                }
                
            }
        }
    }
    func insertCompleted(){
        DispatchQueue.main.async { [self] in
            
            for dataset in getScheduleAssetlist{
                do{
//                    completedJobsData.removeAll()
                 
                    dataSetDate = dataset.lastAuditDate
                    try db?.run(algorithms.insert(audit_schdule_id <- dataset.auditSchduleId ?? 0, id <- dataset.id ?? 0,name <- dataset.name ?? "",asset_tag <- dataset.assetTag ?? "",modelId <- dataset.modelId ?? 0 ,image <- dataset.image ?? "",warrantyMonths <- dataset.warrantyMonths ?? 0,location <- dataset.location ?? "",auditEndDate <- dataset.auditEndDate ?? "",supplierName <- dataset.supplierName ?? "",model_name <- dataset.modelName ?? "",categoryName <- dataset.categoryName ?? "",companyName <- dataset.companyName ?? "",purchaseDate <- dataset.purchaseDate ?? "",auditParamsValues <- dataset.auditParamsValues ?? "",auditName <- dataset.auditName ?? "", lastAuditDate <- dataset.lastAuditDate ?? "", auditParamsTransaction <- dataset.auditParamsTransaction ?? "" , assetTagId <- dataset.assetTagId ?? 0, inspectedBy <- dataset.inspectionBy ?? ""))
//                    readValuesCompleted()
                }
                catch{
                   
                    print("Error in inserting data",error.localizedDescription)
                    
                }
                
            }
        }
    }
    func readValuesCompleted(){
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let today = dateFormatter.string(from: Date())
        do{
           
            // WHERE last_audit_date LIKE '%\(today)%' OR last_audit_date LIKE '%\(apiSelectDate)%' OR last_audit_date LIKE '%\(dataSetDate)%'
            for row in try db!.prepare("SELECT * FROM Completedjobs") {
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[15]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])\n,supplierName:\(row[13]) , companyName:\(row[14])\n,lastAuditDate:\(row[16])")
                completedJobsData.insert(CompletedJobs(audit_schduleId: row[0] as! Int64, id: row[1] as! Int64, name: row[2] as! String, assetTag: row[3] as! String, location: row[9] as! String , warrentyTime: row[8] as! Int64, supplier: row[13]  as! String, purchaseDate: row[6] as! String , modalName: row[5] as! String, category: row[12] as! String, company: row[13] as! String, auditValue: row[11] as! String , dueDate: row[10] as! String  , image: row[7] as! String, auditName: row[15] as! String, lastAuditDate: row[16] as! String , auditParamsTransaction:  row[17] as! String, assetTagId: row[18] as! Int64, inspectedBy:  row[19] as! String), at: 0)
            }
        }
        catch{
            print("Error in reading data",String(describing: error))
            
        }
    }
    func readOfflineValues(){
        do{
            jobsTableView.reloadData()
            for row in try db!.prepare("SELECT * FROM Completedjobs WHERE last_audit_date LIKE '%\(apiSelectDate)%'") {
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[15]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])\n,supplierName:\(row[13]) , companyName:\(row[14])\n,lastAuditDate:\(row[16])")
                activityIndicator.stopAnimating()
                completedJobsData.insert(CompletedJobs(audit_schduleId: row[0] as! Int64, id: row[1] as! Int64, name: row[2] as! String, assetTag: row[3] as! String, location: row[9] as! String , warrentyTime: row[8] as! Int64, supplier: row[13]  as! String, purchaseDate: row[6] as! String , modalName: row[5] as! String, category: row[12] as! String, company: row[13] as! String, auditValue: row[11] as! String , dueDate: row[10] as! String  , image: row[7] as! String, auditName: row[15] as! String, lastAuditDate: row[16] as! String , auditParamsTransaction:  row[17] as! String ,assetTagId : row[18] as! Int64, inspectedBy: row[19] as! String), at: 0)
                jobsTableView.reloadData()
            }
            if completedJobsData.count == 0{
                activityIndicator.stopAnimating()
                self.lblNodataMsg.isHidden = false
            }
        }
        catch{
            activityIndicator.stopAnimating()
            print("Error in reading data",error.localizedDescription)
            
        }

    }
    func readValues(){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let today = dateFormatter.string(from: Date())
        
        do{
            jobsTableView.reloadData()
            print(apiSelectDate)
            print(dataSetDate)
            print(today)
            //"SELECT * FROM Completedjobs WHERE last_audit_date LIKE '%\(today)%' OR last_audit_date LIKE '%\(apiSelectDate)%' OR last_audit_date LIKE '%\(dataSetDate)%'"
            for row in try db!.prepare("SELECT * FROM Completedjobs WHERE last_audit_date LIKE '%\(today)%' OR last_audit_date LIKE '%\(apiSelectDate)%' OR last_audit_date LIKE '%\(dataSetDate)%'") {
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[15]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])\n,supplierName:\(row[13]) , companyName:\(row[14])\n,lastAuditDate:\(row[16])")
                activityIndicator.stopAnimating()
                completedJobsData.insert(CompletedJobs(audit_schduleId: row[0] as! Int64, id: row[1] as! Int64, name: row[2] as! String, assetTag: row[3] as! String, location: row[9] as! String , warrentyTime: row[8] as! Int64, supplier: row[13]  as! String, purchaseDate: row[6] as! String , modalName: row[5] as! String, category: row[12] as! String, company: row[13] as! String, auditValue: row[11] as! String , dueDate: row[10] as! String  , image: row[7] as! String, auditName: row[15] as! String, lastAuditDate: row[16] as! String , auditParamsTransaction:  row[17] as! String, assetTagId: row[18] as! Int64, inspectedBy: row[19] as! String), at: 0)
                print("completedJobsData.count",completedJobsData.count)
                jobsTableView.reloadData()
            }
        }
        catch{
            activityIndicator.stopAnimating()
            print("Error in reading data",error.localizedDescription)
            
        }
    }
    

    //MARK: - ApiCall
    func getCompletedJobsApi(){
        if completedJobsData.count > 0{
            activityIndicator.stopAnimating()
        }else{
            if incrCom == 1{
                activityIndicator.stopAnimating()
            }else{
                activityIndicator.startAnimating()
            }
        }
//        activityIndicator.startAnimating()
        let param : Parameters = [
            .user_id : UserDefaults.standard.string(forKey: "user_id") ?? "" ,
            .date : apiSelectDate,
        ]
        print(param)
        
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .completedJob, baseURL: selectedComp ?? "", setToken: glbtoken)
        URLSession.shared.getCompletedJobs(with: request) { sucess, scheduleAsset , message in
            
            DispatchQueue.main.async { [self] in
                if sucess{
                    incrCom = 1
                    print("success")
                    self.getScheduleAssetlist = scheduleAsset
                    self.insertData()
                    self.lblNodataMsg.isHidden = true
                    self.jobsTableView.reloadData()
                    self.activityIndicator.stopAnimating()
                }else{
                    print(message)
                    self.completedJobsData = []
                    self.readOfflineValues()
                    incrCom = 1
                    activityIndicator.stopAnimating()
                    if completedJobsData.count == 0 {                        
                        self.lblNodataMsg.isHidden = false
                    }else{
                        self.lblNodataMsg.isHidden = true
                    }
                    self.jobsTableView.reloadData()
                }
            }
        }
    }

    @objc private func dateChanged() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        dateSelected()
       
    }
    @objc func dateSelected()
    {
        selectedDate =  dateformatterDateTime(date: datePicker.date)
        apiSelectDate = dateformatterDateTimeForApi(date: datePicker.date)        
        print(selectedDate)
        print(apiSelectDate)
        datePicker.setDate(datePicker.date, animated: true)
        datePicker.removeFromSuperview()
        lblDate.text = selectedDate
        createDB()
        getCompletedJobsApi()
    }
    func dateformatterDateTimeForApi(date: Date) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    func dateformatterDateTime(date: Date) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    // MARK: - Action
    @IBAction func btnOpenCalender(_ sender: Any) {
        if #available(iOS 14.0, *) {
            datePicker.locale = .current
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.preferredDatePickerStyle = .inline
            datePicker.center = self.view.center
            datePicker.backgroundColor = UIColor.white
            datePicker.layer.shadowColor = UIColor.gray.cgColor
            datePicker.layer.shadowRadius = 5
            datePicker.layer.shadowOpacity = 0.3
            datePicker.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            self.datePicker.addTarget(self, action:#selector(self.dateChanged), for: .valueChanged)
            self.view.addSubview(self.datePicker)
        }
    }
   
}
extension CompletedJobsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("completedJobsData",completedJobsData.count)
        return completedJobsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompletedJobsTVC") as! CompletedJobsTVC
        if completedJobsData.count > 0{
            
            let list = completedJobsData[indexPath.row]
            
            cell.lblProductName.text = list.auditName
            cell.lblAssetTag.text = list.assetTag
            cell.lblModalName.text = list.modalName
            cell.lblLocation.text = list.location
            if list.lastAuditDate != ""{
                let newdate = GlobalFunction.convertDateString(dateString: list.lastAuditDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                
                cell.lblDueDate.text = newdate
            }
            if list.inspectedBy != ""{
                cell.lblInspectedBy.text = list.inspectedBy
            }
            print(list.auditParamsTransaction)
            
            let urlImg = URL(string: baseUrlImage + list.image)
            cell.imgProduct.sd_setImage(with: urlImg,placeholderImage: UIImage(named: "setting2"))
            cell.setImage = {
                if list.image != ""{
                    let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                    vc.imgUrl = urlImg
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if completedJobsData.count > 0{
            let list = completedJobsData[indexPath.row]
            let storyBoard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "InspectionDetailVC") as! InspectionDetailVC
            let urlImg = URL(string: baseUrlImage + list.image)
            vc.audit_ScheduleId = list.audit_schduleId
            vc.urlImg = urlImg
            vc.auditValue = list.auditValue
            vc.auditTransactionVal = list.auditParamsTransaction
            
            vc.getAssetName = list.auditName
            vc.getAssetTag = list.assetTag
            vc.getAuditName = list.modalName
            vc.getLocation = list.location
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension Date {
    func today(format : String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

