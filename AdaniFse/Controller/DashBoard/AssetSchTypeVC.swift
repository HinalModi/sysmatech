//
//  AssetSchTypeVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 26/07/22.
//

import UIKit
import SQLite
import AVFoundation

class AssetSchTypeVC: UIViewController {
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventJobTV: UITableView!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    var getAllAssetlist : [AssetTypeTotalAssetScheduledata] = []
    var preventiveJobsData : [PendingJobData] = []
    var refreshControl: UIRefreshControl!
    var isBreakDown = true
    var isDashBoard = false
    var isfromCategory = false
    var categoryID = Int64()
    var mainTainType = "preventive"
    var isFromModel = false
    var modelName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {

        preventJobTV.tableFooterView = UIView()
        if UserDefaults.standard.string(forKey: "Lang") == nil {
            UserDefaults.standard.set("Hindi", forKey: "Lang")
        }
        getPreventiveData()
        createDB()
        if isfromCategory{
            readValuesForCategory()
        }else if isFromModel{
            readValuesForModel()
        }else{
            if isBreakDown{
                readValuesForBreakDown()
            }else{
                readValuesForPreventive()
            }
        }
        addPullRequest()
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                incrval = 0
            }catch{
                print(error.localizedDescription)
            }
            getPreventiveData()
            createDB()
        }
        if isfromCategory{
            readValuesForCategory()
        }else if isFromModel{
            readValuesForModel()
        } else{
            if isBreakDown{
                readValuesForBreakDown()
            }else{
                readValuesForPreventive()
            }
        }
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            preventJobTV.refreshControl = refreshControl
        } else {
            preventJobTV.addSubview(refreshControl)
        }
    }
    //MARK: - ApiCall
    func getPreventiveData(){
        if preventiveJobsData.count > 0{
            activityIndicator.stopAnimating()
        }else{
            preventJobTV.reloadData()
            activityIndicator.startAnimating()
        }
        let param : Parameters = [
            .user_id : UserDefaults.standard.string(forKey: "user_id") ?? "" ,
        ]
       
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .get_my_asset_type_schedule, baseURL: selectedComp ?? "", setToken: glbtoken)
        print("request",request)
        URLSession.shared.getAssetSchType(with: request) { sucess, totalAllAsset , message ,status in
            DispatchQueue.main.async { [self] in
                if sucess{
                    print(totalAllAsset)
                    activityIndicator.stopAnimating()
                    self.getAllAssetlist = totalAllAsset ?? []
                    insertData()
                    readValuesForBreakDown()
                    if isfromCategory{
                        readValuesForCategory()
                    }else if isFromModel{
                        readValuesForModel()
                    }else{
                        if isBreakDown{
                            readValuesForBreakDown()
                            if preventiveJobsData.count > 0{
                                lblNoData.isHidden = true
                            }else{
                                lblNoData.isHidden = false
                            }
                        }else{
                            readValuesForPreventive()
                            if preventiveJobsData.count > 0{
                                lblNoData.isHidden = true
                            }else{
                                lblNoData.isHidden = false
                            }
                        }
                    }
                    preventJobTV.reloadData()
//                    activityIndicator.stopAnimating()
                }else{
                    
                    print(message)
                    print(status)
//                    self.view.makeToast("Something went wrong")
                    if !Reachability.isConnectedToNetwork(){
                        if preventiveJobsData.count == 0{
                            lblNoData.isHidden = false
                        }
                        activityIndicator.stopAnimating()
                        preventJobTV.reloadData()
                        print("Fail")
                    }
                    
                }
            }
        }
    }
    //MARK: -StoreDataInSqlite
    
    let algorithms = Table("PreventiveJobs")
    let audit_schdule_id = Expression<Int>("audit_schdule_id")
    var auditId = Expression<Int>("audit_id")
    var auditName = Expression<String>("audit_name")
    var assetTagId = Expression<Int>("asset_tag_id")
    var auditStartDate = Expression<String>("audit_start_date")
    var auditEndDate = Expression<String>("audit_end_date")
    var scheduleExpireDate = Expression<String>("schedule_expire_date")
    var auditInspectionDate = Expression<String>("audit_inspection_date")
    var auditStatus = Expression<String>("audit_status")
    var auditResult = Expression<String>("audit_result")
    var inspectionDelay = Expression<String>("inspection_delay")
    var auditParamsId = Expression<Int>("audit_params_id")
    var assetOwnerId = Expression<Int>("asset_owner_id")
    var assetUserOwnerId = Expression<String>("asset_user_owner_id")
    var deletedAt = Expression<String>("deleted_at")
    var institutionId = Expression<Int>("institution_id")
    var prevMaintenanaceOwnerId = Expression<Int>("prev_maintenanace_owner_id")
    var prevMaintenanaceOwnerType = Expression<String>("prev_maintenanace_owner_type")
    var paramsMasterId = Expression<String>("params_master_id")
    var createdAt = Expression<String>("created_at")
    var updatedAt = Expression<String>("updated_at")
    var id = Expression<Int>("id")
    let name = Expression<String>("name")
    let asset_tag = Expression<String>("asset_tag")
    var modelId = Expression<Int>("model_id")
    let model_name = Expression<String>("model_name")
    let image = Expression<String>("image")
    var userId = Expression<Int>("user_id")
    let warrantyMonths = Expression<Int>("warranty_months")
    var location = Expression<String>("location")
    let lastAuditDate = Expression<String>("last_audit_date")
    var nextAuditDate = Expression<String>("next_audit_date")
    var auditParamsValues = Expression<String>("audit_params_values")
    var auditParamsTransaction = Expression<String>("audit_params_transaction")
    var categoryName = Expression<String>("category_name")
    var supplierName = Expression<String>("supplier_name")
    var companyName = Expression<String>("company_name")
    var prevMainOwnerType = Expression<String>("prev_main_owner_type")
    var escalatedAuditLevels = Expression<String>("escalated_audit_levels")
    var purchaseDate = Expression<String>("purchase_date")
    var maintenanceType = Expression<String>("maintenance_type")
    var cat_Id = Expression<Int>("category_id")
    var can_checkIn = Expression<Int>("can_checkin")
    var can_checkOut = Expression<Int>("can_checkout")
    var asset_image = Expression<String>("asset_image")
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/preventivejobs.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    if isDashBoard {                        
//                        let drop = algorithms.drop(ifExists: true)
//                        try db?.run(drop)
                    }
                }
                // Initialize tables
                try db?.run(algorithms.create { table in
                    table.column(audit_schdule_id, primaryKey: true)
                    table.column(id)
                    table.column(name)
                    table.column(asset_tag)
                    table.column(modelId)
                    table.column(model_name)
                    table.column(purchaseDate)
                    table.column(image)
                    table.column(warrantyMonths)
                    table.column(location)
                    table.column(auditEndDate)
                    table.column(auditParamsValues)
                    table.column(categoryName)
                    table.column(supplierName)
                    table.column(companyName)
                    table.column(auditName)
                    table.column(scheduleExpireDate)
                    table.column(assetTagId)
                    table.column(escalatedAuditLevels)
                    table.column(maintenanceType)
                    table.column(cat_Id)
                    table.column(can_checkIn)
                    table.column(can_checkOut)
                    table.column(asset_image)
                })
            }
        } catch {
            print(String(describing: error))
            
        }
    }
    
    func insertData(){
        
        for dataset in getAllAssetlist{
            
            do{
                try db?.run(algorithms.insert(audit_schdule_id <- dataset.auditSchduleId ?? 0 , id <- dataset.id ?? 0 ,name <- dataset.name  ?? "",asset_tag <- dataset.assetTag  ?? "",modelId <- dataset.modelId ?? 0 ,image <- dataset.asset_image  ?? "",warrantyMonths <- dataset.warrantyMonths  ?? 0,location <- dataset.location ?? "",auditEndDate <- dataset.auditEndDate  ?? "",supplierName <- dataset.supplierName ?? "",model_name <- dataset.modelName  ?? "",categoryName <- dataset.categoryName  ?? "" ,companyName <- dataset.companyName  ?? "",purchaseDate <- dataset.purchaseDate  ?? "" ,auditParamsValues <- dataset.auditParamsValues  ?? "" ,auditName <- dataset.auditName  ?? "", scheduleExpireDate <- dataset.scheduleExpireDate ?? "", assetTagId <- dataset.assetTagId ?? 0,escalatedAuditLevels <- dataset.escalatedAuditLevels ?? "" , maintenanceType <- dataset.maintenanceType ?? "",cat_Id <- dataset.categoryId ?? 0 , can_checkIn <- dataset.can_checkin  ?? 0, can_checkOut <- dataset.can_checkout ?? 0 , asset_image <- dataset.asset_image))
                if isfromCategory{
                    readValuesForCategory()
                }else if isFromModel{
                    readValuesForModel()
                }else{
                    readValuesForOperational()
                }
            }
            catch{
                print("Error in inserting data",error.localizedDescription)
            }
        }
    }
    func readValuesForPreventive(){
        preventiveJobsData.removeAll()
       let prevent = "preventive"
        do{
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type LIKE '%\(prevent)%' AND schedule_expire_date >= '\(today)'") {
                print(row)
                activityIndicator.stopAnimating()
                preventiveJobsData.append(PendingJobData(audit_schduleId: row[0] as? Int64, id: row[1] as? Int64, name: row[2] as? String, assetTag: row[3] as? String, location: row[9] as? String , warrentyTime: row[8] as? Int64, supplier: row[13]  as? String, purchaseDate: row[6] as? String , modalName: row[5] as? String, category: row[12] as? String, company: row[13] as? String, auditValue: row[11] as? String , dueDate: row[10] as? String  , image: row[7] as? String, auditName: row[15] as? String , assetTagId: row[17] as? Int64 , escalatedAuditLevels: row[18] as? Int64, maintainanceType: row[19] as? String ,can_checkin: row[21] as? Int64,can_checkout: row[22] as? Int64 ,asset_image: row[23] as? String ?? ""))
                preventJobTV.reloadData()
                
            }
            for _ in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type LIKE '%\(prevent)%' AND schedule_expire_date >= '\(today)'") {
                if preventiveJobsData.count > 0{
                    lblNoData.isHidden = true
                }else{
                    activityIndicator.stopAnimating()
                    lblNoData.isHidden = false
                }
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
            print("Error in reading data" , String(describing: error))
        }
        
    }
    func readValuesForCategory(){
        preventiveJobsData.removeAll()
        var type = String()
        if isBreakDown{
            type = "breakdown"
        }else{
            type = "preventive"
        }
        do{
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type LIKE '%\(type)%' AND schedule_expire_date >= '\(today)'AND category_id = \(categoryID)") {
                preventiveJobsData.append(PendingJobData(audit_schduleId: row[0] as? Int64, id: row[1] as? Int64, name: row[2] as? String, assetTag: row[3] as? String, location: row[9] as? String , warrentyTime: row[8] as? Int64, supplier: row[13]  as? String, purchaseDate: row[6] as? String , modalName: row[5] as? String, category: row[12] as? String, company: row[13] as? String, auditValue: row[11] as? String , dueDate: row[10] as? String  , image: row[7] as? String, auditName: row[15] as? String , assetTagId: row[17] as? Int64 , escalatedAuditLevels: row[18] as? Int64, maintainanceType: row[19] as? String ,can_checkin: row[21] as? Int64,can_checkout: row[22] as? Int64,asset_image: row[23] as? String ?? ""))
                preventJobTV.reloadData()
                
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
        
    }
    func readValuesForModel(){
        preventiveJobsData.removeAll()
        var type = String()
        if isBreakDown{
            type = "breakdown"
        }else{
            type = "preventive"
        }
        do{
          // WHERE schedule_expire_date > '\(today)' AND
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type LIKE '%\(type)%' AND schedule_expire_date >= '\(today)'AND category_id = \(categoryID) AND model_name = '\(modelName)'") {
                
                preventiveJobsData.append(PendingJobData(audit_schduleId: row[0] as? Int64, id: row[1] as? Int64, name: row[2] as? String, assetTag: row[3] as? String, location: row[9] as? String , warrentyTime: row[8] as? Int64, supplier: row[13]  as? String, purchaseDate: row[6] as? String , modalName: row[5] as? String, category: row[12] as? String, company: row[13] as? String, auditValue: row[11] as? String , dueDate: row[10] as? String  , image: row[7] as? String, auditName: row[15] as? String , assetTagId: row[17] as? Int64 , escalatedAuditLevels: row[18] as? Int64, maintainanceType: row[19] as? String ,can_checkin: row[21] as? Int64,can_checkout: row[22] as? Int64,asset_image: row[23] as? String ?? ""))
                preventJobTV.reloadData()
                
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
        
    }
    func readValuesForOperational(){
        //        preventiveJobsData.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM PreventiveJobs") {
                preventiveJobsData.append(PendingJobData(audit_schduleId: row[0] as? Int64, id: row[1] as? Int64, name: row[2] as? String, assetTag: row[3] as? String, location: row[9] as? String , warrentyTime: row[8] as? Int64, supplier: row[13]  as? String, purchaseDate: row[6] as? String , modalName: row[5] as? String, category: row[12] as? String, company: row[13] as? String, auditValue: row[11] as? String , dueDate: row[10] as? String  , image: row[7] as? String, auditName: row[15] as? String , assetTagId: row[17] as? Int64 , escalatedAuditLevels: row[18] as? Int64, maintainanceType: row[19] as? String,can_checkin: row[21] as? Int64,can_checkout: row[22] as? Int64 ,asset_image: row[23] as? String ?? ""))
                
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
        
    }
    
    func readValuesForBreakDown(){
        
        preventiveJobsData.removeAll()
       let breakdown = "breakdown"
        do{
          // WHERE schedule_expire_date > '\(today)' AND
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type LIKE '%\(breakdown)%' AND schedule_expire_date > '\(today)'") {
                activityIndicator.stopAnimating()
                preventiveJobsData.append(PendingJobData(audit_schduleId: row[0] as? Int64, id: row[1] as? Int64, name: row[2] as? String, assetTag: row[3] as? String, location: row[9] as? String , warrentyTime: row[8] as? Int64, supplier: row[13]  as? String, purchaseDate: row[6] as? String , modalName: row[5] as? String, category: row[12] as? String, company: row[13] as? String, auditValue: row[11] as? String , dueDate: row[10] as? String  , image: row[7] as? String, auditName: row[15] as? String , assetTagId: row[17] as? Int64 , escalatedAuditLevels: row[18] as? Int64, maintainanceType: row[19] as? String ,can_checkin: row[21] as? Int64,can_checkout: row[22] as? Int64 ,asset_image: row[23] as? String ?? ""))
                
                preventJobTV.reloadData()
                
            }
            for _ in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type NOT LIKE '%\(breakdown)%' AND schedule_expire_date > '\(today)'") {
                if preventiveJobsData.count > 0{
                    lblNoData.isHidden = true
                }else{
                    activityIndicator.stopAnimating()
                    lblNoData.isHidden = false
                }
            }
        }        catch{
            print("Error in reading data" , error.localizedDescription)
        }
    }
    }
extension AssetSchTypeVC : UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("getSChAssetlist",preventiveJobsData.count)
        return preventiveJobsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssetSchTypeTVC", for: indexPath) as! AssetSchTypeTVC
        if preventiveJobsData.count > 0{
            let list = preventiveJobsData[indexPath.row]
            cell.lblAuditName.text = list.auditName
            cell.lblModalName.text = list.modalName
            cell.lblLocation.text = list.location
//            print(list.auditValue)
            if list.dueDate != ""{
                let newdate = GlobalFunction.convertDateString(dateString: list.dueDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                
                let dateformatter = DateFormatter()
                dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let currentTime = dateformatter.string(from: Date())
                if list.dueDate > currentTime {
                    cell.lblDueTitle.text = "Due:"
                }else{
                    cell.lblDueTitle.text = "Overdue:"
                }
                cell.lblDate.text = newdate
            }
            cell.lblAssetTag.text = "-\(list.assetTag)"
            if list.escalatedAuditLevels > 0 {
                cell.stackEscView.isHidden = false
                cell.lblEscValue.text = "L\(list.escalatedAuditLevels)"
            }else{
                cell.stackEscView.isHidden = true
            }
            if list.asset_image != "" {
                cell.imgAudit.sd_setImage(with: URL(string: list.asset_image), placeholderImage: UIImage(named: "setting2"))
            }else{
                cell.imgAudit.sd_setImage(with: URL(string: baseUrlImage + list.image), placeholderImage: UIImage(named: "setting2"))
            }
            cell.setImage = {
                if list.image != ""{
                    let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                    if list.asset_image != "" {
                        vc.imgUrl = URL(string: list.asset_image)
                    }else{
                        vc.imgUrl = URL(string: baseUrlImage + list.image)
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
//        print(preventiveJobsData.count)
        if preventiveJobsData.count > 0{
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
            let list = preventiveJobsData[indexPath.row]
            vc.productName = list.name
            vc.assetTag = list.assetTag
            vc.location = list.location
            vc.dueDate = list.dueDate
            vc.warranty = list.warrentyTime
            vc.supplier = list.supplier
            vc.purchaseDate = list.purchaseDate
            vc.modalnNo = list.modalName
            vc.category = list.category
            vc.companyName = list.company
            vc.assetTagiD = list.assetTagId
            vc.auditScheduleID = list.audit_schduleId
            print(list.audit_schduleId)
            if list.asset_image != "" {
                vc.auditImageUrl = URL(string: list.asset_image)
            }else{
                vc.auditImageUrl = URL(string: baseUrlImage + list.image)
            }
            vc.auditValues = list.auditValue
            vc.maintainType = list.maintainanceType
            vc.isPendingJob = true
            vc.checkInCount = Int(list.can_checkin)
            vc.checkOutCount = Int(list.can_checkout)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension URL {
    func download(to directory: FileManager.SearchPathDirectory, using fileName: String? = nil, overwrite: Bool = false, completion: @escaping (URL?, Error?) -> Void) throws {
        let directory = try FileManager.default.url(for: directory, in: .userDomainMask, appropriateFor: nil, create: true)
        let destination: URL
        if let fileName = fileName {
            destination = directory
                .appendingPathComponent(fileName)
                .appendingPathExtension(self.pathExtension)
        } else {
            destination = directory
            .appendingPathComponent(lastPathComponent)
        }
        if !overwrite, FileManager.default.fileExists(atPath: destination.path) {
            completion(destination, nil)
            return
        }
        URLSession.shared.downloadTask(with: self) { location, _, error in
            guard let location = location else {
                completion(nil, error)
                return
            }
            do {
                if overwrite, FileManager.default.fileExists(atPath: destination.path) {
                    try FileManager.default.removeItem(at: destination)
                }
                try FileManager.default.moveItem(at: location, to: destination)
                completion(destination, nil)
            } catch {
                print(error)
            }
        }.resume()
    }
}
