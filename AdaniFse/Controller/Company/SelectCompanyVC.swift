//
//  SelectCompanyVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 22/07/22.
//

import UIKit
import SQLite
import LGSideMenuController

class SelectCompanyVC: UIViewController {

    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var compListTV: UITableView!
    var phoneNo = String()
    var getCompanyList : [[String : Any]] = []
    var dataList : [getCompData] = []
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let mobile = UserDefaults.standard.string(forKey: "mobile")
    let companyName = UserDefaults.standard.string(forKey: "companyName")
    let fullName = UserDefaults.standard.string(forKey: "user_full_name")
    var selectedIndex = IndexPath(row: 0, section: 0)
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showCompany()
    }
    
    override func viewDidLayoutSubviews() {
        compListTV.tableFooterView = UIView()
        if #available(iOS 11.0, *) {
            compListTV.backgroundColor = Constant.Color.color_launch
            self.view.backgroundColor = Constant.Color.color_launch
        } else {
            compListTV.backgroundColor = .clear
        }
        btnProceed.setCorner(_tp: 10)
    }
    //MARK: - Api call
    private func getPostString(params: [String: String]) -> String {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
  
    func showCompany(){
        activityIndicator.startAnimating()

        print(phoneNo)
        if phoneNo != ""{
            UserDefaults.standard.set(phoneNo, forKey: "mobile")
        }
        let parameters : [String: String] = [
            "phone" : UserDefaults.standard.string(forKey: "mobile") ?? "" ,
        ]
        let url = URL(string: "https://staging.sysmatech.com/public/db_check.php")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        let postString = self.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let data = data {
                print(data)
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    print(dic["api_status"])
                    self.getCompanyList = dic["data"] as! [[String : Any]]
                    print(self.getCompanyList)
                    for data in self.getCompanyList{
                        print(data.count)
                        self.dataList.append(getCompData(companyName: data["company_name"] as? String ?? "", companyId: data["company_id"] as? String ?? "", domain: data["domain_name"] as? String ?? "", user_id: data["user_id"] as? String ?? "",assetImgPath: data["asset_img_path"] as? String ?? "" ,assetInsepctionImgPath: data["asset_insepction_img_path"] as? String ?? "" , assetBreakdwon_insepction_img_path: data["asset_breakdwon_insepction_img_path"] as? String ?? "",bearerToken: data["BearerToken"] as? String ?? "", audit_param_audio_path: data["audit_param_audio_path"] as? String ?? "", fullName: data["user_full_name"] as? String ?? ""))
                    }
                    print(self.dataList)
                    DispatchQueue.main.async {
                        self.compListTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }
                }catch{
                    print(error)
                }
            }
        }.resume()
        if !Reachability.isConnectedToNetwork(){
            self.activityIndicator.stopAnimating()
            self.view.makeToast("Please check your internet connection!")
        }
    }
    @IBAction func btnProceedFrwd(_ sender: UIButton) {
        var alert = UIAlertController()
        if UserDefaults.standard.string(forKey: "domain") != nil{
            alert = UIAlertController(title: "", message: "Do you want to change company?", preferredStyle: .alert)
        }else{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            vc.setCompanyBase = dataList[selectedIndex.row].domain
            UserDefaults.standard.removeObject(forKey: "user_id")
            UserDefaults.standard.removeObject(forKey: "domain")
            UserDefaults.standard.removeObject(forKey: "companyName")
            UserDefaults.standard.removeObject(forKey: "assetImgPath")
            UserDefaults.standard.removeObject(forKey: "assetInsepctionImgPath")
            UserDefaults.standard.removeObject(forKey: "assetBreakdwon_insepction_img_path")
            UserDefaults.standard.removeObject(forKey: "bearerToken")
            UserDefaults.standard.removeObject(forKey: "audit_param_audio_path")
            glbtoken = ""
            UserDefaults.standard.set(dataList[selectedIndex.row].user_id, forKey: "user_id")
            print(UserDefaults.standard.string(forKey: "user_id"))
            UserDefaults.standard.set(dataList[selectedIndex.row].fullName, forKey: "user_full_name")
            UserDefaults.standard.set(dataList[selectedIndex.row].domain, forKey: "domain")
            UserDefaults.standard.set(dataList[selectedIndex.row].companyName, forKey: "companyName")
            UserDefaults.standard.set(dataList[selectedIndex.row].assetImgPath, forKey: "assetImgPath")
            UserDefaults.standard.set(dataList[selectedIndex.row].assetInsepctionImgPath, forKey: "assetInsepctionImgPath")
            UserDefaults.standard.set(dataList[selectedIndex.row].assetBreakdwon_insepction_img_path, forKey: "assetBreakdwon_insepction_img_path")
            UserDefaults.standard.set(dataList[selectedIndex.row].bearerToken, forKey: "bearerToken")
            UserDefaults.standard.set(dataList[selectedIndex.row].audit_param_audio_path, forKey: "audit_param_audio_path")
            glbtoken = "Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "")
            showSideMenu(viewController: self)
            do {
                let table = Table("Pendingjobs")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table2 = Table("Completedjobs")
                let drop2 = table2.drop(ifExists: true)
                try db?.run(drop2)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table3 = Table("SubmitInspection")
                let drop3 = table3.drop(ifExists: true)
                try db?.run(drop3)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table3 = Table("PMHistory")
                let drop2 = table3.drop(ifExists: true)
                try db?.run(drop2)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("AssetList")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("CauseMaster")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("PreventiveJobs")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Accessories")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Consumables")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Licences")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("PredefinedKit")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Components")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [self] action in
            let vc = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            UserDefaults.standard.removeObject(forKey: "user_id")
            UserDefaults.standard.removeObject(forKey: "domain")
            UserDefaults.standard.removeObject(forKey: "companyName")
            UserDefaults.standard.removeObject(forKey: "user_full_name")
            UserDefaults.standard.removeObject(forKey: "assetImgPath")
            UserDefaults.standard.removeObject(forKey: "assetInsepctionImgPath")
            UserDefaults.standard.removeObject(forKey: "assetBreakdwon_insepction_img_path")
            UserDefaults.standard.removeObject(forKey: "bearerToken")
            UserDefaults.standard.removeObject(forKey: "audit_param_audio_path")
            glbtoken = ""
            UserDefaults.standard.set(dataList[selectedIndex.row].user_id, forKey: "user_id")
            print(UserDefaults.standard.string(forKey: "user_id"))
            
            UserDefaults.standard.set(dataList[selectedIndex.row].fullName, forKey: "user_full_name")
            UserDefaults.standard.set(dataList[selectedIndex.row].domain, forKey: "domain")
            UserDefaults.standard.set(dataList[selectedIndex.row].companyName, forKey: "companyName")
            UserDefaults.standard.set(dataList[selectedIndex.row].assetImgPath, forKey: "assetImgPath")
            UserDefaults.standard.set(dataList[selectedIndex.row].assetInsepctionImgPath, forKey: "assetInsepctionImgPath")
            UserDefaults.standard.set(dataList[selectedIndex.row].assetBreakdwon_insepction_img_path, forKey: "assetBreakdwon_insepction_img_path")
            UserDefaults.standard.set(dataList[selectedIndex.row].bearerToken, forKey: "bearerToken")
            UserDefaults.standard.set(dataList[selectedIndex.row].audit_param_audio_path, forKey: "audit_param_audio_path")
            glbtoken = "Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "")
            showSideMenu(viewController: self)
            do {
                let table = Table("Pendingjobs")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table2 = Table("Completedjobs")
                let drop2 = table2.drop(ifExists: true)
                try db?.run(drop2)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table3 = Table("SubmitInspection")
                let drop3 = table3.drop(ifExists: true)
                try db?.run(drop3)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("AssetList")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table3 = Table("PMHistory")
                let drop2 = table3.drop(ifExists: true)
                try db?.run(drop2)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table3 = Table("CategoryList")
                let drop2 = table3.drop(ifExists: true)
                try db?.run(drop2)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table3 = Table("ModelList")
                let drop2 = table3.drop(ifExists: true)
                try db?.run(drop2)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Accessories")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Consumables")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Licences")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("PredefinedKit")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            do{
                let table = Table("Components")
                let drop = table.drop(ifExists: true)
                try db?.run(drop)
            }catch{
                print(error.localizedDescription)
            }
            self.navigationController?.pushViewController(vc, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "cancel", style: .destructive, handler: nil))
        present(alert, animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    func showSideMenu(viewController : UIViewController){
        let leftView = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier:
            "SideMenuVC") as! SideMenuVC
        let nav = UINavigationController(rootViewController: viewController)
        nav.navigationBar.isHidden = true
        let lgMenu = LGSideMenuController(rootViewController: nav, leftViewController: leftView, rightViewController: nil)
        lgMenu.isRightViewSwipeGestureEnabled = false
        lgMenu.isLeftViewSwipeGestureEnabled = false
        lgMenu.leftViewWidth = UIScreen.main.bounds.width / 1.4
        lgMenu.rightViewWidth = UIScreen.main.bounds.width / 1.4
        lgMenu.leftViewPresentationStyle = .slideAbove
        lgMenu.rightViewPresentationStyle = .slideAbove
        if #available(iOS 11.0, *) {
            lgMenu.rootViewCoverColor = UIColor(named: "blackTop")?.withAlphaComponent(0.75)
        }
        Constant.APPDELEGATE.window?.rootViewController = lgMenu
    }
}
extension SelectCompanyVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCompanyTVC") as! SelectCompanyTVC
        if #available(iOS 11.0, *) {
            cell.backgroundColor = Constant.Color.color_launch
        }
        cell.lblComName.text = dataList[indexPath.row].companyName
        if UserDefaults.standard.string(forKey: "companyName") == dataList[indexPath.row].companyName{
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
        }
        if indexPath.row == 0{
            if UserDefaults.standard.string(forKey: "domain") == nil{
                tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SelectCompanyTVC
        selectedIndex = indexPath
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        if #available(iOS 11.0, *) {
            cell.backgroundColor = Constant.Color.color_launch
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
struct getCompData {
    init(companyName: String, companyId: String, domain: String, user_id: String, assetImgPath: String, assetInsepctionImgPath: String, assetBreakdwon_insepction_img_path: String, bearerToken: String , audit_param_audio_path : String , fullName : String) {
        self.companyName = companyName
        self.companyId = companyId
        self.domain = domain
        self.user_id = user_id
        self.assetImgPath = assetImgPath
        self.assetInsepctionImgPath = assetInsepctionImgPath
        self.assetBreakdwon_insepction_img_path = assetBreakdwon_insepction_img_path
        self.bearerToken = bearerToken
        self.audit_param_audio_path = audit_param_audio_path
        self.fullName = fullName
    }
    
    var companyName : String
    var companyId : String
    var domain : String
    var user_id : String
    var assetImgPath : String
    var assetInsepctionImgPath : String
    var assetBreakdwon_insepction_img_path : String
    var bearerToken : String
    var audit_param_audio_path : String
    var fullName : String
}
