//
//  AssetDetailVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit
import SDWebImage
import SQLite

class AssetDetailVC: UIViewController , UIGestureRecognizerDelegate{

    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var viewCheckIn: UIView!
    @IBOutlet weak var viewOwnerShip: UIView!
    @IBOutlet weak var btnOwnerShip: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStartInsp: UILabel!
    
    @IBOutlet weak var imgWidthCons: NSLayoutConstraint!
    @IBOutlet weak var lblReport: UILabel!
    @IBOutlet weak var lblFiles: UILabel!
    @IBOutlet weak var lblInsplog: UILabel!
    @IBOutlet weak var lblReady: UILabel!
    @IBOutlet weak var lblLocTitle: UILabel!
    @IBOutlet weak var lblTitleWar: UILabel!
    @IBOutlet weak var lblTitlePur: UILabel!
    @IBOutlet weak var lblModelTitle: UILabel!
    @IBOutlet weak var lblAssetStatus: UILabel!
    @IBOutlet weak var lblAssetStatusTitle: UILabel!
    @IBOutlet weak var viewAssetStatus: UIView!
    @IBOutlet weak var viewLocationHistory: UIView!
    @IBOutlet weak var btnLocationHistory: UIButton!
    @IBOutlet weak var viewFiles: UIView!
    @IBOutlet weak var btnFiles: UIButton!
    @IBOutlet weak var btnInspLog: UIButton!
    @IBOutlet weak var viewInspLog: UIView!
    @IBOutlet weak var btnOpenView: UIButton!
    @IBOutlet weak var btnStartNewInsp: UIButton!
    @IBOutlet weak var startNewInspView: UIView!
    @IBOutlet weak var reportPbView: UIView!
    @IBOutlet weak var btnReportPb: UIButton!
    @IBOutlet weak var startInspView: UIView!
    @IBOutlet weak var noInsptView: UIView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var viewSupplier: UIView!
    @IBOutlet weak var viewModalNo: UIView!
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var auditImg: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblWarranty: UILabel!
    @IBOutlet weak var lblSupplier: UILabel!
    @IBOutlet weak var lblPurchaseDate: UILabel!
    @IBOutlet weak var lblModalNo: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblModalNum: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var detailsTableview: UITableView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnStartIns: UIButton!
    
    @IBOutlet weak var inspView: UIView!
    @IBOutlet weak var lblSetSchedule: UILabel!
    @IBOutlet weak var btnNoInspect: UIButton!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenOfDetail = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    var productName = String()
    var assetTag = String()
    var location = String()
    var warranty = Int64()
    var supplier = String()
    var purchaseDate = String()
    var modalnNo = String()
    var category = String()
    var companyName = String()
    var auditValues = String()
    var auditImageUrl : URL!
    var dueDate = String()
    var getAllAssetlist : [ScheduledData] = []
    var getSchedule : [SelectSchedule] = []
    var assetTagiD = Int64()
    var isFromQrScanned = false
    var auditScheduleID = Int64()
    var urlImage : URL!
    var pmHistory = String()
    var getAssetHistory : [AssetPmHistory] = []
    var getScheduleDetail : TotalAssetScheduledata!
    var isPendingJob = Bool()
    var tap = UITapGestureRecognizer()
    var tap_2 = UITapGestureRecognizer()
    var pmHistoryList : [pmHistoryData] = []
    var maintainType = String()
    var assetStatus = Int64()
    var isFromAssetStatus = false
    var assetSchVC = AssetSchTypeVC()
    var break_prevent = String()
    var getOfflineLicenses : [LicenceOfflineData] = []
    var isfromLicence = Bool()
    var licenceId = Int()
    var licenceDetail : [LicenceDetail] = []
    var searchable = [String : Any]()
    var isfromSearch = Bool()
    var assetTagSearch = String()
    var accessoryId = Int64()
    var accessoryName = String()
    var isFromAccessory = Bool()
    var isFromConsumable = Bool()
    var consumableId = Int()
    var isFromComponent = Bool()
    var componentId = Int()
    var consumableName = String()
    var componentName = String()
    var supplierId = Int()
    var checkInCount = Int()
    var checkOutCount = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            lblProductName.textColor = Constant.Color.color_launch
            lblStatus.textColor = Constant.Color.color_red
            lblModalNum.textColor = Constant.Color.color_grey
            lblCategory.textColor = Constant.Color.gradient_3
            lblSupplier.textColor = Constant.Color.gradient_3
            btnStartIns.backgroundColor = Constant.Color.color_launch
            viewCompany.backgroundColor = Constant.Color.color_bg
            viewModalNo.backgroundColor = Constant.Color.color_bg
            viewSupplier.backgroundColor = Constant.Color.color_bg
            viewLocation.backgroundColor = Constant.Color.color_bg
            topView.backgroundColor = Constant.Color.color_bg
            superView.backgroundColor = Constant.Color.color_bg
            btnNoInspect.backgroundColor = Constant.Color.color_launch
            btnNoInspect.setTitleColor(Constant.Color.color_bg, for: .normal)
            btnStartIns.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 3))
            btnOpenView.setShadow(shadowColor: .gray, shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: CGSize(width: 0, height: 3))
            btnReportPb.backgroundColor = Constant.Color.color_launch
            btnNoInspect.backgroundColor = Constant.Color.color_launch
            btnOwnerShip.backgroundColor = Constant.Color.color_launch
            btnStartIns.backgroundColor = Constant.Color.color_launch
            btnOpenView.backgroundColor = Constant.Color.color_launch
            btnInspLog.backgroundColor = Constant.Color.color_launch
            btnFiles.backgroundColor = Constant.Color.color_launch
            btnLocationHistory.backgroundColor = Constant.Color.color_launch
            btnStartNewInsp.backgroundColor = Constant.Color.color_launch
            btnCheckIn.backgroundColor = Constant.Color.color_launch
            
        }

        assetSchVC.createDB()
        assetSchVC.insertData()
        assetSchVC.readValuesForOperational()
        btnStartIns.roundedUsingHeight()
        btnStartNewInsp.roundedUsingHeight()
        btnReportPb.setRoundCorner()
        startNewInspView.setCorner(_tp: 5)
        reportPbView.setCorner(_tp: 5)
        btnOpenView.setRoundCorner()
        btnLocationHistory.setRoundCorner()
        btnFiles.setRoundCorner()
        btnInspLog.setRoundCorner()
        btnOwnerShip.setRoundCorner()
        viewLocationHistory.setCorner(_tp: 5)
        viewOwnerShip.setCorner(_tp: 5)
        viewFiles.setCorner(_tp: 5)
        viewInspLog.setCorner(_tp: 5)
        viewCheckIn.setCorner(_tp: 5)
        btnCheckIn.setRoundCorner()
        if !isfromSearch{
            pmHistoryData()
        }
        if isfromLicence || isFromComponent || isFromConsumable || isFromAccessory {
            btnCheckIn.isHidden = true
            viewCheckIn.isHidden = true
        }
        tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        bgView.addGestureRecognizer(tap)
        tap.delegate = self
        tap_2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnButtons(_:)))
        self.view.addGestureRecognizer(tap_2)
        tap_2.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        assetSchVC.createDB()
        assetSchVC.insertData()
        assetSchVC.readValuesForOperational()
        setLoc = location
        assettag = assetTag
        modalAudit = modalnNo
        lblModalNum.text = assetTag
        lblProductName.text = productName
        lblLocation.text = location
        lblWarranty.text = "\(warranty)"
        lblSupplier.text = supplier
        print(purchaseDate)
        if purchaseDate != ""{
            let newdate = GlobalFunction.convertDateString(dateString: purchaseDate, fromFormat: "yyyy-MM-dd", toFormat: "dd-MM-yy")
            lblPurchaseDate.text = newdate
        }
         lblModalNo.text = modalnNo
         lblCategory.text = category
         lblCompanyName.text = companyName
        auditImg.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
        if !isPendingJob && !isFromAssetStatus{
            if isFromQrScanned {
                readValuesFromQRAssetTagId()
            }else{
                readValuesFromAssetTag()
            }
        }
        if isfromLicence{
            lblTitle.text = "Licence Detail"
            lblInsplog.text = "Manage seats"
            lblStartInsp.text = "Location History"
            lblReport.text = "Files"
            btnStartNewInsp.setImage(UIImage(named: "location_on_24"), for: .normal)
            btnReportPb.setImage(UIImage(named: "insert_drive_file_24"), for: .normal)
            viewFiles.isHidden = true
            btnFiles.isHidden = true
            viewLocationHistory.isHidden = true
            btnLocationHistory.isHidden = true
            viewOwnerShip.isHidden = true
            btnOwnerShip.isHidden = true
            imgWidthCons.constant = 0
            if isfromSearch{
                licenceId = searchable["id"] as? Int ?? 0
            }
            getLicenceDetailCall()
        }
        
        if isFromAssetStatus == true {
            readValueforAssetStatus()
            
            viewAssetStatus.isHidden = false
            if assetStatus == 1{
                lblAssetStatus.text = "Operational"
                maintainType = "preventive"
                if #available(iOS 11.0, *) {
                    lblAssetStatus.textColor = Constant.Color.gradient_3
                }
            }else if assetStatus == 0{
                lblAssetStatus.text = "Breakdown"
                maintainType = "breakdown"
                if #available(iOS 11.0, *) {
                    lblAssetStatus.textColor = Constant.Color.color_red
                }
            }
        }
        if isPendingJob {
            readValuesFromPendingJob()
        }

        if isfromLicence {
            noInsptView.isHidden = true
            startInspView.isHidden = false
            inspView.isHidden = false
            
        }
        if isFromAccessory {
            lblProductName.text = accessoryName
            lblLocation.text = location
            lblPurchaseDate.text = dueDate
            lblSupplier.text = supplier
            lblCompanyName.text = companyName
            lblCategory.text = category
            lblModalNo.text = modalnNo
            auditImg.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
            lblTitle.text = "Accessory Detail"
            lblInsplog.text = "User Log"
            lblStartInsp.text = "Check In"
            lblReport.text = "Check Out"
            noInsptView.isHidden = true
            startNewInspView.isHidden = true
            btnStartNewInsp.isHidden = true
            startInspView.isHidden = false
            inspView.isHidden = false
        }
        if isFromConsumable {
            lblProductName.text = consumableName
            lblLocation.text = location
            lblPurchaseDate.text = dueDate
            lblSupplier.text = supplier
            lblCompanyName.text = companyName
            lblCategory.text = category
            lblModalNo.text = modalnNo
            auditImg.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
            lblTitle.text = "Consumable Detail"
            lblInsplog.text = "User Log"
            lblStartInsp.text = "Restock"
            lblReport.text = "Check Out"
            noInsptView.isHidden = true
            startInspView.isHidden = false
            inspView.isHidden = false
            isFromConsumable = true
        }
        if isFromComponent {
            lblProductName.text = componentName
            lblLocation.text = location
            lblPurchaseDate.text = dueDate
            lblSupplier.text = supplier
            lblCompanyName.text = companyName
            lblCategory.text = category
            lblModalNo.text = modalnNo
            auditImg.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
            lblTitle.text = "Component Detail"
            lblInsplog.text = "User Log"
            lblStartInsp.text = "Check In"
            lblReport.text = "Check Out"
            noInsptView.isHidden = true
            startInspView.isHidden = false
            inspView.isHidden = false
        }
        if isfromSearch{
            lblModalNum.text = searchable["model_name"] as? String ?? ""
            lblProductName.text = searchable["name"] as? String ?? ""
            lblLocation.text = searchable["location"] as? String ?? ""
            lblWarranty.text = "\(searchable["warranty_months"] as? Int ?? 0)"
            lblSupplier.text = searchable["supplier_name"] as? String ?? ""
            if searchable["purchase_date"] as? String != ""{
                let newdate = GlobalFunction.convertDateString(dateString: searchable["purchase_date"] as? String ?? "", fromFormat: "yyyy-MM-dd", toFormat: "dd-MM-yy")
                lblPurchaseDate.text = newdate
            }
            lblModalNo.text = searchable["asset_tag"] as? String ?? ""
            lblCategory.text = searchable["category_name"] as? String ?? ""
            lblCompanyName.text = searchable["company_name"] as? String ?? ""
            auditImg.sd_setImage(with: URL(string: baseUrlImage + "\(searchable["image"] as? String ?? "")") , placeholderImage: UIImage(named: "default"))
            setLoc = searchable["location"] as? String ?? ""
            assetTagSearch = searchable["asset_tag"] as? String ?? ""
            modalAudit = searchable["asset_tag"] as? String ?? ""
            readSearchValue()
            pmHistoryData()
           
        }
        if !isfromLicence && !isFromAccessory && !isFromComponent && !isFromConsumable {
            if getAllAssetlist.count == 0{
                noInsptView.isHidden = false
                startInspView.isHidden = true
                inspView.isHidden = true
            }else{
                noInsptView.isHidden = true
                startInspView.isHidden = false
                inspView.isHidden = false
            }
                    }
    }
    override func viewWillLayoutSubviews() {
        if detailsTableview.isHidden == false{
            self.btnStartIns.transform = CGAffineTransform.identity
            self.reportPbView.isHidden = true
            self.btnReportPb.isHidden = true
            
            self.startNewInspView.isHidden = true
            self.btnStartNewInsp.isHidden = true
            
            self.viewInspLog.isHidden = true
            self.btnInspLog.isHidden = true
            
            self.btnOpenView.transform = CGAffineTransform.identity
            self.viewFiles.isHidden = true
            self.btnFiles.isHidden = true
            
            self.viewLocationHistory.isHidden = true
            self.btnLocationHistory.isHidden = true
            
            self.viewOwnerShip.isHidden = true
            self.btnOwnerShip.isHidden = true
            self.viewCheckIn.isHidden = true
            self.btnCheckIn.isHidden = true
        }
    }
    func readAccessory(){
        getAllAssetlist.removeAll()
        do {
            for row in try db!.prepare("SELECT * FROM Accessories") {
                print(row)
//                assetTagiD = (row[17] as! Int64)
//                maintainType = row[19] as! String
//                getAllAssetlist.append(ScheduledData(auditName: row[15] as! String, auditValue: row[11] as! String, auditScheduleId: row[0] as? Int64 ?? 0, dueDate:  row[10] as? String ?? ""))
                detailsTableview.reloadData()
            }
        }catch{
            print("error301",String(describing: error))
            print("error301",error.localizedDescription)
        }
    }
    
    func readSearchValue(){
        getAllAssetlist.removeAll()
        do {
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE asset_tag = '\(assetTagSearch)' AND schedule_expire_date >= '\(today)'") {
                print(row)
                assetTagiD = (row[17] as! Int64)
                maintainType = row[19] as! String
                getAllAssetlist.append(ScheduledData(auditName: row[15] as! String, auditValue: row[11] as! String, auditScheduleId: row[0] as? Int64 ?? 0, dueDate:  row[10] as? String ?? ""))
                detailsTableview.reloadData()
            }
        }catch{
            print("error301",String(describing: error))
            print("error301",error.localizedDescription)
        }
    }
    
    func readValuesFromAssetTag(){
        getAllAssetlist.removeAll()
        do {
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE asset_tag = '\(assetTag)' AND schedule_expire_date >= '\(today)'") {
//                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[15]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])\n,supplierName:\(row[13]) , companyName:\(row[14]), scheduleEnddate :\(row[16]) , assetTagId : \(row[17])")
                
                auditScheduleID = (row[0] as! Int64)
                print("auditScheduleID",auditScheduleID)
                lblModalNum.text = assetTag
                lblProductName.text = (row[2] as! String)
                lblLocation.text = (row[9] as! String)
                location = (row[9] as! String)
                dueDate = (row[10] as! String)
                lblWarranty.text = "\(row[8]  ?? "") Months"
                lblSupplier.text = (row[13] as! String)
                if (row[6] as! String) != ""{
                    let newdate = GlobalFunction.convertDateString(dateString: (row[6] as! String), fromFormat: "yyyy-MM-dd", toFormat: "dd-MM-yy")
                    lblPurchaseDate.text = newdate
                }
                lblModalNo.text = "\(row[5] ?? "")"
                lblCategory.text = (row[12] as! String)
                lblCompanyName.text = (row[14] as! String)
                assetTagiD = (row[17] as! Int64)
                maintainType = row[19] as! String
                urlImage = URL(string: baseUrlImage + (row[7] as! String))
                auditImageUrl = URL(string: baseUrlImage + (row[7] as! String))
                auditImg.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default"))
                auditValues = (row[11] as! String)
                getAllAssetlist.append(ScheduledData(auditName: row[15] as! String, auditValue: row[11] as! String, auditScheduleId: row[0] as? Int64 ?? 0, dueDate:  row[10] as? String ?? ""))
                
                if getAllAssetlist.count == 0{
                    noInsptView.isHidden = false
                    startInspView.isHidden = true
                    inspView.isHidden = true
                }else{
                    noInsptView.isHidden = true
                    startInspView.isHidden = false
                    inspView.isHidden = false
                }
            }
        }catch{
            print("error314",String(describing: error))
            print(error.localizedDescription)
        }
    }
    func readValuesFromQRAssetTagId(){
        getAllAssetlist.removeAll()
        do {
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE asset_tag_id = '\(assetTagiD)' AND schedule_expire_date >= '\(today)'") {
                //                print("Scheduleid: \(row[0])\n, id: \(row[1])\n , name :\(row[15]) \n,assetTag :\(row[3])\n,modalid:\(row[4])\n,modalname:\(row[5])\n,purchasedate:\(row[6])\n,image:\(row[7])\n,warrantymonth:\(row[8])\n,location:\(row[9])\n,duedate:\(row[10]),auditValues:\(row[11])\n,category:\(row[12])\n,supplierName:\(row[13]) , companyName:\(row[14]), scheduleEnddate :\(row[16]) , assetTagId : \(row[17])")
                auditScheduleID = (row[0] as! Int64)
                print("auditScheduleID",auditScheduleID)
                lblModalNum.text = assetTag
                lblProductName.text = (row[2] as! String)
                lblLocation.text = (row[9] as! String)
                location = (row[9] as! String)
                dueDate = (row[10] as! String)
                lblWarranty.text = "\(row[8]  ?? "") Months"
                lblSupplier.text = (row[13] as! String)
                if (row[6] as! String) != ""{
                    let newdate = GlobalFunction.convertDateString(dateString: (row[6] as! String), fromFormat: "yyyy-MM-dd", toFormat: "dd-MM-yy")
                    lblPurchaseDate.text = newdate
                }
                lblModalNo.text = "\(row[5] ?? "")"
                lblCategory.text = (row[12] as! String)
                lblCompanyName.text = (row[14] as! String)
                assetTagiD = (row[17] as! Int64)
                maintainType = row[19] as! String
                urlImage = URL(string: baseUrlImage + (row[7] as! String))
                auditImageUrl = URL(string: baseUrlImage + (row[7] as! String))
                auditImg.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "default"))
                auditValues = (row[11] as! String)
                getAllAssetlist.append(ScheduledData(auditName: row[15] as! String, auditValue: row[11] as! String, auditScheduleId: row[0] as? Int64 ?? 0, dueDate:  row[10] as? String ?? ""))
                if getAllAssetlist.count == 0{
                    noInsptView.isHidden = false
                    startInspView.isHidden = true
                    inspView.isHidden = true
                }else{
                    noInsptView.isHidden = true
                    startInspView.isHidden = false
                    inspView.isHidden = false
                }
            }
        }catch{
            print("error314",String(describing: error))
            print(error.localizedDescription)
            
        }
        
    }
    func readValuesFromPendingJob(){
        
        getAllAssetlist.removeAll()
        do {
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE (asset_tag_id = \(assetTagiD) AND schedule_expire_date >= '\(today)')") {
                getAllAssetlist.append(ScheduledData(auditName: row[15] as! String, auditValue: row[11] as! String, auditScheduleId: row[0] as? Int64 ?? 0, dueDate: row[10] as? String ?? ""))
            }
        }catch{
            print("error314",String(describing: error))
            print("error314",error.localizedDescription)
        }
    }
    func readValueforAssetStatus(){
//        getAllAssetlist.removeAll()
            
        do {
            for row in try db!.prepare("SELECT * FROM PreventiveJobs WHERE maintenance_type LIKE '%\(break_prevent)%' AND (asset_tag_id = \(assetTagiD) AND schedule_expire_date >= '\(today)')") {
                getAllAssetlist.append(ScheduledData(auditName: row[15] as! String, auditValue: row[11] as! String, auditScheduleId: row[0] as? Int64 ?? 0, dueDate: row[10] as? String ?? "" ))
            }
        }catch{
            print("error314",String(describing: error))
            print("error314",error.localizedDescription)
        }
    }
    
    //MARK: -Api call
    //PmHistory
    func pmHistoryData(){
        print("assetTagiD",assetTagiD)
        let param : Parameters = [
            .asset_id :  assetTagiD,
        ]
        
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .get_asset_pm_history, baseURL: selectedComp ?? "", setToken: glbtoken)
        URLSession.shared.getPmHistory(with: request) { isSuccess, assetHistory, message, status in
            if isSuccess{
                self.getAssetHistory = assetHistory ?? []
            }else{
                print(message)
            }
        }
    }
   
    
    var auditValue = String()
    var auditTransactionVal = String()
    var auditName = String()
    var totalScheduledata = [String : Any]()
    var apiMessage = String()
    var setLoc = String()
    var assettag = String()
    var modalAudit = String()
    func getInspDetail(scheduleId : Int64){
        print("scheduleId",scheduleId)

        let parameters: [String: Any]  = ["audit_schedule_id" : scheduleId] as Dictionary<String, Any>
       
        var request = URLRequest(url: URL(string: (selectedComp ?? "") + "get_inspection_info_deatils")!)
       
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("\(glbtoken)", forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { [self] data, response, error -> Void in
//            print(response)
            do {
                let json = try JSONSerialization.jsonObject(with: data ?? Data()) as! Dictionary<String, AnyObject>
                print("json",json)
                let totalSchedule = json["totalAssetScheduledata"]
                apiMessage = json["api_message"] as? String ?? ""
                auditValue = totalScheduledata["audit_params_values"] as? String ?? ""
                auditTransactionVal = totalScheduledata["audit_params_transaction"] as? String ?? ""
                print("totalSchedule",auditTransactionVal)
                totalScheduledata = totalSchedule as? [String : Any] ?? [String : Any]()
                print(totalScheduledata.count)
                let stroyBoard = UIStoryboard(name: "Detail", bundle: nil)
                DispatchQueue.main.async { [self] in
                    let vc = stroyBoard.instantiateViewController(withIdentifier: "InspectionDetailVC") as! InspectionDetailVC
                    vc.audit_ScheduleId = totalScheduledata["audit_schdule_id"] as? Int64 ?? 0
                    vc.auditValue = totalScheduledata["audit_params_values"] as? String ?? ""
                    vc.urlImg = auditImageUrl
                    vc.getLocation = setLoc
                    vc.auditTransactionVal = totalScheduledata["audit_params_transaction"] as? String ?? ""
                    vc.getAssetName = auditName//totalScheduledata["audit_name"] as? String ?? ""
                    print(auditValue)
                    print(totalScheduledata["audit_params_values"] as? String ?? "")
                    vc.getAssetTag = assettag
                    vc.getAuditName = modalAudit
                    
                    if totalScheduledata["audit_params_values"] as? String ?? "" != ""{
                        navigationController?.pushViewController(vc, animated: true)
                    }else{
                        if apiMessage != ""{
                            self.view.makeToast(apiMessage)
                        }else{
                            self.view.makeToast("User Inspection Not Found")
                        }
                    }
                    
                }
            } catch {
                print("error",error.localizedDescription)
            }
        })
        
        task.resume()
    }
    //MARK: - LicenceDetail Api
    func getLicenceDetailCall(){
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")licenses/\(licenceId)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenOfDetail)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
//                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let data = json as? [String: Any] else {
                            return
                        }
                        print(data)
                        let id = data["id"] as? Int
                        let supplier = data["supplier"] as? [String: Any]
                        let name = data["name"] as? String
                        let manuFacturer = data["manufacturer"] as? [String: Any]
                        let category = data["category"] as? [String: Any]
                        let expiration_date = data["expiration_date"] as? [String: Any]
                        let seats = data["seats"] as? Int
                        let notes = data["notes"] as? String
                        let company = data["company"] as? [String: Any]
                        let purchase = data["purchase_date"] as? [String: Any]
                        self.lblCompanyName.text = company?["name"] as? String ?? ""
                        self.lblCategory.text = category?["name"] as? String ?? ""
                        self.lblSupplier.text = supplier?["name"] as? String ?? ""
                        self.lblProductName.text = name
                        self.lblModelTitle.text = "Expiration Date:"
                        self.lblModalNo.text = expiration_date?["formatted"] as? String ?? ""
                        self.lblReady.text = manuFacturer?["name"] as? String ?? ""
                        self.lblLocTitle.text = "Seats:"
                        self.lblLocation.text = "\(seats ?? 0)"
                        self.lblTitleWar.text = "Notes:"
                        self.lblWarranty.text = notes
                        self.lblPurchaseDate.text = purchase?["formatted"] as? String ?? ""
                    }catch{
                        
                        print(String(describing: error))
                        
                    }
                }
            }
        }.resume()
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        UIView.animate(withDuration: 0.1) {
            self.bgView.isHidden = true
            self.detailsTableview.isHidden = true
        }
    }
    
    @objc func handleTapOnButtons(_ sender: UITapGestureRecognizer? = nil){
        
        if allBtnVisible == true{
            UIView.animate(withDuration: 0.2, animations: {
                self.btnStartIns.transform = CGAffineTransform.identity
                self.reportPbView.isHidden = true
                self.btnReportPb.isHidden = true
                self.startNewInspView.isHidden = true
                self.btnStartNewInsp.isHidden = true
                self.viewInspLog.isHidden = true
                self.btnInspLog.isHidden = true
                self.btnOpenView.transform = CGAffineTransform.identity
                self.viewFiles.isHidden = true
                self.btnFiles.isHidden = true
                self.viewLocationHistory.isHidden = true
                self.btnLocationHistory.isHidden = true
                self.btnOwnerShip.isHidden = true
                self.viewOwnerShip.isHidden = true
                self.viewCheckIn.isHidden = true
                self.btnCheckIn.isHidden = true
                self.view.layoutIfNeeded()
                self.allBtnVisible = false
            })
        }
     
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    // MARK: -Action
    
    @IBAction func btnCheckInAsset(_ sender: UIButton) {
        if checkOutCount == 1{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckOutAssetVC") as! CheckOutAssetVC
            vc.assetId = Int(assetTagiD)
            navigationController?.pushViewController(vc, animated: true)
        }
        if checkInCount == 1 {
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckInAssetVC") as! CheckInAssetVC
            vc.assetTagid = assetTagiD
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnReportPb(_ sender: UIButton) {
        if isfromLicence{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "FileUploadVC") as! FileUploadVC
            vc.licenceId = licenceId
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromAccessory{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            vc.accessoryId = Int(accessoryId)
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromConsumable{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            vc.consumeId = consumableId
            vc.isFromConsumable = true
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromComponent{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            vc.isFromComponent = true
            vc.componentId = componentId
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CauseReportVC") as! CauseReportVC
            vc.assetId = assetTagiD
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnOpenAssetDetail(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5, animations: {
            self.btnStartIns.transform = CGAffineTransform(rotationAngle: ((120.0 * CGFloat(M_PI)) / 120.0) * -1)
               self.view.layoutIfNeeded()
           })
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1) {
                self.reportPbView.isHidden = false
                self.btnReportPb.isHidden = false
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            UIView.animate(withDuration: 0.1) {
                if !self.isFromComponent && !self.isFromConsumable && !self.isFromAccessory {
                    self.startNewInspView.isHidden = false
                    self.btnStartNewInsp.isHidden = false
                    self.allBtnVisible = false
                }else{
                    self.allBtnVisible = true
                }
                if self.isfromLicence{
                    self.allBtnVisible = true
                }
            }
        }
        if !isfromLicence && !isFromComponent && !isFromConsumable && !isFromAccessory {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 0.1) {
                    self.viewCheckIn.isHidden = false
                    self.btnCheckIn.isHidden = false
                   
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                UIView.animate(withDuration: 0.1) {
                    self.allBtnVisible = true
                }
            }
        }
    }
    @IBAction func btnStartInspection(_ sender: UIButton) {
        btnInspLog.isSelected = false
        if isfromLicence{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckOutHistoryTV") as! CheckOutHistoryTV
            vc.itemId = licenceId
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromAccessory{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
            vc.accessoryId = Int(accessoryId)
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromConsumable{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ConsumableRestockVC") as! ConsumableRestockVC
            vc.consumableId = consumableId
            vc.supplierid = supplierId
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromComponent{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
            vc.componentId = componentId
            navigationController?.pushViewController(vc, animated: true)
        }else{
            if getAllAssetlist.count > 0{
                bgView.isHidden = false
                detailsTableview.isHidden = false
                detailsTableview.reloadData()
                if tblHeightConstraint.constant == UIScreen.main.bounds.height{
                    self.tblHeightConstraint.constant = UIScreen.main.bounds.height / 2
                    detailsTableview.isScrollEnabled = true
                }else{
                    detailsTableview.isScrollEnabled = false
                    UIView.animate(withDuration: 0.1) {
                        self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                        self.view.layoutIfNeeded()
                        self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                        self.view.layoutIfNeeded()
                        self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                    }
                }
            }
        }
    }
    var allBtnVisible = false
    @IBAction func btnOpenAllFeatures(_ sender: Any) {
        btnOpenView.transform = CGAffineTransform(rotationAngle: -.pi / 1.5)
        if isfromLicence{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 0.1) {
                    self.viewInspLog.isHidden = false
                    self.btnInspLog.isHidden = false
                    self.allBtnVisible = true
                }
            }
        }else if isFromAccessory{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 0.1) {
                    self.viewInspLog.isHidden = false
                    self.btnInspLog.isHidden = false
                    self.allBtnVisible = true
                }
            }
        }else if isFromConsumable{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 0.1) {
                    self.viewInspLog.isHidden = false
                    self.btnInspLog.isHidden = false
                    self.allBtnVisible = true
                }
            }
        }else if isFromComponent{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 0.1) {
                    self.viewInspLog.isHidden = false
                    self.btnInspLog.isHidden = false
                    self.allBtnVisible = true
                }
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 0.1) {
                    self.viewInspLog.isHidden = false
                    self.btnInspLog.isHidden = false
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                UIView.animate(withDuration: 0.1) {
                    self.viewFiles.isHidden = false
                    self.btnFiles.isHidden = false
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                UIView.animate(withDuration: 0.1) {
                    self.viewLocationHistory.isHidden = false
                    self.btnLocationHistory.isHidden = false
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                UIView.animate(withDuration: 0.1) {
                    self.viewOwnerShip.isHidden = false
                    self.btnOwnerShip.isHidden = false
                    self.allBtnVisible = true
                }
            }
        }
    }
    @IBAction func btnInspectionLogs(_ sender: UIButton) {
        if isfromLicence{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "LicenceSeatVC") as! LicenceSeatVC
            vc.licenceId = licenceId
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromAccessory{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewAllAccessoryListVC") as! ViewAllAccessoryListVC
            vc.accessoryId = Int(accessoryId)
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromConsumable{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewAllAccessoryListVC") as! ViewAllAccessoryListVC
            vc.isFromConsumable = true
            vc.consumableId = consumableId
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromComponent{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewAllAccessoryListVC") as! ViewAllAccessoryListVC
            vc.isFromComponent = true
            vc.componentId = componentId
            navigationController?.pushViewController(vc, animated: true)
        }else{
            btnInspLog.isSelected = true
            if allBtnVisible == true{
                handleTapOnButtons()
                allBtnVisible = false
                if Reachability.isConnectedToNetwork(){
                    if getAssetHistory.count > 0{
                        bgView.isHidden = false
                        detailsTableview.isHidden = false
                        detailsTableview.reloadData()
                        if tblHeightConstraint.constant == UIScreen.main.bounds.height{
                            self.tblHeightConstraint.constant = UIScreen.main.bounds.height / 2
                            detailsTableview.isScrollEnabled = true
                        }else{
                            detailsTableview.isScrollEnabled = false
                            UIView.animate(withDuration: 0.1) {
                                self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                                self.view.layoutIfNeeded()
                                self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                                self.view.layoutIfNeeded()
                                self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                            }
                        }
                    }
                }else{
                    
                    detailsTableview.reloadData()
                    if pmHistoryList.count > 0{
                        bgView.isHidden = false
                        detailsTableview.isHidden = false
                        if tblHeightConstraint.constant == UIScreen.main.bounds.height{
                            self.tblHeightConstraint.constant = UIScreen.main.bounds.height / 2
                            detailsTableview.isScrollEnabled = true
                        }else{
                            detailsTableview.isScrollEnabled = false
                            UIView.animate(withDuration: 0.1) {
                                self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                                self.view.layoutIfNeeded()
                                self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                                self.view.layoutIfNeeded()
                                self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func btnRevealImg(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
        vc.imgUrl = auditImageUrl
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.detailsTableview) == true {
            return false
        }else{
            return true
        }
    }
}
extension AssetDetailVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btnInspLog.isSelected{
            if Reachability.isConnectedToNetwork(){
                return getAssetHistory.count
            }else{
                return pmHistoryList.count
            }
        }else{
            print("getSchedule",getAllAssetlist.count)
            return getAllAssetlist.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if btnInspLog.isSelected{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PmHistoryTVC") as! PmHistoryTVC
            lblSetSchedule.text = "Previous Schedule"
            if Reachability.isConnectedToNetwork(){
                let list = getAssetHistory[indexPath.row]
                
                print("list427",list)
                cell.lblAuditName.text = list.auditName
                if list.auditInspectionDate != "" {
                    let newdate = GlobalFunction.convertDateString(dateString: list.auditInspectionDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                    let dateformatter = DateFormatter()
                    dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    cell.lblDate.text = newdate
                }
                cell.lblStatus.text = list.auditResult
                if isfromSearch{
                    cell.auditImage.sd_setImage(with:  URL(string: baseUrlImage + "\(searchable["image"] as? String ?? "")"), placeholderImage: UIImage(named: "default"))
                }else{
                    cell.auditImage.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
                }
                cell.setImage = {
                    let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                    vc.imgUrl = self.auditImageUrl
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                
                let list = pmHistoryList[indexPath.row]
                getInspDetail(scheduleId: Int64(list.audit_SchduleId))
                print("list427",list)
                cell.lblAuditName.text = list.audit_Name
                if list.auditinspectionDate != "" {
                    let newdate = GlobalFunction.convertDateString(dateString: list.auditinspectionDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                    let dateformatter = DateFormatter()
                    dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    cell.lblDate.text = newdate
                }
                
                cell.lblStatus.text = list.audit_Result
                if isfromSearch{
                    cell.auditImage.sd_setImage(with:  URL(string: baseUrlImage + "\(searchable["image"] as? String ?? "")"), placeholderImage: UIImage(named: "default"))
                }else{
                    cell.auditImage.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
                }
                cell.setImage = {
                    let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                    vc.imgUrl = self.auditImageUrl
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AssetDetailTVC") as! AssetDetailTVC
            lblSetSchedule.text = "Select Schedule"
            cell.lblLocation.text = location
            if isfromSearch{
                cell.lblAssetTagId.text = assetTagSearch
                cell.auditImage.sd_setImage(with: URL(string: baseUrlImage + "\(searchable["image"] as? String ?? "")") , placeholderImage: UIImage(named: "default"))
            }else{
                cell.lblAssetTagId.text = assetTag
                cell.auditImage.sd_setImage(with: auditImageUrl, placeholderImage: UIImage(named: "default"))
            }
            cell.lblAuditName.text = getAllAssetlist[indexPath.row].auditName
            print("dueDate",getAllAssetlist[indexPath.row].dueDate)
            if getAllAssetlist[indexPath.row].dueDate != "" {
                let newdate = GlobalFunction.convertDateString(dateString: getAllAssetlist[indexPath.row].dueDate, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                let dateformatter = DateFormatter()
                dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let currentTime = dateformatter.string(from: Date())
                if dueDate > currentTime {
                    cell.lblDueTitle.text = "Due:"
                }else{
                    cell.lblDueTitle.text = "Overdue:"
                }
                print(newdate)
                cell.lblDueDate.text = newdate
            }
            cell.showImage = {
                let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                vc.imgUrl = self.auditImageUrl
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if btnInspLog.isSelected{
            
            let stroyBoard = UIStoryboard(name: "Detail", bundle: nil)
            let vc = stroyBoard.instantiateViewController(withIdentifier: "InspectionDetailVC") as! InspectionDetailVC
            print(auditValue)
            if Reachability.isConnectedToNetwork(){
                let list = getAssetHistory[indexPath.row]
                print(totalScheduledata)
                auditName = list.auditName
                getInspDetail(scheduleId: Int64(list.auditSchduleId))
                
            }else{
                let list = pmHistoryList[indexPath.row]
                vc.auditValue = list.audited_Params
                vc.auditTransactionVal = auditTransactionVal
                vc.audit_ScheduleId = Int64(list.audit_SchduleId)
                print("auditImageUrl",auditImageUrl)
                vc.getAssetName = list.audit_Name
                
                vc.urlImg = auditImageUrl
                getInspDetail(scheduleId: Int64(list.audit_SchduleId))
                print(totalScheduledata)
                if auditValue != ""{
                    navigationController?.pushViewController(vc, animated: true)
                }else{
                    if apiMessage != ""{
                        self.view.makeToast("User Inspection Not Found")
                    }else{
                        self.view.makeToast("User Inspection Not Found")
                    }
                }
            }
            
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRVC
            if isfromSearch{
                vc.assettag = assetTagSearch
                vc.productImage = URL(string: baseUrlImage + "\(searchable["image"] as? String ?? "")")
            }else{
                vc.assettag = assetTag
                vc.productImage = auditImageUrl
            }
            vc.isFromAssetDetail = true
            vc.assetTagId = assetTagiD
            if auditScheduleID == 0{
                auditScheduleID = getAllAssetlist[indexPath.row].auditScheduleId
            }
            vc.auditScheduleId = auditScheduleID
            vc.maintainanceType = maintainType
            print("auditValue",getAllAssetlist[indexPath.row].auditValue)
            vc.auditValues = getAllAssetlist[indexPath.row].auditValue
            if getAllAssetlist[indexPath.row].auditValue != "[]"{
                navigationController?.pushViewController(vc, animated: true)
            }else{
                if apiMessage != ""{
                    self.view.makeToast("User Inspection Not Found")
                }else{
                    self.view.makeToast("User Inspection Not Found")
                }
            }
//            navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tblHeightConstraint.constant = self.detailsTableview.contentSize.height
        self.view.layoutIfNeeded()
    }
  
}

class SelectSchedule : Equatable{
    static func == (lhs: SelectSchedule, rhs: SelectSchedule) -> Bool {
        true
    }
    var audit_params_id: Int
    var audit_params_name: String
    init(audit_params_id : Int, audit_params_name: String){
        self.audit_params_id = audit_params_id
        self.audit_params_name = audit_params_name
    }
}

class ScheduledData : Equatable{
    static func == (lhs: ScheduledData, rhs: ScheduledData) -> Bool {
        true
    }
    
    var auditName: String
    var auditValue : String
    var auditScheduleId : Int64
    var dueDate : String
    init(auditName: String , auditValue : String , auditScheduleId : Int64 , dueDate : String){
        self.auditName = auditName
        self.auditValue = auditValue
        self.auditScheduleId = auditScheduleId
        self.dueDate = dueDate
    }
}

class LicenceDetail {
    init(id: Int, supplier: String, name: String, manuFacturer: String, category: String, expiration_date: String, seats: Int, notes: String) {
        self.id = id
        self.supplier = supplier
        self.name = name
        self.manuFacturer = manuFacturer
        self.category = category
        self.expiration_date = expiration_date
        self.seats = seats
        self.notes = notes
    }    
    
    let id  : Int
    let supplier  : String
    let name  : String
    let manuFacturer : String
    let category  : String
    let expiration_date : String
    let seats  : Int
    let notes  : String
}

