//
//  TakeAssetPhotoVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 30/05/22.
//

import UIKit
import AVKit

protocol AddPhotoDelegate {
    func addAssetPhoto(photo: UIImage)
}
class TakeAssetPhotoVC: UIViewController {

    @IBOutlet weak var topSuperView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnRetake: UIButton!
    @IBOutlet weak var btnTakePic: UIButton!
    @IBOutlet weak var imgAsset: UIImageView!
    var setAssetimg : ((UIImage)->())?
    var delegate: AddPhotoDelegate?
    var arrayVAl : [PageValue] = []
    var auditPhotoKey = [String]()
    var auditPhotoValue = [UIImage]()
    var auditCommentValue = [String]()
    var auditCommentKey = [String]()
    var auditAudioKey = [String]()
    var auditAudioValue = [Data]()
    var auditValues = String()
    var auditScheduleId = Int64()
    var productImage : URL!
    var maintainType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.setCorner(_tp: 10)
        btnRetake.setCorner(_tp: 10)
        btnTakePic.setCorner(_tp: 10)
        navigationController?.isNavigationBarHidden = true
        if #available(iOS 11.0, *) {
            btnNext.backgroundColor = Constant.Color.color_launch
            btnTakePic.backgroundColor = Constant.Color.color_launch
            btnRetake.backgroundColor = Constant.Color.color_launch
            topView.backgroundColor = Constant.Color.color_bg
            topSuperView.backgroundColor = Constant.Color.color_bg
        }else{
            btnNext.backgroundColor = .blue
            btnTakePic.backgroundColor = .blue
            btnRetake.backgroundColor = .blue
            topView.backgroundColor = .white
        }
      
    }
    //MARK: - Actions
    @IBAction func btnTakePhoto(_ sender: UIButton) {
        presentCamera()
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true)
    }
    
    @IBAction func btnReTakePhoto(_ sender: UIButton) {
        presentCamera()
    }
    @IBAction func btnNext(_ sender: UIButton) {

        let vc = storyboard?.instantiateViewController(withIdentifier: "FillParameterVC") as! FillParameterVC
        vc.assetImgFromTakePhoto = imgAsset.image ?? UIImage()
        vc.paramValueNew = arrayVAl
        vc.auditValues = auditValues
        vc.auditNewPhotoKey = auditPhotoKey
        vc.auditNewCommentKey = auditCommentKey
        vc.auditNewCommentValue = auditCommentValue
        vc.auditNewPhotoValue = auditPhotoValue
        vc.fromFillparamPage = true
        vc.auditScheduleId = auditScheduleId
        vc.productImage = productImage
        vc.maintenanceType = maintainType
        vc.auditAudioKey = auditAudioKey
        vc.auditAudioValue = auditAudioValue
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: -Check Camera and Settings
    func checkCameraAccess(isAllowed: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for:.video) {
        case .denied:
            isAllowed(false)
        case .restricted:
            isAllowed(false)
        case .authorized:
            isAllowed(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { isAllowed($0) }
        default:
            print("There is no authorization")
        }
    }
    func presentCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }else{
            imagePicker.sourceType = .photoLibrary
//            failed()
        }
        present(imagePicker, animated: true, completion: nil)
    }
    func failed() {
        let ac = UIAlertController(title: "Device not supported", message: "Please use a device with a camera. Because this device does not support camera", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    func presentCameraSettings() {
        let alert = UIAlertController.init(title: "Allow the Camera", message: "Move To the setting", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: { (_) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true)
    }
}
extension TakeAssetPhotoVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        
        imgAsset.image = image
        btnNext.isHidden = false
        btnRetake.isHidden = false
        btnTakePic.isHidden = true
    }
}

