//
//  InspectionDetailVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 24/05/22.
//

import UIKit

class InspectionDetailVC: UIViewController {
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAssetTag: UILabel!
    @IBOutlet weak var lblAuditName: UILabel!
    @IBOutlet weak var lblAssetName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var auditAssetImg: UIImageView!
    @IBOutlet weak var detailTV: UITableView!
    var audit_ScheduleId = Int64()
    var auditValue = String()
    var auditTransactionVal = String()
    var getInspDetail : [InspDetailAuditScheduleId] = []
    var urlImg : URL!
    var auditTransData = [CompletedJobDetail]()
    var getallValues : [FillParamValue] = []
    var getAssetName = String()
    var getAuditName = String()
    var getAssetTag = String()
    var getLocation = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(auditTransactionVal)
        auditAssetImg.sd_setImage(with: urlImg,placeholderImage: UIImage(named: "default"))
        if #available(iOS 11.0, *) {
            lblAssetName.textColor = Constant.Color.color_launch
        } else {
            // Fallback on earlier versions
        }
        lblLocation.text = getLocation
        lblAssetTag.text = getAssetTag
        lblAuditName.text = getAuditName
        lblAssetName.text = getAssetName
        activityIndicator.startAnimating()
        readAuditValueJson()
        readTransactionJson()
        activityIndicator.stopAnimating()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UIView.animate(withDuration: 0.1) { [self] in
            tblHeightConstraint.constant = detailTV.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = detailTV.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = detailTV.contentSize.height
        }
        
    }
    
    //MARK: - Read Json File of Audit Values
    func readAuditValueJson(){
        getallValues.removeAll()
        let newData = auditValue.replacingOccurrences(of: "\n", with: "\\n")
        if let data = newData.data(using: String.Encoding.utf8) {
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                print(response)
                response.compactMap { data in
                    let auditName = data["audit_params_name"] as! String
                    let auditparams_id = data["audit_params_id"] as! Int
                    let assetModel = (data["asset_model"] as! NSString).integerValue
                    let createdDate = data["created_date"] as! String
                    let updateDate = data["updated_date"] as! String
                    let deleteAt = data["deleted_at"] as? String ?? ""
                    let audit_camera_photos = (data["audit_camera_photos"] as? NSString)?.integerValue ?? 0
                    let show_when_photos = data["show_when_photos"] as? Int ?? 0
                    let show_when_comment = data["show_when_comment"] as? Int ?? 0
                    let id = data["id"] as! Int
                    let status = data["status"] as! String
                    let param_disp_name = data["param_disp_name"] as! String
                    let param_type = data["param_type"] as! String
                    let benchmark_range_low = data["benchmark_range_low"] as? Int ?? 0
                    let benchmark_range_high = data["benchmark_range_high"] as? Int ?? 0
                    let dropdown_values = data["dropdown_values"] as! String
                    let dropdown_reject_value = data["dropdown_reject_value"] as! String
                    let dropdown_accept_value = data["dropdown_accept_value"] as! String
                    let audit_comment = data["audit_comment"] as? Int ?? 0
                    let audit_camera_photos_key = data["audit_camera_photos_key"] as! String
                    let audit_comment_key = data["audit_comment_key"] as! String
                    let validation = data["validation"] as! String
                    getallValues.append((FillParamValue(auditName: auditName, auditparams_id: auditparams_id, assetModel: assetModel, createdDate: createdDate, updateDate: updateDate, deleteAt: deleteAt, audit_camera_photos: audit_camera_photos, show_when_photos: show_when_photos, show_when_comment: show_when_comment, id: id, status: status, param_disp_name: param_disp_name, param_type: param_type, benchmark_range_low: benchmark_range_low, benchmark_range_high: benchmark_range_high, dropdown_values: dropdown_values, dropdown_reject_value: dropdown_reject_value, dropdown_accept_value: dropdown_accept_value, audit_comment: audit_comment, audit_camera_photos_key: audit_camera_photos_key, audit_comment_key: audit_comment_key , validation: validation)))
                    detailTV.reloadData()
                }
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    //MARK: - Read Json File of Audit Transaction Values
    func readTransactionJson(){
        auditTransData.removeAll()
        let newData = auditTransactionVal.replacingOccurrences(of: "\n", with: "\\n")
        if let data = newData.data(using: String.Encoding.utf8) {
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                print(response)
                response.compactMap { data in
                    let id = data["id"]
                    let audit_params_transaction_id = data["audit_params_transaction_id"]
                    let audit_update_date = data["audit_update_date"]
                    let audit_asset_image = data["audit_asset_image"]
                    let audit_gps_location = data["audit_gps_location"]
                    let audit_param_name = data["audit_param_name"]
                    let inspection_img = data["inspection_img"]
                    let inspection_comment = data["inspection_comment"]
                    let audit_param_value = data["audit_param_value"]
                    let audit_schedule_id = data["audit_schedule_id"]
                    let audit_param_result = data["audit_param_result"]
                    let audit_param_name_val_id = data["audit_param_name_val_id"]
                    let audit_param_audio = data["inspection_audio"]
                    auditTransData.append(CompletedJobDetail(id: id as? Int64 ?? 0, audit_params_transaction_id: audit_params_transaction_id as? Int64 ?? 0, audit_update_date: audit_update_date as? String ?? "", audit_asset_image: audit_asset_image as? String ?? "", audit_gps_location: audit_gps_location as? String ?? "", audit_param_name: audit_param_name as? String ?? "", inspection_img: inspection_img as? String ?? "", inspection_comment: inspection_comment as? String ?? "", audit_param_value: audit_param_value as? String ?? "", audit_schedule_id: audit_schedule_id as? Int64 ?? 0, audit_param_result: audit_param_result as? String ?? "", audit_param_name_val_id: audit_param_name_val_id as? Int64 ?? 0, audit_param_audio: audit_param_audio as? String ?? ""))
                    for(key , value) in data{
                        print(key ,":", value)
                    }
                }
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    //MARK: - Action
    
    @IBAction func backToPrevious(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnRevealImage(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
        vc.imgUrl = urlImg
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension InspectionDetailVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("getallValues",getallValues.count)
        print("auditTransData",auditTransData.count)
        return getallValues.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InspDetailTVC", for: indexPath) as! InspDetailTVC
        cell.separatorInset = .zero

        let cellSize = { [self] in
            tblHeightConstraint.constant = tableView.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = tableView.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = tableView.contentSize.height
        }
        
        let list = getallValues[indexPath.row]
        cell.lblAuditParamName.text = list.param_disp_name.components(separatedBy: "_").joined(separator: " ").firstCapitalized
        print(auditTransData)
       
        auditTransData.map { data in
            if data.audit_param_name_val_id == list.id{
                cell.txtAuditParam.text = data.audit_param_value.firstCapitalized
                print(data.inspection_img)
                let urlshowImg = URL(string: (UserDefaults.standard.string(forKey: "assetInsepctionImgPath") ?? "") + (data.inspection_img))
                if data.inspection_img == ""{
                    cell.viewImg.isHidden = true
                    cell.comentImgStackView.isHidden = true
                }else{
                    cell.comentImgStackView.isHidden = false
                    cell.viewImg.isHidden = false
                    DispatchQueue.main.async {
                        cell.imgInspection.sd_setImage(with: urlshowImg , placeholderImage: UIImage(named: "default"))
                    }
                }
                
                cell.comentImgStackView.isHidden = true
                if data.inspection_comment == ""{
                    cell.viewComment.isHidden = true
                }else{
                    cell.comentImgStackView.isHidden = false
                    cell.viewComment.isHidden = false
                    cell.txtComment.text = data.inspection_comment
                }
                
                cell.setImage = {
                    if data.inspection_img != ""{
                        let storyBoard = UIStoryboard(name: "CausesReporting", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                        vc.imgUrl = urlshowImg
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        cellSize()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tblHeightConstraint.constant = tableView.contentSize.height
        let additionalSeparatorThickness = CGFloat(5)
        let additionalSeparator = UIView(frame: CGRect(x: 0,
                                                       y: cell.frame.size.height - additionalSeparatorThickness,
                                                       width: cell.frame.size.width,
                                                       height: additionalSeparatorThickness))
        if #available(iOS 13.0, *) {
            additionalSeparator.backgroundColor = .systemGray5
        }
        cell.addSubview(additionalSeparator)
    }
}
