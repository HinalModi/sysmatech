//
//  FillParameterVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 24/05/22.
//

import UIKit
import DropDown
import AVKit
import CoreLocation
import Alamofire
import SQLite

class FillParameterVC: UIViewController, URLSessionDelegate ,AddPhotoDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgHeightCons: NSLayoutConstraint!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var asset_Image: UIImageView!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnSubmitInsp: UIButton!
    @IBOutlet weak var btnTakeAsset: UIButton!
    @IBOutlet weak var paramTView: UITableView!
    let dropDown = DropDown()
    var isSelect = false
    var auditValues = String()
    var productImage : URL!
    var auditName = String()
    var auditparams_id = Int()
    var assetModel = Int()
    var createdDate = String()
    var updateDate = String()
    var deleteAt = String()
    var audit_camera_photos = Int()
    var show_when_photos = Int()
    var show_when_comment = Int()
    var id = Int()
    var status = String()
    var param_disp_name = String()
    var param_type = String()
    var benchmark_range_low = Int()
    var benchmark_range_high = Int()
    var dropdown_values = String()
    var dropdown_reject_value = String()
    var dropdown_accept_value = String()
    var audit_comment = Int()
    var audit_camera_photos_key = String()
    var audit_comment_key = String()
    
    var txtCommentAceept = String()
    var txtRejectPhoto = String()
    var txtCommentReject = String()
    var txtAcceptPhoto = String()
    var dropDownAcceptComment = String()
    var dropDownAcceptPhoto = String()
    var dropDownRejectComment = String()
    var dropDownRejectPhoto = String()
    var validation = String()
    var dropDownsetvalidation : [String : Any]!
    
    var getallValues : [FillParamValue] = []
    var getDroppDownValues : [DropDownValues] = []
    var getTextFieldValues : [TextFieldValues] = []
    var imagePicker: UIImagePickerController!
    var selectedIndexPath : IndexPath!
    var cellIndexPath : IndexPath!
    var selectedImage = UIImage()
    var assetImage = UIImage()
    var paramName = [String]()
    var paramValue = [String]()
    var i = 0
    var auditCommentValue = [String]()
    var auditPhotoValue = [UIImage]()
    var auditCommentKey = [String]()
    var auditPhotoKey = [String]()
    var paramDic = [String : Any]()
    var auditScheduleId = Int64()
    var addComment = [String]()
    var locationManager = CLLocationManager()
    var currentLocation:CLLocation!
    var jsonString = String()
    var isSwitchPress = false
    var row: Int! = nil
    var assetImgFromTakePhoto = UIImage()
    var paramValueNew = [PageValue]()
    var auditNewPhotoKey = [String]()
    var auditNewPhotoValue = [UIImage]()
    var auditNewCommentValue = [String]()
    var auditNewCommentKey = [String]()
    var auditAudioKey = [String]()
    var auditAudioValue = [Data]()
    var commentDic: [String: Any] = [:]
    var picDic: [UIImage: Any] = [:]
    var fromFillparamPage = false
    var isfromSetting = false
    var maintenanceType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(auditAudioKey)
        print(auditAudioValue)
        
        for (index, element) in auditNewCommentValue.enumerated() {
            commentDic[element] = auditNewCommentKey[index]
        }
        let newComdictionary = commentDic.filter({ $0.key != ""})
        print(newComdictionary)
        for (index, element) in auditNewPhotoValue.enumerated() {
            picDic[element] = auditNewPhotoKey[index]
        }
        activityIndicator.stopAnimating()
        if #available(iOS 11.0, *) {
            activityIndicator.color =  .black
        } else {
            activityIndicator.color = .blue
        }
        if #available(iOS 11.0, *) {
            topView.backgroundColor = Constant.Color.color_bg
            superView.backgroundColor = Constant.Color.color_bg
            btnSubmitInsp.backgroundColor = .black
            btnTakeAsset.backgroundColor = Constant.Color.color_launch
        }else{
            btnSubmitInsp.backgroundColor = .blue
            btnTakeAsset.backgroundColor = .blue
        }
        btnTakeAsset.setCorner(_tp: 5)
        btnSubmitInsp.setCorner(_tp: 5)
        
        imgProduct.sd_setImage(with: productImage, placeholderImage: UIImage(named: "default"))
        btnSubmitInsp.setTitleColor(.white, for: .normal)
        btnTakeAsset.setTitleColor(.white, for: .normal)
        btnSubmitInsp.tintColor = .clear
        btnTakeAsset.tintColor = .clear
        readJson()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        asset_Image.image = assetImgFromTakePhoto
        assetImage = assetImgFromTakePhoto
        imgHeightCons.constant = 100
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        UIView.animate(withDuration: 0.1) { [self] in
            tblHeightConstraint.constant = paramTView.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = paramTView.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = paramTView.contentSize.height
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("Cannot Handle")
    }
    
    //MARK: - Current Location
    func authorizelocationstates(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            currentLocation = locationManager.location
        }
        else{
            // Note : This function is overlap permission
            locationManager.requestWhenInUseAuthorization()
            authorizelocationstates()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager = manager
        // Only called when variable have location data
        authorizelocationstates()
    }
    //MARK: -ApiCall
    
    func submitInspectionAlamofire(){
    
        let url = URL(string: submitUrlZydus)!
        var latlong = String()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            latlong = "\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)"
            print(latlong)
            
        }
        var params : [String:Any] = [:]
        params["user_id"]  = user_Id
        params["audit_status"]  = "completed"
        params["audit_schedule_id"] = "\(auditScheduleId)"
        params["type"] = "Pending"
        params["lat_long"] = latlong == "" ? "37.785834,-122.406417" : latlong
        for (index, element) in paramName.enumerated() {
            params[element] = paramValue[index]
        }
        
        if auditCommentValue.count > 0{
            for (index, element) in auditCommentKey.enumerated() {
                params[element] = auditCommentValue[index]
            }
        }
       
        let headers : HTTPHeaders = [
            "Content-Type": "multipart/form-data",
            "Authorization":"\(tokenFormData)",
            "Accept" : "application/json"
        ]
        
        AF.upload(multipartFormData: { multipartFormData in
            for imageData in self.auditPhotoValue {
                if self.auditPhotoValue.count > 0{
                    for (_, element) in self.auditPhotoKey.enumerated() {
                        print(element)
                        let image = imageData.jpegData(compressionQuality: 0.5)
                        multipartFormData.append(image ?? Data(), withName: element , fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: ".jpeg")
                        multipartFormData.append(self.assetImage.jpegData(compressionQuality: 0.5) ?? Data(), withName: "asset_img" , fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: ".jpeg")
                        
                    }
                }
                for (key, value) in params {
                    print(key,":",value)
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue) ?? Data(), withName: key)
                }
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response { (result) in
            if result.error != nil{
                print(result.error)
            }else{
                if let jsonData = result.data
                {
                    do{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        let status = parsedData["api_status"] as? NSInteger ?? 0
                        let msg = parsedData["api_message"] as? String ?? ""
                        print("message",msg)
                        print("parsedData=", parsedData)
                        if status == 1{
                            self.view.makeToast(msg)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.navigationController?.popToViewController(ofClass: DashBoardVC.self)
                            }
                        }else{
                            GlobalFunction.showAlert(message: "Please fill all the fields!", actionTitle: "OK", viewcontroller: self)
                        }
                    }catch{
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
 
    //MARK: - Create and Store In DB
    let submitIns = Table("SubmitInspection")
    let jsonStoreData = Expression<String?>("jsondata")
    
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("submitIns",path)
                
                // Connect to database
                db = try Connection("\(path)/submitIns.sqlite3")
                print(isfromSetting)
               
                try db?.run(submitIns.create(ifNotExists: true) { table in
                    table.column(jsonStoreData)
                })
                try db?.run(submitIns.insert(jsonStoreData <- jsonString ))
                for row in try db!.prepare("SELECT * FROM SubmitInspection") {
                    let rowData = row[0] as! String
                    print(rowData)
                    if rowData != "" {
                        if Reachability.isConnectedToNetwork(){
                            print("Internet Connection Available!")
                            submitInsThroughDBToApi()
                        }else{
                            
                            activityIndicator.startAnimating()
                            activityIndicator.stopAnimating()
                            if isSwitchPress == true{
                                self.view.makeToast("User details saved successfully")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    self.navigationController?.popToViewController(ofClass: DashBoardVC.self)
                                }
                            }
                            print("Internet Connection not Available!")
                        }
                    }
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    func insertJsonData(){
        
        var latlong = String()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            latlong = "\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)"
        }
        var params : [String:Any] = [:]
        params["user_id"]  = user_Id
        params["audit_status"]  = "completed"
        params["audit_schedule_id"] = "\(auditScheduleId)"
        params["type"] = "Pending"
        params["lat_long"] = latlong == "" ? "37.785834,-122.406417" : latlong
        params["maintenance_type"] = maintenanceType
        for (index, element) in paramName.enumerated() {
            params[element] = paramValue[index]
        }
        
        if auditCommentValue.count > 0{
            for (index, element) in auditCommentKey.enumerated() {
                params[element] = auditCommentValue[index]
            }
        }
//        if auditAudioKey.count > 0{
//            for (index, element) in auditAudioKey.enumerated() {
//                params[element] = auditAudioValue[index]
//            }
//        }
        saveImageLocally(image: assetImage, fileName: "Asset_Image")
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let url = documentsDirectory.appendingPathComponent("Asset_Image")
        
        var photoParam : [String : Any] = [:]
        photoParam["asset_img"] = "\(url)"
        for imageData in self.auditPhotoValue {
            if self.auditPhotoValue.count > 0{
                for (_, element) in self.auditPhotoKey.enumerated() {
                    saveImageLocally(image: imageData, fileName: "imageSel.jpeg")
                    let photourl = documentsDirectory.appendingPathComponent("imageSel.jpeg")
                    photoParam[element] = "\(photourl)"
                }
            }
        }
        var audioParam : [String : Any] = [:]
        for audioData in self.auditAudioValue {
            if self.auditAudioValue.count > 0{
                for (_, element) in self.auditAudioKey.enumerated() {
                    saveAudioLocally(audiofile: audioData, fileName: "converted.mp3")
                    let audiourl = documentsDirectory.appendingPathComponent("converted.mp3")
                    audioParam[element] = "\(audiourl)"
                    print(element)
                    print(auditAudioValue)
                }
            }
        }
        let dic = ["Data" :params , "filepath" : photoParam , "audiopath" : audioParam]
        print(dic)
        do{
            let data = try JSONSerialization.data(withJSONObject: dic, options: [])
            let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            jsonString = string! as String
            
            createDB()
        }catch{
            print(error.localizedDescription)
        }
        
    }
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    func submitInsThroughDBToApi(){
        if isSwitchPress == true{
            activityIndicator.startAnimating()
        }
    
        let compUrl = (selectedComp ?? "") + "submit_type_inspection_info"
        let url = URL(string:compUrl)!
        let headers : HTTPHeaders = [
            "Content-Type": "multipart/form-data",
            "Authorization": glbtoken,
            "Accept" : "application/json"
        ]
        do{
            for row in try db!.prepare("SELECT * FROM SubmitInspection") {
                let newData : String = row[0] as! String
                if let data = newData.data(using: String.Encoding.utf8) {
                    do {
                        let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                        let filePathData = response["filepath"] as! [String : Any]
                        let data = response["Data"] as! [String : Any]
                        let audioPath = response["audiopath"] as! [String : Any]
                        var image = Data()
                        var audioData = Data()
                        AF.upload(multipartFormData: { multipartFormData in
                            for (key , value) in filePathData{
                                print(value)
                                let filePath : String = value as! String
                                guard let url = URL(string: filePath) else { return }
                                do{
                                    let urldata = try Data(contentsOf: url)
                                    let imageData = UIImage(data: urldata)
                                    image = imageData?.jpegData(compressionQuality: 0.1) ?? Data()
                                }catch{
                                    print(error.localizedDescription)
                                }
                                multipartFormData.append(image , withName: key , fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: ".jpeg")
                            }
                            for (key , value) in audioPath{
                                print(value)
                                let filePath : String = value as! String
                                guard let url = URL(string: filePath) else { return }
                                do{
                                    let urldata = try Data(contentsOf: url)
                                    audioData = urldata
                                }catch{
                                    print(error.localizedDescription)
                                }
                                multipartFormData.append( audioData, withName: key , fileName: "\(Date().timeIntervalSince1970).mp3", mimeType: ".mp3")
                            }
                            for (key, value) in data {
                                print(key,":",value)
                                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue) ?? Data(), withName: key)
                            }
                        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response { (result) in
                            if result.error != nil{
                                print(result.error)
                            }else{
                                if let jsonData = result.data
                                {
                                    do{
                                        let parsedData = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String : Any]
                                        let status = parsedData["api_status"] as? NSInteger ?? 0
                                        let message = parsedData["api_message"] as? String ?? ""
                                        if self.isSwitchPress == true {
                                            if status == 1{
                                                self.activityIndicator.stopAnimating()
                                                self.view.makeToast(message)
                                                if message == ""{
                                                    self.view.makeToast("User details saved successfully")
                                                }else{
                                                    self.view.makeToast(message)
                                                }
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                                    self.navigationController?.popToViewController(ofClass: DashBoardVC.self)
                                                }
                                            }else{
                                                self.activityIndicator.stopAnimating()
                                                if message == ""{
                                                    self.view.makeToast("Something went wrong! Please try again later")
                                                }else{
                                                    self.view.makeToast(message)
                                                }
                                            }
                                        }
                                        print(parsedData)
                                    }catch{
                                        print(error.localizedDescription)
                                        self.activityIndicator.stopAnimating()
                                        self.view.makeToast("Something went wrong! Please try again later")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    
    func saveImageLocally(image: UIImage, fileName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let url = documentsDirectory.appendingPathComponent(fileName)
        if let data = image.pngData() {
            do {
                try data.write(to: url) // Writing an Image in the Documents Directory
            } catch {
                print("Unable to Write \(fileName) Image Data to Disk")
            }
        }
    }
    func saveAudioLocally(audiofile: Data, fileName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let url = documentsDirectory.appendingPathComponent(fileName)
        do {
            try audiofile.write(to: url) // Writing an Image in the Documents Directory
        } catch {
            print("Unable to Write \(fileName) Image Data to Disk")
        }
    }
    func getImageFromName(fileName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let url = documentsDirectory.appendingPathComponent(fileName)
        
        if let imageData = try? Data(contentsOf: url) {
            print(imageData)
        } else {
            print("Couldn't get image for \(fileName)")
        }
    }
    //MARK: - Read Json File of AuditValues
    func readJson(){
        let newData = auditValues.replacingOccurrences(of: "\n", with: "\\n")
        if let data = newData.data(using: String.Encoding.utf8) {
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: Any]]
                print(response)
                response.compactMap { data in
                    auditName = data["audit_params_name"] as? String ?? ""
                    auditparams_id = data["audit_params_id"] as? Int ?? 0
                    assetModel = (data["asset_model"] as! NSString).integerValue
                    createdDate = data["created_date"] as? String ?? ""
                    updateDate = data["updated_date"] as? String ?? ""
                    deleteAt = data["deleted_at"] as? String ?? ""
                    audit_camera_photos = (data["audit_camera_photos"] as? NSString)?.integerValue ?? 0
                    show_when_photos = data["show_when_photos"] as? Int ?? 0
                    show_when_comment = data["show_when_comment"] as? Int ?? 0
                    id = data["id"] as? Int ?? 0
                    status = data["status"] as? String ?? ""
                    param_disp_name = data["param_disp_name"] as? String ?? ""
                    param_type = data["param_type"] as? String ?? ""
                    benchmark_range_low = data["benchmark_range_low"] as? Int ?? 0
                    benchmark_range_high = data["benchmark_range_high"] as? Int ?? 0
                    dropdown_values = data["dropdown_values"] as? String ?? ""
                    dropdown_reject_value = data["dropdown_reject_value"] as? String ?? ""
                    dropdown_accept_value = data["dropdown_accept_value"] as? String ?? ""
                    audit_comment = data["audit_comment"] as? Int ?? 0
                    audit_camera_photos_key = data["audit_camera_photos_key"] as? String ?? ""
                    audit_comment_key = data["audit_comment_key"] as? String ?? ""
                    validation = data["validation"] as? String ?? ""
                    paramName.append("\(id)")
//                    paramName.append(param_disp_name.components(separatedBy: "_").joined(separator: " ").firstCapitalized)
                    if param_type == "Drop-down"{
                        if let validationData = validation.data(using: String.Encoding.utf8) {
                            do {
                                let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [[String: Any]]
                                print(response)
                               
                            }catch{
                                print(error.localizedDescription)
                            }
                        }
                    }else if param_type == "Number"{
                        if let validationData = validation.data(using: String.Encoding.utf8) {
                            do {
                                let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [String: Any]
                                txtCommentAceept = response?["textfield_accept_comment_validation"] as? String ?? ""
                                txtRejectPhoto = response?["textfield_reject_photos_validation"] as? String ?? ""
                                txtCommentReject = response?["textfield_reject_comment_validation"] as? String ?? ""
                                txtAcceptPhoto = response?["textfield_aacept_photos_validation"] as? String ?? ""
                                getTextFieldValues.append(TextFieldValues(txtCommentAceept: txtCommentAceept, txtAcceptphotos: txtAcceptPhoto, txtRejectphotos: txtRejectPhoto, txtRejectcomment: txtCommentReject))
                            }catch{
                                print(error.localizedDescription)
                            }
                        }
                    }
                    
                    getallValues.append((FillParamValue(auditName: auditName, auditparams_id: auditparams_id, assetModel: assetModel, createdDate: createdDate, updateDate: updateDate, deleteAt: deleteAt, audit_camera_photos: audit_camera_photos, show_when_photos: show_when_photos, show_when_comment: show_when_comment, id: id, status: status, param_disp_name: param_disp_name, param_type: param_type, benchmark_range_low: benchmark_range_low, benchmark_range_high: benchmark_range_high, dropdown_values: dropdown_values, dropdown_reject_value: dropdown_reject_value, dropdown_accept_value: dropdown_accept_value, audit_comment: audit_comment, audit_camera_photos_key: audit_camera_photos_key, audit_comment_key: audit_comment_key , validation: validation)))
                    paramTView.reloadData()
                }
                
            } catch {
                print("ERROR \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: -Check Camera and Settings
    func checkCameraAccess(isAllowed: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for:.video) {
        case .denied:
            isAllowed(false)
        case .restricted:
            isAllowed(false)
        case .authorized:
            isAllowed(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { isAllowed($0) }
        default:
            print("There is no authorization")
        }
    }
    func presentCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }else{
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
   
    func presentCameraSettings() {
        let alert = UIAlertController.init(title: "Allow the Camera", message: "Move To the setting", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: { (_) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true)
    }
    //MARK: - Action
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnToTakeAsset(_ sender: UIButton) {
        btnTakeAsset.isSelected = true
        for rowIndex in 0..<paramTView.numberOfRows(inSection: 0) {
            let indexPath = IndexPath(row: rowIndex, section: 0)
            if let editPriceCell = paramTView.cellForRow(at: indexPath) as? ParameterTVC {
                
                if editPriceCell.txtSelectedVal.text == ""{
                    self.view.makeToast("Please fill all the Data!")
                }else if editPriceCell.viewImage.isHidden == false {
                    if editPriceCell.imageCamera.image == UIImage(named: "default"){
                        self.view.makeToast("Please fill all the Data!")
                    }else{
                        presentCamera()
                    }
                }else if editPriceCell.viewComment.isHidden == false{
                    if editPriceCell.txtComment.text == "" {
                        self.view.makeToast("Please fill all the Data!")
                    }else{
                        presentCamera()
                    }
                }else {
                    presentCamera()
                }
            }
        }
    }
    
    @IBAction func btnSubmitIns(_ sender: UIButton) {
        activityIndicator.startAnimating()
        changeValue()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            isSwitchPress = true
            insertJsonData()
        }
    }
    func changeValue() {
        auditCommentKey.removeAll()
        auditCommentValue.removeAll()
        auditPhotoKey.removeAll()
        paramValue.removeAll()
        for rowIndex in 0..<paramTView.numberOfRows(inSection: 0) {
            let indexPath = IndexPath(row: rowIndex, section: 0)
            if let editPriceCell = paramTView.cellForRow(at: indexPath) as? ParameterTVC {
                print(editPriceCell.txtSelectedVal.text ?? "")
                
                paramValue.append(editPriceCell.txtSelectedVal.text ?? "")
                if editPriceCell.viewComment.isHidden == false{
                   
                    auditCommentValue.append(editPriceCell.txtComment.text)
                    auditCommentKey.append(getallValues[indexPath.row].audit_comment_key)
                }
                if editPriceCell.viewImage.isHidden == false{
                    print(editPriceCell.imageCamera.image)
                    auditPhotoValue.append(editPriceCell.imageCamera.image ?? UIImage())
                    auditPhotoKey.append(getallValues[indexPath.row].audit_camera_photos_key)
                }
            }
        }
    }
    func addAssetPhoto(photo: UIImage) {

    }
    func reload(tableView: UITableView) {
        let contentOffset = tableView.contentOffset
        paramTView.reloadData()
        paramTView.layoutIfNeeded()
        paramTView.setContentOffset(contentOffset, animated: false)

    }
    
}
//MARK: -Tableview Delegate Methods
extension FillParameterVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getallValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParameterTVC", for: indexPath) as! ParameterTVC
        cellIndexPath = indexPath
        row = indexPath.row
        cell.separatorInset = .zero
        let listedVal = getallValues[indexPath.row]
        let dispName = listedVal.param_disp_name.components(separatedBy: "_").joined(separator: " ").firstCapitalized
        cell.lblValues.text = dispName
        let cellSize = { [self] in
            tblHeightConstraint.constant = tableView.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = tableView.contentSize.height
            view.layoutIfNeeded()
            tblHeightConstraint.constant = tableView.contentSize.height
        }
        
        if fromFillparamPage == true{
            let listedVal2 = paramValueNew[indexPath.row]
            let dispName2 = listedVal2.paramValue
            cell.txtSelectedVal.text = dispName2
            cell.txtComment.text = auditNewCommentValue[indexPath.row]
            print("auditNewCommentValue",auditNewCommentValue[indexPath.row])
            if cell.txtComment.text == ""{
                cell.viewComment.isHidden = true
//                cell.commentImgStackView.isHidden = true
                cellSize()
            }else{
                cell.viewComment.isHidden = false
                cell.commentImgStackView.isHidden = false
                cellSize()
            }
            cell.imageCamera.image = auditNewPhotoValue[indexPath.row]
            
            print("auditNewPhotoValue",auditNewPhotoValue[indexPath.row])
            if auditNewPhotoValue[indexPath.row] == UIImage() {
                cell.viewImage.isHidden = true
//                cell.commentImgStackView.isHidden = true
                cellSize()
            }else{
                cell.viewImage.isHidden = false
                cell.commentImgStackView.isHidden = false
                cellSize()
            }
            if auditNewPhotoValue[indexPath.row] == UIImage() && cell.txtComment.text == "" {
                cell.commentImgStackView.isHidden = true
            }
            
        }
        
        
        if listedVal.param_type == "Drop-down"{
            cell.btnDrop.isHidden = false
            cell.imgDown.isHidden = false
            cell.txtSelectedVal.placeholder = "Select"
        }else{
            cell.btnDrop.isHidden = true
            self.dropDown.hide()
            cell.imgDown.isHidden = true
            cell.txtSelectedVal.placeholder = "Enter"
        }
        
        //Textfield
        cell.endEditing = {
            self.fromFillparamPage = false
            self.dropDown.hide()
            
            if listedVal.param_type == "Number"{
                print(self.getTextFieldValues)
                if self.getTextFieldValues.count < indexPath.row{
                    print("Yes")
//                    let textfieldVal = self.getTextFieldValues[indexPath.row]
                }else{
                    print("No")
                    let textfieldVal = self.getTextFieldValues[indexPath.row]
                    let textfieldInt: Int? = Int(cell.txtSelectedVal.text!)
                    
                    //                self.paramValue.append(cell.txtSelectedVal.text ?? "")
                    if (textfieldInt ?? 0 == 0) || (cell.txtSelectedVal.text == ""){
                        cell.viewComment.isHidden = true
                        cell.commentImgStackView.isHidden = true
                        cell.viewImage.isHidden = true
                        cell.txtSelectedVal.text = ""
                        cell.imageCamera.image = UIImage(named: "default")
                        self.reload(tableView: tableView)
                        cellSize()
                        return
                    }
                    if (textfieldInt ?? 0 < listedVal.benchmark_range_low) || (textfieldInt ?? 0 > listedVal.benchmark_range_high){
                        if textfieldVal.txtCommentAceept == "Required"{
                            cell.viewComment.isHidden = false
                            cell.commentImgStackView.isHidden = false
                        }else{
                            cell.viewComment.isHidden = true
                            cell.commentImgStackView.isHidden = true
                        }
                        if textfieldVal.txtAcceptphotos == "Required"{
                            cell.commentImgStackView.isHidden = false
                            cell.viewImage.isHidden = false
                        }else{
                            cell.viewImage.isHidden = true
                            cell.commentImgStackView.isHidden = true
                        }
                        if textfieldVal.txtRejectphotos == "Required"{
                            cell.viewImage.isHidden = false
                            cell.commentImgStackView.isHidden = false
                        }else{
                            cell.viewImage.isHidden = true
                            cell.commentImgStackView.isHidden = true
                        }
                        if textfieldVal.txtRejectcomment == "Required"{
                            cell.viewComment.isHidden = false
                            cell.commentImgStackView.isHidden = false
                        }else{
                            cell.viewComment.isHidden = true
                            cell.commentImgStackView.isHidden = true
                        }
                        self.reload(tableView: tableView)
                        cellSize()
                    }
                    else{
                        
                        cell.viewComment.isHidden = true
                        cell.viewImage.isHidden = true
                        cell.commentImgStackView.isHidden = true
                        self.reload(tableView: tableView)
                        cellSize()
                    }
                }
            }
        }
        //DropDown
        cell.btnDrop.tag = indexPath.row
        cell.openDropDown = {
            self.fromFillparamPage = false
            let dropdownValue = self.getallValues[cell.btnDrop.tag].dropdown_values.components(separatedBy: ",")
            self.dropDown.dataSource = dropdownValue
            self.getDroppDownValues.removeAll()
            self.dropDown.show()
            self.dropDown.anchorView = cell.dropDownView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                
                cell.txtSelectedVal.text = item
                let validationData = getallValues[indexPath.row].validation
                
                if let validationData = validationData?.data(using: String.Encoding.utf8) {
                    do {
                        let response = try JSONSerialization.jsonObject(with: validationData, options: .allowFragments) as? [[String: Any]]
                        response?.map { data in
                            let dropDownvalidation = data["validation"] as? [String : Any]
                            dropDownvalidation.map { valid in
                                dropDownAcceptComment = valid["dropdown_values_accept_comment_validation"] as? String ?? ""
                                dropDownAcceptPhoto = valid["dropdown_values_aacept_photos_validation"] as? String ?? ""
                                dropDownRejectComment = valid["dropdown_values_reject_comment_validation"] as? String ?? ""
                                dropDownRejectPhoto = valid["dropdown_values_reject_photos_validation"] as? String ?? ""
                                
                                getDroppDownValues.append(DropDownValues(dropDownCommentAceept: dropDownAcceptComment, dropDownAcceptphotos: dropDownAcceptPhoto, dropDownRejectphotos: dropDownRejectPhoto, dropDownRejectcomment: dropDownRejectComment))
                            }
                        }
                    }catch{
                        print(error.localizedDescription)
                    }
                }
                let dropdownList = getDroppDownValues[index]
                
                if(dropdownList.dropDownRejectcomment == "Required"){
                    cell.viewComment.isHidden = false
                    cell.commentImgStackView.isHidden = false
                }
                if(dropdownList.dropDownRejectphotos == "Required"){
                    cell.viewImage.isHidden = false
                    cell.commentImgStackView.isHidden = false
                }
                if(dropdownList.dropDownAcceptphotos == "Required"){
                    cell.viewImage.isHidden = false
                    cell.commentImgStackView.isHidden = false
                }
                if(dropdownList.dropDownCommentAceept == "Required"){
                    cell.viewComment.isHidden = false
                    cell.commentImgStackView.isHidden = false
                }
                if(dropdownList.dropDownRejectcomment == "NotRequired" || dropdownList.dropDownRejectcomment == "Not Required"){
                    cell.viewComment.isHidden = true
                    cell.commentImgStackView.isHidden = true
                }
                if(dropdownList.dropDownRejectphotos == "NotRequired" || dropdownList.dropDownRejectphotos == "Not Required"){
                    cell.viewImage.isHidden = true
                    cell.commentImgStackView.isHidden = true
                }
                if(dropdownList.dropDownAcceptphotos == "NotRequired" || dropdownList.dropDownAcceptphotos == "Not Required"){
                    cell.viewImage.isHidden = true
                    cell.commentImgStackView.isHidden = true
                }
                if(dropdownList.dropDownCommentAceept == "NotRequired" || dropdownList.dropDownCommentAceept == "Not Required"){
                    cell.viewComment.isHidden = true
                    cell.commentImgStackView.isHidden = true
                }
                reload(tableView: tableView)
                cellSize()
            }
        }
        
        cell.openCamera = {
            self.selectedIndexPath = indexPath
            self.presentCamera()
            cell.btnCamera.isSelected = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(5)
        let additionalSeparator = UIView(frame: CGRect(x: 0,
                                                       y: cell.frame.size.height - additionalSeparatorThickness,
                                                       width: cell.frame.size.width,
                                                       height: additionalSeparatorThickness))
        if #available(iOS 13.0, *) {
            additionalSeparator.backgroundColor = .systemGray5
        }
        cell.addSubview(additionalSeparator)
    }
    
}
extension FillParameterVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if selectedIndexPath != nil{
            let cell:ParameterTVC = paramTView.cellForRow(at: selectedIndexPath) as! ParameterTVC
            if cell.btnCamera.isSelected == true{
                cell.btnCamera.isSelected = false
            }
        }
        if btnTakeAsset.isSelected == true{
            btnTakeAsset.isSelected = false
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if selectedIndexPath != nil{
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                let cell:ParameterTVC = paramTView.cellForRow(at: selectedIndexPath) as! ParameterTVC
                if cell.btnCamera.isSelected == true{
                    btnTakeAsset.isSelected = false
                    cell.imageCamera.image = nil
                    cell.imageCamera.image = image
//                    auditPhotoValue.append(image)
                    if cell.imageCamera.image != image{
                        cell.btnCamera.isSelected = true
                    }else{
                        cell.btnCamera.isSelected = false
                    }
                }
            }
        }
        if btnTakeAsset.isSelected == true{
            btnTakeAsset.isSelected = false
            if let assetimage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                asset_Image.image = assetimage
                imgHeightCons.constant = 100
                if asset_Image.image == nil{
                    btnTakeAsset.isHidden = false
                    btnSubmitInsp.isHidden = true
                }else{
                    btnTakeAsset.isHidden = true
                    btnSubmitInsp.isHidden = false
                }
                
            }
        }
    }
}


class jsonVal : Encodable{
    var _key : String!
    var _value : String!
    init(_key: String? = nil, _value: String? = nil) {
        self._key = _key
        self._value = _value
    }
}
