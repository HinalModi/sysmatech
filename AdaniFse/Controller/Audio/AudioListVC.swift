//
//  AudioListVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 14/10/22.
//

import UIKit
import AVKit
import AVFoundation
import Alamofire
import Combine

class AudioListVC: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var audioSlide: UISlider!
    @IBOutlet weak var lblRecTime: UILabel!
    @IBOutlet weak var btnRec: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var viewRecord: UIView!
    @IBOutlet weak var viewPlay: UIView!
    @IBOutlet weak var presentView: UIView!
    @IBOutlet weak var audioTV: UITableView!
    var audioId = Int()
    var setAudioList : [AudioList] = []
    var getAudioList : [[String : Any]] = []
    var audioRecorder: AVAudioRecorder?
    var audioPlayer: AVAudioPlayer?
    var timeTimer: Timer?
    var milliseconds: Int = 0
    var mp3FileData = Data()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckOut = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAdd.cornerRadius = 10
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        presentView.addGestureRecognizer(tap)
        listAudio()
        setAudioName()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if viewRecord.isHidden {
            return
        }else{
            btnAdd.isHidden = true
        }
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        UIView.animate(withDuration: 0.1) {
            self.presentView.isHidden = true
            self.viewRecord.isHidden = true
            self.viewPlay.isHidden = true
            self.timeTimer?.invalidate()
            self.audioSlide.value = 0
            self.btnRec.isHidden = false
            self.btnStop.isHidden = true
            self.milliseconds = 0
            self.lblRecTime.text = "00:00:00"
            self.btnAdd.isHidden = true
        }
    }
//MARK: - Action
    var player: AVAudioPlayer!
    @IBAction func btnSubmit(_ sender: UIButton) {
        var params : [String:Any] = [:]
        params["lang"]  = "en"
        
        print(params)
        let url = URL(string: "\(selectedComp ?? "")audit-parameters/audio-params/\(audioId)")!
        let headers : HTTPHeaders = [
//            "Content-Type": "multipart/form-data",
            "Authorization":"\(tokenCheckOut)",
            "Accept" : "application/json"
        ]
       
        AF.upload(multipartFormData: { multipartFormData in
//            let image = self.docImg.image?.jpegData(compressionQuality: 0.5)\
            
            multipartFormData.append(self.mp3FileData, withName: "audio" , fileName: "audio", mimeType: "mp3")
            
            for (key, value) in params {
                print(key,":",value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue) ?? Data(), withName: key)
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response { (result) in
            if result.error != nil{
                print(result.error)
            }else{
                if let jsonData = result.data
                {
                    do{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        print(parsedData)
                        let message = parsedData["messages"] as? String ?? ""
                        let success = parsedData["status"] as? String ?? ""
                        if success == "success"{
                            self.view.makeToast(message)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                                self.navigationController?.popToViewController(ofClass: DashBoardVC.self)
                            })
                        }else{
                            self.view.makeToast(message)
                        }
                    }catch{
                        self.view.makeToast("Something went wrong please try again later!")
                        print(String(describing: error))
                    }
                }
            }
        }
    }
    @IBAction func btnStopPlayer(_ sender: UIButton) {
        timeTimer?.invalidate()
        audioPlayer?.pause()
    }
    @IBAction func btnPlay(_ sender: UIButton) {
        
        if audioRecorder?.isRecording == false {
            
            do {
                try audioPlayer = AVAudioPlayer(contentsOf:
                                                    (audioRecorder?.url)!)
            
                milliseconds = 0
                audioPlayer!.prepareToPlay()
                audioPlayer!.delegate = self
                audioPlayer!.play()
                
                audioSlide.maximumValue = Float((self.audioPlayer?.duration ?? 0))
                timeTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(playRecSlider), userInfo: nil, repeats: true)
                
            } catch let error as NSError {
                print(String(describing: error))
                print("audioPlayer error: \(error.localizedDescription)")
            }
        }
    }
    @objc func playRecSlider(){
        audioSlide.value = Float(audioPlayer?.currentTime ?? 0)
        print(Float(audioPlayer?.currentTime ?? 0))
    }
    @IBAction func btnAddAudio(_ sender: UIButton) {        
        if player != nil{
            player.stop()
        }
        UIView.animate(withDuration: 0.2) { [self] in
            presentView.isHidden = false
            viewRecord.isHidden = false
            
        }
    }
    var disposable = Set<AnyCancellable>()
    
    func convertAudio(){
        let input = GlobalFunction.getDocumentsDirectory().appendingPathComponent("sound.caf")
        let output = GlobalFunction.getDocumentsDirectory().appendingPathComponent("converted.mp3")
        print("Outputpath",output)
        let converter = MP3Converter()
        /// Run it in a different queue and update the UI when it's done executing, otherwise your UI will freeze.
        DispatchQueue.global(qos: .userInteractive).async {
                converter.convert(input: input, output: output)
                    .receive(on: DispatchQueue.global())
                    .sink(receiveCompletion: { result in
                        DispatchQueue.main.async {
                            if case .failure(let error) = result {
                                print("Conversion failed: \n\(error.localizedDescription)")
                                
                            }
                        }
                    }, receiveValue: { result in
                        DispatchQueue.main.async {
                            print(result)
                            if let imageData = try? Data(contentsOf: result) {
                                self.mp3FileData = imageData // HERE IS YOUR IMAGE! Do what you want with it!
                                print(self.mp3FileData)
                            }
                            print("File saved as converted.mp3 in \nthe documents directory.")

                        }
                    }).store(in: &self.disposable)
           
        }
    }
    @IBAction func btnDeleteRec(_ sender: UIButton) {
        viewPlay.isHidden = true
        viewRecord.isHidden = false
        timeTimer?.invalidate()
        milliseconds = 0
        lblRecTime.text = "00:00:00"
        btnRec.isHidden = false
        btnStop.isHidden = true
        if audioRecorder?.isRecording == true {
            audioRecorder?.stop()
        } else {
            audioPlayer?.stop()
        }
    }
    @IBAction func btnStopRec(_ sender: Any) {
        btnAdd.isHidden = false
        viewPlay.isHidden = false
        viewRecord.isHidden = true
        audioSlide.value = 0
        timeTimer?.invalidate()
        
        if audioRecorder?.isRecording == true {
            audioRecorder?.stop()
        } else {
            audioPlayer?.stop()
        }
        convertAudio()
    }
    
    @IBAction func btnStartRec(_ sender: UIButton) {
        btnAdd.isHidden = true
        btnRec.isHidden = true
        btnStop.isHidden = false
        timeTimer?.invalidate()
        milliseconds = 0
        lblRecTime.text = "00:00:00"
        timeTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTimeLabel), userInfo: nil, repeats: true)
        
        if audioRecorder?.isRecording == false {
            audioRecorder?.record()
        }
    }
    @objc func updateTimeLabel(timer: Timer) {
        milliseconds+=1
        let milli = (milliseconds % 60) + 39
        let sec = (milliseconds / 60) % 60
        let min = milliseconds / 3600
        lblRecTime.text = NSString(format: "%02d:%02d.%02d", min, sec, milli) as String
        
    }
   
    //MARK: - Api Call
    func listAudio(){
        let param : [String : String] = [:]
       
        let url = URL(string: " \(selectedComp ?? "")audit-parameters/audio-params_list/\(audioId)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: param)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
//                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        self.getAudioList = dic["payload"] as! [[String : Any]]
                        
                        for data in self.getAudioList{
                            let id = data["id"]
                            let file_name = data["file_name"]
                            let parameter_id = data["parameter_id"]
                            let language_name = data["language_name"]
                            let created_by = data["created_by"]
                            let created_at = data["created_at"]
                            let updated_at = data["updated_at"]
                            let deleted_at = data["deleted_at"]
                            let status = data["status"]
                            self.setAudioList.append(AudioList(id: id as? Int ?? 0 , file_name: file_name as? String ?? "", parameter_id: parameter_id as? Int ?? 0, language_name: language_name as? String ?? "", created_by: created_by  as? String ?? "", created_at: created_at  as? String ?? "", updated_at: updated_at as? String ?? "", deleted_at:deleted_at  as? String ?? "", status: status as? Int ?? 0))
                            self.audioTV.reloadData()
                        }
                    }catch{
                       print(String(describing: error))
                        
                    }
                }
            }
        }.resume()
    }
    
    //MARK: - Audio Setting
    func setAudioName(){
        let fileMgr = FileManager.default
        
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.caf")
        
        let recordSettings =
        [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
              AVEncoderBitRateKey: 16,
            AVNumberOfChannelsKey: 2,
                  AVSampleRateKey: 44100.0,
                   ] as [String : Any]

        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(
                AVAudioSession.Category.playAndRecord)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        do {
            try audioRecorder = AVAudioRecorder(url: soundFileURL,
                                                settings: recordSettings as [String : AnyObject])
            audioRecorder?.prepareToRecord()
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {       
        timeTimer?.invalidate()
    }
    func setDefaultAudio(id : Int){
        let param : [String : String] = [:]
        let url = URL(string: "\(selectedComp ?? "")audit-parameters/audio-params_set_default/\(id)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: param)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    var inc = 0
//MARK: - Action
    @IBAction func btnPrevious(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        player = nil
    }
}
extension AudioListVC  :UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setAudioList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioListTVC") as! AudioListTVC
        cell.lblName.text = setAudioList[indexPath.row].created_by
        if setAudioList[indexPath.row].status == 1{
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
        }
        
        if setAudioList[indexPath.row].created_at != ""{
            let newdate = GlobalFunction.convertDateString(dateString: setAudioList[indexPath.row].created_at, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
            cell.lblDate.text = newdate
        }
        cell.play = {
            do{
                let url = URL(string: "https://uat.sysmatech.com/public/uploads/audio/" + self.setAudioList[indexPath.row].file_name)
                
                let data = try Data(contentsOf: url ?? URL(fileURLWithPath: ""))
                try AVAudioSession.sharedInstance().setCategory(.playback ,mode: .default, options: [])
                try AVAudioSession.sharedInstance().setActive(true)
                self.player = try AVAudioPlayer(data: data)
                DispatchQueue.main.async {
                    self.player.play()
                }
            }catch{
                print(String(describing: error))
            }
        }
        cell.pause = {
            if self.player.isPlaying{
                self.player.pause()
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AudioListTVC
        let alert = UIAlertController(title: "", message: "Do you want to change default audio?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [self] action in
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            setDefaultAudio(id: self.setAudioList[indexPath.row].id)
            navigationController?.popToViewController(ofClass: DashBoardVC.self)
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .destructive, handler: nil))
        present(alert, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AudioListTVC
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
struct AudioList{
    init(id: Int = Int(), file_name: String = String(), parameter_id: Int = Int(), language_name: String = String(), created_by: String = String(), created_at: String = String(), updated_at: String = String(), deleted_at: String = String(), status: Int = Int()) {
        self.id = id
        self.file_name = file_name
        self.parameter_id = parameter_id
        self.language_name = language_name
        self.created_by = created_by
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at
        self.status = status
    }
    
    var id = Int()
    var file_name = String()
    var parameter_id = Int()
    var language_name = String()
    var created_by = String()
    var created_at = String()
    var updated_at = String()
    var deleted_at = String()
    var status = Int()
}
