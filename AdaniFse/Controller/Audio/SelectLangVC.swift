//
//  SelectLangVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 03/11/22.
//

import UIKit

class SelectLangVC: UIViewController {

    @IBOutlet weak var langTV: UITableView!
    var langArray = ["Hindi" , "Spanish" , "German" , "French"]
    var appLangArry = ["English" , "Arabic"]
    var isFromAudioSelect = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    @IBAction func btnPrev(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        navigationController?.popToViewController(ofClass: DashBoardVC.self)

    }
    
    @IBAction func btnAudioLang(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Do you want to change language?", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Hindi", style: .default, handler: {  action in
            UserDefaults.standard.set( "Hindi", forKey: "AppLang")
        }))
        
        alert.addAction(UIAlertAction(title: "Spanish", style: .default, handler: {  action in
            UserDefaults.standard.set( "Spanish", forKey: "AppLang")
        }))
        alert.addAction(UIAlertAction(title: "German", style: .default, handler: {  action in
            UserDefaults.standard.set( "German", forKey: "AppLang")
        }))
        alert.addAction(UIAlertAction(title: "French", style: .default, handler: {  action in
            UserDefaults.standard.set( "French", forKey: "AppLang")
        }))
        
        present(alert, animated: true)
    }
    
    @IBAction func btnAppLang(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Do you want to change language?", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { [self] action in
            let vc = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            VCLangauageManager.sharedInstance.changeAppLanguage(To: .english)
            Constant.APPDELEGATE.setInitialViewController()
            navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Arabic", style: .default, handler: { [self] action in
            let vc = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            if VCLangauageManager.sharedInstance.isLanguageEnglish() {
                VCLangauageManager.sharedInstance.changeAppLanguage(To: .arabic)
                    Constant.APPDELEGATE.setInitialViewController()
            }
                navigationController?.pushViewController(vc, animated: true)
        }))
        present(alert, animated: true)
    }
}

