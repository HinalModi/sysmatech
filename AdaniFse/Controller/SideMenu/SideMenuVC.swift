//
//  SideMenuVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit
import FirebaseAuth
import SQLite
import LGSideMenuController

class SideMenuVC: UIViewController {
   

    @IBOutlet weak var menuTVC: UITableView!
    @IBOutlet weak var lblLoggedEmail: UILabel!
    @IBOutlet weak var lblLoggedName: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var topView: UIView!
    var setlogName = String()
    var setLogEmail = String()
    var menuItemArray = ["Jobs / Work Orders" , "Accessories", "Consumables", "Licences", "Components" ,"Predefined Kits", "Category List", "Asset Status", "Settings","Language", "Logout"]
    var menuImageArray : [UIImage] = [
        UIImage(named: "house-black-silhouette-without-door") ?? UIImage()
        ,UIImage(named: "keyboard") ?? UIImage()
        ,UIImage(named: "water-drop") ?? UIImage(),UIImage(named: "certificate") ?? UIImage(),UIImage(named: "folder") ?? UIImage(),UIImage(named: "framerate") ?? UIImage(),UIImage(named: "categories") ?? UIImage(),UIImage(named: "handyman_24") ?? UIImage(),UIImage(named: "settings") ?? UIImage(),UIImage(named: "language") ?? UIImage(),UIImage(named: "power-off") ?? UIImage()]
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            topView.backgroundColor = Constant.Color.neon_light
            statusView.backgroundColor = Constant.Color.neon_light
        } else {
            topView.backgroundColor = .white
            statusView.backgroundColor = .white
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        menuTVC.reloadData()
        lblLoggedName.text = UserDefaults.standard.string(forKey: "user_full_name")
        lblLoggedEmail.text = UserDefaults.standard.string(forKey: "companyName")
        
    }
    
    @IBAction func toChangeDB(_ sender: UIButton) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "SelectCompanyVC") as? SelectCompanyVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
        
    }
   
}
extension SideMenuVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC") as! SideMenuTVC
        cell.imgMenu.image = menuImageArray[indexPath.row]
        cell.lblMenuName.text = menuItemArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 1{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AccessoryVC") as? AccessoryVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 2{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ConsumableVC") as? ConsumableVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 3{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "LicenceVC") as? LicenceVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 4{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ComponentVC") as? ComponentVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 5{
            let storyBoard = UIStoryboard(name: "List", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PredefinedKitVC") as? PredefinedKitVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 6{
            let storyBoard = UIStoryboard(name: "CategoryModal", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CategoryListVC") as? CategoryListVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 7{
            let storyBoard = UIStoryboard(name: "AssetStatus", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetStatusVC") as? AssetStatusVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 8{
            let vc = storyboard!.instantiateViewController(withIdentifier: "SettingVC") as? SettingVC
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
        if indexPath.row == 9{
            let vc = storyboard!.instantiateViewController(withIdentifier: "SelectLangVC") as? SelectLangVC
            vc?.isFromAudioSelect = false
            if let viewController = vc {
                (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            self.sideMenuController?.hideLeftView()
        }
       
        if indexPath.row == 10{
            let alert = UIAlertController(title: "", message: "Do you want to Logout?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                if let viewController = vc {
                    (self.sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
                }
                UserDefaults.standard.removeObject(forKey: "domain")
                UserDefaults.standard.removeObject(forKey: "user_full_name")                
                UserDefaults.standard.removeObject(forKey: "companyName")
                UserDefaults.standard.removeObject(forKey: "assetImgPath")
                UserDefaults.standard.removeObject(forKey: "assetInsepctionImgPath")
                UserDefaults.standard.removeObject(forKey: "assetBreakdwon_insepction_img_path")
                UserDefaults.standard.removeObject(forKey: "bearerToken")
                do {
                    try Auth.auth().signOut()
                    UserInfoManager.removeUserInfo()
                }catch{
                    print(error.localizedDescription)
                }
                do {
                    let table = Table("Pendingjobs")
                    let drop = table.drop(ifExists: true)
                    try db?.run(drop)
                }catch{
                    print(error.localizedDescription)
                }
                do {
                    let table = Table("PreventiveJobs")
                    let drop = table.drop(ifExists: true)
                    try db?.run(drop)
                }catch{
                    print(error.localizedDescription)
                }
                do{
                    let table2 = Table("Completedjobs")
                    let drop2 = table2.drop(ifExists: true)
                    try db?.run(drop2)
                }catch{
                    print(error.localizedDescription)
                }
                do{
                    let table3 = Table("SubmitInspection")
                    let drop3 = table3.drop(ifExists: true)
                    try db?.run(drop3)
                }catch{
                    print(error.localizedDescription)
                }
                do{
                    let table3 = Table("PMHistory")
                    let drop2 = table3.drop(ifExists: true)
                    try db?.run(drop2)
                }catch{
                    print(error.localizedDescription)
                }
                do{
                    let table = Table("AssetList")
                    let drop = table.drop(ifExists: true)
                    try db?.run(drop)
                }catch{
                    print(error.localizedDescription)
                }
                do{
                    let table = Table("CauseMaster")
                    let drop = table.drop(ifExists: true)
                    try db?.run(drop)
                }catch{
                    print(error.localizedDescription)
                }
                
                do{
                    let table = Table("CategoryList")
                    let drop = table.drop(ifExists: true)
                    try db?.run(drop)
                }catch{
                    print(error.localizedDescription)
                }
                do{
                    let table = Table("ModelList")
                    let drop = table.drop(ifExists: true)
                    try db?.run(drop)
                }catch{
                    print(error.localizedDescription)
                }
                
                self.sideMenuController?.hideLeftView()
              }))

            alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
              }))
            present(alert, animated: true)
        }
    }
    
}
