//
//  LGSideMenu.swift
//  AdaniFse
//
//  Created by Ankit Dave on 13/05/22.
//

import Foundation
import LGSideMenuController

class LGSideMenu: LGSideMenuController {
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        leftViewPresentationStyle = .slideBelow
        leftViewWidth = view.frame.width / 1.5
        rootViewCoverBlurEffect = UIBlurEffect(style: .prominent)
        isLeftViewSwipeGestureEnabled = false
        
    }
    
}
