//
//  SettingVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 12/05/22.
//

import UIKit

class SettingVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var loadDataBgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadDataBgView.roundedUsingHeight()
        if #available(iOS 11.0, *) {
            loadDataBgView.backgroundColor = Constant.Color.color_launch
            btnSync.backgroundColor = .black
        } else {
            loadDataBgView.backgroundColor = .blue
            btnSync.backgroundColor = .blue
        }
        btnSync.setCorner(_tp: 5)
        btnSync.tintColor = .white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblDate.text = UserDefaults.standard.string(forKey: "currentDate")  ?? ""
        if lblDate.text == ""{
            lblTitle.isHidden = true
            lblDate.isHidden = true
        }else{
            lblDate.isHidden = false
            lblTitle.isHidden = false
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    @IBAction func btSyncData(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork(){
        lblDate.isHidden = false
        lblTitle.isHidden = false
        let currDate = Date.getCurrentDate()
        UserDefaults.standard.set(currDate, forKey: "currentDate")
        lblDate.text = UserDefaults.standard.string(forKey: "currentDate")  ?? ""
        print(currDate)
        let vc = FillParameterVC()
            vc.isfromSetting = true
            vc.createDB()            
        }else{
            GlobalFunction.showAlert(message: "Please connect to internet to sync data", actionTitle: "Ok", viewcontroller: self)
        }
    }
}
extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"

        return dateFormatter.string(from: Date())

    }
}
