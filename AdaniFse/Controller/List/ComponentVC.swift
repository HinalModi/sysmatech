//
//  ComponentVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 01/09/22.
//

import UIKit
import LGSideMenuController
import SQLite
class ComponentVC: UIViewController {
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var componentTV: UITableView!
    var getComponentList : [[String : Any]] = []
    var componentList : [ComponentData] = []
    var getOfflineComponent : [ComponentOfflineData] = []
    var refreshControl: UIRefreshControl!
    var isfromSearch = Bool()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        componentTV.tableFooterView = UIView()
        getComponents()
        createDB()
        readValues()
        addPullRequest()
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            getComponents()
            createDB()
        }
        readValues()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            componentTV.refreshControl = refreshControl
        } else {
            componentTV.addSubview(refreshControl)
        }
    }
    @IBAction func btnSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
    //MARK: - Api Call
    func getComponents(){
        activityIndicator.startAnimating()
      
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")components?user_id=\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.getComponentList = dic["rows"] as! [[String : Any]]
                    self.componentList.removeAll()
                    for data in self.getComponentList{
                        let name = data["name"] as? String
                        let category = data["category"] as? String
                        let image = data["image"] as? String
                        let min_amt = data["min_amt"] as? Int
                        let location = data["location"] as? [String: Any]
                        let remaining = data["remaining"] as? Int
                        let id = data["id"] as? Int
                        let supplier = data["supplier"] as? [String: Any]
                        let created_at = data["created_at"] as? [String: Any]
                        let company = data["company"] as? [String: Any]
                        let model_number = data["model_number"] as? String
                        self.componentList.append(ComponentData(id: id  ?? 0, name: name ?? "", category: category ?? "", minQty: min_amt ?? 0, locationId: location?["id"] as? Int ?? 0 , locationName: location?["name"] as? String ?? "" , remainings: remaining ?? 0, image: image ?? "",date : created_at?["formatted"] as? String ?? "", create_at:created_at ?? [String : Any]() ,
                             companyId: company?["id"] as? Int ?? 0 ,companyName: company?["name"] as? String ?? "",
                             company: company ?? [String : Any]() , supplierName: supplier?["name"] as? String ?? "",supplier: supplier  ??
                             [String: Any](),supplierId: supplier?["id"] as? Int ?? 0, model_number: model_number ?? ""))
                        }
                    self.insertData()
                    self.readValues()
                    if self.getOfflineComponent.count == 0 {
                        self.lblMsg.isHidden = false
                    }else{
                        self.lblMsg.isHidden = true
                    }
                    self.componentTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
    //MARK: -StoreDataInSqlite
    let algorithms = Table("Components")
    var name = Expression<String>("name")
    var category = Expression<String>("category")
    var image = Expression<String>("image")
    var min_amt = Expression<Int>("min_amt")
    var locationId = Expression<Int>("locationId")
    var locationName = Expression<String>("locationName")
    var remaining = Expression<Int>("remaining")
    var comp_id = Expression<Int>("id")
    var supplierId  = Expression<Int>("supplierId")
    var supplierName  = Expression<String>("supplierName")
    var companyName  = Expression<String>("companyName")
    var companyId  = Expression<Int>("companyId")
    var date  = Expression<String>("date")
    var model_number = Expression<String>("model_number")
        func createDB(){
            do {
                // Get database path
                if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                    print("jobspath",path)
                    
                    // Connect to database
                    db = try Connection("\(path)/components.sqlite3")
                    if Reachability.isConnectedToNetwork(){
                        let drop = algorithms.drop(ifExists: true)
                        try db?.run(drop)
                    }
                    // Initialize tables
                    try db?.run(algorithms.create { table in
                        table.column(name)
                        table.column(category)
                        table.column(image)
                        table.column(min_amt)
                        table.column(locationId)
                        table.column(locationName)
                        table.column(remaining)
                        table.column(comp_id,primaryKey: true)
                        table.column(date)
                        table.column(companyId)
                        table.column(companyName)
                        table.column(supplierName)
                        table.column(supplierId)
                        table.column(model_number)
                    })
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        func insertData(){
            for dataset in componentList{
                do{
                    try db?.run(algorithms.insert(name <- dataset.name , category <- dataset.category , image <- dataset.image , min_amt <- dataset.minQty , locationId <- dataset.locationId , locationName <- dataset.locationName , remaining <- dataset.remainings,date <- dataset.date ,companyId <- dataset.companyId, companyName <- dataset.companyName,supplierName <- dataset.supplierName ,supplierId <- dataset.supplierId , model_number <- dataset.model_number))
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
        func readValues(){
            getOfflineComponent.removeAll()
            do{
                for row in try db!.prepare("SELECT * FROM Components") {
                    print("row173",row)
                    getOfflineComponent.append(ComponentOfflineData(id: Int(row[7] as! Int64), name: row[0] as? String ?? "", category:  row[1] as? String ?? "", minQty:  Int(row[3] as? Int64 ?? 0), locationId: Int(row[4] as? Int64 ?? 0), locationName:  row[5] as? String ?? "", remainings: Int(row[6] as? Int64 ?? 0), image: row[2] as? String ?? "" ,date : row[8] as? String ?? "", companyId: row[9] as? Int ?? 0, companyName: row[10] as? String ?? "", supplierName:row[11] as? String ?? "", supplierId: row[12] as? Int ?? 0, model_number:row[13] as? String ?? "" ))
                        componentTV.reloadData()
                }
            }catch{
                print(error.localizedDescription)
            }
        }
}
extension ComponentVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getOfflineComponent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComponentTVC") as! ComponentTVC
        let list = getOfflineComponent[indexPath.row]
        cell.lblRemTitle.text = "Qty - \(list.remainings)(Avail) - \(list.minQty)(Min)"
        
        if list.locationName == ""{
            cell.lblLoc.text = "N/A"
        }else{
            cell.lblLoc.text = list.locationName
        }
        if list.category == "" {
            cell.lblcatTitle.text = "Component"
        }else{
            cell.lblcatTitle.text = list.category
        }
        cell.compName.text = list.name
        let urlImg = URL(string: list.image)
        cell.imgComp.sd_setImage(with: urlImg, placeholderImage: UIImage(named: "setting2"))
        cell.checkOut = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            vc.isFromComponent = true
            vc.componentId = list.id
            if list.category == "" {
                vc.categoryName = "Component"
            }else{
                vc.categoryName = list.category
            }
            vc.accessName = list.name
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if getOfflineComponent.count > 0{
//            let list = getOfflineComponent[indexPath.row]
//            let vc = storyboard?.instantiateViewController(withIdentifier: "AccessoryDetailVC") as! AccessoryDetailVC
//            vc.componentId = list.id
//            vc.isFromComponent = true
//            vc.componentName = list.name
//            navigationController?.pushViewController(vc, animated: true)
//        }
        if getOfflineComponent.count > 0{
            let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
            let list = getOfflineComponent[indexPath.row]
            vc.componentId = list.id
            vc.componentName = list.name
            vc.location = list.locationName
            vc.dueDate = list.date
            vc.supplier = list.supplierName
            vc.companyName = list.companyName
            vc.isFromComponent = true
            vc.category = list.category
            vc.modalnNo = list.model_number
            vc.auditImageUrl = URL(string: baseUrlImage + list.image)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
struct ComponentData {
    init(id : Int ,name: String, category: String, minQty: Int, locationId: Int, locationName: String, remainings: Int ,image : String,date : String ,
         create_at : [String: Any] ,companyId : Int , companyName : String , company : [String: Any] ,
         supplierName : String , supplier : [String: Any] ,supplierId : Int ,model_number : String) {
        self.name = name
        self.category = category
        self.minQty = minQty
        self.locationId = locationId
        self.locationName = locationName
        self.remainings = remainings
        self.image = image
        self.id = id
        self.supplier = supplier
        self.supplierId = supplierId
        self.supplierName = supplierName
        self.company = company
        self.companyId = companyId
        self.companyName = companyName
        self.create_at = create_at
        self.date = date
        self.model_number = model_number
    }
    var name : String
    var category : String
    var minQty : Int
    var locationId : Int
    var locationName : String
    var remainings : Int
    var image : String
    var id : Int
    var supplierId : Int
    var supplier : [String: Any]
    var supplierName : String
    var company : [String: Any]
    var companyName : String
    var companyId : Int
    var create_at : [String: Any]
    var date : String
    var model_number : String
}
struct ComponentOfflineData {
    init(id : Int ,name: String, category: String, minQty: Int, locationId: Int, locationName: String, remainings: Int , image : String ,date : String ,
         companyId : Int , companyName : String , supplierName : String ,supplierId : Int,model_number : String) {
        self.name = name
        self.category = category
        self.minQty = minQty
        self.locationId = locationId
        self.locationName = locationName
        self.remainings = remainings
        self.image = image
        self.id = id
        self.supplierId = supplierId
        self.supplierName = supplierName
        self.companyId = companyId
        self.companyName = companyName
        self.date = date
        self.model_number = model_number
    }
    var name : String
    var category : String
    var minQty : Int
    var locationId : Int
    var locationName : String
    var remainings : Int
    var image : String
    var id : Int
    var supplierId : Int
    var supplierName : String
    var companyName : String
    var companyId : Int
    var date : String
    var model_number : String
}
