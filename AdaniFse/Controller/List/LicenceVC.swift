//
//  LicenceVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 01/09/22.
//

import UIKit
import LGSideMenuController
import SQLite

class LicenceVC: UIViewController {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var licenceVC: UITableView!
    var getLicenceList : [[String : Any]] = []
    var licenceList : [LicenceData] = []
    var getOfflineLicenses : [LicenceOfflineData] = []
    var refreshControl: UIRefreshControl!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    var isfromSearch = Bool()
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        licenceVC.tableFooterView = UIView()
        getLicenceCall()
        createDB()
        readValues()
        addPullRequest()
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            getLicenceCall()
            createDB()
        }
        readValues()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            licenceVC.refreshControl = refreshControl
        } else {
            licenceVC.addSubview(refreshControl)
        }
    }
    
    @IBAction func btnSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
    
   //MARK: - Api call
  
    func getLicenceCall(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")licenses?user_id=\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.getLicenceList = dic["rows"] as! [[String : Any]]
//                    self.licenceList.removeAll()
                    for data in self.getLicenceList{
                        let id = data["id"] as? Int
                        let name = data["name"] as? String
                        let seats = data["seats"] as? Int
                        let free_seats_count = data["free_seats_count"] as? Int
                        let manufacturer = data["manufacturer"] as? [String: Any]
                        let expirationDate = data["expiration_date"] as? [String: Any]
                        self.licenceList.append(LicenceData(id: id ?? 0, name: name ?? "", seats: seats ?? 0, free_seats_count: free_seats_count  ?? 0, manufacturerId:  manufacturer?["id"] as? Int ?? 0, manufacturerName:  manufacturer?["name"] as? String ?? "", expirationDate: expirationDate?["date"] as? String ?? "", expiartionDateFormat: expirationDate?["formatted"] as? String ?? ""))
                        }
                    self.insertData()
                    self.readValues()
                    if self.getOfflineLicenses.count == 0{
                        self.lblMsg.isHidden = false
                    }else{
                        self.lblMsg.isHidden = true
                    }
                    self.licenceVC.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()

    }
    
    func licenceDetail(){
        
    }
    //MARK: -StoreDataInSqlite
    let algorithms = Table("Licences")
    var licName = Expression<String>("name")
    var seats = Expression<Int>("seats")
    var id = Expression<Int>("id")
    var free_seats_count = Expression<Int>("free_seats_count")
    var manufacturerId = Expression<Int>("manufacturerId")
    var manufacturerName = Expression<String>("manufacturerName")
    var expirationDate = Expression<String>("expirationDate")
    var expiartionDateFormat = Expression<String>("expiartionDateFormat")
    
    func createDB(){
        do {
                // Get database path
                if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                    print("jobspath",path)
                    
                    // Connect to database
                    db = try Connection("\(path)/licences.sqlite3")
                    if Reachability.isConnectedToNetwork(){
                        let drop = algorithms.drop(ifExists: true)
                        try db?.run(drop)
                    }
                    // Initialize tables
                    try db?.run(algorithms.create { table in
                        table.column(licName)
                        table.column(seats)
                        table.column(free_seats_count)
                        table.column(manufacturerId)
                        table.column(manufacturerName)
                        table.column(expirationDate)
                        table.column(expiartionDateFormat)
                        table.column(id, primaryKey: true)
                    })
                    
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        func insertData(){
            for dataset in licenceList{
                do{
                    try db?.run(algorithms.insert(id <- dataset.id , licName <- dataset.name , seats <- dataset.seats , free_seats_count <- dataset.free_seats_count  , manufacturerId <- dataset.manufacturerId , manufacturerName <- dataset.manufacturerName , expirationDate <- dataset.expirationDate , expiartionDateFormat <- dataset.expiartionDateFormat))
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
        func readValues(){
            getOfflineLicenses.removeAll()
            do{
                for row in try db!.prepare("SELECT * FROM Licences") {
                    print("row173",row)
                    getOfflineLicenses.append(LicenceOfflineData(id: Int(row[7] as? Int64 ?? 0), name: row[0] as? String ?? "", seats: Int(row[1] as? Int64 ?? 0), free_seats_count: Int(row[2] as? Int64 ?? 0), manufacturerId:  Int(row[3] as? Int64 ?? 0), manufacturerName: row[4] as? String ?? "", expirationDate: row[5] as? String ?? "", expiartionDateFormat:  row[6] as? String ?? ""))
                        licenceVC.reloadData()
                }
            }catch{
                print(error.localizedDescription)
            }
        }
}
extension LicenceVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getOfflineLicenses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LicenceTVC") as! LicenceTVC
        let list = getOfflineLicenses[indexPath.row]
        cell.lbllicName.text = list.name
        cell.lblExpDate.text = list.expiartionDateFormat
        cell.lblManuFTitle.text = list.manufacturerName
        cell.lblManu.text = "Seats - \(list.seats)(Total) - \(list.free_seats_count)(Free)"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
        vc.getOfflineLicenses = getOfflineLicenses
        vc.isfromLicence = true
        vc.licenceId = getOfflineLicenses[indexPath.row].id
        navigationController?.pushViewController(vc, animated: true)
    }
}
struct LicenceData {
    init(id : Int,name: String, seats: Int, free_seats_count: Int, manufacturerId: Int, manufacturerName: String, expirationDate: String, expiartionDateFormat: String) {
        self.name = name
        self.seats = seats
        self.free_seats_count = free_seats_count
        self.manufacturerId = manufacturerId
        self.manufacturerName = manufacturerName
        self.expirationDate = expirationDate
        self.expiartionDateFormat = expiartionDateFormat
        self.id = id
    }
    var name : String
    var seats : Int
    var free_seats_count : Int
    var manufacturerId : Int
    var manufacturerName : String
    var expirationDate : String
    var expiartionDateFormat : String
    var id : Int
}
struct LicenceOfflineData {
    init(id : Int,name: String, seats: Int, free_seats_count: Int, manufacturerId: Int, manufacturerName: String, expirationDate: String, expiartionDateFormat: String) {
        self.name = name
        self.seats = seats
        self.free_seats_count = free_seats_count
        self.manufacturerId = manufacturerId
        self.manufacturerName = manufacturerName
        self.expirationDate = expirationDate
        self.expiartionDateFormat = expiartionDateFormat
        self.id = id
    }
    
    var name : String
    var seats : Int
    var free_seats_count : Int
    var manufacturerId : Int
    var manufacturerName : String
    var expirationDate : String
    var expiartionDateFormat : String
    var id : Int
}

