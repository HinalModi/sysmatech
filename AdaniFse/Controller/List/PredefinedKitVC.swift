//
//  PredefinedKitVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 02/09/22.
//

import UIKit
import LGSideMenuController
import SQLite

class PredefinedKitVC: UIViewController {

    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var kitTV: UITableView!
    var getPredefinedList : [[String : Any]] = []
    var predefinedList : [PredefinedData] = []
    var getOfflinePredefined : [OfflinePredefinedData] = []
    var refreshControl: UIRefreshControl!
    var isfromSearch = Bool()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        kitTV.tableFooterView = UIView()
        getPredefinedVal()
        createDB()
        readValues()
        addPullRequest()
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            getPredefinedVal()
            createDB()
        }
        readValues()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            kitTV.refreshControl = refreshControl
        } else {
            kitTV.addSubview(refreshControl)
        }
    }
    @IBAction func btnShowMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
//MARK: -Api call
    func getPredefinedVal(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")kits?\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.getPredefinedList = dic["rows"] as! [[String : Any]]
                    self.predefinedList.removeAll()
                    for data in self.getPredefinedList{
                        let id = data["id"] as? Int
                        let name = data["name"] as? String
                        self.predefinedList.append(PredefinedData(id: id ?? 0, name: name ?? ""))
                    }
                    self.insertData()
                    self.readValues()
                    if self.getOfflinePredefined.count == 0 {
                        self.lblMsg.isHidden = false
                    }else{
                        self.lblMsg.isHidden = true
                    }
                    self.kitTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
    //MARK: -StoreDataInSqlite
        let algorithms = Table("PredefinedKit")
        var id = Expression<Int>("id")
        var name = Expression<String>("name")
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/predefinedKit.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    let drop = algorithms.drop(ifExists: true)
                    try db?.run(drop)
                }
                // Initialize tables
                try db?.run(algorithms.create { table in
                    table.column(id)
                    table.column(name)
                    
                })
                
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    func insertData(){
        for dataset in predefinedList{
            do{
                try db?.run(algorithms.insert(id <- dataset.id , name <- dataset.name))
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    func readValues(){
        getOfflinePredefined.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM PredefinedKit") {
                getOfflinePredefined.append(OfflinePredefinedData(id: Int(row[0] as? Int64 ?? 0), name: row[1] as? String ?? ""))
                kitTV.reloadData()
            }
        }catch{
            print(error.localizedDescription)
        }
    }
}
extension PredefinedKitVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getOfflinePredefined.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreddefinedKitTVC") as! PreddefinedKitTVC
        cell.lblName.text = getOfflinePredefined[indexPath.row].name
        cell.checkOut = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            vc.isfromPredefined = true
            vc.kitId = self.getOfflinePredefined[indexPath.row].id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "CategoryModal", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AssetModalVC") as! AssetModalVC
        vc.kitId = getOfflinePredefined[indexPath.row].id
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

struct PredefinedData {
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    var id : Int
    var name : String
}
struct OfflinePredefinedData {
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    var id : Int
    var name : String
}
