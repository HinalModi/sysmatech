//
//  ConsumableVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 31/08/22.
//

import UIKit
import LGSideMenuController
import SQLite

class ConsumableVC: UIViewController {
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var consumableTV: UITableView!
    var getConsumableList : [[String : Any]] = []
    var consumableList : [ConsumableData] = []
    var getOfflineConsumable : [ConsumableOfflineData] = []
    var refreshControl: UIRefreshControl!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        consumableTV.tableFooterView = UIView()
        getConsumables()
        createDB()
        readValues()
        addPullRequest()
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            getConsumables()
            createDB()
        }
        readValues()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            consumableTV.refreshControl = refreshControl
        } else {
            consumableTV.addSubview(refreshControl)
        }
    }
    @IBAction func btnSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
//MARK: - Api call
   
    func getConsumables(){
//        activityIndicator.startAnimating()
        if incrval == 0{
            activityIndicator.startAnimating()
        }else{
            activityIndicator.stopAnimating()
        }
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")consumables?user_id=\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.getConsumableList = dic["rows"] as! [[String : Any]]
                    self.consumableList.removeAll()
                    for data in self.getConsumableList{
                        let name = data["name"] as? String
                        let id = data["id"] as? Int
                        let imageAccess = data["image"] as? String
                        let OrderNo = data["order_number"] as? String
                        let min_amt = data["min_amt"] as? Int
                        let remaining = data["remaining"] as? Int
                        let location = data["location"] as? [String: Any]
                        let category = data["category"] as? [String: Any]
                        let supplier = data["supplier"] as? [String: Any]
                        let created_at = data["created_at"] as? [String: Any]
                        let company = data["company"] as? [String: Any]
                        let model_number = data["model_number"] as? String
                        self.consumableList.append(ConsumableData(id: id ?? 0, name: name ?? "", imageAccess: imageAccess ?? "", min_amt:  min_amt ?? 0, remaining: remaining ?? 0, locId: location?["id"] as? Int ?? 0, locationname: location?["name"] as? String ?? "", categoryId: category?["id"] as? Int ?? 0, categoryName: category?["name"] as? String ?? "", orderNo: OrderNo ?? "" ,date : created_at?["formatted"] as? String ?? "", create_at:created_at ?? [String : Any]() ,
                                                                  companyId: company?["id"] as? Int ?? 0 ,companyName: company?["name"] as? String ?? "",
                                                                  company: company ?? [String : Any]() , supplierName: supplier?["name"] as? String ?? "",supplier: supplier  ??
                                                              [String: Any](),supplierId: supplier?["id"] as? Int ?? 0, model_number: model_number ?? "" ))
                        }
                    self.insertData()
                    self.readValues()
                    if self.getOfflineConsumable.count == 0{
                        self.lblMsg.isHidden = false
                    }else{
                        self.lblMsg.isHidden = true
                    }
                    self.consumableTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
    //MARK: -StoreDataInSqlite
    let algorithms = Table("Consumables")
    var conName = Expression<String>("name")
    var imageAccess = Expression<String>("image")
    var min_amt = Expression<Int>("min_amt")
    var remaining = Expression<Int>("remaining")
    var locId = Expression<Int>("locId")
    var locationname = Expression<String>("locationname")
    var categoryId = Expression<Int>("categoryId")
    var categoryName = Expression<String>("categoryName")
    var orderNumber = Expression<String>("orderNumber")
    var id = Expression<Int>("id")
    var supplierId  = Expression<Int>("supplierId")
    var supplierName  = Expression<String>("supplierName")
    var companyName  = Expression<String>("companyName")
    var companyId  = Expression<Int>("companyId")
    var date  = Expression<String>("date")
    var model_number = Expression<String>("model_number")
    func createDB(){
            do {
                // Get database path
                if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                    print("jobspath",path)
                    
                    // Connect to database
                    db = try Connection("\(path)/consumables.sqlite3")
                    if Reachability.isConnectedToNetwork(){
//                        let drop = algorithms.drop(ifExists: true)
//                        try db?.run(drop)
                    }
                    // Initialize tables
                    try db?.run(algorithms.create { table in
                        table.column(id, primaryKey: true)
                        table.column(conName)
                        table.column(imageAccess)
                        table.column(min_amt)
                        table.column(remaining)
                        table.column(locId)
                        table.column(locationname)
                        table.column(categoryId)
                        table.column(categoryName)
                        table.column(orderNumber)
                        table.column(date)
                        table.column(companyId)
                        table.column(companyName)
                        table.column(supplierName)
                        table.column(supplierId)
                        table.column(model_number)
                    })
                    
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        func insertData(){
            for dataset in consumableList{
                do{
                    try db?.run(algorithms.insert(id <- dataset.id , conName <- dataset.name , imageAccess <- dataset.imageAccess , min_amt <- dataset.min_amt , remaining <- dataset.remaining , locId <- dataset.locId , locationname <- dataset.locationname , categoryId <- dataset.categoryId , categoryName <- dataset.categoryName , orderNumber <- dataset.orderNo ,date <- dataset.date ,companyId <- dataset.companyId, companyName <- dataset.companyName,supplierName <- dataset.supplierName ,supplierId <- dataset.supplierId , model_number <- dataset.model_number))
                }catch{
                    print(error.localizedDescription)
                }
            }
        }
        func readValues(){
            getOfflineConsumable.removeAll()
            do{
                for row in try db!.prepare("SELECT * FROM Consumables") {
                    print("row173",row[2])
                    getOfflineConsumable.append(ConsumableOfflineData(id: Int(row[0] as? Int64 ?? 0), name: row[1] as? String ?? "", imageAccess: row[2] as? String ?? "", min_amt: Int(row[3] as? Int64 ?? 0), remaining: Int(row[4] as? Int64 ?? 0),locId: row[5] as? Int ?? 0, locationname:  row[6] as? String ?? "" , categoryId: row[7] as? Int ?? 0, categoryName:  row[8] as? String ?? "", orderNo: row[9] as? String ?? "", date : row[10] as? String ?? "", companyId: row[11] as? Int ?? 0, companyName: row[12] as? String ?? "", supplierName:row[13] as? String ?? "", supplierId: row[14] as? Int ?? 0, model_number: row[15] as? String ?? ""))
                        consumableTV.reloadData()
                }
            }catch{
                print(error.localizedDescription)
            }
        }
        
}
extension ConsumableVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getOfflineConsumable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConsumablesTVC") as! ConsumablesTVC
        let list = getOfflineConsumable[indexPath.row]
        cell.lblName.text = list.name
        if list.locationname == ""{
            cell.lblLoc.text = "N/A"
        }else{
            cell.lblLoc.text = list.locationname
        }
        if list.categoryName == "" {
            cell.lblCatTitle.text = "Consumable"
        }else{
            cell.lblCatTitle.text = list.categoryName
        }
        cell.lblCat.text = "OrdNo. - \(list.orderNo)"
        
        cell.lblMinQtyTitle.text = "Qty - \(list.remaining)(Avail) - \(list.min_amt)(Min)"
        let urlImg = URL(string: list.imageAccess)
        cell.imgConsumable.sd_setImage(with: urlImg, placeholderImage: UIImage(named: "setting2"))
        cell.checkout = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            vc.consumeId = list.id
            vc.isFromConsumable = true
            if list.categoryName == "" {
                vc.categoryName = "Consumable"
            }else{
                vc.categoryName = list.categoryName
            }
            vc.accessName = list.name
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.restock = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConsumableRestockVC") as! ConsumableRestockVC
            vc.consumableId = list.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let list = getOfflineConsumable[indexPath.row]
//        let vc = storyboard?.instantiateViewController(withIdentifier: "AccessoryDetailVC") as! AccessoryDetailVC
//        vc.consumableId = list.id
//        vc.consumableName = list.name
//        vc.isFromConsumable = true
//        navigationController?.pushViewController(vc, animated: true)
        if getOfflineConsumable.count > 0{
            let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
            let list = getOfflineConsumable[indexPath.row]
            vc.consumableId = list.id
            vc.consumableName = list.name
            vc.location = list.locationname
            vc.dueDate = list.date
            vc.supplierId = list.supplierId
            vc.supplier = list.supplierName
            vc.companyName = list.companyName
            vc.category = list.categoryName
            vc.modalnNo = list.model_number
            vc.auditImageUrl = URL(string: baseUrlImage + list.imageAccess)
            vc.isFromConsumable = true
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
struct ConsumableData {
    init(id : Int,name: String, imageAccess: String, min_amt: Int, remaining: Int,locId: Int, locationname: String , categoryId : Int ,categoryName : String ,orderNo : String ,date : String ,
         create_at : [String: Any] ,companyId : Int , companyName : String , company : [String: Any] ,
         supplierName : String , supplier : [String: Any] ,supplierId : Int ,model_number : String) {
        self.name = name
        self.imageAccess = imageAccess
        self.min_amt = min_amt
        self.remaining = remaining
        self.locId = locId
        self.locationname = locationname
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.orderNo = orderNo
        self.id = id
        self.supplier = supplier
        self.supplierId = supplierId
        self.supplierName = supplierName
        self.company = company
        self.companyId = companyId
        self.companyName = companyName
        self.create_at = create_at
        self.date = date
        self.model_number = model_number
    }
    var name : String
    var imageAccess: String
    var min_amt: Int
    var remaining : Int
    var locId : Int
    var locationname : String
    var categoryId : Int
    var categoryName : String
    var orderNo : String
    var id : Int
    var supplierId : Int
    var supplier : [String: Any]
    var supplierName : String
    var company : [String: Any]
    var companyName : String
    var companyId : Int
    var create_at : [String: Any]
    var date : String
    var model_number : String
}
struct ConsumableOfflineData {
    init(id : Int,name: String, imageAccess: String, min_amt: Int, remaining: Int,locId: Int, locationname: String , categoryId : Int ,categoryName : String ,orderNo : String , date : String ,
         companyId : Int , companyName : String , supplierName : String ,supplierId : Int,model_number : String) {
        self.name = name
        self.imageAccess = imageAccess
        self.min_amt = min_amt
        self.remaining = remaining
        self.locId = locId
        self.locationname = locationname
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.orderNo = orderNo
        self.id = id
        self.supplierId = supplierId
        self.supplierName = supplierName
        self.companyId = companyId
        self.companyName = companyName
        self.date = date
        self.model_number = model_number
    }
    var name : String
    var imageAccess: String
    var min_amt: Int
    var remaining : Int
    var locId : Int
    var locationname : String
    var categoryId : Int
    var categoryName : String
    var orderNo : String
    var id : Int
    var supplierId : Int
    var supplierName : String
    var companyName : String
    var companyId : Int
    var date : String
    var model_number : String
}
