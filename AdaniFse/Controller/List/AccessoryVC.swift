//
//  AccessoryVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 30/08/22.
//

import UIKit
import Alamofire
import SQLite
import LGSideMenuController

class AccessoryVC: UIViewController {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var accessoryTV: UITableView!
    var getAccessoryList : [[String : Any]] = []
    var accessoryList : [AccesssoryData] = []
    var getOfflineAccess : [AccesssoryOfflineData] = []
    var refreshControl: UIRefreshControl!
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAcc = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        getAccessoryData()
        createDB()
        readValues()
        addPullRequest()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        accessoryTV.tableFooterView = UIView()
       
        
    }
    @objc func refresh(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            do{
                let drop = algorithms.drop(ifExists: true)
                try db?.run(drop)
                activityIndicator.startAnimating()
            }catch{
                print(error.localizedDescription)
            }
            getAccessoryData()
            createDB()
        }
        readValues()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            accessoryTV.refreshControl = refreshControl
        } else {
            accessoryTV.addSubview(refreshControl)
        }
    }
    
    @IBAction func btnSideMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
//MARK: -StoreDataInSqlite
    let algorithms = Table("Accessories")
    var id = Expression<Int>("id")
    var accName = Expression<String>("name")
    var imageAccess = Expression<String>("image")
    var min_qty = Expression<Int>("min_qty")
    var qty = Expression<Int>("qty")
    var locId = Expression<Int>("locId")
    var locationname = Expression<String>("locationname")
    var categoryId = Expression<Int>("categoryId")
    var categoryName = Expression<String>("categoryName")
    var manufacturerId = Expression<Int>("manufacturerId")
    var manufacturerName = Expression<String>("manufacturerName")
    var supplierId  = Expression<Int>("supplierId")
    var supplierName  = Expression<String>("supplierName")
    var companyName  = Expression<String>("companyName")
    var companyId  = Expression<Int>("companyId")
    var date  = Expression<String>("date")
    var model_number = Expression<String>("model_number")
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/accessories.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    let drop = algorithms.drop(ifExists: true)
                    try db?.run(drop)
                    activityIndicator.stopAnimating()
                }
                // Initialize tables
                try db?.run(algorithms.create { table in
                    table.column(accName)
                    table.column(imageAccess)
                    table.column(min_qty)
                    table.column(qty)
                    table.column(locId)
                    table.column(locationname)
                    table.column(categoryId)
                    table.column(categoryName)
                    table.column(manufacturerId)
                    table.column(manufacturerName)
                    table.column(id , primaryKey: true)
                    table.column(date)
                    table.column(companyId)
                    table.column(companyName)
                    table.column(supplierName)
                    table.column(supplierId)
                    table.column(model_number)
                })
                
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    func insertData(){
        for dataset in accessoryList{
            do{
                try db?.run(algorithms.insert(accName <- dataset.name , imageAccess <- dataset.imageAccess ,
                min_qty <- dataset.min_qty , qty <- dataset.qty , locId <- dataset.locId ,
                locationname <- dataset.locationname , categoryId <- dataset.categoryId ,
                categoryName <- dataset.categoryName , manufacturerId <- dataset.manufacturerId ,
                manufacturerName <- dataset.manufacturerName ,id <- dataset.id ,date <- dataset.date ,companyId <- dataset.companyId, companyName <- dataset.companyName,supplierName <- dataset.supplierName ,supplierId <- dataset.supplierId , model_number <- dataset.model_number))
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    func readValues(){
        getOfflineAccess.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM Accessories") {
                getOfflineAccess.append(AccesssoryOfflineData(id: Int(row[10] as? Int64 ?? 0), name: row[0] as? String ?? "", imageAccess: row[1] as? String ?? "", min_qty: Int(row[2] as? Int64 ?? 0), qty:  Int(row[3] as? Int64 ?? 0) ,locId: row[4] as? Int ?? 0, locationname:  row[5] as? String ?? "" , categoryId: row[6] as? Int ?? 0, categoryName:  row[7] as? String ?? "" ,manufacturerId: row[8] as? Int ?? 0, manufacturerName : row[9] as? String ?? "",date : row[11] as? String ?? "", companyId: row[12] as? Int ?? 0, companyName: row[13] as? String ?? "", supplierName:row[14] as? String ?? "", supplierId: row[15] as? Int ?? 0, model_number:row[16] as? String ?? "" ))
                    accessoryTV.reloadData()
            }
            
        }catch{
            print(error.localizedDescription)
        }
        
    }
    func readSearchValues(){
        getOfflineAccess.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM Accessories") {
                getOfflineAccess.append(AccesssoryOfflineData(id: Int(row[10] as? Int64 ?? 0), name: row[0] as? String ?? "", imageAccess: row[1] as? String ?? "", min_qty: Int(row[2] as? Int64 ?? 0), qty:  Int(row[3] as? Int64 ?? 0) ,locId: row[4] as? Int ?? 0, locationname:  row[5] as? String ?? "" , categoryId: row[6] as? Int ?? 0, categoryName:  row[7] as? String ?? "" ,manufacturerId: row[8] as? Int ?? 0, manufacturerName : row[9] as? String ?? "" ,date : row[11] as? String ?? "", companyId: row[12] as? Int ?? 0, companyName: row[13] as? String ?? "", supplierName:row[14] as? String ?? "", supplierId: row[15] as? Int ?? 0, model_number: row[16] as? String ?? ""))
//                    accessoryTV.reloadData()
            }
            
        }catch{
            print(error.localizedDescription)
        }
        
    }
    
//MARK: - Api Call
    
    func getAccessoryData(){

        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")accessories?user_id=\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        print("url170",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAcc)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {                    
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    
                    self.getAccessoryList = dic["rows"] as? [[String : Any]] ?? [[String : Any]]()
                    self.accessoryList.removeAll()
                    for data in self.getAccessoryList{
                        let id = data["id"] as? Int
                        let name = data["name"] as? String
                        let imageAccess = data["image"] as? String
                        let manufac = data["manufacturer"] as? [String: Any]
                        let min_qty = data["min_qty"] as? Int
                        let qty = data["qty"] as? Int
                        let location = data["location"] as? [String: Any]
                        let category = data["category"] as? [String: Any]
                        let supplier = data["supplier"] as? [String: Any]
                        let created_at = data["created_at"] as? [String: Any]
                        let company = data["company"] as? [String: Any]
                        let model_number = data["model_number"] as? String
                        self.accessoryList.append(AccesssoryData(id: id ?? 0, name: name ?? "", imageAccess: imageAccess ?? "",
                            manufac: manufac ?? [String: Any](), min_qty: min_qty ?? 0, qty: qty ?? 0,
                            location: location ?? [String: Any](),locId:  location?["id"] as? Int ?? 0,
                            locationname: location?["name"] as? String ?? "" , categoryId: category?["id"] as? Int ?? 0,
                            categoryName: category?["name"] as? String ?? "" ,manufacturerId: manufac?["id"] as? Int ?? 0,
                            manufacturerName : manufac?["name"] as? String ?? "" ,
                            date : created_at?["formatted"] as? String ?? "", create_at:created_at ?? [String : Any]() ,
                            companyId: company?["id"] as? Int ?? 0 ,companyName: company?["name"] as? String ?? "",
                            company: company ?? [String : Any]() , supplierName: supplier?["name"] as? String ?? "",supplier: supplier  ??
                        [String: Any](),supplierId: supplier?["id"] as? Int ?? 0, model_number: model_number ?? "" ))
                        }
                    self.activityIndicator.stopAnimating()
                    self.insertData()
                    self.readValues()
                    incrval += 1
                    if self.getOfflineAccess.count == 0{
                        self.lblMsg.isHidden = false
                    }else{
                        self.lblMsg.isHidden = true
                    }
                    self.accessoryTV.reloadData()
                    
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                }
            }
            }
        }.resume()
    }
    

}
extension AccessoryVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getOfflineAccess.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoryTVC") as! AccessoryTVC
        if getOfflineAccess.count > 0{
            let list = getOfflineAccess[indexPath.row]
            cell.lblName.text = list.name
            if list.locationname == ""{
                cell.lblLoc.text = "N/A"
            }else{
                cell.lblLoc.text = list.locationname
            }
            if list.categoryName == "" {
                cell.lblCat.text = "Accessory" + " " + list.manufacturerName
            }else{
                cell.lblCat.text = list.categoryName + " " + list.manufacturerName
            }
            
            cell.lblMinQtyTitle.text = "Qty - \(list.qty)(Avail) - \(list.min_qty)(Min)"
            let urlImg = URL(string: list.imageAccess)
            cell.imgAccessory.sd_setImage(with: urlImg, placeholderImage: UIImage(named: "setting2"))
            cell.checkOut = {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
                vc.accessoryId = list.id
                if list.categoryName == "" {
                    vc.categoryName = "Accessory" + " " + list.manufacturerName
                }else{
                    vc.categoryName = list.categoryName + " " + list.manufacturerName
                }
                vc.accessName = list.name
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if getOfflineAccess.count > 0{
//            let list = getOfflineAccess[indexPath.row]
//            let vc = storyboard?.instantiateViewController(withIdentifier: "AccessoryDetailVC") as! AccessoryDetailVC
//            vc.accessoryId = list.id
//            vc.accessoryName = list.name
//            vc.isFromAccessory = true
//            navigationController?.pushViewController(vc, animated: true)
//        }
        if getOfflineAccess.count > 0{
            let storyBoard = UIStoryboard(name: "Jobs", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssetDetailVC") as! AssetDetailVC
            let list = getOfflineAccess[indexPath.row]
            vc.accessoryId = Int64(list.id)
            vc.accessoryName = list.name
            vc.location = list.locationname
            vc.dueDate = list.date
            vc.supplier = list.supplierName
            vc.companyName = list.companyName
            vc.isFromAccessory = true
            vc.category = list.categoryName
            vc.modalnNo = list.model_number
            vc.auditImageUrl = URL(string: baseUrlImage + list.imageAccess)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
struct AccesssoryData {
    init(id : Int ,name: String, imageAccess: String, manufac: [String : Any], min_qty: Int,
         qty: Int, location: [String : Any],locId: Int, locationname: String , categoryId : Int
         ,categoryName : String ,manufacturerId : Int , manufacturerName : String ,date : String ,
         create_at : [String: Any] ,companyId : Int , companyName : String , company : [String: Any] ,
         supplierName : String , supplier : [String: Any] ,supplierId : Int ,model_number : String) {
        self.name = name
        self.imageAccess = imageAccess
        self.manufac = manufac
        self.min_qty = min_qty
        self.qty = qty
        self.location = location
        self.locId = locId
        self.locationname = locationname
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.manufacturerId = manufacturerId
        self.manufacturerName = manufacturerName
        self.id = id
        self.supplier = supplier
        self.supplierId = supplierId
        self.supplierName = supplierName
        self.company = company
        self.companyId = companyId
        self.companyName = companyName
        self.create_at = create_at
        self.date = date
        self.model_number = model_number
    }
    var id : Int
    var name : String
    var imageAccess: String
    var manufac: [String: Any]
    var min_qty: Int
    var qty : Int
    var location: [String: Any]
    var locId : Int
    var locationname : String
    var categoryId : Int
    var categoryName : String
    var manufacturerId : Int
    var manufacturerName : String
    var supplierId : Int
    var supplier : [String: Any]
    var supplierName : String
    var company : [String: Any]
    var companyName : String
    var companyId : Int
    var create_at : [String: Any]
    var date : String
    var model_number : String
}
struct AccesssoryOfflineData {
    init(id : Int ,name: String, imageAccess: String, min_qty: Int, qty: Int,locId: Int, locationname: String , categoryId : Int ,categoryName : String ,manufacturerId : Int , manufacturerName : String ,date : String ,
         companyId : Int , companyName : String , supplierName : String ,supplierId : Int,model_number : String) {
        self.name = name
        self.imageAccess = imageAccess
        self.min_qty = min_qty
        self.qty = qty
        self.locId = locId
        self.locationname = locationname
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.manufacturerId = manufacturerId
        self.manufacturerName = manufacturerName
        self.id = id
        self.supplierId = supplierId
        self.supplierName = supplierName
        self.companyId = companyId
        self.companyName = companyName
        self.date = date
        self.model_number = model_number
    }
    var id : Int
    var name : String
    var imageAccess: String
    var min_qty: Int
    var qty : Int
    var locId : Int
    var locationname : String
    var categoryId : Int
    var categoryName : String
    var manufacturerId : Int
    var manufacturerName : String
    var supplierId : Int
    var supplierName : String
    var companyName : String
    var companyId : Int
    var date : String
    var model_number : String
}
