//
//  LoginVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 13/05/22.
//

import UIKit
import CountryPickerView
import FirebaseAuth
import OTPFieldView
class LoginVC: UIViewController ,CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnGenerateOtp: UIButton!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    
    let countryPickerView = CountryPickerView()
    var currentVerificationId = ""
    var phone_Code = ""
    var loginData: PhoneNumberLogin!
    var dicProfileDetail = [String : AnyObject]()
    var userId = String()
    var userAction : UserAvailableAction!
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.view.backgroundColor = Constant.Color.neon_light
        } else {
            self.view.backgroundColor = .white
        }
        if #available(iOS 11.0, *) {
            activityIndicator.color = .black
        } else {
            activityIndicator.color = .blue
        }
        activityIndicator.stopAnimating()
        txtPhone.setLeftPaddingPoints(100)
        prepareCountryPicker()
        if #available(iOS 11.0, *) {
            btnGenerateOtp.backgroundColor = .black
        }else{
            btnGenerateOtp.backgroundColor = .black
        }
        btnGenerateOtp.tintColor = .white
        btnGenerateOtp.setCorner(_tp: 5)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOtpView"{
            let verificationVC = segue.destination as! VerifyPinVC
            verificationVC.loginData = loginData
            verificationVC.dicUserDetail = dicProfileDetail
        }
    }
    
    //9879830389
    //MARK: -Firebase Auth login
    func loginThroughPhone(){
        Auth.auth().languageCode = "en"
        DispatchQueue.main.async {
            let phoneNumber = "\(self.phone_Code)\(self.txtPhone.text ?? "")"
            print(phoneNumber)
        }
        // Step 4: Request SMS
        let phonenumber = loginData.formatedPhoneNumber
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) {(verificationid, error) in
            
            if error == nil{
                guard let verify = verificationid else {return}
                self.loginData.firebaseVerificationID = verify
                UserDefaults.standard.set(self.loginData.firebaseVerificationID, forKey: "authVID")
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.loginData.firebaseVerificationID, verificationCode: self.loginData.otpCode)
                
                Auth.auth().signIn(with: credential) { (user, error) in
                    self.performSegue(withIdentifier: "toOtpView", sender: self)
                    self.activityIndicator.stopAnimating()
                }
            }else{
                print("unable to get verification:", error?.localizedDescription as Any)
                GlobalFunction.showAlert(message: error?.localizedDescription ?? "Something went wrong please try again later", actionTitle: "Ok", viewcontroller: self)
                self.activityIndicator.stopAnimating()
                
            }
        }
    }
    
    //MARK: - ApiCall
    func loginApi(){
        activityIndicator.startAnimating()
        let parameters : Parameters = [
            .mobile : txtPhone.text ?? "",
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .login, baseURL: .baseUAT)
        URLSession.shared.userLogin(with: request) { success, msg , userdata in
            DispatchQueue.main.async { [self] in
                if success{
                    print(msg)
                    userAction = userdata?.availableActions
                    userAction.map { data in
                        print(data.changeParameterAudio)
                        UserDefaults.standard.set(data.changeParameterAudio, forKey: "change_parameter_audio")
                    }
                    self.loginThroughPhone()
                }else{
                    print(msg)
                    if msg == ""{
                        self.view.makeToast("Something went wrong , Please try again later!")
                    }else{
                        self.view.makeToast(msg)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    //MARK: - Action
    @IBAction func toDashBoard(_ sender: UIButton) {
        
        if txtPhone.text?.count ?? 0 < 10{
            GlobalFunction.showAlert( message: "Please enter valid Phone Number", actionTitle: "Ok", viewcontroller: self)
        }
        if let phoneNumber = txtPhone.text, !phoneNumber.isEmpty, phoneNumber.count > 7  {
            let selectedCountry = countryPickerView.selectedCountry
            loginData = PhoneNumberLogin(phoneCode: selectedCountry.phoneCode, phoneNumber: phoneNumber, selectedCountry: selectedCountry)
            loginApi()
        } else{
            GlobalFunction.showAlert( message: "Please enter valid Phone Number", actionTitle: "Ok", viewcontroller: self)
        }
        
        
    }
    @IBAction func showCountryPicker(_ sender: UIButton) {
        showCountryPicker()
    }
    func prepareCountryPicker() {
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        getDefaultCountryInfo()
    }
    
    //Delegates
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        updateCountryCode(withSelected: country)
    }
    
    //DataSource
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func setCountryCode(with flag: UIImage, code: String, phoneCode: String) {
        lblCountryCode.text =  phoneCode
        phone_Code = phoneCode
    }
    
    func getDefaultCountryInfo() {
        //get current country
        _ = Locale.current
        //        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String
        let code = "IN"
        if let defaultCountry = countryPickerView.getCountryByCode(code) {
            countryPickerView.setCountryByCode(code)
            updateCountryCode(withSelected: defaultCountry)
        }
    }
    
    func updateCountryCode(withSelected country: Country) {
        print(country.code)
        setCountryCode(with: country.flag, code: country.code, phoneCode: country.phoneCode)
    }
    
    func showCountryPicker() {
        countryPickerView.showCountriesList(from: self)
    }
}
extension LoginVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)
        return newString.count <= maxLength
    }
}
class PhoneNumberLogin {
    //+973 3719 9887
    var phoneCode: String = ""
    var phoneNumber: String = ""
    var selectedCountry: Country? = nil
    var otpCode: String = ""
    var firebaseVerificationID: String = ""
    
    var formatedPhoneNumber: String {
        return (phoneCode) + phoneNumber
    }
    
    init(phoneCode: String, phoneNumber: String, selectedCountry: Country) {
        self.phoneCode = phoneCode
        self.phoneNumber = phoneNumber
        self.selectedCountry = selectedCountry
    }
    
}
