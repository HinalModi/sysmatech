//
//  VerifyPinVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 13/05/22.
//

import UIKit
import OTPFieldView
import FirebaseAuth
class VerifyPinVC: UIViewController  {
    
    
    @IBOutlet weak var btnTimer: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnotpReSend: UIButton!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var otpView: OTPFieldView!
    var loginData: PhoneNumberLogin!
    var dicUserDetail = [String : AnyObject]()
    var otpVal = ""
    var totalTime = 0
    var remainingTime = ""
    var countdownTimer: Timer!
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.view.backgroundColor = Constant.Color.neon_light
            otpView.backgroundColor = Constant.Color.neon_light
        } else {
            self.view.backgroundColor = .white
        }
        if #available(iOS 11.0, *) {
            activityIndicator.color =  .black
        } else {
            activityIndicator.color = .blue
        }
        activityIndicator.stopAnimating()
        setupOtpView()
        if #available(iOS 11.0, *) {
            btnotpReSend.tintColor = Constant.Color.color_red
            lblTimer.textColor = .white
        }
        if #available(iOS 11.0, *) {
            btnLogin.backgroundColor = .black
        }else{
            btnLogin.backgroundColor = .black
        }
        btnLogin.tintColor = .white
        btnLogin.setCorner(_tp: 5)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startOTPTimer()
    }
    func setupOtpView(){
        self.otpView.fieldsCount = 6
        self.otpView.fieldBorderWidth = 1
        self.otpView.defaultBorderColor = UIColor.darkGray
        self.otpView.filledBorderColor = UIColor.black
        self.otpView.cursorColor = UIColor.black
        self.otpView.displayType = .underlinedBottom
        self.otpView.fieldSize = 40
        self.otpView.separatorSpace = 15
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.initializeUI()
    }
        func getCredentials() -> PhoneAuthCredential? {
    //        if let otp = pinCodeTextField.text, !otp.isEmpty {
    
            if otpVal != "" {
                loginData.otpCode = otpVal
    //            self.showAlert(message: "OTP : \(otp)", title: "Alert")
                return PhoneAuthProvider.provider().credential(withVerificationID: loginData.firebaseVerificationID, verificationCode: loginData.otpCode)
            } else {
                return nil
            }
        }
    
        func authenticateUser() {
            activityIndicator.startAnimating()
            guard let credential = getCredentials() else {
                print("Enter correct OTP")
//                self.showAlert(message: "Enter correct OTP", title: "")
                GlobalFunction.showAlert(message: "Enter correct OTP", actionTitle: "Ok", viewcontroller: self)
                return
            }
            Auth.auth().signIn(with: credential) { [self] authResult, error in
                if let error = error {
                    print("unable to login:", error.localizedDescription as Any)
                    GlobalFunction.showAlert(message: "Please check otp!", actionTitle: "Ok", viewcontroller: self)
                    activityIndicator.stopAnimating()
                } else {
                    if (authResult != nil){
                        activityIndicator.stopAnimating()
                        userInfo = UserInfoManager.getUserInfoModel()
//                        performSegue(withIdentifier: "toDashboard", sender: self)
                        let vc = storyboard?.instantiateViewController(withIdentifier: "SelectCompanyVC") as! SelectCompanyVC
                        vc.phoneNo = loginData.phoneNumber
                        navigationController?.pushViewController(vc, animated: true)
                    }else{
                        GlobalFunction.showAlert(message: "Something went wrong!", actionTitle: "Ok", viewcontroller: self)
                        activityIndicator.stopAnimating()
                    }
                    
                }
            }
        }
    func resendOTP() {
        let phonenumber = loginData.formatedPhoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) {(verificationid, error) in
            if error == nil{
                guard let verify = verificationid else {return}
                self.loginData.firebaseVerificationID = verify
                
            }else{
                print("unable to get verification:", error?.localizedDescription as Any)
            }
        }
    }
    //MARK: - Action
    @IBAction func btnToDashBoard(_ sender: UIButton) {
        authenticateUser()
//        performSegue(withIdentifier: "toDashboard", sender: self)
    }
    
    @IBAction func btnResentOTP(_ sender: UIButton) {
        resendOTP()
    }
    
    func startOTPTimer() {
//        loginData.otpCode = ""
        remainingTime = ""
//        self.showAlert(message: "OTP : 123456", title: "Alert")
        startTimer()
    }
    
    func startTimer() {
        totalTime = 120
        btnTimer.isHidden = true
        lblMsg.isHidden = true
        lblTimer.isHidden = !btnTimer.isHidden
        print(countdownTimer)
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        remainingTime = "\(timeFormatted(totalTime))"
        lblTimer.text = "\(remainingTime)"
        if totalTime != 0 {
            totalTime -= 2
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        btnTimer.isHidden = false
        lblTimer.isHidden = !btnTimer.isHidden
        lblMsg.isHidden = false
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
}
extension VerifyPinVC : OTPFieldViewDelegate{
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        otpVal = otpString
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        //        print("Has entered all OTP? \(hasEntered)")
        return false
    }
}
