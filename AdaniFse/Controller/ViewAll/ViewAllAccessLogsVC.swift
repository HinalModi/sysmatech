//
//  ViewAllAccessLogsVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 06/09/22.
//

import UIKit
import DropDown

class ViewAllAccessLogsVC: UIViewController {
    
    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var logsTV: UITableView!
    var accessLogsList = [AccesssoryLogsData]()
    var getAccessoryLogsList : [[String : Any]] = []
    var viewallLogs : [ViewAllAccessoryLogs] = []
    let dropDown = DropDown()
    var refreshControl: UIRefreshControl!
    var limit = Int()
    var nextOffset = 0
    var labelType = "CheckOut"
    var selectedDate = String()
    var totalData = Int()
    var accessoryId = Int()
    var consumableId = Int()
    var isFromConsumable = Bool()
    var consumableLogs : [[String : Any]] = []
    var restockConsumableLogs : [[String : Any]] = []
    var consumableLogsList : [ConsumableLogsData] = []
    var conRestockLogsList : [ConsumableRestock] = []
    var componentId = Int()
    var isFromComponent = Bool()
    var componentLogs : [[String : Any]] = []
    var componentLogsList : [AccesssoryLogsData] = []
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromConsumable{
            getConsumableLogs()
        }else if isFromComponent{
            getComponentLogs()
        }else{
            getLogsData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblFilterName.text = labelType
        
    }
    
    
//MARK: - Api Call
    func getComponentLogs(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")components/\(componentId)/checkInoutlog?offset=\(nextOffset)&label_type=\(labelType)")
        print(url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.totalData = dic["total"] as! Int
                        self.componentLogs = dic["rows"] as! [[String : Any]]
                        print("getAccessoryLogsList",self.componentLogs.count)
                        self.consumableLogsList.removeAll()
                        for data in self.componentLogs{
                            let checkedout_from = data["checkout_form"]
                            let label_type = data["label"]
                            let checkedin_to = data["checkin"]
                            let date_time = data["date_time"]
                            let qty = data["qty"]
                            let type = data["type"]
                            self.componentLogsList.append(AccesssoryLogsData(checkedout_from: checkedout_from as? String ?? "" ,label_type: label_type as? String ?? "",checkedin_to: checkedin_to as? String ?? "",date_time: date_time as! String, qty:qty as? Int ?? 0 ,type: type as? String ?? ""))
                            
                        }
                        self.logsTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    func getRestockLogs(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")consumables/\(consumableId)/consumablerestocklog?offset=\(nextOffset)&label_type=\(labelType)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        self.totalData = dic["total"] as! Int
                        self.restockConsumableLogs = dic["rows"] as! [[String : Any]]
                        print("restockConsumableLogs",self.restockConsumableLogs)
//                        self.conRestockLogsList.removeAll()
                        for data in self.restockConsumableLogs{
                            let id = data["id"]
                            let consumables_id = data["consumables_id"]
                            let supplier_id = data["supplier_id"]
                            let date_procured = data["date_procured"]
                            let po_number = data["po_number"]
                            let cost_procured = data["cost_procured"]
                            let qty_procured = data["qty_procured"]
                            let receipt_documents = data["receipt_documents"]
                            let created_at = data["created_at"]
                            let deleted_at = data["deleted_at"]
                            let updated_at = data["updated_at"]
                            let suppliername = data["suppliername"]
                            self.conRestockLogsList.append(ConsumableRestock(id: id as? Int ?? 0, consumables_id: consumables_id  as? Int ?? 0, supplier_id: supplier_id as? Int ?? 0 , date_procured: date_procured as? String ?? "", po_number:po_number as? String ?? "", cost_procured: cost_procured as? Int ?? 0, qty_procured: qty_procured as? Int ?? 0, receipt_documents: receipt_documents as? String ?? "", created_at: created_at as? String ?? "", deleted_at:deleted_at as? String ?? "", updated_at:updated_at as? String ?? "", suppliername:suppliername as? String ?? "" ))
                        }
                        self.logsTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    
    func getConsumableLogs(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")consumables/\(consumableId)/checkoutconsumablelog?offset=\(nextOffset)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.totalData = dic["total"] as! Int
                        self.consumableLogs = dic["rows"] as! [[String : Any]]
                        print("getAccessoryLogsList",self.consumableLogs.count)
                        for data in self.consumableLogs{
                            let checkedout_from = data["checkout_form"]
                            let label_type = data["label"]
                            let checkedin_to = data["checkin"]
                            let qty = data["qty"]
                            let type = data["type"]
                            self.consumableLogsList.append(ConsumableLogsData(checkedout_from: checkedout_from as? String ?? "" ,label_type: label_type as? String ?? "",checkedin: checkedin_to as? String ?? "",qty:qty as? Int ?? 0 ,type: type as? String ?? ""))
                            
                        }
                       
                        self.logsTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    func getLogsData(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")accessories/\(accessoryId)/checkinoutaccessorylog?offset=\(nextOffset)&label_type=\(labelType)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.totalData = dic["total"] as! Int
                    self.getAccessoryLogsList = dic["rows"] as! [[String : Any]]
                    print("getAccessoryLogsList",self.getAccessoryLogsList)
//                    self.accessLogsList.removeAll()
                    for data in self.getAccessoryLogsList{
                        let checkedout_from = data["checkout_form"]
                        let label_type = data["label"]
                        let checkedin_to = data["checkin"]
                        let approving_user = data["approving_user"]
                        let date_time = data["date_time"]
                        let qty = data["qty"]
                        let type = data["type"]
                        self.accessLogsList.append(AccesssoryLogsData(id: 0, accessory_id: 0, checkedout_from: checkedout_from as? String ?? "", label_type: label_type as? String ?? "", checkedin_to: checkedin_to as? String ?? "", approving_user: approving_user as? Int ?? 0, date_time: date_time as? String ?? "", qty: qty as? Int ?? 0, type: type as? String ?? "", created_at: "", updated_at: "", deleted_at:""))
                        
                    }
                    self.logsTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
//MARK: - Action
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFilter(_ sender: UIButton) {
        if isFromConsumable == true {
            self.dropDown.dataSource = ["Restock","CheckOut"]
            self.dropDown.show()
            self.dropDown.anchorView = dropView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                lblFilterName.text = item
                labelType = item
                nextOffset = 0
                accessLogsList.removeAll()
                if lblFilterName.text == "Restock"{
                    getRestockLogs()
                }else{
                    getConsumableLogs()
                }
            }
        }else if isFromComponent{
            self.dropDown.dataSource = ["CheckIn","CheckOut"]
            self.dropDown.show()
            self.dropDown.anchorView = dropView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                lblFilterName.text = item
                labelType = item
                nextOffset = 0
                componentLogsList.removeAll()
                getComponentLogs()
            }
        }else{
            self.dropDown.dataSource = ["CheckIn","CheckOut"]
            self.dropDown.show()
            self.dropDown.anchorView = dropView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                lblFilterName.text = item
                labelType = item
                nextOffset = 0
                accessLogsList.removeAll()
                getLogsData()
            }
        }
    }
}
extension ViewAllAccessLogsVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("accessLogsList.count",accessLogsList.count)
        if isFromConsumable{
            if lblFilterName.text == "Restock"{
                return conRestockLogsList.count
            }else{
                return consumableLogsList.count
            }
        }else if isFromComponent{
            return componentLogsList.count
        }else{
            return accessLogsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewAllLogsTVC") as! ViewAllLogsTVC
        if isFromConsumable{
            if lblFilterName.text == "Restock"{
                if conRestockLogsList.count > 0{
                    let list = conRestockLogsList[indexPath.row]
                    cell.lblQty.text = "\(list.qty_procured)"
                    cell.lblStore.text = list.suppliername
                    cell.lblStoreTitle.text = "Supplier"
                    if list.date_procured != ""{
                        cell.lblDate.text = list.date_procured
                    }
                }
                return cell
            }else{
                if consumableLogsList.count > 0{
                    let list = consumableLogsList[indexPath.row]
                    cell.lblQty.text = "\(list.qty)"
                    cell.lblStore.text = list.checkedin
                    cell.lblStoreTitle.text = list.checkedout_from + "->"
                }
                return cell
            }
        }else if isFromComponent{
            if componentLogsList.count > 0{
                let list = componentLogsList[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                cell.lblStore.text = list.checkedin_to
                cell.lblStoreTitle.text = list.checkedout_from + "->"
                if list.date_time != ""{
                    cell.lblDate.text = list.date_time
                }
            }
            return cell
        }else{
            if accessLogsList.count > 0{
                let list = accessLogsList[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                cell.lblStore.text = list.checkedin_to
                cell.lblStoreTitle.text = list.checkedout_from + "->"
                if list.date_time != ""{
                    cell.lblDate.text = list.date_time
                }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            if #available(iOS 13.0, *) {
                let spinner = UIActivityIndicatorView(style: .medium)
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))               
                if totalData > 0{
                    spinner.startAnimating()
                    logsTV.tableFooterView = spinner
                    logsTV.tableFooterView?.isHidden = false
                }else{
                    logsTV.tableFooterView?.isHidden = true
                    spinner.stopAnimating()
                }
            }
        }
    }
}
extension ViewAllAccessLogsVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offset = logsTV.contentOffset.y
        let maxsize = logsTV.contentSize.height - logsTV.frame.height
        if maxsize - offset <= 2{
            self.nextOffset += 1
            if isFromConsumable{
                if lblFilterName.text == "Restock"{
                    getRestockLogs()
                }else{
                    getConsumableLogs()
                }
            }else if isFromComponent{
                if totalData > 0{
                    getComponentLogs()
                }
            }else{
                if totalData > 0{
                    getLogsData()
                }
            }
            activityIndicator.stopAnimating()
        }else{
            self.nextOffset += 1
        }
    }
}
struct ConsumableRestock{
    internal init(id: Int = Int(), consumables_id: Int = Int(), supplier_id: Int = Int(), date_procured: String = String(), po_number: String = String(), cost_procured: Int = Int(), qty_procured: Int = Int(), receipt_documents: String = String(), created_at: String = String(), deleted_at: String = String(), updated_at: String = String(), suppliername: String = String()) {
        self.id = id
        self.consumables_id = consumables_id
        self.supplier_id = supplier_id
        self.date_procured = date_procured
        self.po_number = po_number
        self.cost_procured = cost_procured
        self.qty_procured = qty_procured
        self.receipt_documents = receipt_documents
        self.created_at = created_at
        self.deleted_at = deleted_at
        self.updated_at = updated_at
        self.suppliername = suppliername
    }
    
    var id = Int()
    var consumables_id = Int()
    var supplier_id = Int()
    var date_procured = String()
    var po_number = String()
    var cost_procured = Int()
    var qty_procured = Int()
    var receipt_documents = String()
    var created_at = String()
    var deleted_at = String()
    var updated_at = String()
    var suppliername = String()
}
