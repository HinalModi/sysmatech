//
//  ViewAllAccessoryList.swift
//  AdaniFse
//
//  Created by Ankit Dave on 05/09/22.
//

import UIKit
import DropDown

class ViewAllAccessoryListVC: UIViewController {

    @IBOutlet weak var lblTitleList: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userTV: UITableView!
    var viewalluser : [ViewAllAccessoryUser] = []
    var getAccessoryUserList : [[String : Any]] = []
    var accessoryUserList : [AccesssoryUserData] = []
    var refreshControl: UIRefreshControl!
    var accessoryId = Int()
    var nextOffset = 0
    var totalData = 0
    var consumableId = Int()
    var getConsumableUserList : [[String : Any]] = []
    var consumableUserList : [ConsumableUser] = []
    var isFromConsumable = false
    var componentId = Int()
    var isFromComponent = Bool()
    var getComponentUserList : [[String : Any]] = []
    var componentUserList : [AccesssoryUserData] = []
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromConsumable{
            getConsumableUser()
            lblTitleList.text = "Consumables List"
        }else if isFromComponent{
            getComponentUser()
            lblTitleList.text = "Component List"
        }else{
            getUserData()
            lblTitleList.text = "Accessory List"
        }
        addPullRequest()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    @objc func refresh(_ sender: Any) {
        getUserData()
        refreshControl.endRefreshing()
    }
    func addPullRequest(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            userTV.refreshControl = refreshControl
        } else {
            userTV.addSubview(refreshControl)
        }
    }
    //MARK: - Api Call
    func getComponentUser(){
        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")components/\(componentId)/users") //limit=50
        print(url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.getComponentUserList = dic["rows"] as? [[String : Any]] ?? [[String : Any]]()
                        for data in self.getComponentUserList{
                            let id = data["id"]
                            let userId = data["user_id"]
                            let assignedType = data["assigned_type"]
                            let accessoryId = data["accessory_id"]
                            let assignedTo = data["assigned_to"]
                            let assignAsset = data["assigned_asset"]
                            let assignedLoc = data["assigned_location"]
                            let qty = data["qty"]
                            let createAt = data["created_at"]
                            let updateAt = data["updated_at"]
                            let note = data["note"]
                            let user = data["user"]
                            let location = data["location"]
                            let assets = data["assets"]
                            self.componentUserList.append(AccesssoryUserData(id: id as? Int ?? 0, userId: userId as? Int ?? 0, assignedType: assignedType as? String ?? "", accessoryId: accessoryId as? Int ?? 0, assignedTo: assignedTo as? String ?? "", assignAsset: assignAsset as? String ?? "", assignedLoc: assignedLoc as? Int ?? 0, qty: qty as? Int ?? 0, createAt: createAt as? String ?? "", updateAt: updateAt as? String ?? "", note: note as? String ?? "",user: user as? String ?? "" ,location: location as? String ?? "" , assets: assets as? String ?? ""))
                        }
                        self.userTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    func getConsumableUser(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")consumables/\(consumableId)/users?offset=\(nextOffset)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.getConsumableUserList = dic["rows"] as! [[String : Any]]
                        for data in self.getConsumableUserList{
                            let id = data["id"]
                            let userId = data["user_id"]
                            let assignedType = data["assigned_type"]
                            let consumableId = data["consumable_id"]
                            let assignedTo = data["assigned_to"]
                            let assignAsset = data["assigned_asset"]
                            let assignedLoc = data["assigned_location"]
                            let qty = data["qty"]
                            let createAt = data["created_at"]
                            let updateAt = data["updated_at"]
                            let note = data["note"]
                            let user = data["user"]
                            let location = data["location"]
                            let assets = data["assets"]
                            self.consumableUserList.append(ConsumableUser(id: id as? Int ?? 0, userId: userId as? Int ?? 0, assignedType: assignedType as? String ?? "", consumableId: consumableId as? Int ?? 0, assignedTo: assignedTo as? String ?? "", assignAsset: assignAsset as? String ?? "", assignedLoc: assignedLoc as? Int ?? 0, qty: qty as? Int ?? 0, createAt: createAt as? String ?? "", updateAt: updateAt as? String ?? "", note: note as? String ?? "",user: user as? String ?? "" ,location: location as? String ?? "" , assets: assets as? String ?? ""))
                        }
                       
                        self.userTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }

    func getUserData(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")accessories/\(accessoryId)/users?offset=\(nextOffset)")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenAll)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print("json",json)
                   print("dic",dic)
//                    self.nextOffset = dic["next_page_id"] as! Int
                    self.totalData = dic["total"] as? Int ?? 0
                    
                    self.getAccessoryUserList = dic["rows"] as! [[String : Any]]
                    for data in self.getAccessoryUserList{
                        let id = data["id"]
                        let userId = data["user_id"]
                        let assignedType = data["assigned_type"]
                        let accessoryId = data["accessory_id"]
                        let assignedTo = data["assigned_to"]
                        let assignAsset = data["assigned_asset"]
                        let assignedLoc = data["assigned_location"]
                        let qty = data["qty"]
                        let createAt = data["created_at"]
                        let updateAt = data["updated_at"]
                        let note = data["note"]
                        let user = data["user"]
                        let location = data["location"]
                        let assets = data["assets"]
                        self.accessoryUserList.append(AccesssoryUserData(id: id as? Int ?? 0, userId: userId as? Int ?? 0, assignedType: assignedType as? String ?? "", accessoryId: accessoryId as? Int ?? 0, assignedTo: assignedTo as? String ?? "", assignAsset: assignAsset as? String ?? "", assignedLoc: assignedLoc as? Int ?? 0, qty: qty as? Int ?? 0, createAt: createAt as? String ?? "", updateAt: updateAt as? String ?? "", note: note as? String ?? "",user: user as? String ?? "" ,location: location as? String ?? "" , assets: assets as? String ?? ""))
                    }
                    self.userTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
    
    
    //MARK: - Action
    
    @IBAction func btnPrev(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension ViewAllAccessoryListVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("accessoryUserList.count",accessoryUserList.count)
        if isFromConsumable{
            return consumableUserList.count
        }else if isFromComponent{
            return componentUserList.count
        }else{
            return accessoryUserList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewAllUserAccTVC") as! ViewAllUserAccTVC
        if isFromConsumable{
            cell.btnCheckIn.setTitle("Restock", for: .normal)
            if consumableUserList.count > 0{
                let list = consumableUserList[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                let newdate = GlobalFunction.convertDateString(dateString: list.createAt, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                cell.lblType.text = list.assignedType
                if list.user != ""{
                    cell.lblCheck.text = list.user
                }else if list.location != ""{
                    cell.lblCheck.text = list.location
                }else if list.assets != ""{
                    cell.lblCheck.text = list.assets
                }
                cell.checkIn = {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConsumableRestockVC") as! ConsumableRestockVC
                    vc.consumableId = list.consumableId
//                    vc.supplierid = list.id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else if isFromComponent{
            
            if componentUserList.count > 0{
                cell.btnCheckIn.setTitle("Check In", for: .normal)
                cell.btnCheckIn.isHidden = false
                let list = componentUserList[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                let newdate = GlobalFunction.convertDateString(dateString: list.createAt, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                cell.lblType.text = list.assignedType
                if list.user != ""{
                    cell.lblCheck.text = list.user
                }else if list.location != ""{
                    cell.lblCheck.text = list.location
                }else if list.assets != ""{
                    cell.lblCheck.text = list.assets
                }
                cell.checkIn = {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
                    vc.componentId = list.id
                    vc.isfromComponent = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }else{
            
            if accessoryUserList.count > 0{
                cell.btnCheckIn.isHidden = false
                let list = accessoryUserList[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                let newdate = GlobalFunction.convertDateString(dateString: list.createAt, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                cell.lblType.text = list.assignedType
                if list.user != ""{
                    cell.lblCheck.text = list.user
                }else if list.location != ""{
                    cell.lblCheck.text = list.location
                }else if list.assets != ""{
                    cell.lblCheck.text = list.assets
                }
                cell.checkIn = {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
                    vc.accessoryId = list.id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            if #available(iOS 13.0, *) {
                let spinner = UIActivityIndicatorView(style: .medium)
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                if totalData > 0{
                    spinner.startAnimating()
                    userTV.tableFooterView = spinner
                    userTV.tableFooterView?.isHidden = false
                }else{
                    userTV.tableFooterView?.isHidden = true
                    spinner.stopAnimating()
                }
            }
        }
    }
    
}

extension ViewAllAccessoryListVC : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offset = userTV.contentOffset.y
        let maxsize = userTV.contentSize.height - userTV.frame.height
        if maxsize - offset <= 2{
            self.nextOffset += 1
            if isFromConsumable{
                if totalData > 0{
                    getConsumableUser() 
                }
            }else{
                if totalData > 0{
                    getUserData()
                }
            }
            activityIndicator.stopAnimating()
        }
    }

}
