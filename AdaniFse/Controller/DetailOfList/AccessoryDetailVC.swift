//
//  AccessoryDetailVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 02/09/22.
//

import UIKit
import LGSideMenuController
import SQLite

class AccessoryDetailVC: UIViewController {

    @IBOutlet weak var lblCheckInOutStockTitle: UILabel!
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblLogsMsg: UILabel!
    @IBOutlet weak var lblUserMsg: UILabel!
    
    @IBOutlet weak var viewAccessLogs: UIView!
    @IBOutlet weak var viewAccessUser: UIView!
    @IBOutlet weak var btnViewAllLogs: UIButton!
    @IBOutlet weak var btnViewAllUser: UIButton!
    @IBOutlet weak var lblAccessName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var detailTableheightCons: NSLayoutConstraint!
    @IBOutlet weak var logsTV: UITableView!
    @IBOutlet weak var detailTV: UITableView!
    var getAccessoryUserList : [[String : Any]] = []
    var getConsumableUserList : [[String : Any]] = []
    var getComponentUserList : [[String : Any]] = []
    var accessoryUserList : [AccesssoryUserData] = []
    var consumableUserList : [ConsumableUser] = []
    var componentUserList : [AccesssoryUserData] = []
    var getOfflineAccessDetail : [AccesssoryUserOfflineData] = []
    var getOfflineConsumableUser : [ConsumableUserOffline] = []
    var getOfflineComponentUser : [AccesssoryUserOfflineData] = []
    var refreshControl: UIRefreshControl!
    var getAccessLogs : [[String : Any]] = []
    var consumableLogs : [[String : Any]] = []
    var componentLogs : [[String : Any]] = []
    var accessoryLogsList : [AccesssoryLogsData] = []
    var componentLogsList : [AccesssoryLogsData] = []
    var consumableLogsList : [ConsumableLogsData] = []
    var getOfflineAccessLogs : [AccessoryLogsOfflineData] = []
    var getOfflineComponentLogs : [AccessoryLogsOfflineData] = []
    var getOfflineConsumableLogs : [ConsumableLogsOfflineData] = []
    var accessoryId = Int()
    var accessoryName = String()
    var consumableName = String()
    var viewalluser : [ViewAllAccessoryUser] = []
    var viewallCoumableuser : [ViewAllConsumableUser] = []
    var viewallLogs : [ViewAllAccessoryLogs] = []
    var viewallCoumableLogs : [ViewAllConsumableLogs] = []
    var viewallComponentUser : [ViewAllAccessoryUser] = []
    var viewallComponentLogs : [ViewAllAccessoryLogs] = []
    var currentId = String()
    var isFromConsumable = Bool()
    var consumableId = Int()
    var componentId = Int()
    var isFromComponent = Bool()
    var componentName = String()
    var isFromAccessory = Bool()
    var isFromSearch = Bool()
    var searchable = [String : Any]()
    var getOfflineAccess : [AccesssoryOfflineData] = []
    var assessVC = AccessoryVC()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckOut = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(searchable)
        if !isFromSearch{
            if isFromComponent{
                getComponentUser()
                getComponentLogs()
                createDBForComponent()
                lblMainTitle.text = "Component Detail"
                lblAccessName.text = componentName + " Component"
                lblCheckInOutStockTitle.text = componentName + " CheckIn and Restock Log"
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFromSearch{
            if isFromAccessory{
                accessoryId = searchable["id"] as? Int ?? 0
                getUserData()
                getLogsData()
                createAccessoryUserDB()
                lblMainTitle.text = "Accessory Detail"
                lblAccessName.text = accessoryName + " Accessory"
                lblCheckInOutStockTitle.text =  accessoryName + " CheckIn and Checkout Log"
            }else if isFromComponent{
                componentId = searchable["id"] as? Int ?? 0
                getComponentUser()
                getComponentLogs()
                createDBForComponent()
                lblMainTitle.text = "Component Detail"
                lblAccessName.text = componentName + " Component"
                lblCheckInOutStockTitle.text = componentName + " CheckIn and Restock Log"
            }else if isFromConsumable{
                consumableId = searchable["id"] as? Int ?? 0
                getConsumableUser()
                getConsumableLogs()
                createDBForConsumable()
                lblMainTitle.text = "Consumable Detail"
                lblAccessName.text = consumableName + " Consumable"
                lblCheckInOutStockTitle.text = consumableName + " CheckIn and Restock Log"
            }
        }else{
            if isFromConsumable{
                getConsumableUser()
                getConsumableLogs()
                createDBForConsumable()
                lblMainTitle.text = "Consumable Detail"
                lblAccessName.text = consumableName + " Consumable"
                lblCheckInOutStockTitle.text = consumableName + " CheckIn and Restock Log"
            }else if isFromAccessory{
                getUserData()
                getLogsData()
                createAccessoryUserDB()
                lblMainTitle.text = "Accessory Detail"
                lblAccessName.text = accessoryName + " Accessory"
                lblCheckInOutStockTitle.text =  accessoryName + " CheckIn and Checkout Log"
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        detailTableheightCons.constant = detailTV.contentSize.height
    }
   
//MARK: - Api Call
    
    
    //MARK: - Component User data
    func getComponentLogs(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")components/\(componentId)/checkInoutlog")
        print(url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.componentLogs = dic["rows"] as? [[String : Any]] ?? [[String : Any]]()
                        print("getAccessoryLogsList",self.componentLogs.count)
                        self.consumableLogsList.removeAll()
                        for data in self.componentLogs{
                            let checkedout_from = data["checkout_form"]
                            let label_type = data["label"]
                            let checkedin_to = data["checkin"]
                            let date_time = data["date_time"]
                            let qty = data["qty"]
                            let type = data["type"]
                            self.componentLogsList.append(AccesssoryLogsData(checkedout_from: checkedout_from as? String ?? "" ,label_type: label_type as? String ?? "",checkedin_to: checkedin_to as? String ?? "",date_time: date_time as! String, qty: qty as? Int ?? 0 ,type: type as? String ?? ""))
                            
                        }
                        self.insertComponentLogs()
                        self.readComponentLogs()
                        self.viewAllComponentlog()
                        self.logsTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    func getComponentUser(){
//        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")components/\(componentId)/users") //limit=50
        print(url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        request.setValue("\(listToken)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.getComponentUserList = dic["rows"] as? [[String : Any]] ?? [[String : Any]]()
                        for data in self.getComponentUserList{
                            let id = data["id"]
                            let userId = data["user_id"]
                            let assignedType = data["assigned_type"]
                            let accessoryId = data["accessory_id"]
                            let assignedTo = data["assigned_to"]
                            let assignAsset = data["assigned_asset"]
                            let assignedLoc = data["assigned_location"]
                            let qty = data["assigned_qty"]
                            let createAt = data["created_at"]
                            let updateAt = data["updated_at"]
                            let note = data["note"]
                            let user = data["user"]
                            let location = data["location"]
                            let assets = data["assets"]
                            print(qty)
                            self.componentUserList.append(AccesssoryUserData(id: id as? Int ?? 0, userId: userId as? Int ?? 0, assignedType: assignedType as? String ?? "", accessoryId: accessoryId as? Int ?? 0, assignedTo: assignedTo as? String ?? "", assignAsset: assignAsset as? String ?? "", assignedLoc: assignedLoc as? Int ?? 0, qty: Int(qty as? Int64 ?? 0), createAt: createAt as? String ?? "", updateAt: updateAt as? String ?? "", note: note as? String ?? "",user: user as? String ?? "" ,location: location as? String ?? "" , assets: assets as? String ?? ""))
                        }
                        self.insertComponentUser()
                        self.readComponentUser()
                        self.viewAllComponentUser()
                        self.detailTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    //MARK: - Consumables User data
    func getConsumableUser(){
//        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")consumables/\(consumableId)/users") //limit=50
        print(url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        request.setValue("\(listToken)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.getConsumableUserList = dic["rows"] as! [[String : Any]]
                        for data in self.getConsumableUserList{
                            let id = data["id"]
                            let userId = data["user_id"]
                            let assignedType = data["assigned_type"]
                            let consumableId = data["consumable_id"]
                            let assignedTo = data["assigned_to"]
                            let assignAsset = data["assigned_asset"]
                            let assignedLoc = data["assigned_location"]
                            let qty = data["qty"]
                            let createAt = data["created_at"]
                            let updateAt = data["updated_at"]
                            let note = data["note"]
                            let user = data["user"]
                            let location = data["location"]
                            let assets = data["assets"]
                            self.consumableUserList.append(ConsumableUser(id: id as? Int ?? 0, userId: userId as? Int ?? 0, assignedType: assignedType as? String ?? "", consumableId: consumableId as? Int ?? 0, assignedTo: assignedTo as? String ?? "", assignAsset: assignAsset as? String ?? "", assignedLoc: assignedLoc as? Int ?? 0, qty: qty as? Int ?? 0, createAt: createAt as? String ?? "", updateAt: updateAt as? String ?? "", note: note as? String ?? "",user: user as? String ?? "" ,location: location as? String ?? "" , assets: assets as? String ?? ""))
                        }
                        self.insertConsumableUser()
                        self.readConsumableUser()
                        self.viewAllConsumableUser()
                        self.detailTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    //MARK: - Consumables Logs data
    
    func getConsumableLogs(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")consumables/\(consumableId)/checkoutconsumablelog") //limit=50
        print(url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        request.setValue("\(listToken)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:[])
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print("dic",dic)
                        self.consumableLogs = dic["rows"] as! [[String : Any]]
                        print("getAccessoryLogsList",self.consumableLogs.count)
                        self.consumableLogsList.removeAll()
                        for data in self.consumableLogs{
                            let checkedout_from = data["checkout_form"]
                            let label_type = data["label"]
                            let checkedin_to = data["checkin"]
                            let qty = data["qty"]
                            let type = data["type"]
                            self.consumableLogsList.append(ConsumableLogsData(checkedout_from: checkedout_from as? String ?? "" ,label_type: label_type as? String ?? "",checkedin: checkedin_to as? String ?? "",qty:qty as? Int ?? 0 ,type: type as? String ?? ""))
                            
                        }
                        self.insertConsumableLogs()
                        self.readConsumableLogs()
                        self.viewAllConsumableLogs()
                        self.logsTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
    }
    
    //MARK: - AccessoryUser data
    func getUserData(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")accessories/\(accessoryId)/users") //limit=50
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        request.setValue("\(listToken)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                  
                    self.getAccessoryUserList = dic["rows"] as! [[String : Any]]
                    for data in self.getAccessoryUserList{
                        let id = data["id"]
                        let userId = data["user_id"]
                        let assignedType = data["assigned_type"]
                        let accessoryId = data["accessory_id"]
                        let assignedTo = data["assigned_to"]
                        let assignAsset = data["assigned_asset"]
                        let assignedLoc = data["assigned_location"]
                        let qty = data["qty"]
                        let createAt = data["created_at"]
                        let updateAt = data["updated_at"]
                        let note = data["note"]
                        let user = data["user"]
                        let location = data["location"]
                        let assets = data["assets"]
                        self.accessoryUserList.append(AccesssoryUserData(id: id as? Int ?? 0, userId: userId as? Int ?? 0, assignedType: assignedType as? String ?? "", accessoryId: accessoryId as? Int ?? 0, assignedTo: assignedTo as? String ?? "", assignAsset: assignAsset as? String ?? "", assignedLoc: assignedLoc as? Int ?? 0, qty: qty as? Int ?? 0, createAt: createAt as? String ?? "", updateAt: updateAt as? String ?? "", note: note as? String ?? "",user: user as? String ?? "" ,location: location as? String ?? "" , assets: assets as? String ?? ""))
                    }
                    self.insertAccessoryUser()
                    self.readAccessoryUser()
                    self.viewAllAccessoryUser()
                    self.detailTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }
    //MARK: - Accessory Logs data
    func getLogsData(){
        activityIndicator.startAnimating()
        
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")accessories/\(accessoryId)/checkinoutaccessorylog")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        request.setValue("\(listToken)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                   
                    self.getAccessLogs = dic["rows"] as! [[String : Any]]
                    print("getAccessoryLogsList",self.getAccessLogs.count)
                    self.accessoryLogsList.removeAll()
                    for data in self.getAccessLogs{
                        let checkedout_from = data["checkout_form"]
                        let label_type = data["label"]
                        let checkedin_to = data["checkin"]
                        let approving_user = data["approving_user"]
                        let date_time = data["date_time"]
                        let qty = data["qty"]
                        let type = data["type"]
                        self.accessoryLogsList.append(AccesssoryLogsData(id: 0, accessory_id: 0, checkedout_from: checkedout_from as? String ?? "", label_type: label_type as? String ?? "", checkedin_to: checkedin_to as? String ?? "", approving_user: approving_user as? Int ?? 0, date_time: date_time as? String ?? "", qty: qty as? Int ?? 0, type: type as? String ?? "", created_at: "", updated_at: "", deleted_at:""))
                        
                    }
                    self.insertAccessoryLogs()
                    self.readAccessoryLogs()
                    self.viewAllAccessoryLogs()
                    self.logsTV.reloadData()
                    self.activityIndicator.stopAnimating()
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
            }
        }.resume()
    }

//MARK: - Actions
    @IBAction func btnDetailViewAll(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewAllAccessoryListVC") as! ViewAllAccessoryListVC
        if isFromConsumable{
            vc.consumableId = consumableId
            vc.isFromConsumable = isFromConsumable
        }else if isFromComponent{
            vc.componentId = componentId
            vc.isFromComponent = true
        }else{
            vc.viewalluser = viewalluser
            vc.accessoryId = accessoryId
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLogsViewAll(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewAllAccessLogsVC") as! ViewAllAccessLogsVC
        if isFromConsumable{
            vc.consumableId = consumableId
            vc.isFromConsumable = true
        }else if isFromComponent{
            vc.componentId = componentId
            vc.isFromComponent = true
        }else{
            vc.viewallLogs = viewallLogs
            vc.accessoryId = accessoryId
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: -StoreDataInSqlite
    let accessoryUserTable = Table("AccessoryUser")
    let accessoryLogsTable = Table("AccessoryLogs")
    let consumableUserTable = Table("ConsumableUser")
    let consumableLogTable = Table("ConsumableLogs")
    let componentLogTable = Table("ComponentLogs")
    let componentUserTable = Table("ComponentUser")
    var id = Expression<Int>("id")
    var logid = Expression<Int>("logid")
    var user_id = Expression<Int>("user_id")
    var assigned_type = Expression<String>("assigned_type")
    var accessory_id = Expression<Int>("accessory_id")
    var consumable_id = Expression<Int>("consumable_id")
    var logaccessory_id = Expression<Int>("logaccessory_id")
    var assigned_to = Expression<String>("assigned_to")
    var assigned_asset = Expression<String>("assigned_asset")
    var assigned_location = Expression<Int>("assigned_location")
    var qty = Expression<Int>("qty")
    var qtyLogs = Expression<Int>("qty")
    var created_at = Expression<String>("created_at")
    var updated_at = Expression<String>("updated_at")
    var log_created_at = Expression<String>("log_created_at")
    var log_updated_at = Expression<String>("log_updated_at")
    var note = Expression<String>("note")
    var checkedout_from = Expression<String>("checkedout_from")
    var label_type = Expression<String>("label_type")
    var approving_user = Expression<Int>("approving_user")
    var date_time = Expression<String>("date_time")
    var deleted_at = Expression<String>("deleted_at")
    var checkedin_to = Expression<String>("checkedin_to")
    var type = Expression<String>("type")
    let user = Expression<String>("user")
    let location = Expression<String>("location")
    let assets = Expression<String>("assets")
    func createAccessoryUserDB(){
            do {
                // Get database path
                if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                    print("jobspath",path)
                    
                    // Connect to database
                    db = try Connection("\(path)/accessoryUser.sqlite3")
                    if Reachability.isConnectedToNetwork(){
                        let drop = accessoryUserTable.drop(ifExists: true)
                        try db?.run(drop)
                        let drop2 = accessoryLogsTable.drop(ifExists: true)
                        try db?.run(drop2)
                       
                    }
                    // Initialize tables
                    try db?.run(accessoryUserTable.create { table in
                        table.column(id,primaryKey: true)
                        table.column(user_id)
                        table.column(assigned_type)
                        table.column(accessory_id)
                        table.column(assigned_to)
                        table.column(assigned_asset)
                        table.column(assigned_location)
                        table.column(qty)
                        table.column(created_at)
                        table.column(updated_at)
                        table.column(note)
                        table.column(user)
                        table.column(location)
                        table.column(assets)
                    })
                    try db?.run(accessoryLogsTable.create { table in
                        table.column(logid)
                        table.column(logaccessory_id)
                        table.column(checkedout_from)
                        table.column(label_type)
                        table.column(checkedin_to)
                        table.column(approving_user)
                        table.column(date_time)
                        table.column(qtyLogs)
                        table.column(type)
                        table.column(log_updated_at)
                        table.column(log_created_at)
                        table.column(deleted_at)
                    })
                    
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    func createDBForConsumable(){
        do{
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/consumableUserLogs.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    let drop = consumableUserTable.drop(ifExists: true)
                    try db?.run(drop)
                    let drop2 = consumableLogTable.drop(ifExists: true)
                    try db?.run(drop2)
                }
                try db?.run(consumableUserTable.create { table in
                    table.column(id,primaryKey: true)
                    table.column(user_id)
                    table.column(assigned_type)
                    table.column(consumable_id)
                    table.column(assigned_to)
                    table.column(assigned_asset)
                    table.column(assigned_location)
                    table.column(qty)
                    table.column(created_at)
                    table.column(updated_at)
                    table.column(note)
                    table.column(user)
                    table.column(location)
                    table.column(assets)
                })
                try db?.run(consumableLogTable.create { table in
                    table.column(checkedout_from)
                    table.column(label_type)
                    table.column(checkedin_to)
                    table.column(qtyLogs)
                    table.column(type)
                })
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    func createDBForComponent(){
        do{
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                print("jobspath",path)
                
                // Connect to database
                db = try Connection("\(path)/componentLogs.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    let drop2 = componentUserTable.drop(ifExists: true)
                    try db?.run(drop2)
                    let drop = componentLogTable.drop(ifExists: true)
                    try db?.run(drop)
                }
                try db?.run(componentUserTable.create { table in
                    table.column(id,primaryKey: true)
                    table.column(user_id)
                    table.column(assigned_type)
                    table.column(accessory_id)
                    table.column(assigned_to)
                    table.column(assigned_asset)
                    table.column(assigned_location)
                    table.column(qty)
                    table.column(created_at)
                    table.column(updated_at)
                    table.column(note)
                    table.column(user)
                    table.column(location)
                    table.column(assets)
                })
                try db?.run(componentLogTable.create { table in
                    table.column(checkedout_from)
                    table.column(label_type)
                    table.column(checkedin_to)
                    table.column(qtyLogs)
                    table.column(type)
                    table.column(date_time)
                })
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    //MARK: - Insert Of Accessory User and Logs
        func insertAccessoryUser(){
            for dataset in accessoryUserList{
                do{
                    try db?.run(accessoryUserTable.insert(id <- dataset.id , user_id <- dataset.userId , assigned_type <- dataset.assignedType , accessory_id <- dataset.accessoryId , assigned_to <- dataset.assignedTo , assigned_asset <- dataset.assignAsset , assigned_location <- dataset.assignedLoc , qty <- dataset.qty , created_at <- dataset.createAt , updated_at <- dataset.updateAt ,note <- dataset.note , user <- dataset.user , location <- dataset.location , assets <- dataset.assets))
                }catch{
                    print(String(describing: error))
                    print(error.localizedDescription)
                }
            }
        }
    func insertAccessoryLogs(){
        for dataset in accessoryLogsList{
            do{
                try db?.run(accessoryLogsTable.insert(logid <- dataset.id , logaccessory_id <- dataset.accessory_id ,checkedout_from <- dataset.checkedout_from , label_type <- dataset.label_type ,checkedin_to <- dataset.checkedin_to,approving_user <- dataset.approving_user ,date_time <- dataset.date_time , qtyLogs <- dataset.qty , type <- dataset.type , log_created_at <- dataset.created_at ,log_updated_at <- dataset.updated_at , deleted_at <- dataset.deleted_at))
            }catch{
                print(String(describing: error))
                print(error.localizedDescription)
            }
        }
    }
    //MARK: - Insert Of Consumables User and logs
    func insertConsumableUser(){
        for dataset in consumableUserList{
            do{
                try db?.run(consumableUserTable.insert(id <- dataset.id , user_id <- dataset.userId , assigned_type <- dataset.assignedType , consumable_id <- dataset.consumableId , assigned_to <- dataset.assignedTo , assigned_asset <- dataset.assignAsset , assigned_location <- dataset.assignedLoc , qty <- dataset.qty , created_at <- dataset.createAt , updated_at <- dataset.updateAt ,note <- dataset.note , user <- dataset.user , location <- dataset.location , assets <- dataset.assets))
            }catch{
                print(String(describing: error))
                print(error.localizedDescription)
            }
        }
    }
    func insertConsumableLogs(){
    
        for dataset in consumableLogsList{
            do{
                try db?.run(consumableLogTable.insert(checkedout_from <- dataset.checkedout_from , label_type <- dataset.label_type ,checkedin_to <- dataset.checkedin, qtyLogs <- dataset.qty , type <- dataset.type ))
            }catch{
                print(String(describing: error))
                print(error.localizedDescription)
            }
        }
    }
    //MARK: - Insert Of Component User and Logs
    func insertComponentUser(){
        for dataset in componentUserList{
            do{
                try db?.run(componentUserTable.insert(id <- dataset.id , user_id <- dataset.userId , assigned_type <- dataset.assignedType , accessory_id <- dataset.accessoryId , assigned_to <- dataset.assignedTo , assigned_asset <- dataset.assignAsset , assigned_location <- dataset.assignedLoc , qty <- dataset.qty , created_at <- dataset.createAt , updated_at <- dataset.updateAt ,note <- dataset.note , user <- dataset.user , location <- dataset.location , assets <- dataset.assets))
            }catch{
                print(String(describing: error))
                print(error.localizedDescription)
            }
        }
    }
    func insertComponentLogs(){
    
        for dataset in componentLogsList{
            do{
                try db?.run(componentLogTable.insert(checkedout_from <- dataset.checkedout_from , label_type <- dataset.label_type ,checkedin_to <- dataset.checkedin_to, qtyLogs <- dataset.qty , type <- dataset.type ,date_time <- dataset.date_time))
            }catch{
                print(String(describing: error))
                print(error.localizedDescription)
            }
        }
    }
    //MARK: - Read Accessory User and Logs
        func readAccessoryUser(){
            getOfflineAccessDetail.removeAll()
            do{
                for row in try db!.prepare("SELECT * FROM AccessoryUser LIMIT 2") {
                    getOfflineAccessDetail.append(AccesssoryUserOfflineData(id: Int(row[0] as? Int64 ?? 0), userId: Int(row[1] as? Int64 ?? 0), assignedType: row[2] as? String ?? "", accessoryId: Int(row[3] as? Int64 ?? 0), assignedTo: row[4] as? String ?? "", assignAsset: row[5] as? String ?? "", assignedLoc: Int(row[6] as? Int64 ?? 0), qty: Int(row[7] as? Int64 ?? 0), createAt: row[8] as? String ?? "", updateAt: row[9] as? String ?? "", note: row[10] as? String ?? "" , user:  row[11] as? String ?? "" ,location:  row[12] as? String ?? "",assets:  row[13] as? String ?? ""))
                        detailTV.reloadData()
                }
            }catch{
                print(String(describing: error))
                print(error.localizedDescription)
            }
        }
    
    func readAccessoryLogs(){
        getOfflineAccessLogs.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM AccessoryLogs LIMIT 2") {
                print("row233",row)
                getOfflineAccessLogs.append(AccessoryLogsOfflineData(id: Int(row[0] as? Int64 ?? 0), accessory_id: Int(row[1] as? Int64 ?? 0), checkedout_from:  row[2] as? String ?? "", label_type:  row[3] as? String ?? "", checkedin_to:  row[4] as? String ?? "", approving_user: Int(row[5] as? Int64 ?? 0), date_time:  row[6] as? String ?? "", qty: Int(row[7] as? Int64 ?? 0), type:  row[8] as? String ?? "", created_at:  row[9] as? String ?? "", updated_at:  row[10] as? String ?? "", deleted_at:  row[11] as? String ?? ""))
                    logsTV.reloadData()
            }
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
    }
    //MARK: - Read Consumable User and Logs
    func readConsumableLogs(){
        getOfflineConsumableLogs.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ConsumableLogs LIMIT 2") {
                print("row233",row)
                getOfflineConsumableLogs.append(ConsumableLogsOfflineData(checkedout_from: row[0] as? String ?? "", label_type:  row[1] as? String ?? "", checkedin:  row[2] as? String ?? "", qty:  row[3] as? Int ?? 0, type:  row[4] as? String ?? ""))
                    logsTV.reloadData()
            }
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
    }
    
    func readConsumableUser(){
        getOfflineConsumableUser.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ConsumableUser LIMIT 2") {
                getOfflineConsumableUser.append(ConsumableUserOffline(id: Int(row[0] as? Int64 ?? 0), userId: Int(row[1] as? Int64 ?? 0), assignedType: row[2] as? String ?? "", consumableId: Int(row[3] as? Int64 ?? 0), assignedTo: row[4] as? String ?? "", assignAsset: row[5] as? String ?? "", assignedLoc: Int(row[6] as? Int64 ?? 0), qty: Int(row[7] as? Int64 ?? 0), createAt: row[8] as? String ?? "", updateAt: row[9] as? String ?? "", note: row[10] as? String ?? "" , user:  row[11] as? String ?? "" ,location:  row[12] as? String ?? "",assets:  row[13] as? String ?? ""))
                    detailTV.reloadData()
            }
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
    }
    //MARK: - Read Component User and Logs
    
    func readComponentUser(){
        getOfflineComponentUser.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ComponentUser LIMIT 2") {
                print(row[7])
                getOfflineComponentUser.append(AccesssoryUserOfflineData(id: Int(row[0] as? Int64 ?? 0), userId: Int(row[1] as? Int64 ?? 0), assignedType: row[2] as? String ?? "", accessoryId: Int(row[3] as? Int64 ?? 0), assignedTo: row[4] as? String ?? "", assignAsset: row[5] as? String ?? "", assignedLoc: Int(row[6] as? Int64 ?? 0), qty: Int(row[7] as? Int64 ?? 0), createAt: row[8] as? String ?? "", updateAt: row[9] as? String ?? "", note: row[10] as? String ?? "" , user:  row[11] as? String ?? "" ,location:  row[12] as? String ?? "",assets:  row[13] as? String ?? ""))
                    detailTV.reloadData()
            }
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
    }
    func readComponentLogs(){
        getOfflineComponentLogs.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ComponentLogs LIMIT 2") {
                print("row233",row[3])
                getOfflineComponentLogs.append(AccessoryLogsOfflineData(checkedout_from: row[0] as? String ?? "", label_type:  row[1] as? String ?? "", checkedin_to:  row[2] as? String ?? "", date_time: row[5] as? String ?? "", qty:  Int(row[3] as! Int64), type: row[4] as? String ?? ""))
                    logsTV.reloadData()
            }
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
    }
    func viewAllAccessoryUser(){
        viewalluser.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM AccessoryUser") {
                viewalluser.append(ViewAllAccessoryUser(id: Int(row[0] as? Int64 ?? 0), userId: Int(row[1] as? Int64 ?? 0), assignedType: row[2] as? String ?? "", accessoryId: Int(row[3] as? Int64 ?? 0), assignedTo: row[4] as? String ?? "", assignAsset: row[5] as? String ?? "", assignedLoc: Int(row[6] as? Int64 ?? 0), qty: Int(row[7] as? Int64 ?? 0), createAt: row[8] as? String ?? "", updateAt: row[9] as? String ?? "", note: row[10] as? String ?? "" , user:  row[11] as? String ?? "" ,location:  row[12] as? String ?? "",assets:  row[13] as? String ?? ""))
                    logsTV.reloadData()
            }
            btnViewAllUser.setTitle("View All( " + "\(viewalluser.count) items )", for: .normal)
            
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
        if viewalluser.count == 0{
            viewAccessUser.isHidden = true
            lblUserMsg.isHidden = false
        }else{
            viewAccessUser.isHidden = false
            lblUserMsg.isHidden = true
        }
    }
    func viewAllAccessoryLogs(){
        viewallLogs.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM AccessoryLogs") {
 
                viewallLogs.append(ViewAllAccessoryLogs(id: Int(row[0] as? Int64 ?? 0), accessory_id: Int(row[1] as? Int64 ?? 0), checkedout_from:  row[2] as? String ?? "", label_type:  row[3] as? String ?? "", checkedin_to:  row[4] as? String ?? "", approving_user: Int(row[5] as? Int64 ?? 0), date_time:  row[6] as? String ?? "", qty: Int(row[7] as? Int64 ?? 0), type:  row[8] as? String ?? "", created_at:  row[9] as? String ?? "", updated_at:  row[10] as? String ?? "", deleted_at:  row[11] as? String ?? ""))
                    logsTV.reloadData()
            }
            print("viewallLogs.count",viewallLogs.count)
            btnViewAllLogs.setTitle("View All( " + "\(viewallLogs.count) items )", for: .normal)
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
        if viewallLogs.count == 0{
            viewAccessLogs.isHidden = true
            lblLogsMsg.isHidden = false
        }else{
            viewAccessLogs.isHidden = false
            lblLogsMsg.isHidden = true
        }
    }
    func viewAllConsumableUser(){
        viewallCoumableuser.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ConsumableUser") {
                viewallCoumableuser.append(ViewAllConsumableUser(id: Int(row[0] as? Int64 ?? 0), userId: Int(row[1] as? Int64 ?? 0), assignedType: row[2] as? String ?? "", accessoryId: Int(row[3] as? Int64 ?? 0), assignedTo: row[4] as? String ?? "", assignAsset: row[5] as? String ?? "", assignedLoc: Int(row[6] as? Int64 ?? 0), qty: Int(row[7] as? Int64 ?? 0), createAt: row[8] as? String ?? "", updateAt: row[9] as? String ?? "", note: row[10] as? String ?? "" , user:  row[11] as? String ?? "" ,location:  row[12] as? String ?? "",assets:  row[13] as? String ?? ""))
                    logsTV.reloadData()
            }
            btnViewAllUser.setTitle("View All( " + "\(viewallCoumableuser.count) items )", for: .normal)
            
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
        if viewallCoumableuser.count == 0{
            viewAccessUser.isHidden = true
            lblUserMsg.isHidden = false
        }else{
            viewAccessUser.isHidden = false
            lblUserMsg.isHidden = true
        }
    }
    func viewAllConsumableLogs(){
        viewallCoumableLogs.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ConsumableLogs") {
                print(row)
                viewallCoumableLogs.append(ViewAllConsumableLogs(checkedout_from:  row[0] as? String ?? "", label_type:  row[1] as? String ?? "", checkedin:  row[2] as? String ?? "", qty: Int(row[3] as? Int64 ?? 0), type:  row[4] as? String ?? ""))
                    logsTV.reloadData()
            }
            print("viewallLogs.count",viewallCoumableLogs.count)
            btnViewAllLogs.setTitle("View All( " + "\(viewallCoumableLogs.count) items )", for: .normal)
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
        if viewallCoumableLogs.count == 0{
            viewAccessLogs.isHidden = true
            lblLogsMsg.isHidden = false
        }else{
            viewAccessLogs.isHidden = false
            lblLogsMsg.isHidden = true
        }
    }
    func viewAllComponentUser(){
        viewallComponentUser.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ComponentUser") {
                viewallComponentUser.append(ViewAllAccessoryUser(id: Int(row[0] as? Int64 ?? 0), userId: Int(row[1] as? Int64 ?? 0), assignedType: row[2] as? String ?? "", accessoryId: Int(row[3] as? Int64 ?? 0), assignedTo: row[4] as? String ?? "", assignAsset: row[5] as? String ?? "", assignedLoc: Int(row[6] as? Int64 ?? 0), qty: Int(row[7] as? Int64 ?? 0), createAt: row[8] as? String ?? "", updateAt: row[9] as? String ?? "", note: row[10] as? String ?? "" , user:  row[11] as? String ?? "" ,location:  row[12] as? String ?? "",assets:  row[13] as? String ?? ""))
                detailTV.reloadData()
            }
            btnViewAllUser.setTitle("View All( " + "\(viewallComponentUser.count) items )", for: .normal)
            
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
        if viewallComponentUser.count == 0{
            viewAccessUser.isHidden = true
            lblUserMsg.isHidden = false
        }else{
            viewAccessUser.isHidden = false
            lblUserMsg.isHidden = true
        }
    }
    func viewAllComponentlog(){
        viewallComponentLogs.removeAll()
        do{
            for row in try db!.prepare("SELECT * FROM ComponentLogs") {
                viewallComponentLogs.append(ViewAllAccessoryLogs(checkedout_from: row[0] as? String ?? "",label_type: row[1] as? String ?? "",checkedin_to: row[2] as? String ?? "",date_time: row[3] as? String ?? "",qty: row[4] as? Int ?? 0,type: row[5] as? String ?? ""))
//                logsTV.reloadData()
            }
            print("viewallComponentLogs",viewallComponentLogs.count)
            btnViewAllLogs.setTitle("View All( " + "\(viewallComponentLogs.count) items )", for: .normal)
        }catch{
            print(String(describing: error))
            print(error.localizedDescription)
        }
        if viewallComponentLogs.count == 0{
            viewAccessLogs.isHidden = true
            lblLogsMsg.isHidden = false
        }else{
            viewAccessLogs.isHidden = false
            lblLogsMsg.isHidden = true
        }
    }
}
extension AccessoryDetailVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == detailTV {
            if isFromConsumable{
                return getOfflineConsumableUser.count
            }else if isFromComponent{
                return getOfflineComponentUser.count
            }else{
                return getOfflineAccessDetail.count
            }
        }else{
            if isFromConsumable{
                return getOfflineConsumableLogs.count
            }else if isFromComponent{
                return getOfflineComponentLogs.count
            }else{
                return getOfflineAccessLogs.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == detailTV {
            if isFromConsumable{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoryDetailTVC") as! AccessoryDetailTVC
                let list = getOfflineConsumableUser[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                let newdate = GlobalFunction.convertDateString(dateString: list.createAt, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                cell.lblType.text = list.assignedType
                cell.btnCheckInUser.isHidden = true
                if list.user != ""{
                    cell.lblCheck.text = list.user
                }else if list.location != ""{
                    cell.lblCheck.text = list.location
                }else if list.assets != ""{
                    cell.lblCheck.text = list.assets
                }
                return cell
            }else if isFromComponent{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoryDetailTVC") as! AccessoryDetailTVC
                let list = getOfflineComponentUser[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                let newdate = GlobalFunction.convertDateString(dateString: list.createAt, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                cell.lblType.text = list.assignedType
                cell.btnCheckInUser.isHidden = false
                if list.user != ""{
                    cell.lblCheck.text = list.user
                }else if list.location != ""{
                    cell.lblCheck.text = list.location
                }else if list.assets != ""{
                    cell.lblCheck.text = list.assets
                }
                cell.checkIn = {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
                    vc.componentId = list.id
                    vc.isfromComponent = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoryDetailTVC") as! AccessoryDetailTVC
                cell.btnCheckInUser.isHidden = false
                let list = getOfflineAccessDetail[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                let newdate = GlobalFunction.convertDateString(dateString: list.createAt, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                cell.lblType.text = list.assignedType
                if list.user != ""{
                    cell.lblCheck.text = list.user
                }else if list.location != ""{
                    cell.lblCheck.text = list.location
                }else if list.assets != ""{
                    cell.lblCheck.text = list.assets
                }
                cell.checkIn = {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
                    vc.accessoryId = list.id // self.accessoryId
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                return cell
            }
        }else{
            if isFromConsumable{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccessLogsTVC") as! AccessLogsTVC
                let list = getOfflineConsumableLogs[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                cell.lblStore.text = list.checkedin
                cell.lblStoreTitle.text = list.checkedout_from + "->"
//                let newdate = GlobalFunction.convertDateString(dateString: list.date_time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
//                cell.lblDate.text = newdate
                return cell
            }else if isFromComponent{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccessLogsTVC") as! AccessLogsTVC
                let list = getOfflineComponentLogs[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                cell.lblStore.text = list.checkedin_to
                cell.lblStoreTitle.text = list.checkedout_from + "->"
                let newdate = GlobalFunction.convertDateString(dateString: list.date_time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AccessLogsTVC") as! AccessLogsTVC
                let list = getOfflineAccessLogs[indexPath.row]
                cell.lblQty.text = "\(list.qty)"
                cell.lblStore.text = list.checkedin_to
                cell.lblStoreTitle.text = list.checkedout_from + "->"
                let newdate = GlobalFunction.convertDateString(dateString: list.date_time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd-MM-yy HH:mm")
                cell.lblDate.text = newdate
                
                return cell
            }
        }
    }
    
}
struct ConsumableUser{
    internal init(id: Int = Int(), userId: Int = Int(), assignedType: String = String(), consumableId: Int = Int(), assignedTo: String = String(), assignAsset: String = String(), assignedLoc: Int = Int(), qty: Int = Int(), createAt: String = String(), updateAt: String = String(), note: String = String(), user: String = String(), location: String = String(), assets: String = String()) {
        self.id = id
        self.userId = userId
        self.assignedType = assignedType
        self.consumableId = consumableId
        self.assignedTo = assignedTo
        self.assignAsset = assignAsset
        self.assignedLoc = assignedLoc
        self.qty = qty
        self.createAt = createAt
        self.updateAt = updateAt
        self.note = note
        self.user = user
        self.location = location
        self.assets = assets
    }
   
    var id = Int()
    var userId = Int()
    var assignedType = String()
    var consumableId = Int()
    var assignedTo = String()
    var assignAsset = String()
    var assignedLoc = Int()
    var qty = Int()
    var createAt = String()
    var updateAt = String()
    var note = String()
    var user = String()
    var location = String()
    var assets = String()
}
struct ConsumableUserOffline{
    internal init(id: Int = Int(), userId: Int = Int(), assignedType: String = String(), consumableId: Int = Int(), assignedTo: String = String(), assignAsset: String = String(), assignedLoc: Int = Int(), qty: Int = Int(), createAt: String = String(), updateAt: String = String(), note: String = String(), user: String = String(), location: String = String(), assets: String = String()) {
        self.id = id
        self.userId = userId
        self.assignedType = assignedType
        self.consumableId = consumableId
        self.assignedTo = assignedTo
        self.assignAsset = assignAsset
        self.assignedLoc = assignedLoc
        self.qty = qty
        self.createAt = createAt
        self.updateAt = updateAt
        self.note = note
        self.user = user
        self.location = location
        self.assets = assets
    }
   
    var id = Int()
    var userId = Int()
    var assignedType = String()
    var consumableId = Int()
    var assignedTo = String()
    var assignAsset = String()
    var assignedLoc = Int()
    var qty = Int()
    var createAt = String()
    var updateAt = String()
    var note = String()
    var user = String()
    var location = String()
    var assets = String()
}
struct AccesssoryUserData {
    internal init(id: Int = Int(), userId: Int = Int(), assignedType: String = String(), accessoryId: Int = Int(), assignedTo: String = String(), assignAsset: String = String(), assignedLoc: Int = Int(), qty: Int = Int(), createAt: String = String(), updateAt: String = String(), note: String = String(), user: String = String(), location: String = String(), assets: String = String()) {
        self.id = id
        self.userId = userId
        self.assignedType = assignedType
        self.accessoryId = accessoryId
        self.assignedTo = assignedTo
        self.assignAsset = assignAsset
        self.assignedLoc = assignedLoc
        self.qty = qty
        self.createAt = createAt
        self.updateAt = updateAt
        self.note = note
        self.user = user
        self.location = location
        self.assets = assets
    }
   
    var id = Int()
    var userId = Int()
    var assignedType = String()
    var accessoryId = Int()
    var assignedTo = String()
    var assignAsset = String()
    var assignedLoc = Int()
    var qty = Int()
    var createAt = String()
    var updateAt = String()
    var note = String()
    var user = String()
    var location = String()
    var assets = String()
}
struct AccesssoryUserOfflineData {
    internal init(id: Int = Int(), userId: Int = Int(), assignedType: String = String(), accessoryId: Int = Int(), assignedTo: String = String(), assignAsset: String = String(), assignedLoc: Int = Int(), qty: Int = Int(), createAt: String = String(), updateAt: String = String(), note: String = String(), user: String = String(), location: String = String(), assets: String = String()) {
        self.id = id
        self.userId = userId
        self.assignedType = assignedType
        self.accessoryId = accessoryId
        self.assignedTo = assignedTo
        self.assignAsset = assignAsset
        self.assignedLoc = assignedLoc
        self.qty = qty
        self.createAt = createAt
        self.updateAt = updateAt
        self.note = note
        self.user = user
        self.location = location
        self.assets = assets
    }
    
    var id = Int()
    var userId = Int()
    var assignedType = String()
    var accessoryId = Int()
    var assignedTo = String()
    var assignAsset = String()
    var assignedLoc = Int()
    var qty = Int()
    var createAt = String()
    var updateAt = String()
    var note = String()
    var user = String()
    var location = String()
    var assets = String()
}

struct AccesssoryLogsData{
    init(id: Int = Int(), accessory_id: Int = Int(), checkedout_from: String = String(), label_type: String = String(), checkedin_to: String = String(), approving_user: Int = Int(), date_time: String = String(), qty: Int = Int(), type: String = String(), created_at: String = String(), updated_at: String = String(), deleted_at: String = String()) {
        self.id = id
        self.accessory_id = accessory_id
        self.checkedout_from = checkedout_from
        self.label_type = label_type
        self.checkedin_to = checkedin_to
        self.approving_user = approving_user
        self.date_time = date_time
        self.qty = qty
        self.type = type
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at
    }
    
    var id = Int()
    var accessory_id = Int()
    var checkedout_from = String()
    var label_type = String()
    var checkedin_to = String()
    var approving_user = Int()
    var date_time = String()
    var qty = Int()
    var type =  String()
    var created_at = String()
    var updated_at = String()
    var deleted_at = String()
}
struct AccessoryLogsOfflineData {
    init(id: Int = Int(), accessory_id: Int = Int(), checkedout_from: String = String(), label_type: String = String(), checkedin_to: String = String(), approving_user: Int = Int(), date_time: String = String(), qty: Int = Int(), type: String = String(), created_at: String = String(), updated_at: String = String(), deleted_at: String = String()) {
        self.id = id
        self.accessory_id = accessory_id
        self.checkedout_from = checkedout_from
        self.label_type = label_type
        self.checkedin_to = checkedin_to
        self.approving_user = approving_user
        self.date_time = date_time
        self.qty = qty
        self.type = type
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at
    }
    
    var id = Int()
    var accessory_id = Int()
    var checkedout_from = String()
    var label_type = String()
    var checkedin_to = String()
    var approving_user = Int()
    var date_time = String()
    var qty = Int()
    var type =  String()
    var created_at = String()
    var updated_at = String()
    var deleted_at = String()
}
struct ConsumableLogsData{
    init(checkedout_from: String = String(), label_type: String = String(), checkedin: String = String(), qty: Int = Int(), type: String = String()) {
        self.checkedout_from = checkedout_from
        self.label_type = label_type
        self.checkedin = checkedin
        self.qty = qty
        self.type = type
        
    }
    var checkedout_from = String()
    var label_type = String()
    var checkedin = String()
    var qty = Int()
    var type =  String()
}
struct ConsumableLogsOfflineData{
    init(checkedout_from: String = String(), label_type: String = String(), checkedin: String = String(), qty: Int = Int(), type: String = String()) {
        self.checkedout_from = checkedout_from
        self.label_type = label_type
        self.checkedin = checkedin
        self.qty = qty
        self.type = type
        
    }
    var checkedout_from = String()
    var label_type = String()
    var checkedin = String()
    var qty = Int()
    var type =  String()
}
struct ViewAllAccessoryUser{
    init(id: Int = Int(), userId: Int = Int(), assignedType: String = String(), accessoryId: Int = Int(), assignedTo: String = String(), assignAsset: String = String(), assignedLoc: Int = Int(), qty: Int = Int(), createAt: String = String(), updateAt: String = String(), note: String = String(), user: String = String(), location: String = String(), assets: String = String()) {
        self.id = id
        self.userId = userId
        self.assignedType = assignedType
        self.accessoryId = accessoryId
        self.assignedTo = assignedTo
        self.assignAsset = assignAsset
        self.assignedLoc = assignedLoc
        self.qty = qty
        self.createAt = createAt
        self.updateAt = updateAt
        self.note = note
        self.user = user
        self.location = location
        self.assets = assets
    }
    
    var id = Int()
    var userId = Int()
    var assignedType = String()
    var accessoryId = Int()
    var assignedTo = String()
    var assignAsset = String()
    var assignedLoc = Int()
    var qty = Int()
    var createAt = String()
    var updateAt = String()
    var note = String()
    var user = String()
    var location = String()
    var assets = String()
}
struct ViewAllAccessoryLogs{
    init(id: Int = Int(), accessory_id: Int = Int(), checkedout_from: String = String(), label_type: String = String(), checkedin_to: String = String(), approving_user: Int = Int(), date_time: String = String(), qty: Int = Int(), type: String = String(), created_at: String = String(), updated_at: String = String(), deleted_at: String = String()) {
        self.id = id
        self.accessory_id = accessory_id
        self.checkedout_from = checkedout_from
        self.label_type = label_type
        self.checkedin_to = checkedin_to
        self.approving_user = approving_user
        self.date_time = date_time
        self.qty = qty
        self.type = type
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at
    }
    
    var id = Int()
    var accessory_id = Int()
    var checkedout_from = String()
    var label_type = String()
    var checkedin_to = String()
    var approving_user = Int()
    var date_time = String()
    var qty = Int()
    var type =  String()
    var created_at = String()
    var updated_at = String()
    var deleted_at = String()
}
struct ViewAllConsumableUser{
    init(id: Int = Int(), userId: Int = Int(), assignedType: String = String(), accessoryId: Int = Int(), assignedTo: String = String(), assignAsset: String = String(), assignedLoc: Int = Int(), qty: Int = Int(), createAt: String = String(), updateAt: String = String(), note: String = String(), user: String = String(), location: String = String(), assets: String = String()) {
        self.id = id
        self.userId = userId
        self.assignedType = assignedType
        self.accessoryId = accessoryId
        self.assignedTo = assignedTo
        self.assignAsset = assignAsset
        self.assignedLoc = assignedLoc
        self.qty = qty
        self.createAt = createAt
        self.updateAt = updateAt
        self.note = note
        self.user = user
        self.location = location
        self.assets = assets
    }
    
    var id = Int()
    var userId = Int()
    var assignedType = String()
    var accessoryId = Int()
    var assignedTo = String()
    var assignAsset = String()
    var assignedLoc = Int()
    var qty = Int()
    var createAt = String()
    var updateAt = String()
    var note = String()
    var user = String()
    var location = String()
    var assets = String()
}
struct ViewAllConsumableLogs{
    init(checkedout_from: String = String(), label_type: String = String(), checkedin: String = String(), qty: Int = Int(), type: String = String()) {
        self.checkedout_from = checkedout_from
        self.label_type = label_type
        self.checkedin = checkedin
        self.qty = qty
        self.type = type
        
    }
    var checkedout_from = String()
    var label_type = String()
    var checkedin = String()
    var qty = Int()
    var type =  String()
}
