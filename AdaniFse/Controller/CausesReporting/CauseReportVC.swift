//
//  CauseReportVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 01/08/22.
//

import UIKit
import SQLite
import AVKit
import Alamofire

class CauseReportVC: UIViewController {

    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnAddImg: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgHeightCons: NSLayoutConstraint!
    @IBOutlet weak var imgCV: UICollectionView!
    
    @IBOutlet weak var btnPrv: UIButton!
    @IBOutlet weak var btnBrDown: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var queTVCHeight: NSLayoutConstraint!
    @IBOutlet weak var queTVC: UITableView!
    var assetId = Int64()
    var getCauseAsset : [CausesAssetsData] = []
    var getCausesList : [CausesReport] = []
    var imageArray : [UIImage] = []
    var selectedIndex : IndexPath = IndexPath(row: 0, section: 0)
    var getStatus = "breakdown"
    var causesId = [Int64]()
    var imgUrl = [URL]()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenReport = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            queTVC.backgroundColor = Constant.Color.color_launch
            self.view.backgroundColor = Constant.Color.color_launch
        } else {
            queTVC.backgroundColor = .white
            self.view.backgroundColor = .white
        }
        btnSubmit.setCorner(_tp: 5)
        btnAddImg.setCorner(_tp: 5)
        txtDesc.setCorner(_tp: 5)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createDB()
        getCause()
        
    }
    func reload(tableView: UITableView) {
        let contentOffset = tableView.contentOffset
        queTVC.reloadData()
        queTVC.layoutIfNeeded()
        queTVC.setContentOffset(contentOffset, animated: false)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    //MARK: - SqlLite Save Db
    private var db: Connection?
    let algorithms = Table("CauseMaster")
    let id = Expression<Int>("id")
    var causeName = Expression<String>("cause_name")
    var causeType = Expression<String>("cause_type")
    var causeQuestion = Expression<String>("cause_question")
    
    func createDB(){
        do {
            // Get database path
            if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                // If delta-math-helper.sqlite3 exists, rename it
                print(path)
               
                // Connect to database
                db = try Connection("\(path)/CauseMaster.sqlite3")
                if Reachability.isConnectedToNetwork(){
                    let drop = algorithms.drop(ifExists: true)
                    try db?.run(drop)
                }
                // Initialize tables
                try db?.run(algorithms.create(ifNotExists: true) { table in
                    table.column(id, primaryKey: true)
                    table.column(causeName)
                    table.column(causeType)
                    table.column(causeQuestion)
                })
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func insertCauses(){
        for assetdata in getCauseAsset{
            do{
                try db?.run(algorithms.insert(id <- assetdata.id , causeName <- assetdata.causeName , causeType <- assetdata.causeType, causeQuestion <- assetdata.causeQuestion))
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    func readCauses(){
        do {
            for row in try db!.prepare("SELECT * FROM CauseMaster"){
                print(row)
                getCausesList.append(CausesReport(id: row[0] as? Int64 ?? 0, causeName: row[1] as? String ?? "", causeType:  row[2] as? String ?? "", causeQuestion:  row[3] as? String ?? ""))
                DispatchQueue.main.async {
                    self.queTVC.reloadData()
                }
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    //MARK: - Api Call
    func getCauseNew(){
        let param : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")public/asset/\(assetId)/causes/questions")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenReport)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenReport)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: param)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let data = json as? [String: Any] else {
                            return
                        }
                        print(data)
                    }catch{
                        print(String(describing: error))
                    }
                }
            }
        }.resume()
                    
    }
    func getCause(){
        let param : Parameters = [
            .assets_id : assetId
            ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .cause_web_service, baseURL: .baseUAT )
        URLSession.shared.getCausesAsset(with: request) { isSuccess, causeAsset, msg, status in
            if isSuccess{
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    print(msg)
                    self.getCauseAsset = causeAsset ?? []
                    self.insertCauses()
                    self.readCauses()
                    self.reload(tableView: self.queTVC)
                }
    
            }else{
                print(msg)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    func submitCause(){
        for rowIndex in 0..<queTVC.numberOfRows(inSection: 0) {
            let indexPath = IndexPath(row: rowIndex, section: 0)
            if let editCell = queTVC.cellForRow(at: indexPath) as? CauseMasterTVC {
                if editCell.accessoryType == .checkmark{
                    let queId = getCausesList[indexPath.row].id
                    print(queId)
                    causesId.insert(queId, at: 0)
                }
            }
        }
        var params : [String:Any] = [:]
        params["asset_id"]  = "\(assetId)"
        params["audit_status"]  = getStatus
        params["title"] = txtTitle.text ?? ""
        params["description"] = txtDesc.text ?? ""
        params["phone"] = userInfo.phone
        let data = try! JSONSerialization.data(withJSONObject: causesId, options: [])
        print(data)
        let jsonString = String(data: data, encoding: .utf8)!
        let str = jsonString.replacingOccurrences(of: "[", with: "")
        let finalStr = str.replacingOccurrences(of: "]", with: "")
        print(finalStr)
        let param : [String : Any]  = [
            "causes" : finalStr
        ]
        print(params)
        let url = URL(string: "\(selectedComp ?? "")cause_store_work_order")!
        let headers : HTTPHeaders = [
            "Content-Type": "multipart/form-data",
            "Authorization":"\(tokenReport)",
            "Accept" : "application/json"
        ]
        AF.upload(multipartFormData: { multipartFormData in
            for imageData in self.imageArray {
                let image = imageData.jpegData(compressionQuality: 0.5)
                multipartFormData.append(image ?? Data(), withName: "images" , fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: ".jpeg")
            }
            for (key, value) in params {
                print(key,":",value)
                multipartFormData.append(((value) as AnyObject).data(using: String.Encoding.utf8.rawValue) ?? Data(), withName: key)
            }
            for (key, value) in param {
                print(key,":",value)
                multipartFormData.append(((value) as AnyObject).data(using: String.Encoding.utf8.rawValue) ?? Data(), withName: key)
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).response { (result) in
            if result.error != nil{
                print(result.error)
            }else{
                if let jsonData = result.data
                {
                    do{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        print(parsedData)
                        let message = parsedData["messages"] as? String ?? ""
                        let success = parsedData["status"] as? String ?? ""
                        if success == "success"{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast(message)
                        }
                        self.activityIndicator.stopAnimating()
                    }catch{
                        self.view.makeToast("Something went wrong please try again later!")
                        self.activityIndicator.stopAnimating()
                        print(String(describing: error))
                    }
                }
            }
        }
    }
//MARK: - Action
    
    @IBAction func popBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectBreakdown(_ sender: UIButton) {
        btnBrDown.setImage(UIImage(named: "fillcircle"), for: .normal)
        btnPrv.setImage(UIImage(named: "emptycircle"), for: .normal)
        getStatus = "breakdown"
    }
    @IBAction func btnSelectPreventive(_ sender: UIButton) {
        btnBrDown.setImage(UIImage(named: "emptycircle"), for: .normal)
        btnPrv.setImage(UIImage(named: "fillcircle"), for: .normal)
        getStatus = "preventive"
    }
    @IBAction func btnAddImage(_ sender: UIButton) {
        presentCamera()
    }
    
    @IBAction func btnSubmitInsp(_ sender: UIButton) {
        activityIndicator.startAnimating()
        submitCause()
    }
    
    
    //MARK: -Check Camera and Settings
    func checkCameraAccess(isAllowed: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for:.video) {
        case .denied:
            isAllowed(false)
        case .restricted:
            isAllowed(false)
        case .authorized:
            isAllowed(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { isAllowed($0) }
        default:
            print("There is no authorization")
        }
    }
    func presentCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }else{
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_[]")
        return text.filter {okayChars.contains($0) }
    }
    func presentCameraSettings() {
        let alert = UIAlertController.init(title: "Allow the Camera", message: "Move To the setting", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (_) in
        }))
        
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: { (_) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true)
    }
    
}
extension CauseReportVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getCauseAsset.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CauseMasterTVC") as! CauseMasterTVC
        cell.lblQue.text = getCausesList[indexPath.row].causeQuestion
        UIView.animate(withDuration: 0.1) { [self] in
            queTVCHeight.constant = queTVC.contentSize.height
            self.view.layoutIfNeeded()
            queTVCHeight.constant = queTVC.contentSize.height
            self.view.layoutIfNeeded()
            queTVCHeight.constant = queTVC.contentSize.height
        }
        if indexPath.row == 0{
            cell.accessoryType = .checkmark
        }

        return cell
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension CauseReportVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImgCVC", for: indexPath) as! ImgCVC
        selectedIndex.row = indexPath.row
        cell.btnDelete.tag = indexPath.row
        if imageArray.count > 0{
            cell.imgSelected.image = imageArray[indexPath.row]
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imageArray.remove(at: indexPath.row)
        collectionView.deleteItems(at: [indexPath])
        if imageArray.count == 0{
            UIView.animate(withDuration: 0.2) {
                self.imgHeightCons.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let collectionViewWidth = collectionView.bounds.width
           return CGSize(width: collectionViewWidth/3, height: collectionViewWidth/3)
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 20
       }
    
}
extension CauseReportVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func saveImageLocally(image: UIImage, fileName: String) {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let url = documentsDirectory.appendingPathComponent(fileName)
        if let data = image.pngData() {
            do {
                try data.write(to: url) // Writing an Image in the Documents Directory
            } catch {
                print("Unable to Write \(fileName) Image Data to Disk")
            }
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageArray.insert(image, at: 0)
            saveImageLocally(image: image, fileName: "img")
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let url = documentsDirectory.appendingPathComponent("img")
            imgUrl.insert(url, at: 0)
            UIView.animate(withDuration: 0.2) {
                self.imgHeightCons.constant = 130
                self.view.layoutIfNeeded()
            }
            imgCV.reloadData()
            imgCV.scrollToItem(at: selectedIndex, at: .right, animated: true)
        }
    }
}


struct CausesReport {
     init(id: Int64, causeName: String, causeType: String, causeQuestion: String) {
        self.id = id
        self.causeName = causeName
        self.causeType = causeType
        self.causeQuestion = causeQuestion
    }
    
    let id : Int64
    var causeName : String
    var causeType : String
    var causeQuestion : String
}
extension String {

    var strippedtwo: String {
        let okayChars = Set("[]")
        return self.filter {okayChars.contains($0) }
    }
}
