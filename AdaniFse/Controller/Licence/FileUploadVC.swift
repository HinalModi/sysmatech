//
//  FileUploadVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 21/09/22.
//

import UIKit

class FileUploadVC: UIViewController {

    @IBOutlet weak var fileUploadTV: UITableView!
    var licenceId = Int()
    var fileUploadData : [FileDetail] = []
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAcc = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFileList()
    }
    //MARK: - Api call
  
    func getFileList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")licenses/\(licenceId)/licensesFiles")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAcc)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        for data in dic["rows"] as! [[String : Any]]{
                            let path = data["path"] as? String
                            let type = data["type"] as? String
                            let note = data["note"] as? String
                            let date = data["created_at"] as? String
                            
                            self.fileUploadData.append(FileDetail(path: path ?? "", date: date ?? "" , note: note ?? "", type: type ?? ""))
                        }
                        self.fileUploadTV.reloadData()
                    }catch{
                        print(String(describing: error))
                        
                    }
                }
            }
        }.resume()
    }
//MARK: - Action
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    func savePdf(urlString:String, fileName:String) {
                DispatchQueue.main.async {
                    let url = URL(string: urlString)
                    let pdfData = try? Data.init(contentsOf: url!)
                    let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                    let pdfNameFromUrl = "Sysma-\(fileName).pdf"
                    let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                    do {
                        try pdfData?.write(to: actualPath, options: .atomic)
                        print("pdf successfully saved!")
                        self.view.makeToast("pdf successfully saved!")
                    } catch {
                        print(String(describing: error))
                        print("Pdf could not be saved")
                    }
                }
            }
}
extension FileUploadVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileUploadData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileUploadTVC") as! FileUploadTVC
        let list = fileUploadData[indexPath.row]
        cell.lblFile.text = list.path
        print(fileUploadData[indexPath.row].path)
        cell.download = {
            let urlString = URL(string: "https://" + self.fileUploadData[indexPath.row].path)
            if list.type == "image"{
                DispatchQueue.global(qos:.background).async {
                    if let data = try? Data(contentsOf: urlString!),
                       let image = UIImage(data: data) {
                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    }
                }
                DispatchQueue.main.async {
                    self.view.makeToast("Photo saved successfully")
                }
            }else if list.type == "pdf"{
                let fileName = "MyFile"
                self.savePdf(urlString: "https://" + self.fileUploadData[indexPath.row].path, fileName: fileName)
            }
        }
        cell.share = {
            let activityViewController = UIActivityViewController(activityItems: [cell.lblFile.text as Any] , applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
        }
        return cell
    }
    
}
struct FileDetail{
    init(path: String, date: String, note: String , type : String) {
        self.path = path
        self.date = date
        self.note = note
        self.type = type
    }
    
    var path : String
    var date : String
    var note : String
    var type : String
}


