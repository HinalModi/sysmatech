//
//  LicenceSeatVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 26/09/22.
//

import UIKit

class LicenceSeatVC: UIViewController {
    
    @IBOutlet weak var btnAvailable: UIButton!
    @IBOutlet weak var btnAllocate: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var seatTV: UITableView!
    var licenceId = Int()
    var allocSeatlist : [LicenceSeats] = []
    var avaiSeatlist : [LicenceSeats] = []
    var availVal = 0
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAll = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()
        licenceSeatsAllocate()
        btnAllocate.backgroundColor = Constant.Color.color_launch
        btnAllocate.setTitleColor(.white, for: .normal)
        btnAllocate.tintColor = .white
        btnAvailable.tintColor = Constant.Color.color_launch
        btnAvailable.backgroundColor = Constant.Color.color_bg
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAvailableAction(_ sender: UIButton) {
        btnAvailable.backgroundColor = Constant.Color.color_launch
        btnAllocate.backgroundColor = Constant.Color.color_bg
        btnAvailable.setTitleColor(.white, for: .normal)
        btnAllocate.setTitleColor(Constant.Color.color_launch, for: .normal)
        btnAllocate.tintColor = Constant.Color.color_launch
        btnAvailable.tintColor = .white
        availVal = 1
        avaiSeatlist.removeAll()
        licenceSeatAvailable()
    }
    @IBAction func btnAllocateAction(_ sender: UIButton) {
        btnAllocate.backgroundColor = Constant.Color.color_launch
        btnAllocate.setTitleColor(.white, for: .normal)
        btnAllocate.tintColor = .white
        btnAvailable.setTitleColor(Constant.Color.color_launch, for: .normal)
        btnAvailable.tintColor = Constant.Color.color_launch
        btnAvailable.backgroundColor = Constant.Color.color_bg
        availVal = 0
        allocSeatlist.removeAll()
        licenceSeatsAllocate()
    }
    
    func licenceSeatsAllocate(){
        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")licenses/\(licenceId)/licensesseat")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        for data in dic["rows"] as! [[String : Any]]{
                            let seatId = data["id"] as? Int
                            let licId = data["license_id"] as? Int
                            let name = data["name"] as? String
                            let user = data["assigned_user"] as? [String : Any]
                            let asset = data["assigned_asset"] as? [String : Any]
                            let loc = data["location"] as? String
                            let checkInOut = data["user_can_checkout"] as? Int
                            if checkInOut == 0{
                                self.allocSeatlist.append(LicenceSeats(seatId: seatId ?? 0,licId: licId ?? 0 ,seatName: name ?? "", user: user?["name"] as? String ?? "", asset: asset?["name"] as? String ?? "", location: loc ?? "", checkInOut: checkInOut ?? 0))
                            }
                        }
                        self.seatTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                        
                    }
                }
            }
        }.resume()
    }
    func licenceSeatAvailable(){
        activityIndicator.startAnimating()
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")licenses/\(licenceId)/licensesseat")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAll)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        for data in dic["rows"] as! [[String : Any]]{
                            let seatId = data["id"] as? Int
                            let licId = data["license_id"] as? Int
                            let name = data["name"] as? String
                            let user = data["assigned_user"] as? [String : Any]
                            let asset = data["assigned_asset"] as? [String : Any]
                            let loc = data["location"] as? String
                            let checkInOut = data["user_can_checkout"] as? Int
                            if checkInOut == 1{
                                self.avaiSeatlist.append(LicenceSeats(seatId: seatId ?? 0,licId: licId ?? 0,seatName: name ?? "", user: user?["name"] as? String ?? "", asset: asset?["name"] as? String ?? "", location: loc ?? "", checkInOut: checkInOut ?? 0))
                            }
                        }
                        self.seatTV.reloadData()
                        self.activityIndicator.stopAnimating()
                    }catch{
                        print(String(describing: error))
                        
                    }
                }
            }
        }.resume()
    }
    
}
extension LicenceSeatVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if availVal == 1{
            print("avaiSeatlist",avaiSeatlist.count)
            return avaiSeatlist.count
        }else{
            print("allocSeatlist",allocSeatlist.count)
            return allocSeatlist.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LicSeatsTVC") as! LicSeatsTVC
        if availVal == 1{
            let list = avaiSeatlist[indexPath.row]
            if list.user != ""{
                cell.lblUser.text = list.user
            }else if list.asset != ""{
                cell.lblUser.text = list.asset
            }else if list.location != ""{
                cell.lblUser.text = list.location
            }
            cell.btnCheck.setTitle("Checkout", for: .normal)
            cell.btnCheck.backgroundColor = .red
            cell.lblseatName.text = list.seatName
            cell.check = {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
                vc.isFromLicSeat = true
                vc.licId = list.licId
                vc.seatId = list.seatId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let list = allocSeatlist[indexPath.row]
            if list.user != ""{
                cell.lblUser.text = list.user
            }else if list.asset != ""{
                cell.lblUser.text = list.asset
            }else if list.location != ""{
                cell.lblUser.text = list.location
            }
            cell.btnCheck.setTitle("Checkin", for: .normal)
            cell.btnCheck.backgroundColor = Constant.Color.teal
            cell.lblseatName.text = list.seatName
            cell.check = {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
                vc.isfromLicSeat = true
                let list = self.allocSeatlist[indexPath.row]
                vc.licId = list.licId
                vc.seatId = list.seatId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        return cell
    }
}
struct LicenceSeats{
    init(seatId : Int ,licId : Int ,seatName: String, user: String, asset: String, location: String,checkInOut : Int) {
        self.seatName = seatName
        self.user = user
        self.asset = asset
        self.location = location
        self.checkInOut = checkInOut
        self.seatId = seatId
        self.licId = licId
    }
    var seatName : String
    var user : String
    var asset : String
    var location : String
    var checkInOut : Int
    var seatId : Int
    var licId : Int
}
