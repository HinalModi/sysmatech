//
//  CheckOutHistory.swift
//  AdaniFse
//
//  Created by Ankit Dave on 22/09/22.
//

import UIKit

class CheckOutHistoryTV: UIViewController {

    
    @IBOutlet weak var locListTV: UITableView!
    var itemId = Int()
    var locationlist : [LocationHistory] = []
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenAcc = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()

        getHistoryApi()
    }
    
//MARK: - Api Call
    func getHistoryApi(){
        let parameters : [String: String] = [:]
        
        let url = URL(string: "\(selectedComp ?? "")reports/activity?item_id=\(itemId)&item_type=license&order=desc")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenAcc)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error{
                print(error)
                DispatchQueue.main.async {
                    //                    self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
                if let data = data {
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                        guard let dic = json as? [String: Any] else {
                            return
                        }
                        print(dic)
                        for data in dic["rows"] as? [[String : Any]] ?? [[String : Any]](){
                            let admin = data["admin"] as! [String : Any]
                            let actionType = data["action_type"] as? String
                            let item = data["item"] as! [String : Any]
                            let target = data["target"] as? [String : Any]
                            let date = data["action_date"] as! [String : Any]
                            self.locationlist.append(LocationHistory(adminName: admin["name"] as? String ?? "", action:  actionType ?? "", item:  item["name"] as? String ?? "", target:  target?["name"] as? String ?? "", date:  date["formatted"] as? String ?? ""))
                        }
                        self.locListTV.reloadData()
                    }catch{
                        print(String(describing: error))
                        
                    }
                }
            }
        }.resume()
    }
//MARK: - Action
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension CheckOutHistoryTV : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("locationlist",locationlist.count)
        return locationlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckOutHistoryTVC") as! CheckOutHistoryTVC
        let list = locationlist[indexPath.row]
        cell.lblDate.text = list.date
        cell.lblAction.text = list.action
        cell.lblItem.text = list.item
        cell.lblAdmin.text = list.adminName
        cell.lblTarget.text = list.target        
        return cell
    }
    
    
}

struct LocationHistory {
    init(adminName: String, action: String, item: String, target: String, date: String) {
        self.adminName = adminName
        self.action = action
        self.item = item
        self.target = target
        self.date = date
    }
    var adminName : String
    var action : String
    var item : String
    var target : String
    var date : String
}
