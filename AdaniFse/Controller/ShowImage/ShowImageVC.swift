//
//  ShowImageVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 04/08/22.
//

import UIKit

class ShowImageVC: UIViewController {

    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var showImg: UIImageView!
    var imgUrl : URL!
    override func viewDidLoad() {
        super.viewDidLoad()
        showImg.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "default"))
    }
    
    @IBAction func btnDownloadImg(_ sender: UIButton) {
//        if let url = URL(string: imagestring),
        if let data = try? Data(contentsOf: imgUrl),
                    let image = UIImage(data: data) {
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            }
    }
    @IBAction func btnPrevious(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

