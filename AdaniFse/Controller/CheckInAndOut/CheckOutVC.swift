//
//  CheckOutVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 08/09/22.
//

import UIKit
import DropDown
class CheckOutVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var txttitleCheckTo: UILabel!
    @IBOutlet weak var dropUserView: UIView!
    @IBOutlet weak var dropCheckView: UIView!
    @IBOutlet weak var txtSelectUser: UITextField!
    @IBOutlet weak var txtCheckOutTo: UITextField!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblName: UILabel!
    var checkoutToList = ["User","Asset","Location"]
    var checkOutSeat = ["User","Asset"]
    let dropDownCheckOutTo = DropDown()
    let dropDown = DropDown()
    
    var nameList : [[String : Any]] = []
    var nameArray = [String]()
    var filterNameArray = [String]()
    var userList = [UserList]()
    var assetList : [[String : Any]] = []
    var assetArray = [String]()
    var locationList : [[String : Any]] = []
    var locationArray = [String]()
    var locationListArry = [LocationList]()
    var accessoryId = Int()
    var checkOutTo = String()
    var categoryName = String()
    var accessName = String()
    var userData = String()
    var qty = String()
    var notes = String()
    var usedID = Int()
    var locationId = Int()
    var consumeId = Int()
    var isFromConsumable = Bool()
    var isFromComponent = Bool()
    var componentId = Int()
    var licId = Int()
    var seatId = Int()
    var isFromLicSeat = Bool()
    var isfromPredefined = Bool()
    var kitId = Int()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckOut = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    override func viewDidLoad() {
        super.viewDidLoad()

        txtCheckOutTo.setRightPaddingPoints(50)
        txtSelectUser.setRightPaddingPoints(50)
        txtNote.setBorder(borderColor: .lightGray, borderWidth: 0.5)
        txtNote.setCorner(_tp: 5)
        lblName.text = accessName
        lblCategory.text = categoryName
        getUserList()
        getAssetList()
        getLocationList()
        txtSelectUser.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
    }

    
    //MARK: - Api Call
    func getUserList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")users/selectlist")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    self.nameList = dic["results"]  as! [[String : Any]]
                    for name in self.nameList{
                        let nameData = name["text"] as? String ?? ""
                        let id = name["id"] as? Int ?? 0
                        let img = name["image"] as? String ?? ""
                        self.nameArray.append(nameData)
                        self.userList.append(UserList(id: id, name: nameData, image: img))
                    }
                 
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func getAssetList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")hardware")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    self.assetList = dic["rows"]  as! [[String : Any]]
                    print(self.assetList)
                    for name in self.assetList{
                        let nameData = name["name"] as? String ?? ""
                        self.assetArray.append(nameData)
                    }
                    print(self.assetArray)
                    
                    
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    
    func getLocationList(){
        let parameters : [String: String] = [:]
        let url = URL(string: "\(selectedComp ?? "")locations/selectlist")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
//                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    self.locationList = dic["results"]  as! [[String : Any]]
                    print(self.locationList)
                    for name in self.locationList{
                        let nameData = name["text"] as? String ?? ""
                        let id = name["id"] as? Int ?? 0
                        let img = name["image"] as? String ?? ""
                        self.locationArray.append(nameData)
                        self.locationListArry.append(LocationList(id: id, name: nameData, image: img))
                    }
                    print(self.locationArray)
                    
                    
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func getPostString(params: [String: Any]) -> String {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    //MARK: - CheckOutApi
    func checkOutPredefinedKit(){
        let parameters : [String: Any] = [
            "checkout_to_type" : checkOutTo ,
            "assigned_to" : usedID,
            "assigned_asset" : checkOutTo == "asset" ? userData : "",
            "assigned_location" : locationId,
            "assigned_qty": txtQty.text ?? "",
            "note" : txtNote.text ?? ""
        ]
        print(parameters)
        
        let url = URL(string: "\(selectedComp ?? "")kits/\(kitId)/checkout?user_id=\(UserDefaults.standard.string(forKey: "user_id") ?? "")")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String
                    let status = dic["status"] as? String
                    let payLoad = dic["payload"] as! [String : Any]
                    let error = payLoad["errors"] as? [String]

                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked out successfully")
                        }
                       
                    }else{
                        if message != ""{
                            if error != nil {
                                self.view.makeToast((error?[0] ?? ""))
                            }else{
                                self.view.makeToast(message)
                            }
                        }else{
                            self.view.makeToast("Something went wrong!")
                        }
                        
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    func checkOutLicSeat(){
        let parameters : [String: Any] = [
            "checkout_to_type" : checkOutTo ,
            "assigned_to" : usedID,
            "assigned_asset" : checkOutTo == "asset" ? userData : "",
            "assigned_location" : locationId,
            "assigned_qty": txtQty.text ?? "",
            "note" : txtNote.text ?? ""
        ]
        print(parameters)
        
        let url = URL(string: "\(selectedComp ?? "")check-in-out/license/\(licId)/checkout/\(seatId)")
        print("url36",url)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String ?? ""
                    let status = dic["status"] as? String ?? ""
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked out successfully")
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong!")
                        }
                    }
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    
    //MARK: - CheckOut Accessory
    func checkOutAccessory(){
        let parameters : [String: Any] = [
            "checkout_to_type" : checkOutTo ,
            "assigned_to" : usedID,
            "assigned_asset" : checkOutTo == "asset" ? userData : "",
            "assigned_location" : locationId,
            "assigned_qty": txtQty.text ?? "",
            "note" : txtNote.text ?? ""
        ]
        print(parameters)
        
        let url = URL(string: "\(selectedComp ?? "")check-in-out/accessories/\(accessoryId)/checkout")
        print("url36",url)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String ?? ""
                    let status = dic["status"] as? String ?? ""
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked out successfully")
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong!")
                        }
                    }
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    //MARK: - CheckOut Consumable
    func checkOutConsumable(){
        let parameters : [String: Any] = [
            "checkout_to_type" : checkOutTo ,
            "assigned_user" : usedID,
            "assigned_asset" : checkOutTo == "asset" ? userData : "",
            "assigned_location" : locationId,
            "assigned_qty": txtQty.text ?? "",
            "note" : txtNote.text ?? ""
        ]
        print(parameters)
        let url = URL(string: "\(selectedComp ?? "")check-in-out/consumable/\(consumeId)/checkout")
        print("url36",url)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String ?? ""
                    let status = dic["status"] as? String ?? ""
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Consumable checked out successfully")
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong!")
                        }
                    }
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    //MARK: - CheckOut Component
    
    func checkOutComponent(){
        let parameters : [String: Any] = [
            "checkout_to_type" : checkOutTo ,
            "assigned_user" : usedID,
            "assigned_asset" : checkOutTo == "asset" ? userData : "",
            "assigned_location" : locationId,
            "assigned_qty": txtQty.text ?? "",
            "note" : txtNote.text ?? ""
        ]
        print(parameters)
        let url = URL(string: "\(selectedComp ?? "")check-in-out/components/\(componentId)/checkout")
        print("url36",url)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckOut)", forHTTPHeaderField: "Authorization")
        let postString = getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(String(describing: error))
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String ?? ""
                    let status = dic["status"] as? String ?? ""
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Component checked out successfully")
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong!")
                        }
                    }
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    //MARK: - Action
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSelectUser(_ sender: UIButton) {
        
        if txtCheckOutTo.text == "Location"{
            checkOutTo = "location"
            self.dropDown.dataSource = locationArray
            self.dropDown.show()
            self.dropDown.anchorView = dropUserView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                txtSelectUser.text = item
                let location = locationListArry[index]
                locationId = location.id
            }
        }else if txtCheckOutTo.text == "Asset"{
            checkOutTo = "asset"
            self.dropDown.dataSource = assetArray
            self.dropDown.show()
            self.dropDown.anchorView = dropUserView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                txtSelectUser.text = item
            }
        }else{
            checkOutTo = "user"
            self.dropDown.dataSource = nameArray
            self.dropDown.show()
            self.dropDown.anchorView = dropUserView
            self.dropDown.selectionAction = { [self] (index: Int, item: String) in
                txtSelectUser.text = item
                usedID = userList[index].id
                print(usedID)
                
            }
        }
    }
    
    @IBAction func btnCheckOutTo(_ sender: UIButton) {
        if isFromLicSeat{
            self.dropDownCheckOutTo.dataSource = checkOutSeat
        }else{
            self.dropDownCheckOutTo.dataSource = checkoutToList
        }
        self.dropDownCheckOutTo.show()
        self.dropDownCheckOutTo.anchorView = dropCheckView
        self.dropDownCheckOutTo.selectionAction = { [self] (index: Int, item: String) in
            txtCheckOutTo.text = item
            
            if txtCheckOutTo.text == "Location"{
                txttitleCheckTo.text = "Select Location"
            }else if txtCheckOutTo.text == "Asset"{
                txttitleCheckTo.text = "Select Asset"
            }else if txtCheckOutTo.text == "User"{
                txttitleCheckTo.text = "Select User"
            }
        }
        
    }
    
    @IBAction func btnCheckOut(_ sender: UIButton) {
        activityIndicator.startAnimating()
        userData = txtSelectUser.text ?? ""
        qty = txtQty.text ?? ""
        notes = txtNote.text ?? ""
        if isFromConsumable{
            checkOutConsumable()
        }else if isFromComponent{
            checkOutComponent()
        }else if isFromLicSeat{
            checkOutLicSeat()
        }else if isfromPredefined{
            checkOutPredefinedKit()
        }else{
            checkOutAccessory()
        }
    }
}
extension CheckOutVC : UITextFieldDelegate {
    @objc func editingChanged() {
        if txtSelectUser.text != "" {
            if txtCheckOutTo.text == "Location"{
                filterNameArray = locationArray.filter { $0.localizedCaseInsensitiveContains(txtSelectUser.text ?? "")}
                dropDown.anchorView = dropUserView
                dropDown.dataSource = filterNameArray
                dropDown.show()
            }else if txtCheckOutTo.text == "Asset"{
                filterNameArray = assetArray.filter { $0.localizedCaseInsensitiveContains(txtSelectUser.text ?? "")}
                dropDown.anchorView = dropUserView
                dropDown.dataSource = filterNameArray
                dropDown.show()
            }else{
                filterNameArray = nameArray.filter { $0.localizedCaseInsensitiveContains(txtSelectUser.text ?? "")}
                dropDown.anchorView = dropUserView
                dropDown.dataSource = filterNameArray
                dropDown.show()
            }
        }else{
            filterNameArray = []
        }
    }
}
class UserList{
    init(id: Int? = nil,name : String? = nil,image : String? = nil){
        self.id = id ?? 0
        self.name = name ?? ""
        self.image = image ?? ""
    }
    var id : Int
    var name : String
    var image : String
}
class LocationList{
    init(id: Int? = nil,name : String? = nil,image : String? = nil){
        self.id = id ?? 0
        self.name = name ?? ""
        self.image = image ?? ""
    }
    var id : Int
    var name : String
    var image : String
}
