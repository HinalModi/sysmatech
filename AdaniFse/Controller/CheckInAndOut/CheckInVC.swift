//
//  CheckInVC.swift
//  AdaniFse
//
//  Created by Ankit Dave on 08/09/22.
//

import UIKit

class CheckInVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnCalender: UIButton!
    @IBOutlet weak var txtCheckDate: UITextField!
    @IBOutlet weak var txtNotes: UITextView!
    @IBOutlet weak var txtQty: UITextField!
    var datePicker: UIDatePicker!
    var iselectedDate:String = ""
    var selectedDate: String = String()
    var accessoryId = Int()
    var isfromComponent = Bool()
    var componentId = Int()
    var isfromLicSeat = Bool()
    var licId = Int()
    var seatId = Int()
    let selectedComp = UserDefaults.standard.string(forKey: "domain")
    let tokenCheckIn = ("Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "") )
    let status_id = Int()
    let locationId = Int()
    let assetId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        txtCheckDate.setRightPaddingPoints(50)
        btnCheck.setCorner(_tp: 5)
        txtNotes.setBorder(borderColor: .lightGray, borderWidth: 0.5)
        txtNotes.setCorner(_tp: 5)
        if (self.iselectedDate != ""){
            txtCheckDate.text = iselectedDate
        }else{
            self.datePicker = UIDatePicker(frame: CGRect(x:0,y: 0,width: self.view.frame.size.width,height: (self.view.frame.size.height / 2)))
            self.datePicker.datePickerMode = .date
            self.datePicker.setDate(Date(), animated: true)
            self.datePicker.maximumDate = Date()
            self.dateSelected()
        }
    }
    @objc func dateSelected()
    {
        selectedDate =  dateformatterDateTime(date: datePicker.date)
        print(selectedDate)
        datePicker.setDate(datePicker.date, animated: true)
        datePicker.removeFromSuperview()
        txtCheckDate.text = selectedDate
    }
    func dateformatterDateTime(date: Date) -> String
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    @objc private func dateChanged() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        dateSelected()
       
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCal(_ sender: UIButton) {
        if #available(iOS 14.0, *) {
            datePicker.locale = .current
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            datePicker.preferredDatePickerStyle = .inline
            datePicker.center = self.view.center
            datePicker.backgroundColor = UIColor.white
            datePicker.layer.shadowColor = UIColor.gray.cgColor
            datePicker.layer.shadowRadius = 5
            datePicker.layer.shadowOpacity = 0.3
            datePicker.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            self.datePicker.addTarget(self, action:#selector(self.dateChanged), for: .valueChanged)
            self.view.addSubview(self.datePicker)
        }
    }
    
    @IBAction func btnCheckIn(_ sender: UIButton) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        if isfromComponent{
            checkInComponent()
        }else if isfromLicSeat{
            checkInSeats()
        }else{
            checkInCall()
        }
    }
    //MARK: - Api call
    
    func checkInSeats(){
        let parameters : [String: String] = [
            "assigned_qty": txtQty.text ?? "",
            "note": txtNotes.text,
            "checkin_at": txtCheckDate.text ?? ""
        ]
        ///\(seatId)
        let url = URL(string: "\(selectedComp ?? "")check-in-out/license/\(seatId)/checkin")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
                
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String
                    let status = dic["success"] as? String
                    print(status)
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked in successfully")
                        }
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong , Please try again later!")
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    
    func checkInCall(){
        
        let parameters : [String: String] = [
            "assigned_qty": txtQty.text ?? "",
            "note": txtNotes.text,
            "checkin_at": txtCheckDate.text ?? ""
        ]
        let url = URL(string: "\(selectedComp ?? "")check-in-out/accessories/\(accessoryId)/checkin")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments)
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String
                    let status = dic["success"] as? String
                    print(status)
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked in successfully")
                        }
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong , Please try again later!")
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
    
    func checkInComponent(){
        
        let parameters : [String: String] = [
            "assigned_qty": txtQty.text ?? "",
            "note": txtNotes.text,
            "checkin_at": txtCheckDate.text ?? ""
        ]
        let url = URL(string: "\(selectedComp ?? "")check-in-out/components/\(componentId)/checkin")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "Authorization")
        request.setValue("\(tokenCheckIn)", forHTTPHeaderField: "application/json")
        let postString = GlobalFunction.getPostString(params: parameters)
        request.httpBody = postString.data(using: .utf8)
        let session = URLSession.shared
        session.dataTask(with: request) { (data,response,error) in
            if let response = response{
                print(response)
            }
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                }
            }
            DispatchQueue.main.async {
            if let data = data {
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options:[])
                    guard let dic = json as? [String: Any] else {
                        return
                    }
                    print(dic)
                    let message = dic["messages"] as? String
                    let status = dic["success"] as? String
                    print(status)
                    self.activityIndicator.stopAnimating()
                    if status == "success"{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Accessory checked in successfully")
                        }
                    }else{
                        if message != ""{
                            self.view.makeToast(message)
                        }else{
                            self.view.makeToast("Something went wrong , Please try again later!")
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }catch{
                    print(String(describing: error))
                }
            }
            }
        }.resume()
    }
}
