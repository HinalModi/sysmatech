//
//  APIManager.swift
//  APIManager
//
//  Created by Vikram Chaudhary on 08/04/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import Foundation

public struct HTTPMethod: RawRepresentable, Equatable, Hashable {
    /// `GET` method.
    public static let get = HTTPMethod(rawValue: "GET")
    /// `POST` method.
    public static let post = HTTPMethod(rawValue: "POST")
    /// `DELETE` method.
    public static let delete = HTTPMethod(rawValue: "DELETE")
    /// `PUT` method.
    public static let put = HTTPMethod(rawValue: "PUT")
    
    public let rawValue: String
    
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

enum RequestType {
    case form
    case raw
}

public struct Params: RawRepresentable, Equatable, Hashable {
    public static let phone                     = Params(rawValue: "phone")
    public static let password                  = Params(rawValue: "password")
    
    public static let mobile                  = Params(rawValue: "mobile")
    public static let user_id                 = Params(rawValue: "user_id")
    public static let date                 = Params(rawValue: "date")
    public static let auditStatus                 = Params(rawValue: "audit_status")
    public static let assetImg                 = Params(rawValue: "asset_img")
    public static let audit_schedule_id = Params(rawValue: "audit_schedule_id")
    public static let type = Params(rawValue: "type")
    public static let lat_long = Params(rawValue: "lat_long")
    public static let asset_id = Params(rawValue: "asset_id")
    public static let assets_id = Params(rawValue: "assets_id")
    
    public let rawValue: String
    
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

enum BaseURLRole: String {
    case baseAdani = "https://tasolglobal.com/dev/adani_ach/public/api/adani/"
    case baseZydus = "http://staging.sysmatech.com/zydus/public/api/zydus/"
    case baseUAT = "https://uat.sysmatech.com/public/api/uat/"
    case baseDb = "https://staging.sysmatech.com/public/"
    case baseHavMor = "https://havmor.sysmatech.com/public/api/havmor/"
    
    
}

enum API: String {
    //Basic API's
    
    case login = "login"
    case assetList = "get_my_asset_list"
    case pendingJob = "get_my_asset_schedule"
    case completedJob = "get_my_past_asset_schedule"
    case submitInspection = "submit_inspection_info"
    case get_inspection_info_deatils = "get_inspection_info_deatils"
    case get_asset_pm_history = "get_asset_pm_history"
    case db_check = "db_check.php"
    case get_my_asset_type_schedule = "get_my_asset_type_schedule"
    case cause_web_service = "cause_web_service"
    case cause_store_work_order = "cause_store_work_order"
    case category_model_list = "category-model-list"
    case accessories = "accessories"
    
    func url(for role: BaseURLRole) -> String {
        return role.rawValue + self.rawValue
    }
    func urltwo(for role: String) -> String {
        return role + self.rawValue
    }
}

struct Response {
    var data: Data?
    var response: URLResponse?
    var error: Error?
    
    var success: Bool = false
    var status: Int = 200
    var message: String = errorMessage
    var dicResponse: [String: Any] = [String: Any]()
    var arrResponse: [[String: Any]] = [[:]]
    var totalAssetScheduledata : [String: Any] = [String: Any]()
    init(with data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
        self.success = false
        if error != nil {
            self.success = false
        } else {
            if self.data != nil {
                do {
                    let response = try JSONSerialization.jsonObject(with: self.data!, options: [])
//                    print(response)
                    guard let dic = response as? [String: Any] else {
                        return
                    }
                    self.dicResponse = dic
                    print(dic)
                   
                    status = dic["api_status"] as? Int ?? 0
                    self.message = dic["api_message"] as? String ?? errorMessage
                    if (status == 1){
                        self.success = true
                    } else {
                        self.success = false
                    }
                } catch {
                    self.success = false
                    GlobalFunction.printRes(data: error)
                    GlobalFunction.printRes(data: error.localizedDescription)
                  
                }
            }
        }
    }
    
    func getArrayResponse() -> [[String: Any]] {
        return self.dicResponse["data"] as? [[String: Any]] ?? []
    }
}

typealias Parameters = [Params: Any]

protocol UploadProgressDelegate: AnyObject {
    func progress(totalBytesExpectedToSend: Int64, sentFraction: Int64, totalSentFraction: Int64)
}

class APIManager: NSObject {
    
    static let shared = APIManager()
    var task: URLSessionDataTask?
    
    let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
    var delegate: UploadProgressDelegate?
    
    private func getPostString(params:Parameters) -> String {
        var data = [String]()
        GlobalFunction.printRes(data: "", printDate: false)
        for(key, value) in params
        {
            GlobalFunction.printRes(data: "\(key.rawValue): \(value)", printDate: false)
            data.append(key.rawValue + "=\("\(value)".addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? "")")
        }
        GlobalFunction.printRes(data: "", printDate: false)
        return data.map { String($0) }.joined(separator: "&")
    }
    private func getPostStringTwo(params:[String : Any]) -> String {
        var data = [String]()
        GlobalFunction.printRes(data: "", printDate: false)
        for(key, value) in params
        {
            GlobalFunction.printRes(data: "\(key): \(value)", printDate: false)
            data.append(key + "=\("\(value)".addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? "")")
        }
        GlobalFunction.printRes(data: "", printDate: false)
        return data.map { String($0) }.joined(separator: "&")
    }
    func getPostDic(params:Parameters) -> [String: Any] {
        var data = [String: Any]()
        for(key, value) in params
        {
            data[key.rawValue] = value
        }
        GlobalFunction.printRes(data: data, printDate: false)
        return data
    }
    func getPostDicTwo(params:[String : Any]) -> [String: Any] {
        var data = [String: Any]()
        for(key, value) in params
        {
            data[key] = value
        }
        GlobalFunction.printRes(data: data, printDate: false)
        return data
    }
    private func getRawParams(from params: Parameters) -> Data? {
        let dic = self.getPostDic(params: params)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            return jsonData
        } catch {
            return nil
        }
    }
    func getRawParamsTwo(from params: [String : Any]) -> Data? {
        let dic = self.getPostDicTwo(params: params)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            return jsonData
        } catch {
            return nil
        }
    }
    private func getRawParamsDict(from params: Data) {
        do {
            let jsonData = try JSONSerialization.jsonObject(with: params, options: .mutableLeaves)
            print(jsonData)
        } catch {
            print(error)
        }
    }

    func createDataRequest(with method: HTTPMethod, params: Parameters, api: API, baseURL: BaseURLRole, requestType: RequestType = .raw, includeHeader: Bool = true) -> URLRequest {
        let urlString = api.url(for: baseURL)
        
        var apiReq = URL(string: urlString)!
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue

        let postString = self.getPostString(params: params)
        if method == .get {
            let reqURL = urlString + "?" + postString
            apiReq = URL(string: reqURL)!
            request.url = apiReq
        } else {
            if requestType == .form {
                request.httpBody = postString.data(using: .utf8)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
            } else {
                if let rawData = self.getRawParams(from: params) {
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("application/json", forHTTPHeaderField: "Accept")
                    request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
                    self.getRawParamsDict(from: rawData)
                    request.httpBody = rawData
                }
            }
        }
        return request
    }
    func createDataRequestTwo(with method: HTTPMethod, params: Parameters, api: API, baseURL: String, requestType: RequestType = .raw, includeHeader: Bool = true ,setToken : String) -> URLRequest {
        let urlString = api.urltwo(for: baseURL)
        
        var apiReq = URL(string: urlString)!
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue

        let postString = self.getPostString(params: params)
        if method == .get {
            let reqURL = urlString + "?" + postString
            apiReq = URL(string: reqURL)!
            request.url = apiReq
        } else {
            if requestType == .form {
                request.httpBody = postString.data(using: .utf8)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("\(setToken)", forHTTPHeaderField: "Authorization")
            } else {
                if let rawData = self.getRawParams(from: params) {
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("application/json", forHTTPHeaderField: "Accept")
                    request.setValue("\(setToken)", forHTTPHeaderField: "Authorization")
                    print(setToken)
                    self.getRawParamsDict(from: rawData)
                    request.httpBody = rawData
                }
            }
        }
        return request
    }
    func makeMultipartURLRequest(with method: HTTPMethod = .post, params: Parameters, mediaImage: [Media], api: API, baseURL: BaseURLRole, complition: @escaping (_ response: Response) -> Void) {
        guard let apiReq = URL(string: api.url(for: baseURL)) else {
            return
        }
        
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        let boundary = "Boundary-\(UUID().uuidString)"

        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        if userInfo != nil {
            request.setValue("bearer \("")", forHTTPHeaderField: "Authorization")
        }
        
        let httpBody = NSMutableData()
        
        for (key, value) in params {
            httpBody.appendString(convertFormField(named: key.rawValue, value: "\(value)", using: boundary))
        }

        for media in mediaImage {
            httpBody.append(convertFileData(fieldName: media.key,
                                            fileName: media.filename,
                                            mimeType: media.mimeType,
                                            fileData: media.data,
                                            using: boundary))
        }
        
        httpBody.appendString("--\(boundary)--")

        request.httpBody = httpBody as Data
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
            }
            let responseData = Response(with: data, response: response, error: error)
            complition(responseData)
        }
        
        task.resume()
    }
    func makeMultipartURLRequestTwo(with method: HTTPMethod = .post, params: [String : Any], mediaImage: [Media], api: API, baseURL: BaseURLRole, complition: @escaping (_ response: Response) -> Void) {
        guard let apiReq = URL(string: api.url(for: baseURL)) else {
            return
        }
        
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        let boundary = "Boundary-\(UUID().uuidString)"

        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue
        request.setValue("multipart/form-data;boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("\(listToken)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let httpBody = NSMutableData()
        
        for (key, value) in params {
            httpBody.appendString(convertFormField(named: key, value: "\(value)", using: boundary))
        }

        for media in mediaImage {
            httpBody.append(convertFileData(fieldName: media.key,
                                            fileName: media.filename,
                                            mimeType: media.mimeType,
                                            fileData: media.data,
                                            using: boundary))
        }
        
        httpBody.appendString("--\(boundary)--")

        request.httpBody = httpBody as Data
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
            }
            let responseData = Response(with: data, response: response, error: error)
            complition(responseData)
        }
        
        task.resume()
    }
    private func generateBoundary() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    private func convertFormField(named name: String, value: String, using boundary: String) -> String {
        var fieldString = "--\(boundary)\r\n"
        fieldString += "Content-Disposition: form-data; name=\"\(name)\"\r\n"
        fieldString += "\r\n"
        fieldString += "\(value)\r\n"
        let value = name + ":" + value
        print(value)
        return fieldString
    }
    
    private func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
        let data = NSMutableData()
        
        data.appendString("--\(boundary)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        data.append(fileData)
        data.appendString("\r\n")
        
        return data as Data
    }
}

extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}


extension URLSession {
    fileprivate func codableTask<T: Codable>(with request: URLRequest, shouldShowMessage: Bool = true, completionHandler: @escaping (T?) -> Void) {
        if APIManager.shared.task != nil {
            if APIManager.shared.task?.originalRequest?.url == request.url {
                APIManager.shared.task?.cancel()
            }
        }
        
        APIManager.shared.task = self.dataTask(with: request) { (data, response, error) in
            let response = Response(with: data, response: response, error: error)
            if response.success {
                DispatchQueue.main.async {
                }
                guard let data = data, error == nil else {
                    completionHandler(nil)
                    return
                }
                do {
                    let model = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(model)
                } catch {
                    completionHandler(nil)
                }
            } else {
                DispatchQueue.main.async {
                    if shouldShowMessage {
//                        Constant.window.makeToast(response.message)
                    }
                }
                completionHandler(nil)
            }
        }
        
        APIManager.shared.task?.resume()
    }
    
    fileprivate func codableResponseTask<T: Codable>(with request: URLRequest, completionHandler: @escaping (T?, Response) -> Void) {
        if APIManager.shared.task != nil {
            if APIManager.shared.task?.originalRequest?.url == request.url {
                APIManager.shared.task?.cancel()
            }
        }
        
        APIManager.shared.task = self.dataTask(with: request) { (data, response, error) in
            let response = Response(with: data, response: response, error: error)
            if response.success {
                DispatchQueue.main.async {
                }
                guard let data = data, error == nil else {
                    completionHandler(nil, response)
                    return
                }
                do {
                    let model = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(model, response)
                } catch {
                    completionHandler(nil, response)
                }
            } else {
                DispatchQueue.main.async {
//                    Constant.window.makeToast(response.message)
                }
                completionHandler(nil, response)
            }
        }
        
        APIManager.shared.task?.resume()
    }
    
    func normalTask(with request: URLRequest, completionHandler: @escaping (Response) -> Void) {
        if APIManager.shared.task != nil {
            if APIManager.shared.task?.originalRequest?.url == request.url {
                APIManager.shared.task?.cancel()
            }
        }
        
        APIManager.shared.task = self.dataTask(with: request) { (data, response, error) in
            let response = Response(with: data, response: response, error: error)
            if response.success {
                DispatchQueue.main.async {
                }
                completionHandler(response)
            } else {
                DispatchQueue.main.async {
                }
                completionHandler(response)
            }
        }
        
        APIManager.shared.task?.resume()
    }
    func userLogin(with request: URLRequest, result: @escaping (Bool , String , UserData?) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = User(fromDictionary: response.dicResponse)
            if response.success {
                let loginData = response.dicResponse
                if let userData = loginData["data"] {
                    userInfo = UserData(fromDictionary: userData as! [String : Any])
                    UserInfoManager.setUserInfo(userInfoModel: userInfo)
                    result(true , response.message, modal.data)
                } else {
                    result(false , response.message, modal.data)
                }
            } else {
                result(false , response.message, nil)
            }
        }
    }
    func getPendingJobs(with request: URLRequest, result: @escaping (Bool, [PendingTotalAssetScheduledata]? , String?) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = Pending(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.totalAssetScheduledata, modal.apiMessage)
            }else{
                result(false , nil , modal.apiMessage ?? "")
            }
        }
    }
    func getCompletedJobs(with request: URLRequest, result: @escaping (Bool, [CompletedTotalAssetScheduledata] , String? ) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = Completed(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.totalAssetScheduledata , modal.apiMessage)
            }else{
                result(false , [], modal.apiMessage ?? "")
            }
        }
    }
    func getInsDetail(with request: URLRequest, result: @escaping (Bool, [InspDetailAuditScheduleId]? , String? , Int? ) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = InspDetail(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.auditScheduleId , modal.apiMessage , modal.apiStatus)
            }else{
                result(false , nil, modal.apiMessage ?? "" , modal.apiStatus)
            }
        }
    }
    func getPmHistory(with request: URLRequest, result: @escaping (Bool, [AssetPmHistory]? , String? , Int? ) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = PmHistory(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.assetPmHistory , modal.apiMessage , modal.apiStatus)
            }else{
                result(false , nil, modal.apiMessage ?? "" , modal.apiStatus)
            }
        }
    }
    func getCompanyDetails(with request: URLRequest, result: @escaping (Bool, [CheckCompData]? , String? , Int? ) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = CheckComp(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.data , modal.apiMessage , modal.apiStatus)
            }else{
                result(false , modal.data, modal.apiMessage ?? "" , modal.apiStatus)
            }
        }
    }
    func getAssetSchType(with request: URLRequest, result: @escaping (Bool, [AssetTypeTotalAssetScheduledata]? , String? , Int?) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = AssetType(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.totalAssetScheduledata, modal.apiMessage , modal.apiStatus  ?? 0)
            }else{
                result(false , nil , modal.apiMessage ?? "" , modal.apiStatus ?? 0)
            }
        }
    }
    func getAssetList(with request: URLRequest, result: @escaping (Bool, [AllUserAssetAssgainData]? , String? , Int?) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = AssetList(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.allUserAssetAssgainData, modal.apiMessage , modal.apiStatus  ?? 0)
            }else{
                result(false , nil , modal.apiMessage ?? "" , modal.apiStatus ?? 0)
            }
        }
    }
    func getCausesAsset(with request: URLRequest, result: @escaping (Bool, [CausesAssetsData]? , String? , Int?) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = Causes(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.assetsData, modal.messges, modal.apiStatus  ?? 0)
            }else{
                result(false , nil , modal.messges ?? "" , modal.apiStatus ?? 0)
            }
        }
    }
    func getCategoryModal(with request: URLRequest, result: @escaping (Bool, [Categorylist]? , [Modelslist]? , Int? ) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = CategoryModal(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.categorylist , modal.modelslist , modal.apiStatus)
            }else{
                result(false , nil, nil,  modal.apiStatus)
            }
        }
    }
    func getAccessoryModal(with request: URLRequest, result: @escaping (Bool,[AccessoryRow]? , Int? ) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = Accessory(fromDictionary: response.dicResponse)
            if response.success{
                result(true ,modal.rows ,modal.total )
            }else{
                result(false , nil , modal.total)
            }
        }
    }
}
extension APIManager: URLSessionDataDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        guard let delegate = delegate else {
            return
        }
        delegate.progress(totalBytesExpectedToSend: totalBytesExpectedToSend, sentFraction: bytesSent, totalSentFraction: totalBytesSent)
    }
}
