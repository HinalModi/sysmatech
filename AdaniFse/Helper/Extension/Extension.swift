//
//  Extension.swift
//  TouristoApp
//
//  Created by mac on 08/04/21.
//

import Foundation
import UIKit
import SDWebImage
extension UIView{
    func setView(_ radius : CGFloat , color : UIColor,opacity : Float , size : CGSize){
        self.layer.shadowRadius = radius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = size
        self.layer.shadowOpacity = opacity
    }
    func setRoundCorner(){
        self.layer.cornerRadius = self.frame.width / 2
    }
    func setCorner(_tp radius : CGFloat){
        self.layer.cornerRadius = radius
    }
    func setBorder(_ color: UIColor,width : CGFloat){
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    func showToast(x : CGFloat,y : CGFloat,width : CGFloat,height : CGFloat,message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: x , y: y, width: width, height: height))
        toastLabel.backgroundColor = UIColor.black
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.addSubview(toastLabel)
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
extension Notification.Name {
    static let user = Notification.Name("userLogin")
    static let setLanguage = Notification.Name("setLanguage")

}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    var firstUppercased: String { return prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { return prefix(1).capitalized + dropFirst() }
    
    public func components(separatedBy separators: [String]) -> [String] {
        var output: [String] = [self]
        for separator in separators {
            output = output.flatMap { $0.components(separatedBy: separator) }
        }
        return output.map { $0.trimmingCharacters(in: .whitespaces)}
    }
}

extension Bundle {
    class func setLanguage(_ language : String) {
        var onceToken : Int = 0
        if(onceToken == 0) {
            object_setClass(Bundle.main, PrivateBundle.self)
        }
        onceToken = 1
        objc_setAssociatedObject(Bundle.main, &associatedLanguageBundle, (language != nil) ? Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj") ?? "") : nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)

    }
}
    private var associatedLanguageBundle:Character = "0"

    class PrivateBundle: Bundle {
        override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
            let bundle: Bundle? = objc_getAssociatedObject(self, &associatedLanguageBundle) as? Bundle
            return (bundle != nil) ? (bundle!.localizedString(forKey: key, value: value, table: tableName)) : (super.localizedString(forKey: key, value: value, table: tableName))
        }
    }
    

extension UIImageView{
    func setimage(urlString : String , placeholderImage : UIImage = UIImage()){
        if let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            self.sd_setImage(with: URL(string: url), placeholderImage: placeholderImage , options: .refreshCached, completed: { res,data,error,img   in
                print(res ?? "")
                print(data ?? "")
                print(error)
                print(img ?? "")
            })
        }
    }
}
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}
extension UIImage {
    func toPngString() -> String? {
        let data = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
  
    func toJpegString(compressionQuality cq: CGFloat) -> String? {
        let data = self.jpegData(compressionQuality: cq)
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}

extension String {
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters){
            return UIImage(data: data)
        }
        return nil
    }
}
