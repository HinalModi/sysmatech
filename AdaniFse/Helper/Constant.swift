

import UIKit
import CoreLocation
import SQLite

var DELEGATE : AppDelegate!
var db: Connection?
var imageBase: String = ""
var contactNumber: String = ""
var contactEmail: String = ""
var privacyURL: String = ""
var accountDetail: [String: Any] = [:]
var isSearchClicked:Bool = false
var totalEarnings = 0.0
var profileImageURl = ""
var profilePercent = 20
var userInfo: UserData!
let today = Date()
var user_Id = UserDefaults.standard.string(forKey: "user_id") ?? ""
var incr = 0
var incrCom = 0

var listToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI1IiwianRpIjoiNjQyZjczZThiYmUyZjA5OTJkMDEyYmJlN2JjOWIwYmYxN2Y0YzE2N2EyM2YyMWI0NzU5M2QyY2FjZDYxMjhkMWMyOWIxYjJhZjIzMzFhODAiLCJpYXQiOjE2NjI3MjgyNzMsIm5iZiI6MTY2MjcyODI3MywiZXhwIjoyMTM2MTEzODczLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.TusuVjzUs6zabejX_zgkRq1IMxA6zLLqYYvo1-PCalBxOCrp7_ZezSGJFTpd7nc60RGhAx6LV1IrAVRQ0DWAYcjrFoSMi_Mth7vlb5I_T7Hd6-70JFxhk-_1gir1AQGctOrA2im1EfQ0PxH6GUTN5S-YwaDirUW2kcMY-p9AabWjv_o4JFZ8QdtmLK1lFTqJuTLN63KyRo5vWzj3TFEuFMgB4K3r92OqI_7AVc3fmX_FCWg39P1z1KdFaZNiMD_9pSoRWraK6cjAVdIZfKRF0BYZ3YzdiaxuJtPD-oFbA1RCjVm-wGPx7zDOvmY8oFCuiEELs2dgenFyt8lYZa01BzhK-GLeO9en3AJGPT66d3ZIvTV8olk-RgTK4jYQA9WZ_iSw-tZWG6AvIVcE4xjkYuWzIe4tAoDUcC0dxtbiTfv7DvrLmaDMUigTlbV2YSinWLeuEXCVvFIWRv5Pnu8QRDfTHamFYzFg7F1Ek0vDu9dBKgYpDa8PfYjDU27JyRNAnxpMd3uWdQkxvQg2L72bxS75Z3YeEUdsr3mDg9Pmi5jCRjsbFZoVNlt71OFjtGa0xxsuPUwnQOdu1wb2YvJECtZ0vZc4Y4Pfvoc_bsLgexoLKvSRgZq7orxWk29ZRwfKw-BbvCYX-mKEJ1IcBN8QNerYhpt8XE0OhRzU6UrbcHM"
var incrval = 0
var glbtoken = "Bearer " + (UserDefaults.standard.string(forKey: "bearerToken") ?? "")
var baseUrlImage = (UserDefaults.standard.string(forKey: "assetImgPath") ?? "")
//"http://staging.sysmatech.com/zydus/public/uploads/assets/" //"https://tasolglobal.com/dev/adani_ach/public/uploads/assets/"

var baseUrlAssetImage = "https://staging.sysmatech.com/zydus/public/asset_img/"
 //"https://staging.sysmatech.com/zydus/public/asset_img/"
//"https://tasolglobal.com/dev/adani_ach/public/asset_img/"

//For Adani
var tokenAdani = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjU1MWI5MDVkYzQyZmU5YzEwYmIwZjQwM2VhNDkyZjhiNDExMmE0YjUyODc5ZTdiMGEzMWNhMzk2YmM1MDIxMjcxNzhkYTNhMTU3ZmNhZGIiLCJpYXQiOjE2NDgyMTQ3MjQsIm5iZiI6MTY0ODIxNDcyNCwiZXhwIjoyMTIxNjAwMzI0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.BCTKkW1YlxCPRxruMaWxSf1bdlSZfYHixXd8kPTvbM471xAhrt4009ad3eKxBle-3lrX2chIfaN12jG8tSeRudJ-2QfQayHZwQiCCfNCN31rJBrXlYCWBzS6NUhMQJO8ZPlVs_7pZ2OlbSSkbJWOdgbz29l7-HaWMN1n9z5mkTsfXn5vFXQID5-HHVNNXHswLsjje8tAIyB-7NAWG00mOTdHUGWng7vT3dIcqyKNGedUrsLhFAB6hTOCdGlPlMNyLsiVnhcBsX79Isw-iRvWpjscyP0Lz8IMxO6dKpbX3YsRy2sb35oKAW-K1gDMMZl__bFYUVOJ9hDMCLcxXD2sbf5MG7il5QcsrdIyuuxU6AMKRugS0QZ9b5tH4N4yWL9PIJGjFFUkMyh_BlGOkkFaW8LEMJEJALVuurOtYq0fl4cdgvb3RZSampQqAKW5Xh1M7qKQfP_NsHm2MjBAh4CXjduq44C_3uukQpArvHMWx2_QTjMUouGVyvztMcMb85BVhUEsu49CkhOyIMuyIQUiKpqjBfeWVvMv8001jnyUW8-jpxTheUGsbLhjakCBgupnCxjW5KD_9ZrtNpWphRobdtStsAsRhCqOAxtbxwrGxP0s9kgPYDKbuwW4qrHmT6If7a5SCBUIN5qb0wk_R-U4WcGMNv0doPYnWXEsoAHU1fg"
//For Zydus
var tokenFormData = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOTQ2MGFjZGY0NGRmYzBhNzdhMzY3ZGM1Y2U2OTFhMzRiMjkxZDJmNzZmMWRhODc1ZjA4YjVmMTU0MDliODg5ZTM0MWZjMTMyZWFjMWNiOTkiLCJpYXQiOjE2NTg0MDIzOTEsIm5iZiI6MTY1ODQwMjM5MSwiZXhwIjoyMTMxNzg3OTkxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.PBYj0IxKsR_6ywiRj5xwSKBS9p0scxmw17LpQDUQmqGwOit3axa8hgAzjPeHTI1VpkchKIcUSYhOIv_TZeQrmvjJN0xPt3hXhQP7OByrM_46oGfUkUx82BwPtfY40hO2bAIsERfF4mn1kA_pHCpV9z8aKkV8QQE3iPTLh-f56PlVDioczlDjqH7_fGSwiz1tjnF8g47ynvEG07ZoBp2f2BpgIm-NJUeed-DrRxfwHOcbzm9cVF9TNjSyIg_T503G-ZVBF1JgSpTQN1FpL6t2zkxNEdTnVR98aWXuMjwqGrR9M07w9lwDSCvymtXS_9qgi33cFoHFTeUi83brT-4ZsEIpRLtZzWWKO3EHAnqtRXoMGz7qSzneO57RBDxcudisJb9D9P3de0z8tXZWo-yl5h7TAS4qvwCaF519N3eU9zUnHGcRlIGEB8Z-gqe3f1nGgHppzN9K1l0B3aM3ixFHvxL3Vvyugl0JIt9c_3f8y86TcMRi-C7Z9hxvztPXmnBuCU7wN8xYjYGV3K1ITKGOx4MYfa5VCoJUNSkkvQJx_nCy6SXXqFaiekTCMiCtglhBN-WGAk_yOoCuQUmlK9ifa-uv-t95A-IJg_s1U8dPEg2f544bz-AJOcjTsPaLyAv1_jeMA_wWqXNSwOd67u6WYsTTdzuYcGW1FkCewSub-mA"

var submitUrlZydus = "http://staging.sysmatech.com/zydus/public/api/zydus/submit_inspection_info"
var submitUrlAdani = "https://tasolglobal.com/dev/adani_ach/public/api/adani/submit_inspection_info"
class Constant: NSObject {
    
    //Social Media
    static let clientID = ""
    static let AgoraAPPID = ""
    
    static let appName = "Seva Shop"
    
    static var userLocation = CLLocation()
    
    static var APPDELEGATE: AppDelegate!
    
    static var deviceID = UIDevice.current.identifierForVendor?.uuidString ?? "1234567890"
    static var deviceToken = "1234567890"
    
    static var version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    
    struct Screen {
        static let Width         = UIScreen.main.bounds.size.width
        static let Height        = UIScreen.main.bounds.size.height
        static let Max_Length    = max(Width, Height)
        static let Min_Length    = min(Width, Height)
        
        static let IS_IPHONE     = UIDevice.current.userInterfaceIdiom == .phone
        static let IS_IPAD       = UIDevice.current.userInterfaceIdiom == .pad
        static let g_IS_TV       = UIDevice.current.userInterfaceIdiom == .tv
        static let g_IS_CAR_PLAY = UIDevice.current.userInterfaceIdiom == .carPlay
        
        static let g_IS_Notch_Available          = IS_IPHONE && Max_Length > 736
        
        static let g_IS_4_SCREEN                            = Max_Length == 480.0
        
        static let g_IS_5_SCREEN                            = Max_Length == 568.0
        
        static let g_IS_6_SCREEN                            = Max_Length == 667.0
        static let g_IS_6s_SCREEN                           = Max_Length == 667.0
        static let g_IS_7_SCREEN                            = Max_Length == 667.0
        static let g_IS_8_SCREEN                            = Max_Length == 667.0
        
        static let g_IS_6Plus_SCREEN                        = Max_Length == 736.0
        static let g_IS_6sPlus_SCREEN                       = Max_Length == 736.0
        static let g_IS_7Plus_SCREEN                        = Max_Length == 736.0
        static let g_IS_8Plus_SCREEN                        = Max_Length == 736.0
        
        static let g_IS_X_SCREEN                            = Max_Length == 812.0
        static let g_IS_XS_SCREEN                           = Max_Length == 812.0
        static let g_IS_11_PRO_SCREEN                       = Max_Length == 812.0
        
        static let g_IS_XR_SCREEN                           = Max_Length == 896.0
        static let g_IS_XS_MAX_SCREEN                       = Max_Length == 896.0
        static let g_IS_11_SCREEN                           = Max_Length == 896.0
        static let g_IS_11_PRO_MAX_SCREEN                   = Max_Length == 896.0
        
        static let g_IS_IPAD_9_7                            = Max_Length == 1024.0
        static let g_IS_IPAD_GEN_7                          = Max_Length == 1080.0
        static let g_IS_IPAD_10_5                           = Max_Length == 1112.0
        static let g_IS_IPAD_11                             = Max_Length == 1194.0
        static let g_IS_IPAD_PRO_12_9                       = Max_Length == 1366.0
        
    }
    
//    static var window: UIWindow {
//        return APPDELEGATE.window!
//    }
    
    struct GradiantColors {
        @available(iOS 11.0, *)
        static let whiteClean: [CGColor] = [UIColor.white.cgColor, UIColor(named: "background")!.cgColor]
        
    }
    
    struct StoryBoard {
        static let main = UIStoryboard(name: "Main", bundle: nil)
        static let datePicker = UIStoryboard(name: "DatePicker", bundle: nil)
        static let products = UIStoryboard(name: "MyProducts", bundle: nil)
        
    }
    
    struct SegueId {
        static let toLoginVC = "toLoginVC"
        static let toRegisterVC = "toRegisterVC"
        static let toOTPVC = "toOTPVC"
        static let toChangePassword = "toChangePassword"
        static let toHomeVC = "toHomeVC"
        static let toContactDetails = "toContactDetails"
        static let toSupport = "toSupport"
        static let toTNC = "toTNC"
        static let toPaymentHistory = "toPaymentHistory"
        static let toFAQ = "toFAQ"
        static let toInvitationDetailVC = "toInvitationDetailVC"
        static let toResetPassword = "toResetPassword"
        static let toSuccessRegisterVC = "toSuccessRegisterVC"
        static let toBankAccountVC = "toBankAccountVC"
        static let toAadharCardDetailsVC = "toAadharCardDetailsVC"
        static let toPanCardDetailsVC = "toPanCardDetailsVC"
        static let toTutorialVC = "toTutorialVC"
        static let toCompleteProfile = "toCompleteProfile"
        static let toLMSVC = "toLMSVC"
        
        static let toEditProfileVC = "toEditProfileVC"
        static let toInnovationDetailVC = "toInnovationDetail"
        
    }
    
    struct NotificationName {
        static let updateSideMenuInfo = Notification.init(name: Notification.Name("updateSideMenuInfo"))
        static let updateOrders = Notification.init(name: Notification.Name("updateOrders"))
        static let openChatOnNotification = Notification.init(name: Notification.Name("openChatOnNotification"))
        static let reloadTab = Notification.init(name: Notification.Name("reloadTab"))

    }
    
    @available(iOS 11.0, *)
    struct Color {
//        static let theme: UIColor = UIColor(named: "Orange")!
        static let border: UIColor = UIColor(named: "Border")!
        static let dark: UIColor = UIColor(named: "Dark")!
        static let gray: UIColor = UIColor(named: "Gray")!
        static let light: UIColor = UIColor(named: "Light")!
        static let backGray: UIColor = UIColor(named: "Back Gray")!
        static let teal: UIColor = UIColor(named: "color_teal_theme")!
        static let color_launch: UIColor = UIColor(named: "neon")!
        static let neon_light: UIColor = UIColor(named: "neon_light")!
        static let theme: UIColor = UIColor(named: "color_yellow_theme")!
        static let color_bg: UIColor = UIColor(named: "color_bg")!
        static let color_grey: UIColor = UIColor(named: "color_grey")!
        static let color_red: UIColor = UIColor(named: "color_red")!
        static let gradient_3: UIColor = UIColor(named: "gradient_3")!
    }
}

