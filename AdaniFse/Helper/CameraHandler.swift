

import Foundation
import UIKit
import AVKit


class CameraHandler: NSObject {
    static let shared = CameraHandler()
    fileprivate var currentVC: UIViewController!
    
    //MARK: Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    var videoPickedBlock: ((UIImage, URL) -> Void)?
    var isForVideo: Bool = false
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = true
            if isForVideo {
                myPickerController.mediaTypes = ["public.movie"]
            }
            DispatchQueue.main.async {
                self.currentVC.present(myPickerController, animated: true, completion: nil)
            }
        } else {
//            Constant.window.makeToast("Your device dont have a camera")
            self.photoLibrary()
        }
    }
    
    func photoLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.allowsEditing = true
            if isForVideo {
                myPickerController.mediaTypes = ["public.movie"]
            }
            DispatchQueue.main.async {
                self.currentVC.present(myPickerController, animated: true, completion: nil)
            }
        } else {
//            Constant.window.makeToast("Your device dont have a photo library")
        }
    }
    
    func showActionSheet(vc: UIViewController, isSelectVideo: Bool = false, sender: UIButton) {
        currentVC = vc
        isForVideo = isSelectVideo
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: isSelectVideo ? "Record Video" : "Take Picture", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Select From Photo Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        DispatchQueue.main.async {
            vc.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    func showActionSheetwithDefultOption(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
        actionSheet.addAction(UIAlertAction(title: "Take Picture", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Select From Photo Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = currentVC.view
            popoverController.sourceRect = CGRect(x: currentVC.view.bounds.midX, y: currentVC.view.bounds.midY / 2, width: 0, height: 0)
        }
        DispatchQueue.main.async {
            vc.present(actionSheet, animated: true, completion: nil)
        }
    }
}


extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imagePickedBlock?(image)
        } else if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            let asset = AVAsset(url: url)
            let assetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImageGenerator.appliesPreferredTrackTransform = true
            
            var time = asset.duration
            time.value = min(time.value, 2)
            var thumb: UIImage = UIImage()
            do {
                let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
                thumb =  UIImage(cgImage: imageRef)
            } catch {
                thumb = UIImage(named: "splash_logo")!
            }
            self.videoPickedBlock?(thumb, url)
        }
        currentVC.dismiss(animated: true, completion: nil)
    }

}
