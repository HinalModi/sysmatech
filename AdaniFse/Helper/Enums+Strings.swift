//
//  Enums.swift
//  Enums
//
//  Created by Vikram Chaudhary on 25/03/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import Foundation

enum SocialLogin: String {
    case facebook = "1"
    case google = "2"
    case apple = "3"
}

enum IndicatorColor {
    case white
    case black
    case theme
}

enum BusinessType: String {
    case Buyer = "Buyer"
    case Supplier = "Supplier"
    case Manufacturer = "Manufacturer"
    case Other = "Other"
}

enum SelectedImage {
    case None
    case Primary
    case First
    case Second
    case Third
    case Forth
}

let errorMessage = "Oops! Something went wrong. Please try again."


let enterName = "Please enter your name"
let enterEmail = "Please enter your email"
let enterValidEmail = "Please enter valid email"
let selectDistrict = "Please select district"
let enterAgency = "Please enter agency code"
let enterMobileNumber = "Please enter mobile number"
let enterValidMobileNumber = "Please enter valid mobile number"
let enterPassword = "Please enter password"
let enterCPassword = "Please enter confirm password"
let enterValidPassword = "Password must be greater or equal to 8 characters"
let passwordNotMatch = "Password and confirm password is diffrent. Please enter correct password"
let enterCurrentPassword = "Please enter current password"
let enterNewPassword = "Please enter new password"
let enterFirstName = "Please enter first name"
let enterLastName = "Please enter last name"
let selectGender  = "Please select Gender"
let selectState = "Please select state"
let selectCity = "Please select city"
let selectdateOFBirth = "Please select date of birth"
let enterAddress = "Please enter address"
let enterPinCode = "Please enter pin code"
let selectProfileImage = "Please select image for profile"
let enterBankName = "Please enter bank name"
let enterAccountHolderName  = "Please enter account holder name"
let enterAccountNumber = "Please enter account number"
let enterIFSCCode = "Please enter IFSC code"
let enterPanCardNumber = "Please enter pan card number"
let enterAadharCardNumber = "Please enter aadhar card number"
let enterValidAadharNumber = "Please enter valid aadhar number"
let selectPanCardImage = "select your pan card image"
let selectFrontAadharCardImage = "select your front aadhar card image"
let selectBackAadharCardImage = "select your back aadhar card image"
let enterOTP = "Please enter OTP"
let enterMessage = "Please enter message"


