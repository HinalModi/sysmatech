

import UIKit

class GlobalFunction: NSObject {
    
    static let businessType = [
        "Select Business Type",
        "Grocery",
        "Fruits & Vegetables",
        "General Store",
        "Dairy",
        "Pharmacy",
        "Bakery",
        "Apparel",
        "Stationary & Office Supplies",
        "Electrical Supplies",
        "Hardware Store",
        "Laundry",
        "Salon & Spa",
        "Restaurants & Food Joints",
        "Mobile and Accessories",
        "Electronics & Appliances",
        "Home & Kitchen",
        "Electrician",
        "Plumbers",
        "AC Services and Repair",
        "Appliance repair",
        "Painters",
        "Cleaning and Disinfection",
        "Carpenters",
        "Painter",
        "Pest Control",
        "Spa for Women",
        "Massage for Men",
        "Other"
    ]
   static func getPostString(params: [String: String]) -> String {
       var data = [String]()
       for(key, value) in params
       {
           data.append(key + "=\(value)")
       }
       return data.map { String($0) }.joined(separator: "&")
   }
   static func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = desFormat
       return dateFormatter.string(from: date ?? Date())
    }
//    static func showLoadingIndicator(color:Int) {
//        Constant.APPDELEGATE.spinnerView?.stopAnimating()
//
//        UIApplication.shared.beginIgnoringInteractionEvents()
//        Constant.APPDELEGATE.spinnerView = MMMaterialDesignSpinner(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(45), height: CGFloat(45)))
//        // Set the line width of the spinner
//        Constant.APPDELEGATE.spinnerView?.lineWidth = 3.0
//        // Set the tint color of the spinner
//
//        switch color {
//        case 1:
//            Constant.APPDELEGATE.spinnerView?.tintColor = UIColor.black
//            break
//        case 2:
//            if #available(iOS 11.0, *) {
//                Constant.APPDELEGATE.spinnerView?.tintColor = Constant.Color.theme
//            } else {
//                Constant.APPDELEGATE.spinnerView?.tintColor = UIColor.blue
//            }
//            break
//        default:
//            Constant.APPDELEGATE.spinnerView?.tintColor = UIColor.white
//            break
//        }
//
//        Constant.APPDELEGATE.spinnerView?.center = (Constant.APPDELEGATE.window?.center)!
//        Constant.APPDELEGATE.spinnerView?.hidesWhenStopped = true
//        // Add it as a subview
//        Constant.APPDELEGATE.window?.addSubview(Constant.APPDELEGATE.spinnerView!)
//        Constant.APPDELEGATE.spinnerView?.startAnimating()
//    }
    
//    static func hideLoadingIndicator() {
//        DispatchQueue.main.async {
//            UIApplication.shared.endIgnoringInteractionEvents()
//            Constant.APPDELEGATE.spinnerView?.stopAnimating()
//        }
//    }
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    static func printRes(data: Any, printDate: Bool = true) {
        var prefix = ""
        if printDate {
            print("\(Constant.appName) - \(Date()) ===========")
            prefix = Constant.appName + " "
        }
        print("\(prefix)\(data)")
    }
    static func showAlert(message : String , actionTitle : String, viewcontroller : UIViewController){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: actionTitle, style: .default))
            viewcontroller.present(alert, animated: true)
        }
    }
   
    static func convertDateFormatter(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        if let date = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = "hh:mm a ,d MMM yyyy"///this is what you want to convert format
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let timeStamp = dateFormatter.string(from: date)
            return timeStamp
        } else {
            return ""
        }
    }
    
    static func getCountryList() -> NSMutableArray {
        var countryList = NSMutableArray()
        if let path = Bundle.main.path(forResource: "country-calling-codes", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let person = jsonResult["Data"] as? [Any] {
                    
                    countryList = NSMutableArray()
                    let list = NSMutableArray.init(array: person)
                    for i in 0..<list.count {
                        var dict: [String: Any] = list.object(at: i) as! [String: Any]
                        dict["flag"] = self.flag(country: dict["code"] as! String)
                        countryList.add(dict)
                    }
                }
            } catch {
                // handle error
            }
        }
        return countryList
    }
    
    static func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    static func getDateInDMY(from dateString: String, shortFormat: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        if shortFormat {
            dateFormatter.dateFormat = "YYYY-MM-dd"
        }
        guard let date = dateFormatter.date(from: dateString) else {
            return dateString
        }
        dateFormatter.dateFormat = "dd MMM YYYY"
        let formatted = dateFormatter.string(from: date)
        return formatted
    }
    
    
    static func resize(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    static func getCurrentTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = ""
        return ""
    }
    
    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
